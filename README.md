# O MENI

Matevž Novak, rojen 1986

Od nekdaj so me privlačile rešitve, ki ljudem prihranijo čas in živce. Brez zanikanja priznam, da je moje vodilo lenoba. Posledica pa so rešitve, ki od uporabnika ne zahtevajo posebnega znanja, posebnih navodil ali rednega vzdrževanja. Boljša kot je rešitev, manj imam jaz dela, manj pritoževanj, večje bo vaše veselje.

# JEZIKI
* Slovenščina - materni jezik
* Angleščina - nivo: B2

# ZA ZABAVO / LASTNO VESELJE
* pomoč starejšim / 3. univerza - računalniška predavanja
* pomoč starejšim / 3. univerza - sistemska podpora in učenje na domu
* spletna orodja za šole + podpora upravljalcem spletnih strani
* igre (testiranje tehnologij, recimo particle efekta za izgorevanje :) )
* program za vodenje financ (5+ aplikacij)
* project / task manager (12+ aplikacij)
* program za samodejni prenos nadaljevank in filmov (3+ aplikacij)
* "Home", android aplikacija za vodenje doma in skupnih aktivnosti z družino
* sestavljanje in popravljanje računalnikov (hardware)
* glasbeno ozadje - baskitara, trobenta, kontrabas, flavta, tuba, rog, solo petje
* lasti si me 5 mačk

# ZNANJA

## PRIMERI
https://www.dropbox.com/sh/fsftcysclekocht/AADdRejs7vt3puG-HdgUTOHTa?dl=0

## TRENUTNO V UPORABI
* WordPress, full stack developer, 8+ themes, 100+ plugins, 7+ let izkušenj
* Laravel 5, 2 večja projekta, 5 manjših projektov, 1+ let izkušenj
* VueJS, 4+ projektov, 1+ let izkušenj
* Vue-cli, 2+ projektov, 1+ let izkušenj
* AngularJS, 6+ projektov, 1+ let izkušenj
* javascript, typescript
* webpack
* css compile jeziki - stylus, less, sass, scss
* materializecss (http://materializecss.com/)
* bootstrap
* fpdf, tfpdf
* ionic framework (hybrid mobile apps)
* elastic search
* mysql, 10+ let izkušenj
* PostgreSQL, 8 projektov
* C
* C#

## OSTALA ZNANJA
ki služijo kot dodatek k primarnemu delu

* vodenje projektov
* tehnična podpora strankam
* priprava dokumentacije projekta (za stranko, prodajno usmerjeno, brez tehničnih informacij)
* priprava plana dela (za vodstvo podjetja, naročnika, programerje / delavce)
* priprava funkcionalne specifikacije projekta (za programerje / delavce)
* iskanje novih strank / mreženje
* BNI Slovenija - Karantanija
* git
* svn
* Photoshop (osnovne funkcionalnosti za obdelavo in pripravo slik za splet)
* sublime text 3
* Visual Studio
* tortoisegit
* Aptana Studio 3
* Slack, skype
* trello api / integration
* trello extentions
* slack custom commands and api / integration
* slack extentions / bots
* custom chrome extentions
* gulp, grunt, prepros, webpack
* linux, linux scripts
* docker, docker-compose (razvojno okolje za WP, laravel, SlimPHP, mysql, node apps, vue-cli)
* Apache Cordova
* lamp, wamp (lokalni razvoj php)
* mikrotik / omrežja


## HELLO WORLD in TESTNI PROJEKTI
* Yii framework
* Magento spletna trgovina
* CsCart Spletna trgovina
* ReactJS
* EmberJS
* Cordova
* Task manager
* Finance manager
* avtomatiziran media strežnik
* Crm
* bug tracker
* video stream
* Android native apps and games
* sublime text package / extention

## PRETEKLA ZNANJA
* SlimPHP, 5+ projektov, 2+ let uporabe
* Flash, ActionScript 2, ActionScript3, 8+ let izkušenj
* SharePoint 2007, SharePoint 2013, SkyDrive for Business, WinForms
* web hosting, server farm, VMWare
* network security

# ZGODOVINA

## 2001 - 2003

Tehnologije: Flash, Macromedia, ActionScript2, php, mysql
Projekti:
* interaktivne spletne strani
* šolska galerija (v uporabi 5 let)

## 2003 - 2007

__Delodajalec:__ Parsek, Spletna Postaja, CountDownKings, 5+
__Zaposlitev:__ ActionScript2, ActionScript3 razvijalec
__Tehnologije:__ ActionScript2, ActionScript3: video stream player, GPU advanced rendering, 3D games, dinamične vsebine / večjezične podpore, xml, flash apps
__Projekti:__
* flash bannerji, interaktivne spletne strani, media predvajalniki, urejevalniki vsebin, galerije
* namizne aplikacije s tehnologijo flash


## 2007 - 2013
__Delodajalec:__ PNV Group
__Zaposlitev:__ spletni razvijalec, sistemski vzdrževalec, projektni vodja
__Tehnologije:__ AsctionScript3, custom CMS, php, php services, javascript, jQuery, mysql, VMWare, Mikrotik, mass mailing, sms services, css

Omembe vredni projekti:

### Hub za vzdrževanje CMS administracij
__Opis projekta:__ Vzpostavitev sistema, preko katerega se vzdržuje custom CMS
__Tehnologije:__ php, DirectAdmin API, SOAP, mysql, custom CMS, javascript, jQuery
__Kaj smo reševali:__
Za ročno nadgradnjo posameznega CMS-a smo potrebovali 2 - 8 ur, s čimer smo naleteli na malo morje težav.
Za novo administracijo in domeno smo potrebovali 1 - 2 uri usklajevanja, katere CMS module in dostope bomo dodelili, nato pa še 1 - 2 uri, da smo to postavili.
Pogostost nadgradenj in ustvarjanja novih administracij je bilo takrat 3 - 5 nadgradenj / teden.
Ker sem ta postopek prevzel jaz, sem želel ta opravila optimizirati.
Končni rezultat je hub, preko katerega smo vse želene akcije za vzdrževanje samo odkljukali ali jih sprožili. Ostalo je sistem opravil sam. Postopek nadgradnje ali nove inštalacije je izvedel v ozadju in zaključil najkasneje po 5 min.
V roku 6 mesec smo preko hub sistema redno vzdrževali preko 250 administracij.

### service za pošiljanje mail sporočil in sms sporočil
__Opis projekta:__ Masovno pošiljalnje mail in sms sporočil
__Tehnologije:__ mail server farm, php daemon (services), VMWare, mikrotik (lan stuff), php, custom php framework
__Tematike:__
* API za uporabo storitev smailer
* sistem za pošiljanje SMS sporočil
    - v sistem je vključenih več zunanjih ponudnikov, vsak s svojimi prednostmi, ceno in kvaliteto
    - glede na prejemnika je bil izbran najbolj primeren ponudnik
    - failback na kvalitetnejšega ponudnika, ko je to potrebno
    - sistem za določanje cen, marž, itd
* farma strežnikov za masovno pošiljanje mail sporočil
    - ki se samodejno razširi / vklaplja inštance pošiljateljev glede na potrebe pošiljanja
    - samodejna menjava med IP-ji glede na število poslanih sporočil iz posameznega IP-ja za sporočila pomembnejšega značaja (spam prevention)
    - dkim podpisani maili
    - statistika odprtja mailov, kliki na povezave

### smailer.si
__Opis projekta:__ Spletna aplikacija za upravljanje mail in sms kampanj
__Spletna stran:__ http://smailer.si
__Tehnologije:__ php, css, javascript, jQuery, css, api, soap
__Tematike:__

* uporabniški vmesnik
    - drag and drop content builder (kot je danes že stalna praksa)
    - predpripravljene predloge
    - upravljanje nog, glave, ...
    - menjava predloge na obstoječi vsebini
    - statistika
    - vse, kar je tudi v današnjih aplikacijah standard / pričakovano
* autoresponder
    - glede na dejanja prejemnika (odprl mail, kliknil na določeno povezavo, opravil nakup, prejel sms sporočilo), reagiramo z akcijo
    - akcija je lahko novo mail sporočilo, dodatna akcija / popust na izdelek, kuponi, sms sporočilo s kodo popusta, ...
* uvoz / izvoz / upravljanje prejemnikov in vsebin

### video pretvornik in pretvornik za slike
__Opsi projekta:__ Strežnik namenjen pretvorbi in gostovanju video vsebin in slikovnega materiala
__Kaj smo reševali:__
* Stranke so pošiljale video vsebine v visoki kvaliteti, ki jih je bilo potrebno pretvoriti v za splet primernejšo obliko
* Stranke so video vsebine pošiljale v za splet neprimernih formatih, ki smo jih pretvorili v za splet primernejšo obliko
* Razrez slik ekstremnih velikosti, ki so služile detaljnemu pogledu / približevanju.
* Streženje slik za spletne strani, pri čemer je strežnik skrbel za optimalno kvaliteto in velikost slik, glede na potrebo / prikaz


## 2012 - 2013 (1 leto)
__Delodajalec:__ Virtua IT d.o.o.
__Tematika podjetja:__ Razvoj kompleksnih programskih rešitev za srednja in večja podjetja
__Zaposlitev:__ Razvijalec
__Tehnologije:__ C#, MsSQL, SharePoint 2007, SharePoint 2013, Window 8 apps, Skydrive for business, Windows services, WinForms

Omembe vredni projekti:

### namizna aplikacija za tablice
__Opis projekta:__ Optimizacija procesa - Window 8 aplikacija za izdajanje in tiskanje pravnih dokumentov na terenu
__Kaj smo reševali:__
Uradne osebe na terenu izdajajo pravne dokumente, ki so jih morali ob vrnitvi na delovno mesto vnašati v sistem.
Z aplikacijo na tablici z Window 8, smo dosegli pripravo dokumentov preko vmesnika. Iz informacij smo pripravili dokument, ki so ga na
terenu lahko tudi natisnili. Prav tako pa se je dokument sinhroniziral preko poslovnega SkyDrive in nato preko dodatka uvozil v SharePoint.

## 2010 - 2016
__Delodajalec:__ 2gika.si
__Tematika podjetja:__ Izdelava spletnih aplikacij in spletnih strani
__Zaposlitev:__ CTO / backend / customer support / sales
__Tehnologije:__ WordPress, WooCommerce, WordPress vtičniki, php, javascript, TypeScript, css, scss, less, sass, gulp, grunt, SlimPHP, Laravel, DirectAdmin, NginX, php-fpm, python, mysql, augmented reality, nodejs, angularjs, emberjs
__Projekti:__
* Namenske rešitve za WordPress za večja podjetja in stranke z višjimi zahtevami.
* Manjše spletne aplikacije
* Večino optimizacija procesov in povezovanje večih zunanjih sistem "pod skupno streho"
* web scrapping
* dodatki za trello
* dodatki za slack

Omembe vredni projekti:

### optimiziran crm
__Opis projekta:__ Sistem za uprabljanje strank, urejanje postavk storitev, upravljanje vzdrževalnih pogodb, ponudb, računov, bug tracking, ... kjer so vsa ponavljajoča opravila optimizirana in avtomatizirana.
__Tehnologije:__ tfpdf, vtiger, php, wordpress, crm, mysql
__Zanimivost:__ Največ zanimanja je v resnici požel lep (lepši od konkurence) izpis dokumenta (pogodba, račun, ponudba) v PDF obliki.

### slack - trello integration
__Opis projekta:__ Hitri dostop do vseh potrebnih informacij v Trello-u direktno iz Slack-a. Z ukazom v slack-u si dobil po prioriteti razvrščene odprte task-e, vprašanja, task-e v čakanju, bug reports, closed tasks... Kakšna je bila ta prioriteta, je odvisno od vloge, ki jo je uporabnik imel do določenega projekta.
__Tehnologije:__ php, trello api, slack custom commands, mysql
__Zanimivost:__
* Vsak uporabnik si sam nastavi svojo vlogo pri določenem projektu in temu primerno tudi dobiva primerne rezultate / informacije
* Dodeljevanje vlog je vezana na Trello board, card, list, vard check list, card check list item

### export v program za računovodstvo
__Opis projekta:__ WooCommerce naročila izvoziti v datoteko za uvoz v računovodski program.
__Tehnologije:__ WordPress, WooCommerce, wp plugin, php
__Zanimivost:__ Mesečna priprava dokumentov za računovodstvo, za katerega je naročnik poprej vsak mesec potreboval 2 dni, je zdaj opravljeno z enim klikom.

### plačilni sistem za WordPress WooCommerce za Slovenske banke
__Opis projekta:__ Podpora za plačilo naročila s kreditno kartico na transakcijski račun odprt v Sloveniji.
__Tehnologije:__ WordPress, WooCommerce, api, payment method

### drag and drop visual editor vsebin
__Opis projekta:__ Vizualni urejevalnik vsebin in urejevalnik postavitve vsebin v administraciji.
__Tehnologije:__ WordPress, wp custom plugin, Visual Editor, crm, javascript, css, wp custom theme

### sledenje in analiza uporabnikov brez uporabe piškotkov
__Opis projekta:__ Skoraj polna uporaba google analytics vendar brez zahteve po piškotkih.
__Tehnologije:__ WordPress, wp custom plugin, google analytics

### avtomatizirana trgovina - trgovina.zelenisvet.com
__Opis projekta:__ Naročnik in upravljalec spletne trgovine ponuja svojim poslovnim partnerjem prodajo partnerjevih produktov na naročnikovi spletni trgovini. V zameno, naročnik obdrži delež vrednosti nakupa. Deleži so lahko določeni glede na poslovnega partnerja, skupen znesek naročila, produkt, kombinacijo produktov, produkte v akciji.
__Tehnologije:__ WordPress, WooCommerce, wp custom plugin, reseller, b2b partner
__Zanimivost:__ Naročnik vtičnika / upravljalec spletne trgovine nima opravka z naročili iz spletne trgovine, saj je ta proces v celoti avtomatiziran.

### urejanje baze podatkov z Excel uvoz / izvoz ali online - del projekta EUSPR
__Opis projekta:__ Naročnik je svojim zaupanja vrednim partnerjem želel omogočiti samostojno urejanje seznamov, ki jih prikazuje svojem na spletnem portalu. V to skupino spadajo udeleženci določenih seminarjev, člani društev, govorniki ali organizatorji. Sistem omogoča hitro obnovo starejše verzije podatkov.
__Tehnologije:__ WordPress, wp custom plugin, xlsx, subversion, import / export, editor, roles, permissions, log, revert changes, backup
__Zanimivost:__ Na posameznem seznamu lahko hkrati dela tudi več ljudi hkrati, brez da bi se podatki kakorkoli uničili ali izgubili v hkratnem urejanju.

### B2B file share plugin
__Opis projekta:__ Naročnik je svojim B2B partnerjem želel na enostaven način posredovati promo materiale za kupljene produkte. Promo materiali večkrat presegajo nekaj GB, zato klasične rešitve niso prišle v upoštev. Za hranjenje podatkov, se uporablja storitev DropBox.
Ko naročnik za določen produkt posodobi promo materiale, sistem avtomatično pošlje email sporočilo B2B partnerjem, ki so ta produkt tudi kupili / ga uporabljajo. Naročnik na enem mestu upravlja produkte, partnerje, promo materiale.
B2B partnerji naročnika imajo na spletni strani naročnika dostop do promo materialov, kjer imajo za posamezni promo material na izbiro predogled ali prenos. Možen je tudi prenos večih materialov naenkrat v zapakirani obliki *.zip.
__Tehnologije:__ DropBox API, WordPress, custom wp plugin, php
__Zanimivost:__ Na spletni strani (tudi na mobitelu) je mogoč predogled vsebin kar znotraj strani, kjer uporabnik ne ve, da se v ozadju uporablja DropBox.

### 3D soba za izbiro stenskih barv
__Opis projekta:__ Uporabniku prikazati čim bolj realen 360° pogled sobe, kjer uporabnik izbira barvo za na stene. Prikazati je potrebno ton barve in kako ta vpliva na prostor.
__Tehnologije:__ 3D max, rendering, v-ray, ActionScript3
__Zanimivost:__ Ves trik je bil izvoz 360° pogleda v layerjih in pravi globini sive na stenah, na katero smo kasneje lepili barve.

### augmented reality za arhitekte - testni projekt
__Opis projekta:__ Skozi kamero, smo na projektorju projecirali navidezno resničnost. Prednatisnjeni znaki so služili kot pozicija 3D elementov v prostoru navidezne resničnosti, stranke pa so nato s premikanjem natisnjenih znakov, v projekciji premikale pohištvo po prostoru.
__Tehnologije:__ Flash, ActionScript3, augmented reality
__Zanimivost:__ Navdušenost strank ob dodatnem prikazu njihove zgradbe / sanj. Nobena izmed strank, za tako predstavitev ne bi plačala.


## 2016 - 2017 (1 leto)
__Delodajalec:__ Zuum d.o.o.
__Tematika podjetja:__ Razvoj iger na srečo za kazino-je
__Zaposlitev:__ Razvijalec, projektni vodja
__Tehnologije:__ C, OpenGL, GLSL, vuejs, vue-cli, python, python extentions, python C extentions, SQLite, PostreSQL, java, Android apps, Android games, ionic framework, angularjs, nodejs, linux, javasript, typescript

### vmesnik za stranke
__Tehnologije:__ C, vuejs, materializecss
__Zanimivost:__ Strežniški del kode je spisan v C jeziku.
__Opis projekta:__
* Vmesnik za stranke, ki služil pregledu statistike in financ
* Vmesnik za prodajnike, za nadzor in upravljanje strank in prodanih produktov
* Vmesnik za vzdrževalce, ki služi za odkrivanje težav in prevar
* Vmesnik za lastnike / vodje podjetja, ki služi celotnemu splošnemu pregledu


### grafični vmesnik / UI za igre
__Opis projekta:__ Programiranje grafičnega vmesnika za igre.
__Tehnologije:__ C, OpenGL, GLSL
__Zanimivost:__ Igra je morala teči 24/7


### prikaz vsebin iz XML datoteke
__Opis projekta:__ Za prikaz splošnih vsebin (opisi, pomoč, ...) smo potrebovali sistem, ki ga ne bo potrebno urejati v kodi, ampak ga lahko urejamo "od zunaj". Hkrati moramo zagotoviti dostop tudi do nastavitev v igri, če jih bomo v vsebini želeli prikazovati. Za vzor smo vzeli HTML pristop in spisali basic HTML parser, ki bazira na XML standardu, z dodatki, ki smo jih potrebovali. Dosegli smo XHTML 1 standarde in nadzor, le da je to teklo v C jeziku.
__Tehnologije:__ C, OpenGL, GLSL, xml
__Zanimivost:__ Ker vsebina omogoča prikaze različnih elementov glede na nastavitve v igri, se lahko 1x pripravi splošno vsebino, ki se prilagaja, namesto vsakokratne predelave vsebine, ko se nastavitve v igri spremenijo.

### android in web igre
__Opis projekta:__ Za prikaz strankam in testiranje hardware-a smo razvili več Android in web iger za ruleto. Namen je bil testiranje tehnologije in dokazati potrebno varnost, za prehod na android igre.
__Tehnologije:__ java, webgl, hybrid apps, angularjs, typscript, web, custom framework
__Zanimivosti:__ Stvari se zapletejo, ko v aplikaciji skušaš zagotoviti varnost.

## 2016 - danes
__Delodajalec:__ freelancer / Optimiziram.Si
__Tematika:__ Spletne rešitve, ki prihranijo čas
__Tehnologije:__ Laravel, VueJs, vue-cli, wordpress, materializecss, php, javascript, typescript, 

### wp updater - updater ( 1 / 3 )
__Opis projekta:__ Vtičnik, ki skrbi za posodobitve lastnih vtičnikov.
__Tehnologije:__ WordPress, php, api
__Zanimivosti:__ Katerikoli vtičnik lahko "aktiviramo za posodabljanje" z dodano eno samo vrstico v headerju vtičnika.

### wp updater - packager ( 2 / 3 )
__Opis projekta:__ Vtičnik, ki skrbi za pakiranje in pošiljanje vtičnika, ki ga razvijamo, na lastni repository za gostovanje vtičnikov. V navezi z repository-jem skrbi tudi za verzioniranje vtičnika v razvoju.
__Tehnologije:__ WordPress, php, api, zip
__Zanimivosti:__ Vtičnik lahko uporabljamo iz kateregakoli wordpress sistema, ki je povezan v splet. Tudi iz lokalnega razvojnega okolja. Ni potrebe po SVN, FTP, ...

### wp updater - hub / custom repository ( 3 / 3 )
__Opis projekta:__ Katerikoli WordPress lahko spremenimo v repository za gostovanje lastnih vtičnikov in njihovih posodobitev. Ko vtičnik namestimo, je na voljo v prenos tudi:
* vtičnik "updater", opisan zgoraj
* vtičnik "packager", opisan zgoraj
__Tehnologije:__ WordPress, php, api
__Zanimivosti:__ Možnost dodajanja razširitev / fully extensible

### Global Express Prizma - spletna aplikacija
__Opis projekta:__ Spletna aplikacija za upravljanje s pošiljkami.
__Tehnologije:__ Laravel, mysql, vuejs, materializecss, external api, api, custom
__Zanimivosti:__ PDF podpora v brskalnikih je še vedno zelo zmedena in nedodelana, predvsem na mobilnih napravah.

### Product Checkout - wp plugin
__Opis projekta:__
Naročnik prodaja nišni produkt, kjer kupcu ob zaključenem naročilu ponudi tudi akcijske produkte / dodatke. Za povečano prodajo akcijskih produktov, je bilo potrebno izboljšati (skrajšati) uporabniško izkušnjo kupca, ki je k naročilu želel dodati ponujeni akcijki produkt. Namesto, da bi kupca ponovno vodili skozi celoten proces oddaje in plačila naročila, kupec to stori z enim klikom.
__Tehnologije:__ WordPress, custom wp plugin, php, WooCommerce, WooCommerce API, PayPal payment integration, PayMill (credit card) payment integration, online payment integration,
__Zanimivosti:__ Rezultat uvedene spremembe je prodajo povečal za 42% v primerjavi s starejšo verzijo naročilnice.

### Artur Store Review - wp plugin
__Tehnologije:__ WordPress, php, api, publish
__Opis projekta:__
Integracija zunanje storitve za ocenjevanje spletnih trgovin in kupljenih produktov za WooCommerce spletno trgovino.
Ko uporabnik na spletni trgovini odda naročilo, zunanji storitvi javimo informacije o nakupu, zunanja storitev pa nato povabi kupca k oddaji ocene o nakupovanju.
Upravljalev spletne strani ima popoln nadzor nad opcijami za OPT IN, OPT OUT, CONSENT GIVEN, live in testnim delovanje, prepisovanje prejemnika in tip ocenjevanja, ki ga od uporabnika želimo pridobiti.




