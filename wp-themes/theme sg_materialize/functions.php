<?php



define('SG_MATERIALIZECSS_VER', '1.0');

/******************************************************************
*
*       SCRIPTS AND STYLES
*
******************************************************************/
function sg_materialize_enqueue_scripts(){

	// register scripts / styles
	wp_register_style( 'google-material-icons', 'http://fonts.googleapis.com/icon?family=Material+Icons' );
	wp_register_style( 'materializecss', get_template_directory_uri() . '/materialize/css/materialize.min.css', array( 'google-material-icons' ), SG_MATERIALIZECSS_VER, 'screen,projection' );
	wp_register_style( 'photoswipe', get_template_directory_uri() . '/photoswipe/photoswipe.css', array(), SG_MATERIALIZECSS_VER, 'screen,projection' );
	wp_register_style( 'photoswipe-skin', get_template_directory_uri() . '/photoswipe/default-skin/default-skin.css', array( 'photoswipe' ), SG_MATERIALIZECSS_VER, 'screen,projection' );

	wp_register_style( 'sg-materializecss', get_template_directory_uri() . '/css/solska-galerija.css', array( 'materializecss', 'photoswipe-skin' ), SG_MATERIALIZECSS_VER );


	wp_register_script( 'materializecss', get_template_directory_uri() . '/materialize/js/materialize.min.js', array('jquery'), SG_MATERIALIZECSS_VER, true );
	wp_register_script( 'photoswipe', get_template_directory_uri() . '/photoswipe/photoswipe.min.js', array(), SG_MATERIALIZECSS_VER, true );
	wp_register_script( 'photoswipe-ui', get_template_directory_uri() . '/photoswipe/photoswipe-ui-default.min.js', array('photoswipe'), SG_MATERIALIZECSS_VER, true );
	wp_register_script( 'sg-materializecss', get_template_directory_uri() . '/js/solska-galerija.js', array('materializecss', 'photoswipe-ui'), SG_MATERIALIZECSS_VER, true );

	// enqueue scripts
	wp_enqueue_style( 'sg-materializecss' );
	wp_enqueue_script( 'sg-materializecss' );

}
add_action( 'wp_enqueue_scripts', 'sg_materialize_enqueue_scripts' );