<main>
	<div class="section">
		
		<?php
		// če smo na home-u, šola pa ima dislocirane enote / podružnice
		if( sg_is_home() && sg_dislocated_locations_enabled() ){

			if( sg_has_dislocated_locations() ){
				get_template_part( 'partials/main/dislocated_locations' );
			} else {
				get_template_part( 'partials/main/no-dislocated_locations' );
			}

		}
		// prikazujemo "navadno" galerijo / galerije
		else {

			if( sg_gallery_has_subgalleries() ){
				get_template_part( 'partials/main/subgalleries' );		
			}

			if( sg_gallery_has_images() ){
				ob_start();
				get_template_part( 'partials/main/images' );
				$images_html = ob_get_clean();

				// lazy load plugin support
				// if( class_exists( 'LazyLoad_Images' ) ){
				// 	$images_html = LazyLoad_Images::add_image_placeholders( $images_html );
				// }

				echo $images_html;
			}

			if( ! sg_gallery_has_subgalleries() && ! sg_gallery_has_images() ){
				get_template_part( 'partials/main/empty-gallery' );
			}

		}
		?>

	</div>
</main>