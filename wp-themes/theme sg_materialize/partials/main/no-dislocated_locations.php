<div class="row">
	<div class="col s10 m6 l6 offset-m1 offset-m3 l6 offset-l3">
		<div class="card-panel grey lighten-5 z-depth-1 center">
			<div class="valign-wrapper">
				<div class="valign col s12">
					<h5 class="">Podružnice šole še niso vnešene</h5>
					<?php
					// če ima uporabnik pravice, dodamo še gumb DODAJ NOVO PODRUŽNICO
					if( sg_user_can( SgPermissions::MANAGE_DISLOCATED_LOCATIONS ) ){
						// TODO
						?>
						<p>
							<a href="#dodaj">
								Dodaj novo podružnico.
							</a>
						</p>
						<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>