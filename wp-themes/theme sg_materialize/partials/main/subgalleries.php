<div class="row">
	<?php
	$subgalleries = sg_gallery_subgalleries();
	foreach ( $subgalleries as $subgallery ) {
		?>
		<div class="col s12 m4 l3">
			<a href="<?php echo esc_attr( $subgallery['url'] ); ?>">
				<div class="card-panel grey lighten-5 z-depth-1">
					<div class="row valign-wrapper">
						<div class="col s2">
							<img src="<?php echo esc_attr( $subgallery['image_src'] ); ?>" class="circle responsive-img">
						</div>
						<div class="col s10">
							<span class="black-text">
								<?php echo esc_html( $subgallery['title'] ); ?>
							</span>
						</div>
					</div>
				</div>
			</a>
		</div>
		<?php
	}
	?>
</div>