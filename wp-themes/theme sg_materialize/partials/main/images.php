<div class="section row">
	<?php
	$images = sg_gallery_images();
	foreach ( $images as $image ) {
		$title = esc_attr( $image['title'] );
		$alt = esc_attr( $image['alttext'] );
		$caption = esc_attr( $image['caption'] );

		$thumb_src = esc_attr( $image['images']['small']['src'] );
		$thumb_width = esc_attr( $image['images']['small']['width'] );
		$thumb_height = esc_attr( $image['images']['small']['height'] );

		$large_src = esc_attr( $image['images']['large']['src'] );
		$large_width = esc_attr( $image['images']['large']['width'] );
		$large_height = esc_attr( $image['images']['large']['height'] );

		?>
		<div class="col s6 m2 l1">
			<a class="sg-img" href="<?php echo $large_src; ?>" data-size="<?php echo $large_width; ?>x<?php echo $large_height; ?>" data-caption="<?php echo $caption; ?>">
				<img src="<?php echo $thumb_src; ?>" class="responsive-img" alt="<?php echo $thumb_alt; ?>" width="<?php echo $thumb_width; ?>" height="<?php echo $thumb_height; ?>">
			</a>
		</div>
		<?php
	}
	?>
</div>

<?php get_template_part( 'partials/main/images/photoswipe' ); ?>