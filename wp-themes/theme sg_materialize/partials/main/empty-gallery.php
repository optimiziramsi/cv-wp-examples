<div class="row">
	<div class="col s10 m6 l6 offset-m1 offset-m3 l6 offset-l3">
		<div class="card-panel grey lighten-5 z-depth-1 center">
			<div class="valign-wrapper">
				<div class="valign col s12">
					<h5 class="">Galerija je prazna</h5>
					<?php
					// če ima uporabnik pravice, dodamo še gumb DODAJ NOVO GALERIJO
					if( sg_user_can( SgPermissions::MANAGE_GALLERIES ) ){
						// TODO
						?>
						<p>
							<a href="#dodaj">
								Dodaj novo galerijo.
							</a>
						</p>
						<?php
					}
					
					// če ima uporabnik pravice, dodamo še gumb DODAJ NOVE SLIKE
					if( sg_user_can( SgPermissions::ADD_IMAGES ) ){
						// TODO
						?>
						<p>
							<a href="#dodaj">
								Dodaj nove slike.
							</a>
						</p>
						<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>