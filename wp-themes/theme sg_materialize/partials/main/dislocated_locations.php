<div class="row">
	<?php
	$locations = sg_get_dislocated_locations();
	foreach ( $locations as $location ) {
		?>
		<div class="col s12 m4 l3">
			<div class="card">
				<div class="card-image">
					<a href="<?php echo esc_attr( $location['url'] ); ?>">
						<img src="<?php echo esc_attr( $location['image_src'] ); ?>">
					</a>
				</div>
				<div class="card-content">
					<a href="<?php echo esc_attr( $location['url'] ); ?>">
						<span class="card-title grey-text text-darken-4"><?php echo esc_html( $location['title'] ); ?></span>
					</a>
					<?php echo wpautop( esc_html( $location['description'] ) ); ?>
				</div>
				<div class="card-action">
					<a href="<?php echo esc_attr( $location['url'] ); ?>">Vstopi</a>
				</div>
			</div>
		</div>
		<?php
	}
	?>
</div>