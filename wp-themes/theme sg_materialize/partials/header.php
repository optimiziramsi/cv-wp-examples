
		<!-- HEADER -->
		<div class="navbar-fixed">
			<nav>
				<div class="nav-wrapper">
					<!-- logo + ime šole + breadcrumbs -->
					<div class="left">

						<a href="<?php echo esc_attr( sg_get_home_url() ); ?>" class="breadcrumb">
							<img style="height: 2em; vertical-align: middle;" src="<?php echo esc_attr( sg_logo_image_src() ); ?>" class="circle responsive-img">
							<?php sg_title(); ?>
						</a>

						<?php
						$breadcrumbs = sg_gallery_breadcrumbs();
						foreach ( $breadcrumbs as $breadcrumb ) {
							?>

							<a href="<?php echo esc_attr( $breadcrumb['url'] ); ?>" class="breadcrumb"><?php echo esc_html( $breadcrumb['title'] ); ?></a>	

							<?php
						}
						?>
					</div>

					<!-- menu / tools -->
					<ul class="right hide-on-med-and-down">
						<li><a href="#!"><i class="material-icons">account_circle</i></a></li>
					</ul>
				</div>
			</nav>
		</div>