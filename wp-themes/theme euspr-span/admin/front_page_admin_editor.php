<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

class EsFrontPageAdminEditor {

	private static $instance = null;
	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		if( ! is_admin() || ! $this->isFronPageTemplateEdit() ){
			return;
		}

		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );

		add_action( 'tf_create_options', array( $this, 'add_meta_boxes' ) );

	}

	function admin_enqueue_scripts() {
		
		wp_enqueue_style( 'es-admin-front-page', get_stylesheet_directory_uri() . '/admin/css/front-page.css', array(), ES_VERSION );

	}

	public function add_meta_boxes(){

		// TODO - preveri tekste in opise

		$titan = TitanFramework::getInstance( 'es-fp' );

		/*******************  metabox - university  *******************/
		$metaBox_fp = $titan->createMetaBox( array(
			'id' => 'front-page-content-settings',
			'name' => __( 'Front Page content settings', 'es' ),
			'post_type' => 'page',
		) );

		/******************************************************************
		*
		*       FIRST ROW WITH SINGLE POST
		*
		******************************************************************/
		$metaBox_fp->createOption( array(
			'id' => '1st_heading',
		    'type' => 'heading',
		    'desc' => __( 'First row large post', 'es' ),
		) );

		$metaBox_fp->createOption( array(
		    'name' => __( 'First post', 'es' ),
		    'id' => '1st_post',
		    'type' => 'select-posts',
		    'desc' => __( 'First item on homepage with full width', 'es' ),
		) );


		/******************************************************************
		*
		*       SECOND ROW WITH TWO POSTS
		*
		******************************************************************/
		$metaBox_fp->createOption( array(
			'id' => '2nd_heading',
		    'type' => 'heading',
		    'desc' => __( 'Second row two posts', 'es' ),
		) );

		$metaBox_fp->createOption( array(
		    'name' => __( 'Second post', 'es' ),
		    'id' => '2nd_st_post',
		    'type' => 'select-posts',
		    'desc' => __( 'Second row with two posts. This is the left post.', 'es' ),
		) );

		$metaBox_fp->createOption( array(
		    'name' => __( 'Third post', 'es' ),
		    'id' => '2nd_nd_post',
		    'type' => 'select-posts',
		    'desc' => __( 'Second row with two posts. This is the right post.', 'es' ),
		) );


		/******************************************************************
		*
		*       THIRD ROW WITH TWO COLUMNS WITH POSTS
		*
		******************************************************************/
		$metaBox_fp->createOption( array(
			'id' => '3rd_heading',
		    'type' => 'heading',
		    'desc' => __( 'Third row with two posts', 'es' ),
		) );

		$metaBox_fp->createOption( array(
			'name' => __( 'Column title', 'es' ),
			'id' => '3rd_st_title',
			'type' => 'text',
			'desc' => __( 'Column title', 'es' ),
		) );

		$metaBox_fp->createOption( array(
		    'name' => __( 'Post', 'es' ),
		    'id' => '3rd_st_post',
		    'type' => 'select-posts',
		    'desc' => __( '', 'es' ),
		) );

		$metaBox_fp->createOption( array(
			'name' => __( 'Column title', 'es' ),
			'id' => '3rd_nd_title',
			'type' => 'text',
			'desc' => __( 'Column title', 'es' ),
		) );

		$metaBox_fp->createOption( array(
		    'name' => __( 'Post', 'es' ),
		    'id' => '3rd_nd_post',
		    'type' => 'select-posts',
		    'desc' => __( '', 'es' ),
		) );


		/******************************************************************
		*
		*       LARGE IMAGE WITH TEXT
		*
		******************************************************************/
		
		$metaBox_fp->createOption( array(
			'id' => '4th_heading',
		    'type' => 'heading',
		    'desc' => __( 'Forth row / large image with text', 'es' ),
		) );

		$metaBox_fp->createOption( array(
		    'name' => __( 'Select image', 'es' ),
		    'id' => '4th_image',
		    'type' => 'upload',
		    'desc' => __( '', 'es' ),
		) );

		$metaBox_fp->createOption( array(
		    'name' => __( 'Content', 'es' ),
		    'id' => '4th_content',
		    'type' => 'editor',
		    'desc' => __( '', 'es' ),
		) );

		$metaBox_fp->createOption( array(
		    'name' => __( 'Button url', 'es' ),
		    'id' => '4th_btn_url',
		    'type' => 'text',
		    'desc' => __( '', 'es' ),
		) );

		$metaBox_fp->createOption( array(
		    'name' => __( 'Button content', 'es' ),
		    'id' => '4th_btn_content',
		    'type' => 'editor',
		    'desc' => __( '', 'es' ),
		) );

		/******************************************************************
		*
		*       BOTTOM TWO COLUMNS
		*
		******************************************************************/
		
		$metaBox_fp->createOption( array(
			'id' => '5th_heading',
		    'type' => 'heading',
		    'desc' => __( 'Fifth row / last row with two columns', 'es' ),
		) );

		$metaBox_fp->createOption( array(
		    'name' => __( 'Select image', 'es' ),
		    'id' => '5th_image',
		    'type' => 'upload',
		    'desc' => __( '', 'es' ),
		) );

		$metaBox_fp->createOption( array(
		    'name' => __( 'Button url', 'es' ),
		    'id' => '5th_btn_url',
		    'type' => 'text',
		    'desc' => __( '', 'es' ),
		) );

		$metaBox_fp->createOption( array(
		    'name' => __( 'Button content', 'es' ),
		    'id' => '5th_btn_content',
		    'type' => 'editor',
		    'desc' => __( '', 'es' ),
		) );

		$metaBox_fp->createOption( array(
		    'name' => __( 'Content', 'es' ),
		    'id' => '5th_content',
		    'type' => 'editor',
		    'desc' => __( '', 'es' ),
		) );

	}


	private function isFronPageTemplateEdit(){
		$post_id = $this->currentPostId();
		if( ! $post_id ){
			return false;
		}
		$template_file = get_post_meta( $post_id, '_wp_page_template', true );

		return ( "front-page.php" == $template_file ? true : false );
	}

	private function currentPostId(){
		if( isset( $_GET['post'] ) ){
			$post_id = $_GET['post'];
		}
		elseif( isset( $_POST['post_ID'] ) ){
			$post_id = $_POST['post_ID'];
		}

		if( !isset( $post_id ) ){
			return false;
		}

		return $post_id;
	}
}

EsFrontPageAdminEditor::getInstance();