<?php
/**
 * @package My Theme
 */
?>

<?php $classes = array(
	'post-feature-holder',
	'category-post-holder'
); ?>


<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>
	
	<?php if ( es_has_post_thumbnail() ) { ?>
		<a href="<?php echo esc_url( get_permalink()); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
			<figure class="prime-figure thumb">
				<?php es_the_post_thumbnail('es-front-page-featured-news'); ?>
			</figure>
		</a>
	<?php } ?>


	<div class="featured-content">
		<h1 class="entry-title"><a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
	
		<?php the_excerpt(); ?>

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'es' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<div class="clearfix"></div>
	<footer class="entry-footer">
		<?php my_theme_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
