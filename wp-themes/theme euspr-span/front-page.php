<?php
/**
 * Template Name: Front Page
 *
 * @package WordPress
 * @subpackage euspr-span
 * @since 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="home-content">
		
				<?php

				global $post;
				$current_post = $post;
				$current_post_id = $post->ID;

				?>

				<?php

				/******************************************************************
				*
				*       FIRST ROW - large post with full width
				*
				******************************************************************/
				
				$show_post_id = get_post_meta( $current_post_id, 'es-fp_1st_post', true );
				$post = get_post( $show_post_id, OBJECT, 'display' );
				setup_postdata( $post );

				?>
				<div class="post-feature-holder">
					<?php if ( es_has_post_thumbnail() ) { ?>
						<a href="<?php the_permalink(); ?>">
							<figure class="prime-figure thumb">
								<?php es_the_post_thumbnail('es-front-page-featured-news'); ?>
							</figure>
						</a>
					<?php } ?>
					<div class="featured-content">
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<div class="meta-data">
				      		<?php my_theme_entry_footer(); ?>
				        </div>
				        <div class="desc-main">
				        	<?php the_excerpt(); ?>
				        	<a href="<?php the_permalink(); ?>" class="btn-read btn-medium"><?php _e( 'Read more', 'es' ); ?></a>
				        </div>
				    </div>
				</div>
				<?php wp_reset_postdata(); ?>

				<?php
				/******************************************************************
				*
				*       SECOND ROW
				*
				******************************************************************/
				?>
				<div class="home-posts-holder">
					<?php
					/******************************************************************
					*
					*       SECOND ROW - two column posts - FIRST POST
					*
					******************************************************************/
					$show_post_id = get_post_meta( $current_post_id, 'es-fp_2nd_st_post', true );
					$post = get_post( $show_post_id, OBJECT, 'display' );
					setup_postdata( $post );
					?>
					<div class="post-single">
						<?php if ( es_has_post_thumbnail() ) { ?>
							<a href="<?php the_permalink(); ?>">
								<figure class="thumb">
									<?php es_the_post_thumbnail('es-front-page-column-wide'); ?>
								</figure>
							</a>
						<?php } ?>
						<div class="content">
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							<?php the_excerpt(); ?>
							<a href="<?php the_permalink(); ?>" class="btn-read btn-small"><?php _e( 'Read more', 'es' ); ?></a>
						</div>
					</div>
					<?php wp_reset_postdata(); ?>

					<?php
					/******************************************************************
					*
					*       SECOND ROW - two column posts - SECOND POST
					*
					******************************************************************/
					$show_post_id = get_post_meta( $current_post_id, 'es-fp_2nd_nd_post', true );
					$post = get_post( $show_post_id, OBJECT, 'display' );
					setup_postdata( $post );
					?>
					<div class="post-single">
						<?php if ( es_has_post_thumbnail() ) { ?>
							<a href="<?php the_permalink(); ?>">
								<figure class="thumb">
									<?php es_the_post_thumbnail('es-front-page-column-wide'); ?>
								</figure>
							</a>
						<?php } ?>
						<div class="content">
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							<?php the_excerpt(); ?>
							<a href="<?php the_permalink(); ?>" class="btn-read btn-small"><?php _e( 'Read more', 'es' ); ?></a>
						</div>
					</div>
					<?php wp_reset_postdata(); ?>
				</div>
				

				<?php
				/******************************************************************
				*
				*       THIRD ROW WITH TWO COLUMNS
				*
				******************************************************************/
				?>
				<div class="home-posts-holder home-post-conference-holder">
					<div class="post-single">
						<?php
						/******************************************************************
						*
						*       THIRD ROW - two columns - FIRST COLUMN
						*
						******************************************************************/
						$column_title = get_post_meta( $current_post_id, 'es-fp_3rd_st_title', true );
						$show_post_id = get_post_meta( $current_post_id, 'es-fp_3rd_st_post', true );
						$post = get_post( $show_post_id, OBJECT, 'display' );
						setup_postdata( $post );
						?>
						<h4><?php echo apply_filters('the_title', $column_title); ?></h4>
						<?php if ( es_has_post_thumbnail() ) { ?>
							<a href="<?php the_permalink(); ?>">
								<figure class="thumb">
									<?php es_the_post_thumbnail('es-front-page-column-wide'); ?>
								</figure>
							</a>
						<?php } ?>
						<div class="content">
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							<?php the_excerpt(); ?>
							<a href="<?php the_permalink(); ?>" class="btn-read btn-small"><?php _e( 'Read more', 'es' ); ?></a>
						</div>
						<?php wp_reset_postdata(); ?>
					</div>
					<div class="post-single">
						<?php
						/******************************************************************
						*
						*       THIRD ROW - two columns - FIRST COLUMN
						*
						******************************************************************/
						$column_title = get_post_meta( $current_post_id, 'es-fp_3rd_nd_title', true );
						$show_post_id = get_post_meta( $current_post_id, 'es-fp_3rd_nd_post', true );
						$post = get_post( $show_post_id, OBJECT, 'display' );
						setup_postdata( $post );
						?>
						<h4><?php echo apply_filters('the_title', $column_title); ?></h4>
						<?php if ( es_has_post_thumbnail() ) { ?>
							<a href="<?php the_permalink(); ?>">
								<figure class="thumb">
									<?php es_the_post_thumbnail('es-front-page-column-wide'); ?>
								</figure>
							</a>
						<?php } ?>
						<div class="content">
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							<?php the_excerpt(); ?>
							<a href="<?php the_permalink(); ?>" class="btn-read btn-small"><?php _e( 'Read more', 'es' ); ?></a>
						</div>
						<?php wp_reset_postdata(); ?>
					</div>
				</div>
				<?php wp_reset_postdata(); ?>
				

				<?php
				/******************************************************************
				*
				*       FORTH ROW WITH LARGE IMAGE
				*
				******************************************************************/
				$image_id = get_post_meta( $current_post_id, 'es-fp_4th_image', true );
				$content = get_post_meta( $current_post_id, 'es-fp_4th_content', true );
				$btn_url = get_post_meta( $current_post_id, 'es-fp_4th_btn_url', true );
				$btn_content = get_post_meta( $current_post_id, 'es-fp_4th_btn_content', true );

				?>
				<div class="home-early-holder">
					<div class="home-banner-single">
						<?php
						$img_html = wp_get_attachment_image( $image_id, 'es-front-page-full-width', false );
						if( ! empty( $img_html ) ){
							echo $img_html;
						}
						?>
						
						<div class="caption-left">
							<?php echo apply_filters('the_content', $content ); ?>
						</div>
						<div class="caption-right">
							
							<a href="<?php echo esc_url( $btn_url ); ?>" class="btn-transparent"><?php echo apply_filters('the_content', $btn_content ); ?></a>
						</div>
					</div>
				</div>


				<?php
				/******************************************************************
				*
				*       FIFTH / LAST ROW - TWO COLUMNS
				*
				******************************************************************/
				$image_id = get_post_meta( $current_post_id, 'es-fp_5th_image', true );
				$btn_url = get_post_meta( $current_post_id, 'es-fp_5th_btn_url', true );
				$btn_content = get_post_meta( $current_post_id, 'es-fp_5th_btn_content', true );
				$content = get_post_meta( $current_post_id, 'es-fp_5th_content', true );
				?>
				<div class="last-section-holder">
					<div class="half become-member-holder">
						<div class="join-banner">
							<?php
							$img_html = wp_get_attachment_image( $image_id, 'es-front-page-column-wide2', false );
							if( ! empty( $img_html ) ){
								echo $img_html;
							}
							?>
							<div class="caption-right">
								<a href="<?php echo esc_url( $btn_url ); ?>" class="btn-transparent"><?php echo apply_filters('the_content', $btn_content ); ?></a>
							</div>
						</div>
						
						<?php echo apply_filters('the_content', $content ); ?>
					</div>
					<div class="half social-holder">
						<?php echo do_shortcode('[statictweets skin="simplistic" resource="usertimeline" user="EUSPR" list="" query="" count="3" retweets="on" replies="on" show="username,screenname,avatar,time,actions,media"/]'); ?>			

						<a href="https://twitter.com/euspr" target="_blank" class="btn-small btn-read"><?php _e( 'Visit Twitter channel', 'es' ); ?></a>
						
					</div>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
	
<?php get_footer(); ?>