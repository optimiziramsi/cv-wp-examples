<?php


/******************************************************************
*
*       GET POST / PAGE IMAGE
*
******************************************************************/

function es_has_post_thumbnail(){
	if( has_post_thumbnail() ){
		return true;
	}

	$catch_that_image_src = catch_that_image();

	if( '' !== $catch_that_image_src ){
		return true;
	}

	return false;
}

function es_the_post_thumbnail( $size = 'post-thumbnail', $attr = '' ){
	if( has_post_thumbnail() ){
		the_post_thumbnail( $size, $attr );
		return;
	}

	$catch_that_image_src = catch_that_image();
	if( '' !== $catch_that_image_src ){
		$catch_that_image_id = es_get_image_id_from_src( $catch_that_image_src );
		if( $catch_that_image_id ){
			$html = wp_get_attachment_image( $catch_that_image_id, $size, false, $attr );
			if( ! empty( $html ) ){
				echo $html;
				return;
			}
		}

		echo '<img src="' . esc_attr( $catch_that_image_src ).'">';
		return;
	}
}

function es_get_image_id_from_src($image_url) {
	global $post;
	$image_id = false;

	// check in cache
	$image_ids_by_source = get_post_meta( $post->ID, '_es_image_ids_by_src', true );
	if( is_array( $image_ids_by_source ) && isset( $image_ids_by_source[ $image_url ] ) ){
		$image_id = $image_ids_by_source[ $image_url ];
		return $image_id;
	}

	if( ! is_array( $image_ids_by_source ) ){
		$image_ids_by_source = array();
	}

	// Split the $image_url into two parts with the wp-content directory as the separator
	$parsed_url  = explode( parse_url( WP_CONTENT_URL, PHP_URL_PATH ), $image_url );
	// Get the host of the current site and the host of the $image_url, ignoring www
	$this_host = str_ireplace( 'www.', '', parse_url( home_url(), PHP_URL_HOST ) );
	$file_host = str_ireplace( 'www.', '', parse_url( $image_url, PHP_URL_HOST ) );
	// Return nothing if there aren't any $image_url parts or if the current host and $image_url host do not match
	if ( ! isset( $parsed_url[1] ) || empty( $parsed_url[1] ) || ( $this_host != $file_host ) ) {
		$image_ids_by_source[ $image_url ] = $image_id;
		update_post_meta( $post->ID, '_es_image_ids_by_src', $image_ids_by_source );
		return $image_id;
	}
	// Now we're going to quickly search the DB for any attachment GUID with a partial path match
	// Example: /uploads/2013/05/test-image.jpg
	global $wpdb;
	$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}posts WHERE guid RLIKE %s;", $parsed_url[1] ) );
	if( isset( $attachment[0] ) && "" != $attachment[0] ){
		$image_id = $attachment[0];
		$image_ids_by_source[ $image_url ] = $image_id;
		update_post_meta( $post->ID, '_es_image_ids_by_src', $image_ids_by_source );
		return $image_id;
	}

	// search in image resizes
	$image_upload_path = str_replace('/uploads/', "", dirname( $parsed_url[1] ) );
	$image_file_name = basename( $parsed_url[1] );

	if( ! empty( $image_upload_path ) && ! empty( $image_file_name ) ){
		$image_upload_path .= '/';
		$query = $wpdb->prepare( "SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_value LIKE (%s) AND meta_value LIKE (%s)", array( "%%".$image_upload_path."%%", "%%".$image_file_name."%%" ) );
		$attachment = $wpdb->get_col( $query );
		if( isset( $attachment[0] ) && "" != $attachment[0] ){
			$image_id = $attachment[0];
			$image_ids_by_source[ $image_url ] = $image_id;
			update_post_meta( $post->ID, '_es_image_ids_by_src', $image_ids_by_source );
			return $image_id;
		}
	}
	
	return $image_id;
}

/*get first image in the post*/
function catch_that_image() {
	global $post, $posts;

	$first_img = '';

	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);

	if(isset($matches) && is_array($matches) && isset($matches[1]) && is_array($matches[1]) && isset($matches[1][0])){

		$first_img = $matches [1] [0];	

	}

	//var_dump($post->post_content);

	// if(empty($first_img)){ //Defines a default image

		// $first_img = "/images/default.jpg";

	// }

	return $first_img;

}