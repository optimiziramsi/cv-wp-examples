<?php

if ( ! defined( 'ABSPATH' ) ) { 
	exit; // Exit if accessed directly
}

function es_ajax_search_add_query_var(){
	global $wp;
	$wp->add_query_var('es-ajax-search');
}

function es_ajax_results() {
	if ( get_query_var('es-ajax-search') != 1 ) {
		return;
	}

	if( ! isset( $_GET['_n'] ) || ! wp_verify_nonce( $_GET['_n'], 'ajax_search' ) ){
		return;
	}

	$search_str = isset( $_GET['s'] ) ? $_GET['s'] : '';

	$args_posts = array( 's' => $search_str, 'post_type' => 'post', );
	$search_posts = new WP_Query( $args_posts );

	$args_pages = array('s' => $search_str, 'post_type' => 'page', );
	$search_pages = new WP_Query( $args_pages );



	ob_start();
	if ( $search_posts->have_posts() || $search_pages->have_posts() ) :
		global $post;

		?>
		<div class="dummy-column">
			<h2><?php esc_html_e( 'Pages', 'es' ); ?></h2>
			<?php if( $search_pages->have_posts() ): ?>
				
				<?php while ( $search_pages->have_posts() ) : $search_pages->the_post(); ?>

					<a href="<?php the_permalink(); ?>" class="dummy-media-object">
				    	<h3><?php the_title(); ?></h3>
				    </a>

				<?php endwhile; ?>

			<?php else: ?>
				
				<p>
					<?php _e( 'No pages found.', 'es' ); ?>
				</p>

			<?php endif; ?>
		</div>


		<div class="dummy-column">
			<h2><?php esc_html_e( 'Posts', 'es' ); ?></h2>
			<?php if( $search_posts->have_posts() ): ?>
				
				<?php while ( $search_posts->have_posts() ) : $search_posts->the_post(); ?>

					<a href="<?php the_permalink(); ?>" class="dummy-media-object">
				    	<h3><?php the_title(); ?></h3>
				    </a>

				<?php endwhile; ?>

			<?php else: ?>
				
				<p>
					<?php _e( 'No posts found.', 'es' ); ?>
				</p>

			<?php endif; ?>
		</div>

		<?php
	else :
		?>
			<p>
				<?php _e( 'No posts or pages found.', 'es' ); ?>
			</p>
		<?php
	endif;
	
	$content = ob_get_clean();
	
	echo json_encode( array(
		'success' => 'yes',
		'content' => $content,
		's' => $search_str,
	) );
	die();
			
}

add_action( 'init', 'es_ajax_search_add_query_var' );
add_action( 'template_redirect', 'es_ajax_results' );