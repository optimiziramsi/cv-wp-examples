<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package My Theme
 */

get_header(); ?>

	<div id="primary" class="content-area euspr-category-holder">
		<main id="main" class="site-main" role="main">


		<?php
		global $wp_query;
		$sticky_query_args = array_merge( $wp_query->query_vars, array( 'nopaging' => true, 'post__in' => get_option('sticky_posts') ) );
		$sticky_query = new WP_Query( $sticky_query_args );

		if( $sticky_query->have_posts() ){
			while ( $sticky_query->have_posts() ) : $sticky_query->the_post();
				get_template_part( 'content', 'post-short-sticky' );
			endwhile;
		}

		wp_reset_postdata();
		?>



		<?php
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
				get_template_part( 'content', 'post-short' );
			endwhile;

			the_posts_navigation();

		endif;
		?>

		<?php
		if ( ! have_posts() && ! $sticky_query->have_posts() ) :
			get_template_part( 'content', 'none' );
		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
	
<?php
get_footer();