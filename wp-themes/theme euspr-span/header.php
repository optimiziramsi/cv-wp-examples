<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package My Theme
 */
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 9]>
<html id="ie9" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8) | !(IE 9)  ]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

		<?php require dirname( __FILE__ ) . '/inc/browser-icons.php'; ?>
		
		<!-- loader css -->
		<style type="text/css">
			#loading{background:#fff;bottom:0;height:100%;left:0;position:fixed;right:0;transition:opacity .3s ease-in-out;top:0;width:100%;z-index:9999;opacity:1}#loading.hide-loading{opacity:0}#loading span{border-radius:50%;display:block;height:142px;left:50%;position:absolute;top:50%;-webkit-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);-o-transform:translate(-50%,-50%);transform:translate(-50%,-50%);width:142px}#loading .spinner{-webkit-animation:b 1.4s infinite linear;-o-animation:b 1.4s infinite linear;animation:b 1.4s infinite linear;border:2px solid #cccaeb;border-right-color:#eee;border-radius:50%;color:transparent;display:block;height:140px;width:140px}.spinner-small{-webkit-animation:b 1.4s infinite linear;-o-animation:b 1.4s infinite linear;animation:b 1.4s infinite linear;border:2px solid #cccaeb;border-right-color:#eee;border-radius:50%;color:transparent;display:inline-block;height:15px;width:15px;margin-right:3px}
		</style>

		<?php wp_head(); ?>
	</head>

	<!--[if lt IE 9]>
	    <div class="browsehappy card red lighten-1">
	        <p class="card-content white-text">
	            <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true" target="_blank">activate Google Chrome Frame</a> to improve your experience.','edpqs'); ?>
	        </p>
	    </div>
	<![endif]-->

	<body <?php body_class(); ?>>
		<!-- Preloader -->
		<div id="loading"><span><i class="spinner icon-play"></i></span></div>

		<!-- Searcher -->
		<div id="morphsearch" class="morphsearch">
			<form class="morphsearch-form" role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" data-search-url="<?php echo esc_url( wp_nonce_url( home_url( '/' ), 'ajax_search', '_n' ) ); ?>">
				<input type="search" placeholder="<?php esc_attr_e( 'Search ...', 'es' ); ?>" class="morphsearch-input" value="" name="s" id="s" autocomplete="off" />
				<button type="submit" class="morphsearch-submit"></button>
			</form>
			<div class="morphsearch-content">
			</div><span class="morphsearch-close"></span>
		</div>

		<header role="banner" class="main-header">
			<figure class="main-logo-euspr"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo_euspr.png" alt="EUSPR" data-at2x="<?php echo get_template_directory_uri(); ?>/images/logo_euspr_2x.png" /></a></figure>
			<nav id="site-navigation" role="navigation" class="main-navigation">
				<h1 class="screen-reader-text">Main Navigation</h1>
				<div class="navicon closed"><i class="fa fa-navicon"></i></div>
			</nav>
		</header>

		<div class="mobile-menu">
			<nav id="mobile-site-navigation" class="mobile-navigation" role="navigation">
				<h1 class="screen-reader-text">Main Navigation</h1>

				<div class="widget-sidebar-euspr">
					<?php wp_nav_menu( array( 'theme_location' => 'left_euspr', 'depth' => 2 ) ); ?>
				</div>
				
				<div class="widget-sidebar-span">
					<figure class="main-logo-span"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-span.png" data-at2x="<?php echo get_template_directory_uri(); ?>/images/logo_span_2x.png" alt="SPAN"></figure>
					<?php wp_nav_menu( array( 'theme_location' => 'left_span', 'depth' => 2 ) ); ?>
					<div class="banner">
								<figure class="main-logo-span"><img src="<?php echo get_template_directory_uri(); ?>/images/logo_llp.png" alt="Life Long Programme"></figure>
							</div>
				</div>
			</nav><!-- #mobile-navigation -->
		</div>

		<div class="header-content tagline-wrapper">
			<div class="tagline-holder half">
				<h3>The European Society for Prevention Research (EUSPR) promotes the <strong>development of prevention science</strong>, and its application to practice so as to promote <strong>human health and well-being</strong> through high quality research, evidence based interventions, policies and practices.</h3>
			</div>
			<div class="header-twitter half">
				<div class="twitter-wrapper"><?php echo do_shortcode('[rotatingtweets screen_name="euspr"]' ); ?></div>
			</div>
			
		</div>

		<div class="clearfix"></div>

		<div class="page-wrapper">

			<div class="sidebar-bg"><!-- this is just menu background --></div>
			<div class="sidebar">
				<nav id="site-navigation" class="main-navigation" role="navigation">
					<h1 class="screen-reader-text">Main Navigation</h1>

					<div class="widget-sidebar-euspr">
						<?php wp_nav_menu( array( 'theme_location' => 'left_euspr', 'depth' => 2 ) ); ?>
					</div>
					
					<div class="widget-sidebar-span">
						<figure class="main-logo-span"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-span.png" data-at2x="<?php echo get_template_directory_uri(); ?>/images/logo_span_2x.png" alt="SPAN"></figure>
						<?php wp_nav_menu( array( 'theme_location' => 'left_span', 'depth' => 2 ) ); ?>
						<div class="banner">
									<figure class="main-logo-span"><img src="<?php echo get_template_directory_uri(); ?>/images/logo_llp.png" alt="Life Long Programme"></figure>
								</div>
					</div>

				</nav><!-- #site-navigation -->
					<?php
							if ( is_active_sidebar( 'euspr-widget' ) ) :
									dynamic_sidebar( 'euspr-widget' );
							endif;
							?>
			</div>
			<a href="#top" id="go-to-the-top"><i class="fa fa-arrow-up"></i></a>


			<div id="page" class="hfeed site home-content">
				<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'es' ); ?></a>
				<div id="content" class="site-content">