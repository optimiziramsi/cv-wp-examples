<?php
/**
 * @package My Theme
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<!-- <div class="entry-meta">
			<?php my_theme_entry_footer(); ?>
		</div> -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'es' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php my_theme_entry_footer(); ?>

	</footer><!-- .entry-footer -->
			<?php if ( function_exists( 'sharing_display' ) ) {
	    sharing_display( '', true );
	}
	 
	if ( class_exists( 'Jetpack_Likes' ) ) {
	    $custom_likes = new Jetpack_Likes;
	    echo $custom_likes->post_likes( '' );
	} ?>
</article><!-- #post-## -->
