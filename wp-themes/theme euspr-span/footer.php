<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package My Theme
 */
?>

				</div><!-- #content -->
			</div><!-- #page -->
		</div><!-- .page-wrapper -->


		<footer id="colophon" class="footer-bg" role="contentinfo">
			<div class="container">

				<div class="half-c">
					<div class="footer-widget">
						<figure class="main-logo-euspr-footer">
							<a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/images/logo_euspr_white.png" alt="EUSPR | European science for prevention and research" data-at2x="<?php echo get_template_directory_uri(); ?>/images/logo_euspr_white_2x.png" />
							</a>
						</figure>
						<h4>EUSPR</h4><span class="desc">European science for prevention and research</span>
						<span class="year">© 2010 - <?php echo date('Y'); ?></span>
					</div>
					<div class="footer-widget">
						<?php wp_nav_menu( array( 'theme_location' => 'footer_euspr', 'depth' => 1 ) ); ?>
					</div>
					<div class="footer-widget">
						<?php _e( 'Supported by', 'es' ); ?>
						
						<?php
						if( class_exists( 'EusprSpanExtra_SettingsPage' ) ){
							$sett = EusprSpanExtra_SettingsPage::getInstance();
							$supporting_organisations_id = $sett->param('footer_supporting_organisations_page');
							$supporting_organisations_page = get_page( $supporting_organisations_id );
							if( $supporting_organisations_page ){
								$supporting_organisations_url = get_permalink( $supporting_organisations_id );
								$supporting_organisations_title = get_the_title( $supporting_organisations_id );
								?>
								<p><a href="<?php echo esc_attr( $supporting_organisations_url ); ?>"><?php echo esc_html( $supporting_organisations_title ); ?></a></p>
								<?php
							}
						}
						?>
						
					</div>
				</div>
				<div class="half-c">
					<div class="footer-widget">
						<figure class="main-logo-euspr-footer">
							<a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/images/logo-span-white.png" alt="<?php echo esc_attr_e( 'SPAN | Science for Prevention Academic Network', 'es' ); ?>" data-at2x="<?php echo get_template_directory_uri(); ?>/images/logo_span_white_2x.png" />
							</a>
						</figure>

						<h4><?php esc_html_e( 'SPAN', 'es' ); ?></h4>
						<span class="desc"><?php esc_html_e( 'Science for Prevention Academic Network', 'es' ); ?></span>
						<span class="year">© 2012 - 2016</span>
					</div>
					<div class="footer-widget">
						<?php wp_nav_menu( array( 'theme_location' => 'footer_span', 'depth' => 1 ) ); ?>
					</div>
					<div class="footer-widget">
						<?php _e( 'Supported by', 'es' ); ?>
						<figure class="footer-banner"><img src="<?php echo get_template_directory_uri(); ?>/images/logo_llp.png" alt="Life Long Programme"></figure>
					</div>
				</div>
				
				<div class="clearfix"></div>

				<div class="site-info">
					2010 - <?php echo date('Y');?> © Copyright <a href=""><?php printf( __( 'EUSPR', 'es' ), '' ); ?></a>
					<span class="sep"> | </span>
					<?php printf( __( 'Theme: %1$s by %2$s.', 'es' ), 'EUSPR', '<a href="http://www.2gika.si" rel="designer">2gika.si</a>' ); ?>

					<?php
						if( class_exists( 'EusprSpanExtra_SettingsPage' ) ){
							$sett = EusprSpanExtra_SettingsPage::getInstance();
							$cookies_id = $sett->param('footer_cookies_page');
							$cookies_page = get_page( $cookies_id );
							if( $cookies_page ){
								$cookies_url = get_permalink( $cookies_id );
								$cookies_title = $cookies_page->post_title;
								?>
								<span class="right"><a href="<?php echo esc_attr( $cookies_url ); ?>"><?php echo esc_html( $cookies_title ); ?></a></span>
								<?php
							}
						}
					?>
				</div><!-- .site-info -->
			</div>
		</footer><!-- #colophon -->

		<?php wp_footer(); ?>
		<noscript><style>#loading {display:none;}</style></noscript>
	</body>
</html>
