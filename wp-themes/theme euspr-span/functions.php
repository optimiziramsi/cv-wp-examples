<?php
/**
 * My Theme functions and definitions
 *
 * @package My Theme
 */

define('ES_VERSION', '1.0.2');

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 800; /* pixels */
}

// Removes from post and pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}

if ( ! function_exists( 'my_theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function my_theme_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on My Theme, use a find and replace
	 * to change 'es' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'es', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'left_euspr' => __( 'Side menu EUSPR', 'es' ),
	) );
	register_nav_menus( array(
		'left_span' => __( 'Side menu SPAN', 'es' ),
	) );
	register_nav_menus( array(
		'footer_euspr' => __( 'Footer EUSPR', 'es' ),
	) );
	register_nav_menus( array(
		'footer_span' => __( 'Footer SPAN', 'es' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'post','aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'my_theme_custom_background_args', array(
		'default-color' => 'ebe8e5',
		'default-image' => '',
	) ) );
}
endif; // my_theme_setup
add_action( 'after_setup_theme', 'my_theme_setup' );

/**
 * Enqueue scripts and styles.
 */
function my_theme_scripts() {
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'my_theme_scripts' );

/*Enqueue style and scripts*/
function enqueue_styles_scripts() { 
	wp_enqueue_style('gfonts', 'https://fonts.googleapis.com/css?family=Lato:400,300,700,900,400italic&subset=latin,latin-ext');
	wp_enqueue_style('euspr-span', get_template_directory_uri() . '/css/euspr-span.css' );

	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.js' );
	wp_enqueue_script( 'euspr-span', get_template_directory_uri() . '/js/script.js', array('jquery'), false, true );
	wp_localize_script( 'euspr-span', 'es_vars', array(
		'search_loading' => __( 'Loading ...', 'es' ),
	) );
}
add_action('wp_enqueue_scripts', 'enqueue_styles_scripts');


/*remove jetpack icon shares*/
function jptweak_remove_share() {
    remove_filter( 'the_content', 'sharing_display',19 );
    remove_filter( 'the_excerpt', 'sharing_display',19 );
    if ( class_exists( 'Jetpack_Likes' ) ) {
        remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
    }
}
add_action( 'loop_start', 'jptweak_remove_share' );


/*Remove Mimetype plugin styles*/
function mytheme_dequeue_css_from_plugins()  {
	wp_dequeue_style( "mimetypes-link-icons" ); 
	wp_deregister_style( 'mimetypes-link-icons' );
}
add_action( 'wp_enqueue_scripts', 'mytheme_dequeue_css_from_plugins', 9999 );

// Activate support for featured images
add_theme_support( 'post-thumbnails' );

// Set the post thumbnail default size to suit the theme layout
set_post_thumbnail_size( 811, 456, true );

// Add a div wrapper around the "Continue Reading" button
// From https://tommcfarlin.com/more-tag-wrapper/
function add_continue_wrapper( $link, $text ) {
	$html = '<div class="continue_btn">' . $link . '</div>';
	return $html;
}
add_action( 'the_content_more_link', 'add_continue_wrapper', 10, 2 );


// Custom login logo
function my_login_logo() { ?>
    <style type="text/css">
        .login h1 a {
            background-image: url(<?php echo get_template_directory_uri(); ?>/images/euspr-login.png);
            padding-bottom: 10px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );



/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function my_theme_widgets_init() {
	register_sidebar( array(
		'name' => __("Menu Area", "testtheme"),
		'id' => 'euspr-widget',
		'description' => 'First Widget below SPAN Menu',
		'before_widget' => "<div class='sidebar-widget-wrapper'>",
		'after_widget' => "</div>"
	) );
}
add_action( 'widgets_init', 'my_theme_widgets_init' );



/*this function truncates titles to create preview excerpts*/
function truncate_title($amount,$echo=true) {
	global $post;
	$truncate = get_the_title(); 
	if ( strlen($truncate) <= $amount ){
		$echo_out = '';
	} else {
		$echo_out = '...';
	}

	$truncate = mb_substr( $truncate, 0, $amount, 'UTF-8' );

	if ($echo) {
		echo $truncate;
		echo $echo_out;
	} else {
		return ($truncate . $echo_out);
	}
}



/**
 * If page or post has enabled Table Of Content (TOC), show it
 * @return [type] [description]
 */
function es_echo_table_of_content(){
	global $post;

	if( ! is_single() && ! is_page() ){
		return;
	}

	$toc_enabled = get_post_meta( $post->ID, 'ese-table-of-content_enable', true );
	if( ! $toc_enabled ){
		return;
	}

	$toc_data_string = get_post_meta( $post->ID, 'ese-table-of-content_data', true );
	$toc_data = @json_decode( $toc_data_string );
	if( is_object( $toc_data ) ){
		$toc_data = (array)$toc_data;
	}

	if( ! $toc_data || empty( $toc_data ) ){
		return;
	}

	?>
	<div class="toc-wrapper">
		<div class="relative">
			<div class="fixed-toc">
				<div class="button-holder-toc">
					<span id="toc-button" class="toc-close-button"><i class="fa fa-arrow-left"></i></span>
				</div>
				<ul class="section table-of-contents">
					<?php foreach ( $toc_data as $toc_item_id => $toc_item ) {
						$id = '#' . $toc_item_id;
						$class = 'toc-' . $toc_item->tag;
						$text = $toc_item->text;
						?>
						<li class="<?php echo esc_attr( $class ); ?>"><a href="<?php echo esc_attr( $id ); ?>"><?php echo esc_html( $text ); ?></a></li>
						<?php
					} ?>
				</ul>
			</div>
		</div>
	</div>
	<?php
}



/******************************************************************
*
*       WHEN ON CATEGORY LIST, SHOW STICKY POSTS FIRST
*
******************************************************************/

function es_pre_get_posts_get_sticky_posts_first( WP_Query $query ) {
	if ( is_admin() || ! $query->is_main_query() ){
        return;
	}

	if( $query->is_category() ){
		$query->set('post__not_in', get_option('sticky_posts') );
	}

}
add_action('pre_get_posts','es_pre_get_posts_get_sticky_posts_first', 999 );


/******************************************************************
*
*       THEME IMAGE SIZES
*
******************************************************************/

// add_image_size( $name, $width, $height, $crop );
// homepage
add_image_size( 'es-front-page-featured-news', 400, 300, false );
add_image_size( 'es-front-page-column-wide', 200, 150, false );
add_image_size( 'es-front-page-full-width', 1237, 600, true );
add_image_size( 'es-front-page-column-wide2', 619, 300, true );

require dirname( __FILE__ ) . '/inc/template-tags.php';
require dirname( __FILE__ ) . '/inc/jetpack.php';
require dirname( __FILE__ ) . '/inc/ajax_search.php';
require dirname( __FILE__ ) . '/inc/catch_that_image.php';
require dirname( __FILE__ ) . '/admin/front_page_admin_editor.php';