<?php
/**
 * @package My Theme
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class("post-single"); ?>>
	
	<?php if ( es_has_post_thumbnail() ) { ?>
		<a href="<?php esc_url( get_permalink() ); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
			<figure class="thumb">
				<?php es_the_post_thumbnail('thumbnail'); ?>
			</figure>
		</a>
	<?php } ?>

	<div class="entry-content content">
		<h1 class="entry-title"><a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"><?php truncate_title(100, true); ?></a></h1>
		
		<footer class="entry-footer meta-below-title">
			<?php my_theme_meta_data(); ?>
		</footer><!-- .entry-footer -->
		
		<?php the_excerpt(); ?>

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'es' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->


