<?php
if ( ! defined( 'ABSPATH' ) ) {
exit; // Exit if accessed directly
}

?>
<h1><?php esc_html_e("Delivery code", "gextracking"); ?>: <strong><?php echo esc_html( $package['code'] ); ?></strong></h1>
<h2>
<?php esc_html_e("Courier", "gextracking"); ?>: <strong><?php echo esc_html( $package['courier_display_name'] ); ?></strong><br>
<?php esc_html_e("Package AWB", "gextracking"); ?>: <strong><?php echo esc_html( $package['courier_awb'] ); ?></strong></h2>
<div class="table-2">
	<table style="width: 100%;">
		<thead>
			<tr>
				<th><?php esc_html_e("date", "gextracking"); ?></th>
				<th><?php esc_html_e("type / location", "gextracking"); ?></th>
				<th><?php esc_html_e("description", "gextracking"); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			if( is_array( $package['trackings'] ) && count( $package['trackings'] ) > 0 ){
				foreach ($package['trackings'] as $tracking) {
					?>
					<tr>
						<td><?php echo esc_html( $tracking['date'] ); ?></td>
						<td><?php echo esc_html( $tracking['location'] ); ?></td>
						<td><?php echo esc_html( $tracking['description'] ); ?></td>
					</tr>
					<?php
				}
			}

			else {
				?>
				<tr>
					<td colspan="3"><?php esc_html_e("No tracking data available. Please try again later.", "gextracking"); ?></td>
				</tr>
				<?php
			}

			?>
		</tbody>
	</table>
</div>