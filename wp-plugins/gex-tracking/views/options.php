<?php
if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

?>
<div class="wrap">
<h1><?php _e('Gex Tracking', 'gextracking'); ?></h1>

<form method="post" action="<?php echo esc_attr( admin_url('admin-post.php') ); ?>">
	
	<?php wp_nonce_field(self::OPTIONS_NONCE_ACTION); ?>
	<input type="hidden" name="action" value="<?php echo esc_attr(self::OPTIONS_SAVE_ACTION); ?>">
	
	<?php
	/******************************************************************
	*
	*       TRACKING PAGE
	*
	******************************************************************/
	?>

	<h2><?php _e('Tracking results', 'gextracking'); ?></h2>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Page', 'gextracking'); ?></th>
			<td>
				<select name="gextracking_page">
					<?php
					$gextracking_page_id = (int)$this->get_options('gextracking_page');
					$pages = get_posts( array(
							'orderby'          => 'post_title',
							'order'            => 'ASC',
							'posts_per_page'   => -1,
							'post_status'      => 'publish',
							'post_type'        => 'page',
						) );
					foreach ($pages as $page) {
						?>
						<option value="<?php echo esc_attr($page->ID); ?>" <?php selected($gextracking_page_id, $page->ID); ?>><?php echo esc_html( $page->post_title ); ?></option>
						<?php
					}
					?>
				</select>
			</td>
		</tr>
	</table>
	<hr>
	

	<?php
	/******************************************************************
	*
	*       API SPEC
	*
	******************************************************************/
	?>

	<h2><?php _e('API settings', 'gextracking'); ?></h2>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Host', 'gextracking'); ?></th>
			<td>
				<input type="text" name="gextracking_api_host" value="<?php echo esc_attr($this->get_options("gextracking_api_host")); ?>">
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><?php _e('Key', 'gextracking'); ?></th>
			<td>
				<input type="text" name="gextracking_api_key" value="<?php echo esc_attr($this->get_options("gextracking_api_key")); ?>">
			</td>
		</tr>
	</table>
	<hr>
	
	<?php submit_button(); ?>

</form>
</div>