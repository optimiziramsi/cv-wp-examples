��          �   %   �      p     q     ~     �     �  @   �     �     �                    !  0   /  3   `  Z   �  /   �  V     K   v  j   �     -     9     >  %   R     x     �     �     �     �  �  �     �     �     �     �  =   �               +     7     N     U  +   c  9   �  r   �  I   <	  �   �	  �   
  �   �
  
             "  %   3     Y     i     o     t     �                                                          
                   	                                              API settings Courier Delivery code Enter tracking number For tracking info, please provide tracking number in form above. GEX Prizma tracking Gex Tracking GexTracking Host Key Matevž Novak No tracking data available for searched package. No tracking data available. Please try again later. Oops, this should not happen. Missing tracking server credentials. Please try again later. Oops, this should not happen. Please try again. Oops, this should not happen. Tracking service returned error. Please try again later. Oops, this should not happen. Tracking service returned unexpected message. Oops, this should not happen. We have comunication problems with tracking service. Please try again later. Package AWB Page Track Your Shipment Tracking of GEX packages from website Tracking results date description http://optimiziram.si type / location Project-Id-Version: GEX Prizma tracking 1.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/gex-tracking
POT-Creation-Date: 2017-08-07 19:48+0200
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-07 19:54+0200
Language-Team: 
X-Generator: Poedit 1.8.7.1
Last-Translator: 
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
Language: sl_SI
 API nastavitve Kurir Oznaka pošiljke Vpišite številko paketa Za prikaz sledenja, v zgornje okno vpišite številko paketa. GEX Prizma sledenje GexTracking GexTracking Naslov spletnega mesta Ključ Matevž Novak Ni podatkov o sledenju za iskano pošiljko. Ni zapisov o sledenju. Prosimo poskusite ponovno kasneje. Ooops, to se pa ne bi smelo zgoditi. Manjkajo podatki o strežniku za sledenje. Prosimo poskusite ponovno kasneje. Ooops, to se pa ne bi smelo zgoditi. Prosimo ponovno kliknite na iskanje. Ooops, to se pa ne bi smelo zgoditi. Komunikacija s strežnikom za sledenje ni bila uspešna (2). Prosimo poskusite ponovno kasneje. Ooops, to se pa ne bi smelo zgoditi. Komunikacija s strežnikom za sledenje ni bila uspešna (3). Prosimo poskusite ponovno kasneje. Ooops, to se pa ne bi smelo zgoditi. Komunikacija s strežnikom za sledenje ni bila uspešna. Prosimo poskusite ponovno kasneje. AWB paketa Stran Išči pošiljko Tracking of GEX packages from website Sledenje prikaz datum opis http://optimiziram.si tip / lokacija 