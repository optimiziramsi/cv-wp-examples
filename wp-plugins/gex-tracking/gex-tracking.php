<?php
/**
* Plugin Name: GEX Prizma tracking
* Description: Tracking of GEX packages from website
* Plugin URI: http://optimiziram.si
* Author: Matevž Novak
* Author URI: http://optimiziram.si
* Version: 1.0
* License: GPL2
* Text Domain: gextracking
* Domain Path: languages
*/

/*
Copyright (C) 2017  Matevž Novak  matevz@optimiziram.si

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
/**
 * Snippet Wordpress Plugin Boilerplate based on:
 *
 * - https://github.com/purplefish32/sublime-text-2-wordpress/blob/master/Snippets/Plugin_Head.sublime-snippet
 * - http://wordpress.stackexchange.com/questions/25910/uninstall-activate-deactivate-a-plugin-typical-features-how-to/25979#25979
 *
 * By default the option to uninstall the plugin is disabled,
 * to use uncomment or remove if not used.
 *
 * This Template does not have the necessary code for use in multisite. 
 *
 * Also delete this comment block is unnecessary once you have read.
 *
 * Version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

define('GEXTRACKING', __FILE__);

add_action( 'plugins_loaded', array( 'GexTracking', 'getInstance' ) );

class GexTracking {

	const ACTION = "gextracking-request";
	const INPUT_REQUEST_NAME = "gextracking_request";
	const INPUT_REQUEST_CODE = "gextracking_code";

	const OPTIONS = "gextracking";
	const OPTIONS_URL_SLUG = "gextracking";
	const OPTIONS_SAVE_ACTION = "gextracking_save_options";
	const OPTIONS_NONCE_ACTION = "gextracking_save_options";

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {
		$this->load_textdomain();

		add_shortcode( 'gextrackingform', array( $this, 'form_html' ) );
		add_shortcode( 'gextracking', array( $this, 'tracking_results_html' ) );

		if( !is_admin() || ! current_user_can('manage_options')){
			return;
		}
		
		add_action( 'admin_menu', array( $this, 'add_plugin_options_page' ) );
		add_action( 'admin_post_' . self::OPTIONS_SAVE_ACTION, array( $this, 'save_options' ) );
	}

	public function load_textdomain(){
		load_plugin_textdomain( 'gextracking', false, basename( dirname( GEXTRACKING ) ) . '/languages' );
	}

	/*************************************************
			SHORTCODES - START
	*************************************************/
	
	public function form_html(){
		ob_start();
		?>
		<form action="<?php echo esc_attr( $this->get_tracking_results_page_url() ); ?>" method="POST">
			<div class="fusion-button-wrapper fusion-alignright">
				<button class="fusion-button button-input fusion-button-round button-large button-default button-1" target="_self" title="<?php esc_attr_e("Track Your Shipment", "gextracking"); ?>" type="submit" style="display: block;">
					<span class="fusion-button-text"></span>
					<i class="fa fa-search button-icon-center"></i>
				</button>
			</div>
			<div style="width: 100%; padding-right: 88px;">
				<input style="width: 100%; font-size: 22px; height: 42px; vertical-align: middle; padding: 13px;" name="<?php echo esc_attr( self::INPUT_REQUEST_CODE ); ?>" type="text" placeholder="" value="<?php echo esc_attr( $this->tracking_request_code() ); ?>"/>
				<span style="color: #ffffff; font-size: 14px;"><?php esc_html_e("Enter tracking number", "gextracking"); ?></span>
			</div>
			<?php wp_nonce_field( self::ACTION ); ?>
			<input type="hidden" name="<?php echo esc_attr( self::INPUT_REQUEST_NAME ); ?>" value="yes">
		</form>
		<?php

		$content = ob_get_clean();

		return do_shortcode( $content );
	}

	public function tracking_results_html() {

		if( 
			/*******************  no request  *******************/
			! isset( $_POST[self::INPUT_REQUEST_NAME] ) || "yes" != $_POST[self::INPUT_REQUEST_NAME] || 
			/*******************  empty code request  *******************/
			! isset( $_POST[self::INPUT_REQUEST_CODE] ) || "" == $_POST[self::INPUT_REQUEST_CODE]
			){
			ob_start();
			?>
			<h2><?php esc_html_e("For tracking info, please provide tracking number in form above.", "gextracking"); ?></h2>
			<?php
			return ob_get_clean();
		}

		/*******************  check nonce field  *******************/
		if( ! isset( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], self::ACTION ) ){
			ob_start();
			?>
			<h2><?php esc_html_e("Oops, this should not happen. Please try again.", "gextracking"); ?></h2>
			<?php
			return ob_get_clean();	
		}

		/*******************  check API server data  *******************/
		$api_host = $this->get_options("gextracking_api_host");
		$api_key = $this->get_options("gextracking_api_key");

		if( empty( $api_host ) || empty( $api_key ) ){
			ob_start();
			?>
			<h2><?php esc_html_e("Oops, this should not happen. Missing tracking server credentials. Please try again later.", "gextracking"); ?></h2>
			<?php
			return ob_get_clean();
		}

		/*******************  prep request URL  *******************/
		$request_url = rtrim( $api_host, '/' ) . '/api/tracking/json';

		/*******************  prep request data  *******************/
		$request_args = array(
			'method' => 'POST',
			'timeout' => 10,
			'redirection' => 5,
			'httpversion' => '1.0',
			'blocking' => true,
			'headers' => array(
				'gextracking-key' => $api_key,
				),
			'body' => array(
				'lang' => defined('ICL_LANGUAGE_CODE') ? ICL_LANGUAGE_CODE : '',	// support for WPML lang code
				'code' => $_POST[self::INPUT_REQUEST_CODE],
			),
			'cookies' => array()
		);

		/*******************  remote request  *******************/
		$response = wp_remote_post( $request_url, $request_args );

		/*******************  check response error  *******************/
		if ( is_wp_error( $response ) ) {
			$error_message = $response->get_error_message();
			// var_dump($error_message, $request_url);
			// TODO - maybe log this message
			ob_start();
			?>
			<h2><?php esc_html_e("Oops, this should not happen. We have comunication problems with tracking service. Please try again later.", "gextracking"); ?></h2>
			<?php
			return ob_get_clean();
		}

		/*******************  check response code  *******************/
		if( 200 != $response['response']['code'] ){
			// TODO - maybe log this info
			// var_dump( $response['response']['code'], $response['body'] );
			ob_start();
			?>
			<h2><?php esc_html_e("Oops, this should not happen. Tracking service returned error. Please try again later.", "gextracking"); ?></h2>
			<?php
			return ob_get_clean();
		}

		/*******************  decode response body  *******************/
		$data = @json_decode( $response['body'] );

		/*******************  check data is valid  *******************/
		if( ! is_object( $data ) || ! property_exists( $data, "package" ) ){
			ob_start();
			?>
			<h2><?php esc_html_e("Oops, this should not happen. Tracking service returned unexpected message.", "gextracking"); ?></h2>
			<?php
			return ob_get_clean();
		}

		/*******************  check package not found  *******************/
		if( null == $data->package ){
			ob_start();
			?>
			<h2><?php esc_html_e("No tracking data available for searched package.", "gextracking"); ?></h2>
			<?php
			return ob_get_clean();
		}

		/*******************  set package data  *******************/
		$package = $this->get_package_from_data( $data->package );

		/*******************  load and return view  *******************/
		ob_start();
		include dirname( GEXTRACKING ) . '/views/tracking.php';
		return ob_get_clean();
	}
	
	/*************************************************
			SHORTCODES - END
	*************************************************/


	/*************************************************
			OPTIONS MENU / PAGE - START
	*************************************************/
	
	public function add_plugin_options_page(){
		add_options_page(
            __('GexTracking', 'gextracking'), 
            __('GexTracking', 'gextracking'), 
            'manage_options',
            self::OPTIONS_URL_SLUG,
            array( $this, 'options_page_html' )
        );
	}

	public function options_page_html(){
		include dirname( GEXTRACKING ) . '/views/options.php';
	}

	public function save_options(){
		// nonce and user check
		if( ! isset( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], self::OPTIONS_NONCE_ACTION ) || ! current_user_can('manage_options') ){
			return;
		}

		$new_options = $this->get_options();
		foreach ($new_options as $param_name => $val) {
			
			$post_key = $param_name;
			$post_val = isset( $_POST[$post_key] ) ? $_POST[$post_key] : '';

			$post_val = sanitize_text_field( $post_val );

			$new_options[ $param_name ] = $post_val;
		}

		update_option( self::OPTIONS, $new_options );

		wp_redirect( add_query_arg('page', self::OPTIONS_URL_SLUG, admin_url('options-general.php')) );
		exit();
	}

	public function get_options( $param_name = null ){
		$options_val = get_option( self::OPTIONS, false );

		$options = shortcode_atts( array(
			'gextracking_page' => '',
			'gextracking_api_host' => '',
			'gextracking_api_key' => '',
		), $options_val );

		if( ! is_null( $param_name ) ){
			if( isset( $options[$param_name] ) ){
				return $options[$param_name];
			}
			return null;
		}
		return $options;
	}
	
	/*************************************************
			OPTIONS MENU / PAGE - END
	*************************************************/

	/*************************************************
			RESULTS - START
	*************************************************/
	
	public function tracking_request_code(){
		return isset( $_POST[self::INPUT_REQUEST_CODE] ) ? $_POST[self::INPUT_REQUEST_CODE] : '';
	}
	
	/*************************************************
			RESULTS - END
	*************************************************/

	/*************************************************
			HELPERS - START
	*************************************************/
	
	private function get_tracking_results_page_url(){
		$tracking_results_page_id = (int)$this->get_options('gextracking_page');

		// WPML support
		if( function_exists( 'icl_object_id' ) && defined('ICL_LANGUAGE_CODE') ){
			$tracking_results_page_id = icl_object_id($tracking_results_page_id, 'page', false, ICL_LANGUAGE_CODE);
		}

		return get_permalink( $tracking_results_page_id );
	}

	private function get_package_from_data( $data ){

		$package = array();

		$fields = array( "code", "courier_code", "courier_display_name", "courier_awb" );
		foreach ($fields as $field) {
			$package[ $field ] = is_object( $data ) && property_exists( $data, $field ) && ! empty( $data->{$field} ) ? $data->{$field} : "/";
		}

		$package["trackings"] = array();
		if( is_object( $data ) && property_exists( $data, "tracking" ) && is_array( $data->tracking ) ){
			foreach ( $data->tracking as $tracking_obj ) {
				$package["trackings"][] = array(
					"date" => is_object( $tracking_obj ) && property_exists( $tracking_obj, "updated" ) && ! empty( $tracking_obj->updated ) ? $tracking_obj->updated : "/",
					"location" => is_object( $tracking_obj ) && property_exists( $tracking_obj, "location" ) && ! empty( $tracking_obj->location ) ? $tracking_obj->location : "/",
					"description" => is_object( $tracking_obj ) && property_exists( $tracking_obj, "description" ) && ! empty( $tracking_obj->description ) ? $tracking_obj->description : "",
				);
			}
		}

		return $package;
	}
	
	/*************************************************
			HELPERS - END
	*************************************************/

	public static function uninstall() {
		if ( __FILE__ != WP_UNINSTALL_PLUGIN )
			return;

		// TODO - remove credentials data
	}
}
