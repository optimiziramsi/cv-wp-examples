<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Wp_Hujsanje_Export_Strozic_Handler {


	/** @var array order IDs or customer IDs to export */
	public $ids;

	/** @var string file name for export or download */
	public $filename;


	/**
	 * Initializes the export object from an array of valid order/customer IDs and sets the filename
	 *
	 * @since 3.0
	 * @param int|array $ids orders/customer IDs to export / download
	 * @param string $export_type what is being exported, `orders` or `customers`
	 * @return \WC_Customer_Order_CSV_Export_Handler
	 */
	public function __construct( $ids, $export_type, $download_data_type ) {

		// handle single order/customer exports
		if ( ! is_array( $ids ) ) {

			$ids = array( $ids );
		}

		$this->export_type = $export_type;

		$this->download_data_type = $download_data_type;

		$this->ids = $ids;

		// set file name
		$this->filename = $this->replace_filename_variables( $this->download_data_type );

		// instantiate writer here & get CSV
		require_once dirname( __FILE__ ) . '/wp-hujsanje-export-racunovodstvo-generator.php';
		$this->generator = new Wp_Hujsanje_Export_Racunovodstvo_Generator( $this->ids, $this->download_data_type );
	}

	public function download() {

		$this->export_via( 'download' );
	}

	public function export_via( $method ) {

		// try to set unlimited script timeout
		@set_time_limit( 0 );

		try {

			// get method (download, FTP, etc)
			$export = $this->get_export_method( $method );

			if ( ! is_object( $export ) ) {

				throw new Exception( sprintf( __( 'Invalid Export Method: %s', 'wher' ), $method ) );
			}

			if ( 'orders' == $this->download_data_type ) {

				// mark each order as exported
				// this must be done before download, as the download function exits() to prevent additional output from contaminating the CSV file
				$this->mark_orders_as_exported();
			}

			$export->perform_action( $this->filename, $this->generator->content( $this->download_data_type ) );

		} catch ( Exception $e ) {

			// TODO - make custom log
			// log errors
			// wc_customer_order_csv_export()->log( $e->getMessage() );
		}
	}

	/**
	 * Returns the export method object
	 *
	 * @since 3.0
	 * @param string $method the export method, `download`, `ftp`, `http_post`, or `email`
	 * @return object the export method
	 */
	private function get_export_method( $method ) {

		// get the export method specified
		switch ( $method ) {

			case 'download':
				require_once dirname( __FILE__ ) . '/wp-hujsanje-export-racunovodstvo-method-download.php';
				return new Wp_Hujsanje_Export_Racunovodstvo_Method_Download();
			break;
		}	
	}


	/**
	 * Marks orders as exported by setting the `_wp_hujsanje_export_racunovodstvo_is_exported` order meta flag
	 *
	 * @since 3.0
	 * @param string $method the export method, `download`, `ftp`, `http_post`, or `email`
	 */
	public function mark_orders_as_exported() {

		foreach ( $this->ids as $order_id ) {

			$order_number_pip = get_post_meta( $order_id, '_pip_invoice_number', true );

			if( empty( $order_number_pip ) ){
				continue;
			}


			// add exported flag
			update_post_meta( $order_id, '_wp_hujsanje_export_racunovodstvo_is_exported', 1 );

			$order = wc_get_order( $order_id );

			$order->add_order_note( __( 'Order exported for Storžič', 'wher' ) );
		}
	}


	/**
	 * Replaces variables in file name setting (e.g. %%timestamp%% becomes 2013_03_20_16_22_14 )
	 *
	 * @since 3.0
	 * @return string filename with variables replaced
	 */
	private function replace_filename_variables( $download_data_type ) {

		$pre_replace_filename = '%%timestamp%%-' . $download_data_type . '.txt';

		switch ($download_data_type) {
			case 'clients':
				$pre_replace_filename = 'CLIENTS_order.txt';
				break;

			case 'orders':
				$pre_replace_filename = 'order.txt';
				break;

			case 'orders-items':
				$pre_replace_filename = 'POS_order.txt';
				break;
		}

		$variables   = array( '%%timestamp%%', '%%order_ids%%' );
		$replacement = array( date( 'Y_m_d_H_i_s', current_time( 'timestamp' ) ), implode( '-', $this->ids ) );

		$post_replace_filename = str_replace( $variables, $replacement, $pre_replace_filename );

		return $post_replace_filename;
	}

}