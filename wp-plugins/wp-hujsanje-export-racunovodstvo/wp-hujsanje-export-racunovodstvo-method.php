<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

interface Wp_Hujsanje_Export_Racunovodstvo_Method {

	public function perform_action( $filename, $csv );

}

