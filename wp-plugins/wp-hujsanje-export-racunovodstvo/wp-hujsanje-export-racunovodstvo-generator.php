<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Wp_Hujsanje_Export_Racunovodstvo_Generator {

	public $ids;

	public $columns;

	public $order_format;

	private $options;

	private $stream;

	private $order_item_sort_id;

	private $content_begin_line = "<BEGIN>";
	private $content_end_line = "<END>";

	public function __construct( $ids ) {
		
		$this->ids = $ids;

		$this->options = $this->get_options();

		// open output buffer to write CSV to
		$this->stream = fopen( 'php://output', 'w' );
		ob_start();
	}

	private function get_options(){

		$settings_settings = Wp_Hujsanje_Export_Strozic::get_settings_settings();

		$options_data = array();
		$option_key_prefix = 'wp_hujsanje_export_racunovodstvo_';
		$option_key_prefix_strlen = strlen( $option_key_prefix );

		foreach ($settings_settings as $settind_info ) {
			$option_key = isset( $settind_info['id'] ) ? $settind_info['id'] : null;
			$default_value = isset( $settind_info['default'] ) ? $settind_info['default'] : '';
			if( is_null( $option_key ) ){
				continue;
			}

			$option_value = get_option( $option_key, $default_value );

			$options_data_key = substr( $option_key, $option_key_prefix_strlen );

			$options_data[ $options_data_key ] = $option_value;
		}

		return $options_data;

	}

	public function content( $download_data_type ){
		switch ($download_data_type) {
			case 'clients':
				return $this->get_clients();
				break;

			case 'orders':
				return $this->get_orders();
				break;

			case 'orders-items':
				return $this->get_orders_items();
				break;
			
			default:
				// TODO - error
				break;
		}
	}

	private function get_columns( $download_data_type ){
		switch ($download_data_type) {
			case 'clients':
				return array(
					'COM_CODE',
					'COM_DISPLAY',
					'COM_NAME',
					'COM_ADDRESS',
					'CNT_CODE',
					'ZIP_ID',
					'COM_TAXNUM',
					'COM_TAXCUST',
					'COM_FINCUST',
					'COM_TYPE',
					'COM_PAYDUE',
					'COM_ACCOUNT',
				);
				break;

			case 'orders':
				return array(
					'INV_ID',
					'INV_DATE',
					'INV_MDATE',
					'CUR_ID',
					'COM_CODE',
					'INV_PAYDUE',
					'ORD_ORID',
					'ORD_ORDATE',
				);
				break;

			case 'orders-items':
				return array(
					'INV_ID',
					'INI_ZAP',
					'TAX_ID',
					'PRO_CODE',
					'INI_QUANTATY',
					'INI_PRICE',
					'INI_DISCOUNT',
					'INI_CODE',
					'INI_EAN',
					'INI_NAME',
				);
				break;
			
			default:
				# code...
				break;
		}
	}

	public function get_clients(){
		$this->write_line_content( $this->content_begin_line );

		$client_ids_added = array();

		// order columns
		$this->columns = $this->get_columns( 'clients' );

		// iterate through order IDs
		foreach ( $this->ids as $order_id ) {
			// get data for each order
			$order = $this->get_order( $order_id );
			$order_number_pip = $order['order_number_pip'];
			if( empty( $order_number_pip ) ){
				continue;
			}
			$customer = $order['customer'];
			$customer_id = $customer['id'];
			if( in_array( $customer_id, $client_ids_added ) ){
				continue;
			}
			$client_ids_added[] = $customer_id;

			$this->write_line( $order );
		}

		$this->write_line_content( $this->content_end_line );

		return $this->get_content();
	}

	public function get_orders() {
		$this->write_line_content( $this->content_begin_line );

		// order columns
		$this->columns = $this->get_columns( 'orders' );

		// iterate through order IDs
		foreach ( $this->ids as $order_id ) {
			// get data for each order
			$order = $this->get_order( $order_id );

			$order_number_pip = $order['order_number_pip'];
			if( empty( $order_number_pip ) ){
				continue;
			}

			$this->write_line( $order );
		}

		$this->write_line_content( $this->content_end_line );

		return $this->get_content();
	}

	public function get_orders_items() {
		$this->write_line_content( $this->content_begin_line );

		// order columns
		$this->columns = $this->get_columns( 'orders-items' );

		// iterate through order IDs
		foreach ( $this->ids as $order_id ) {
			// get data for each order
			$order = $this->get_order( $order_id );

			$order_number_pip = $order['order_number_pip'];
			if( empty( $order_number_pip ) ){
				continue;
			}

			$this->order_item_sort_id = 1;
			foreach ( $order['line_items'] as $order_item ) {
				$this->write_line( $order, $order_item );
			}
		}

		$this->write_line_content( $this->content_end_line );

		return $this->get_content();
	}






	private function get_order( $order_id ) {

		$order = wc_get_order( $order_id );

		$line_items = $shipping_items = $fee_items = $tax_items = $coupon_items = array();

		// get line items
		foreach ( $order->get_items() as $item_id => $item ) {

			$product = $order->get_product_from_item( $item );

			if ( ! is_object( $product ) ) {
				$product = new WC_Product( 0 );
			}

			$item_meta = new WC_Order_Item_Meta( SV_WC_Plugin_Compatibility::is_wc_version_gte_2_4() ? $item : $item['item_meta'] );
			$meta = $item_meta->display( true, true );

			if ( $meta ) {

				// remove newlines
				$meta = str_replace( array( "\r", "\r\n", "\n" ), '', $meta );

				// switch reserved chars (:;|) to =
				$meta = str_replace( array( ': ', ':', ';', '|' ), '=', $meta );
			}

			$line_item = array(
				'id'       => $product->id,
				'name'     => html_entity_decode( $product->get_title() ? $product->get_title() : $item['name'], ENT_NOQUOTES, 'UTF-8' ),
				'sku'      => $product->get_sku(),
				'quantity' => $item['qty'],
				'total'    => wc_format_decimal( $order->get_line_total( $item ), 2 ),
				'refunded' => wc_format_decimal( $order->get_total_refunded_for_item( $item_id ), 2 ),
				'meta'     => html_entity_decode( $meta, ENT_NOQUOTES, 'UTF-8' ),
			);

			// add line item tax
			$line_tax_data    = isset( $item['line_tax_data'] ) ? $item['line_tax_data'] : array();
			$tax_data         = maybe_unserialize( $line_tax_data );
			$line_item['tax'] = isset( $tax_data['total'] ) ? wc_format_decimal( wc_round_tax_total( array_sum( (array) $tax_data['total'] ) ), 2 ) : '';

			// $line_item = apply_filters( 'wc_customer_order_csv_export_order_line_item', $line_item, $item, $product, $order, $this );
			
			$line_items[] = $line_item;
		}

		foreach ( $order->get_shipping_methods() as $_ => $shipping_item ) {

			$shipping_items[] = array(
				'method' => $shipping_item['name'],
				'total' => wc_format_decimal( $shipping_item['cost'], 2 ),
			);
		}

		// get fee items & total
		$fee_total = 0;
		$fee_tax_total = 0;

		foreach ( $order->get_fees() as $fee_id => $fee ) {

			$fee_items[] = array(
				'name' => $fee['name'],
				'total' => wc_format_decimal( $fee['line_total'], 2 ),
				'tax' => wc_format_decimal( $fee['line_tax'], 2 ),
			);

			$fee_total     += $fee['line_total'];
			$fee_tax_total += $fee['line_tax'];
		}

		// get tax items
		foreach ( $order->get_tax_totals() as $tax_code => $tax ) {

			$tax_items[] = array(
				'code' => $tax_code,
				'total' => wc_format_decimal( $tax->amount, 2 ),
			);
		}

		// add coupons
		foreach ( $order->get_items( 'coupon' ) as $_ => $coupon_item ) {

			$coupon = new WC_Coupon( $coupon_item['name'] );

			$coupon_post = get_post( $coupon->id );

			$coupon_items[] = implode( '|', array(
				'code:' . $coupon_item['name'],
				'description:' . ( is_object( $coupon_post ) ? $coupon_post->post_excerpt : '' ),
				'amount:' . wc_format_decimal( $coupon_item['discount_amount'], 2 ),
			) );
		}

		$order_data = array(
			'order_id'             => $order->id,
			'order_number'         => $order->get_order_number(),
			'order_number_pip'     => get_post_meta( $order->id, '_pip_invoice_number', true ),
			'order_date'           => $order->order_date,
			'status'               => $order->get_status(),
			'shipping_total'       => $order->get_total_shipping(),
			'shipping_tax_total'   => wc_format_decimal( $order->get_shipping_tax(), 2 ),
			'fee_total'            => wc_format_decimal( $fee_total, 2 ),
			'fee_tax_total'        => wc_format_decimal( $fee_tax_total, 2 ),
			'tax_total'            => wc_format_decimal( $order->get_total_tax(), 2 ),
			'cart_discount'        => SV_WC_Plugin_Compatibility::is_wc_version_gte_2_3() ? wc_format_decimal( $order->get_total_discount(), 2 ) : wc_format_decimal( $order->get_cart_discount(), 2 ),
			'order_discount'       => SV_WC_Plugin_Compatibility::is_wc_version_gte_2_3() ? wc_format_decimal( $order->get_total_discount(), 2 ) : wc_format_decimal( $order->get_order_discount(), 2 ),
			'discount_total'       => wc_format_decimal( $order->get_total_discount(), 2 ),
			'order_total'          => wc_format_decimal( $order->get_total(), 2 ),
			'refunded_total'       => wc_format_decimal( $order->get_total_refunded(), 2 ),
			'order_currency'       => $order->get_order_currency(),
			'payment_method'       => $order->payment_method,
			'shipping_method'      => $order->get_shipping_method(),
			'customer'             => $this->get_customer( $order->billing_email, $order->id ),
			'billing_first_name'   => $order->billing_first_name,
			'billing_last_name'    => $order->billing_last_name,
			'billing_company'      => $order->billing_company,
			'billing_email'        => $order->billing_email,
			'billing_phone'        => $order->billing_phone,
			'billing_address_1'    => $order->billing_address_1,
			'billing_address_2'    => $order->billing_address_2,
			'billing_postcode'     => $order->billing_postcode,
			'billing_city'         => $order->billing_city,
			'billing_state'        => $order->billing_state,
			'billing_country'      => $order->billing_country,
			'shipping_first_name'  => $order->shipping_first_name,
			'shipping_last_name'   => $order->shipping_last_name,
			'shipping_company'     => $order->shipping_company,
			'shipping_address_1'   => $order->shipping_address_1,
			'shipping_address_2'   => $order->shipping_address_2,
			'shipping_postcode'    => $order->shipping_postcode,
			'shipping_city'        => $order->shipping_city,
			'shipping_state'       => $order->shipping_state,
			'shipping_country'     => $order->shipping_country,
			'customer_note'        => $order->customer_note,
			'shipping_items'       => $shipping_items,
			'fee_items'            => $fee_items,
			'tax_items'            => $tax_items,
			'coupon_items'         => $coupon_items,
			'order_notes'          => $this->get_order_notes( $order ),
			'download_permissions' => $order->download_permissions_granted ? $order->download_permissions_granted : 0,
		);
		
		$order_data['line_items'] = $line_items;
		

		return $order_data;
	}

	private function get_order_notes( $order ) {

		$callback = array( 'WC_Comments', 'exclude_order_comments' );

		$args = array(
			'post_id' => $order->id,
			'approve' => 'approve',
			'type'    => 'order_note'
		);

		remove_filter( 'comments_clauses', $callback );

		$notes = get_comments( $args );

		add_filter( 'comments_clauses', $callback );

		$order_notes = array();

		foreach ( $notes as $note ) {

			$order_notes[] = str_replace( array( "\r", "\n" ), ' ', $note->comment_content );
		}

		return $order_notes;
	}

	private function get_customer( $customer_email, $order_id ) {

		// za kupca pri izvozu vedno uporabljamo nastavitve iz settings, zato ne vračamo pravih podatkov, ampak samo ID, ki je definiran v nastavitvah
		return array(
			'id' => (int)$this->options['COM_CODE'],
		);

		$user = get_user_by( 'email', $customer_email );

		// guest, get info from order
		if ( ! $user && is_numeric( $order_id ) ) {

			$order = wc_get_order( $order_id );

			// create blank user
			$user = new stdClass();

			// set properties on user
			$user->ID                  = 1;
			$user->first_name          = 'Ime neznano';
			$user->last_name           = 'Priimek neznan';
			$user->user_email          = '';
			$user->user_registered     = date('Y-m-d H:i');
			$user->billing_first_name  = 'Ime neznano';
			$user->billing_last_name   = 'Priimek neznan';
			$user->billing_company     = '';
			$user->billing_email       = '';
			$user->billing_phone       = '';
			$user->billing_address_1   = '';
			$user->billing_address_2   = '';
			$user->billing_postcode    = '';
			$user->billing_city        = '';
			$user->billing_state       = '';
			$user->billing_country     = '';
			$user->shipping_first_name = '';
			$user->shipping_last_name  = '';
			$user->shipping_company    = '';
			$user->shipping_address_1  = '';
			$user->shipping_address_2  = '';
			$user->shipping_postcode   = '';
			$user->shipping_city       = '';
			$user->shipping_state      = '';
			$user->shipping_country    = '';
		}

		$customer_data = array(
			'id'         => $user->ID,
			'first_name'          => $user->first_name,
			'last_name'           => $user->last_name,
			'email'               => $user->user_email,
			'date_registered'     => $user->user_registered,
			'billing_first_name'  => $user->billing_first_name,
			'billing_last_name'   => $user->billing_last_name,
			'billing_company'     => $user->billing_company,
			'billing_email'       => $user->billing_email,
			'billing_phone'       => $user->billing_phone,
			'billing_address_1'   => $user->billing_address_1,
			'billing_address_2'   => $user->billing_address_2,
			'billing_postcode'    => $user->billing_postcode,
			'billing_city'        => $user->billing_city,
			'billing_state'       => $user->billing_state,
			'billing_country'     => $user->billing_country,
			'shipping_first_name' => $user->shipping_first_name,
			'shipping_last_name'  => $user->shipping_last_name,
			'shipping_company'    => $user->shipping_company,
			'shipping_address_1'  => $user->shipping_address_1,
			'shipping_address_2'  => $user->shipping_address_2,
			'shipping_postcode'   => $user->shipping_postcode,
			'shipping_city'       => $user->shipping_city,
			'shipping_state'      => $user->shipping_state,
			'shipping_country'    => $user->shipping_country,
		);

		return $customer_data;
	}

	private function write_line_content( $content ){
		fputs( $this->stream, $content . PHP_EOL );
	}

	private function write_line( $order, $order_item = null ) {

		$data = array();

		foreach ( $this->columns as $column_key ) {

			$column_value = $this->format_column( $column_key, $order, $order_item );

			$data[] = $column_value;
		}

		fputs( $this->stream, implode("", $data ) . PHP_EOL );
	}


	
	private function get_content() {

		$content = ob_get_clean();

		fclose( $this->stream );

		return $content;
	}



	private function format_column( $column_key, $order, $order_item = null ){
		switch ( $column_key ) {
			case 'INV_ID':	// Številka raèuna (èe je prvi znak ?, potem ponovno številèi), poravnano desno z vodilnimi nièlami / (najbolje je, èe številka vsebuje letnico npr.15000001, 15000002...)
			case 'ORD_ORID':	// Originalna številka naroèila (lahko ponovi številka raèuna)
				$strlen = 8;
				$prefix = date( 'y', strtotime( $order['order_date'] ) ) . substr( $this->options['INV_ID_COUNTRY_ID'], 0, 1);

				$order_number = '';
				$order_number_pip = $order['order_number_pip'];
				$order_number_pip_splitted = explode( "-", $order_number_pip, 2);
				if( 2 == count( $order_number_pip_splitted ) ){
					$order_number = (int)$order_number_pip_splitted[0] . '';
				}
				$num_spaces_missing = $strlen - strlen( $prefix ) - strlen( $order_number );
				$inv_id = $prefix . str_repeat('0', $num_spaces_missing) . $order_number;

				if( 'ORD_ORID' == $column_key ){
					$ord_orig_strlen = 20;
					$ord_orig_num_spaces_missing = $ord_orig_strlen - strlen( $inv_id );
					$inv_id = str_repeat(' ', $ord_orig_num_spaces_missing) . $inv_id;
				}

				return $inv_id;
				break;

			case 'INV_DATE':	// Datum raèuna
			case 'INV_MDATE':	// Datum opravljene storitve
			case 'ORD_ORDATE':	// Datum naroèila (lahko ponovi datum racuna )
				// strlen = 8
				return date( 'Ymd', strtotime( $order['order_date'] ) );
				break;

			case 'CUR_ID':	// Šifra valute - potrebno uskladiti šifrante (za EUR naj bo kar 01)  
				$strlen = 2;
				$currency_value = substr( $this->options['CUR_ID'], 0, 2);
				$num_spaces_missing = $strlen - strlen( $currency_value );
				return str_repeat('0', $num_spaces_missing) . $currency_value;
				break;

			case 'COM_CODE':	// šifra stranke
				$strlen = 12;
				$customer_id = substr( $this->options['COM_CODE'], 0, $strlen);
				$num_spaces_missing = $strlen - strlen( $customer_id );
				return str_repeat( ' ', $num_spaces_missing ) . $customer_id;
				return;

			case 'INV_PAYDUE':	// rok plačila definiran s št dni, jaz sem nastavil 7 dni
				$strlen = 3;
				$value = substr( $this->options['COM_PAYDUE'], 0, $strlen );
				$num_spaces_missing = $strlen - strlen( $value );
				return str_repeat( '0', $num_spaces_missing ) . $value;
				break;

			case 'INI_ZAP':
				$strlen = 4;
				$order_item_sort_id = $this->order_item_sort_id . '';
				$this->order_item_sort_id++;
				$num_spaces_left = $strlen - strlen( $order_item_sort_id );
				return str_repeat('0', $num_spaces_left) . $order_item_sort_id;
				break;

			case 'TAX_ID':
				$strlen = 2;
				$paydue_value = substr( $this->options['TAX_ID'], 0, $strlen);
				$num_spaces_missing = $strlen - strlen( $paydue_value );
				return str_repeat( '0', $num_spaces_missing ) . $paydue_value;
				break;

			case 'PRO_CODE':
			case 'INI_CODE':
				// izpisujemo kar id produkta
				// TODO - preveri če je želja po čem drugem
				$strlen = 18;
				$value = $order_item['id'] . '';
				$num_spaces_left = $strlen - strlen( $value );;
				return $value . str_repeat(' ', $num_spaces_left);
				break;

			case 'INI_QUANTATY':
				$strlen = 14;
				$value = number_format((float)$order_item['quantity'], 3, ',', '');
				$num_spaces_left = $strlen - strlen( $value );
				return str_repeat( ' ', $num_spaces_left ) . $value;
				break;

			case 'INI_PRICE':
				$strlen = 14;
				$value = number_format((float)$order_item['total'], 3, ',', '');
				$num_spaces_left = $strlen - strlen( $value );
				return str_repeat( ' ', $num_spaces_left ) . $value;
				break;

			case 'INI_DISCOUNT':
				// TODO - povsod smo dali da popusta ni, preveri če je to ok
				$strlen = 6;
				$discount = 0;
				$value = number_format((float)$discount, 3, ',', '');
				$num_spaces_left = $strlen - strlen( $value );
				return str_repeat( '0', $num_spaces_left ) . $value;
				break;

			case 'INI_EAN':
				// izpisujemo kar id produkta
				// TODO - preveri če je želja po čem drugem
				$strlen = 13;
				$value = $order_item['id'];
				$num_spaces_left = $strlen - strlen( $value );
				return $value . str_repeat(' ', $num_spaces_left);
				break;

			case 'INI_NAME':
				$strlen = 80;
				$name = $this->mb_convert_encoding( $order_item['name'] );
				$name_strlen = strlen( $name );
				if( $strlen >= $name_strlen ){
					$value = $name . str_repeat( ' ', $strlen - $name_strlen );
				} else {
					$value = substr( $name, 0, $strlen );
				}
				return $value;
				break;

			case 'COM_DISPLAY':
				$strlen = 20;
				$value = substr( $this->options['COM_DISPLAY'], 0, $strlen );
				$num_spaces_missing = $strlen - strlen( $value );
				return $value . str_repeat( ' ', $num_spaces_missing );
				break;

			case 'COM_NAME':
				$strlen = 80;
				$value = substr( $this->options['COM_NAME'], 0, $strlen );
				$num_spaces_missing = $strlen - strlen( $value );
				return $value . str_repeat( ' ', $num_spaces_missing );
				break;

			case 'COM_ADDRESS':
				$strlen = 50;
				$value = substr( $this->options['COM_ADDRESS'], 0, $strlen );
				$num_spaces_missing = $strlen - strlen( $value );
				return $value . str_repeat( ' ', $num_spaces_missing );
				break;

			case 'CNT_CODE':
				$strlen = 3;
				$value = substr( $this->options['CNT_CODE'], 0, $strlen );
				$num_spaces_missing = $strlen - strlen( $value );
				return str_repeat( '0', $num_spaces_missing ) . $value;
				break;

			case 'ZIP_ID':
				$strlen = 10;
				$value = substr( $this->options['ZIP_ID'], 0, $strlen );
				$num_spaces_missing = $strlen - strlen( $value );
				return $value . str_repeat( ' ', $num_spaces_missing );
				break;

			case 'COM_TAXNUM':
				$strlen = 15;
				$value = substr( $this->options['COM_TAXNUM'], 0, $strlen );
				$num_spaces_missing = $strlen - strlen( $value );
				return $value . str_repeat( ' ', $num_spaces_missing );
				break;

			case 'COM_TAXCUST':
				$strlen = 1;
				$value = substr( $this->options['COM_TAXCUST'], 0, $strlen );
				$num_spaces_missing = $strlen - strlen( $value );
				return $value . str_repeat( ' ', $num_spaces_missing );
				break;

			case 'COM_FINCUST':
				$strlen = 1;
				$value = substr( $this->options['COM_FINCUST'], 0, $strlen );
				$num_spaces_missing = $strlen - strlen( $value );
				return $value . str_repeat( ' ', $num_spaces_missing );
				break;

			case 'COM_TYPE':
				$strlen = 1;
				$value = substr( $this->options['COM_TYPE'], 0, $strlen );
				$num_spaces_missing = $strlen - strlen( $value );
				return $value . str_repeat( ' ', $num_spaces_missing );
				break;

			case 'COM_PAYDUE':
				$strlen = 3;
				$value = substr( $this->options['COM_PAYDUE'], 0, $strlen );
				$num_spaces_missing = $strlen - strlen( $value );
				return str_repeat( '0', $num_spaces_missing ) . $value;
				break;

			case 'COM_ACCOUNT':
				$strlen = 50;
				$value = substr( $this->options['COM_ACCOUNT'], 0, $strlen );
				$num_spaces_missing = $strlen - strlen( $value );
				return $value . str_repeat( ' ', $num_spaces_missing );
				break;
			
			default:
				return " KEY UNKNOWN $column_key ";
				break;
		}
	}

	private function mb_convert_encoding( $content ){
		return $content; //mb_convert_encoding( htmlspecialchars_decode( $content ), 'HTML-ENTITIES', 'UTF-8');
	}

}
