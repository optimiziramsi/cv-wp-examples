<?php

class DgBni1na1_Shortcode {


	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	

	private function __construct() {

		add_shortcode( 'bni_1na1', array( $this, 'process' ) );
		
	}

	function process() {
		ob_start();
		?>
		<div class="container bni-1-na-1-holder">
			<?php echo $this->get_content(); ?>
		</div>
		<?php
		return ob_get_clean();
	}

	function get_content(){
		if( $this->isMeetup() ){
			return $this->meetup_view();
		}

		if( $this->userCanEdit() ){
			return $this->edit_view();
		}

		return $this->login_view();
	}

	/*************************************************
			MEETUP - START
	*************************************************/
	
	private function isMeetup(){
		// TODO
		return false;
	}

	private function meetup_view(){
		ob_start();
		include dirname( BNI_1na1_FILE ) . '/views/meetup.php';
		return ob_get_clean();
	}
	
	/*************************************************
			MEETUP - END
	*************************************************/



	/*************************************************
			EDIT - START
	*************************************************/
	
	private function userCanEdit(){
		
		if( is_user_logged_in() ){
			return true;
		}

		return false;
	}

	private function edit_view(){
		ob_start();
		include dirname( BNI_1na1_FILE ) . '/views/edit.php';
		return ob_get_clean();
	}

	private function get_week_table(){
		ob_start();
		$days = array( 'Pon', 'Tor', 'Sre', 'Čet', 'Pet' );

		$hours_starts = 7;
		$hours_ends = 19;

		$curr_row_hour = $hours_starts;

		?>
		<table>
			<tr>
				<th><!-- stolpec za uro --></th>

				<?php foreach ( $days as $day ) { ?>
				<th><?php echo esc_html( $day ); ?></th>
				<?php } ?>
			</tr>
			<?php for ($i=$hours_starts; $i <= $hours_ends; $i++ ) { ?>
				<tr>
					<td> <?php echo $i; ?></td>

					<?php echo str_repeat( "<td></td>", count( $days ) ); ?>
				</tr>
				<tr>
					<td>&nbsp;</td>

					<?php echo str_repeat( '<td class="meeting-hover"></td>', count( $days ) ); ?>
				</tr>
			<?php } ?>
		</table>
		<?php
		return ob_get_clean();
	}

	/*************************************************
			EDIT - END
	*************************************************/


	/*************************************************
			LOGIN - START
	*************************************************/
	
	private function login_view(){
		ob_start();
		include dirname( BNI_1na1_FILE ) . '/views/login.php';
		return ob_get_clean();
	}
	
	/*************************************************
			LOGIN - END
	*************************************************/


}