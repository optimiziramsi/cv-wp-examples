(function($){
	var
		$bni,
		$table,
		$t_templates,
		$t_add_meeting_item
	;

	function init(){
		$bni = $(".bni-1-na-1-holder");
		if( ! $bni.length ){
			return;
		}

		t_init();
	}

	/*************************************************
			WEEK TABLE - START
	*************************************************/
	
	function t_init(){
		$table = $bni.find(".week-holder table");
		$t_templates = $bni.find(".week-holder .table-templates");

		// add event on week
		$bni.find( '.table-holer' ).hover( t_add_mouse_move_listener, t_remove_mouse_move_listener );

	}

	function t_add_mouse_move_listener(){
		t_remove_mouse_move_listener();
		$bni.find( '.table-holer' ).mousemove( t_check_mouse_over_td_and_show_add_meeting );
	}

	function t_remove_mouse_move_listener(){
		$bni.find( '.table-holer' ).unbind( 'mousemove' );
	}

	function t_check_mouse_over_td_and_show_add_meeting( event ){

		console.log( "t_check_mouse_over_td_and_show_add_meeting", event );
	}












	var hover_over_add_meeting_item = false;
	function t_hover_over( event ){
		var $td = $( event.currentTarget );
		var td_pos = $td.position();
		var rect = {
			top: td_pos.top,
			left: td_pos.left,
			width: $td.outerWidth(),
			height: 200,
		};

		console.log( $td.position() );
		console.log( $td.offset() );


		var $item = t_get_add_meeting_item();

		$item.hover( function(){ hover_over_add_meeting_item = true; }, function(){ hover_over_add_meeting_item = false; } );

		$item.css({top: rect.top, left: rect.left});
		$item.width( rect.width );
		$item.height( rect.height );

	}

	function t_hover_out( event ){
		t_hover_out_handle();
	}

	var t_hover_out_handle_timeout;
	function t_hover_out_handle( now ){
		clearTimeout( t_hover_out_handle_timeout );
		if( ! now ){
			t_hover_out_handle_timeout = setTimeout(function(){
				t_hover_out_handle( true );
			}, 50);
			return;
		}

		if( hover_over_add_meeting_item ){
			return;
		}

		t_remove_add_meeting_item();
	}

	function t_get_add_meeting_item(){
		if( ! $t_add_meeting_item ){
			$t_add_meeting_item = $t_templates.find(".add-meeting-item").clone();
			$bni.find( '.table-holer' ).append( $t_add_meeting_item );
		}

		return $t_add_meeting_item;
	}

	function t_remove_add_meeting_item(){
		if( $t_add_meeting_item ){
			$t_add_meeting_item.remove();
			$t_add_meeting_item = false;
		}

		hover_over_add_meeting_item = false;
	}
	
	/*************************************************
			WEEK TABLE - END
	*************************************************/


	$(document).ready( init );
})(jQuery);