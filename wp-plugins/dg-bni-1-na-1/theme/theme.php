<?php

class DgBni1na1_Theme {


	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}


	private function __construct() {

		add_action('after_setup_theme', array( $this, 'remove_admin_bar' ) );

		add_action( 'get_header', array( $this, 'genesis_init' ) );
		
	}

	function genesis_init(){

		remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

		// TODO - spremeni footer vsebino
		
	}

	function remove_admin_bar() {
		if ( !current_user_can('administrator') && !is_admin() ) {
			show_admin_bar(false);
		}
	}


}




