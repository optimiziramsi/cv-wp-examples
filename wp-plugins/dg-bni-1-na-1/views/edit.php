<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

?>

<?php
/******************************************************************
*
*       USER ROW
*
******************************************************************/
?>
<div class="user-row alert alert-info">
	<div class="user-avatar">
		<?php echo get_avatar( get_current_user_id(), 64 ); ?>
	</div>
	<a href="<?php echo wp_logout_url( home_url() ); ?>">Odjavi se</a>
</div>


<?php
/******************************************************************
*
*       MAIN CONTENT
*
******************************************************************/
?>
<div>
	
	<div class="info-holder">
		<?php /*
		<div class="alert alert-warning">
			Opozorilo
		</div>
		<div class="alert alert-success">
			Shranjeno ...
		</div>
		<div class="alert alert-info">
			Prenašam podatke ...
		</div>
		<div class="alert alert-danger">
			Napake ...
		</div>
		*/ ?>
	</div>

	<?php
	/******************************************************************
	*
	*       WEEK
	*
	******************************************************************/
	?>
	<div class="three-fourths first week-holder">
		<div class="table-holer">
			<?php echo $this->get_week_table(); ?>
		</div>

		<div class="table-templates">
			<!-- add new meeting -->
			<div class="add-meeting-item">
				PLUS
			</div>
		</div>
	</div>





	<?php
	/******************************************************************
	*
	*       MEMBERS
	*
	******************************************************************/
	?>
	<div class="one-fourth">
		MEMBERS LIST
	</div>
</div>



<?php
/******************************************************************
*
*       HISTORY / LOG
*
******************************************************************/
?>
<div class="log">
	<!-- TODO -->
</div>