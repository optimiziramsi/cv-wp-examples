<?php

class EusprSpanExtra_ScrollToMe {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		// enqueue javascript
		add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ) );

	}

	function wp_enqueue_scripts(){
		wp_enqueue_script( 'ese-scroll-to-me', plugin_dir_url( __FILE__ ) . '/js/scroll-to-me.js', array('jquery'), ESE_VERSION );
	}

}

