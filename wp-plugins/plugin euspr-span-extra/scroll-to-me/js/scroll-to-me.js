(function($){
	function init(){
		var $stm = $("[data-scroll-to-me]:first");
		if( $stm.length ){
			slideToElement( $stm );
		}
	}
	function slideToElement( $element ){
		var offset = $element.offset().top + 1;

		$('html, body').animate({ scrollTop: offset - 200 }, {duration: 600, queue: false, easing: 'easeOutCubic'});
	}
	$(document).ready(init);
})(jQuery);