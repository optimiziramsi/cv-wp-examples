<?php



class EusprSpanExtra_GoogleRecaptcha {

	private static $instance = null;

	private $render_ids = array();

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		$config = $this->config();
		
		// if( ! $config['enabled'] ){
		// 	return;
		// }
		
		// register JS
		add_action( 'wp_enqueue_scripts', array( $this, 'register_scripts' ) );
		// add_action( 'wp_print_scripts', array( $this, 'print_inline_script' ), 10, 2 );
		add_action( 'wp_print_footer_scripts', array( $this, 'print_inline_script' ), 10, 2 );

		// enqueue JS on login
		add_action( 'login_footer', array( $this, 'enqueue_scripts' ) );

		// login form
		add_action( 'login_form', array( $this, 'login_render' ) );
		add_action( 'authenticate', array( $this, 'login_check' ), 9, 3 );

		// reset psw
		add_action( 'lostpassword_form', array( $this, 'login_render' ) );
		add_action( 'lostpassword_post', array( $this, 'lostpassword_check' ) );

		// registration form
		add_action( 'register_form', array( $this, 'login_render' ) );
		add_action( 'register_post', array( $this, 'lostpassword_check' ) );

		// comments
		add_action( 'comment_form_after_fields', array( $this, 'commentform_display' ) );
		add_action( 'comment_form_logged_in_after', array( $this, 'commentform_display' ) );
		add_action( 'pre_comment_on_post', array( $this, 'commentform_check' ) );
	}

	public function login_render(){
		?>
		<style type="text/css">
			#loginform,
			#lostpasswordform,
			#registerform {
				width: 302px !important;
			}
			#login_error,
			.message {
				width: 322px !important;
			}
			#loginform .g-recaptcha,
			#lostpasswordform .g-recaptcha,
			#registerform .g-recaptcha {
				margin-bottom: 10px;
			}
		</style>
		<?php
		$this->render( true );
		return true;
	}

	public function commentform_display(){
		if ( is_user_logged_in() )
			return;
		$this->render( true );
		return true;
	}

	public function login_check( $user, $username, $password ){
		$check = $this->check();

		if ( 'POST' == $_SERVER['REQUEST_METHOD'] && ! $check ) {
			wp_clear_auth_cookie();
			return new WP_Error( 'ese_recaptcha_error', __( 'You have entered an incorrect reCAPTCHA value.', 'ese' ) );
		} else {
			return $user;
		}
	}

	public function lostpassword_check(){
		$check = $this->check();

		if ( ! $check ) {
			$error_message = sprintf( '%s: %s', __( 'Error', 'ese' ), __( 'You have entered an incorrect reCAPTCHA value. Click the BACK button on your browser, and try again.', 'ese' ) );
			wp_die( $error_message );
			return $error;
		} else {
			return;
		}
	}

	public function commentform_check(){
		if ( is_user_logged_in() ) {
			return;
		}
		$this->lostpassword_check();
	}

	public function render( $echo = false ){
		$config = $this->config();
		$render_id = 'ese-recaptcha-' . uniqid();
		$this->render_ids[] = $render_id;
		$content = '<div id="'.esc_attr( $render_id ).'" class="g-recaptcha" data-sitekey="'.esc_attr( $config['site_key'] ).'"></div>';

		$this->enqueue_scripts();

		if( $echo ){
			echo $content;
		} else {
			return $content;
		}
	}

	public function check(){
		$g_recaptcha_response = isset( $_POST["g-recaptcha-response"] ) ? stripslashes( esc_html( $_POST["g-recaptcha-response"] ) ) : false;
		$remote_addr = filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP );

		if( ! $g_recaptcha_response ){
			return false;
		}

		$config = $this->config();

		require_once dirname( dirname( __FILE__ ) ) . "/libs/recaptchalib.php";

		$reCaptcha = new ese_ReCaptcha( $config['secret'] );

		$resp = $reCaptcha->verifyResponse(
			$remote_addr,
			$g_recaptcha_response
		);

		if( $resp != null && $resp->success ){
			return true;
		}

		return false;
	}

	private $scripts_registered = false;
	public function register_scripts(){
		if( $this->scripts_registered ){
			return;
		}
		$config = $this->config();
		wp_register_script( 'ese-google-recaptcha', 'https://www.google.com/recaptcha/api.js?onload=ese_google_recaptcha_callback&render=explicit&hl=' . $config['language'], false, null, true );
		$this->scripts_registered = true;
	}

	public function enqueue_scripts(){
		$this->register_scripts();

		wp_enqueue_script( 'ese-google-recaptcha' );
	}

	public function print_inline_script( $handle ) {
		if ( true || wp_script_is( 'ese-google-recaptcha', 'done' ) ) {
			$config = $this->config();
		?>
			<script type="text/javascript">
				var ese_google_recaptcha_callback = function(){
					<?php foreach ( $this->render_ids as $render_id ) { ?>
					if( document.getElementById(<?php echo json_encode( $render_id ); ?>) ){
						grecaptcha.render(<?php echo json_encode( $render_id ); ?>, {'sitekey' : <?php echo json_encode( $config['site_key'] ); ?> });
					}
					<?php } ?>
				};
			</script>
		<?php
		}
	}

	private function config(){
		$sett = EusprSpanExtra_SettingsPage::getInstance();
		
		// return array(
		// 	'enabled' => true,
		// 	'language' => 'en',
		// 	'secret' => '6LcS7xgTAAAAAAuRaqqle8O50zH8Wd3CB6K2Vd_a',
		// 	'site_key' => '6LcS7xgTAAAAAPzcfYcQCdO8nI0RTtGlrKna8fa_',
		// );
		
		return array(
			// 'enabled' => $sett->param('google_recaptcha_enabled'),
			'language' => $sett->param('google_recaptcha_language'),
			'secret' => $sett->param('google_recaptcha_secret'),
			'site_key' => $sett->param('google_recaptcha_site_key'),
		);
	}

}