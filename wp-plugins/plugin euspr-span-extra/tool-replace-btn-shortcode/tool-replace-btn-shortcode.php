<?php

class EusprSpanExtra_ToolReplaceBtnShortcode {

	const OPTION_REPLACED = "ese_trbs_replaced";

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		if( ! is_admin() || ! current_user_can( 'manage_options' ) ){
			return;
		}

		// create submenu page
		add_action( 'admin_menu', array( $this, 'addSubmenuToToolsMenu' ) );

	}

	function addSubmenuToToolsMenu(){
		// create submenu under TOOLS menu
		add_submenu_page(
			'tools.php',
			__( 'Replace BTN shortcode', 'ese' ),
			__( 'Replace BTN shortcode', 'ese' ),
			'manage_options',
			'ese-tool-replace-btn-shortcodes',
			array( $this, 'renderPage' )
		);
	}

	function renderPage(){
		$data = $this->processRequest();

		include dirname( __FILE__ ) . '/views/tool.php';
	}

	private function processRequest(){
		$data = array(
			'state' => '',
			'errors' => array(),
			'messages' => array(),
		);

		$data['state'] = '';


		// DEVELOP
		// $data['state'] = 'scan_report';
		// $data['scan_report'] = $this->scanContents( $this->scanContentsQuery() );
		// return $data;


		if( isset( $_POST['action'] ) && "ese-replace-btn-shortcodes-scan" == $_POST['action'] ){

			$data['state'] = 'scan_report';

			if( ! isset( $_POST['ese-replace-btn-shortcodes-scan-nonce'] ) || ! wp_verify_nonce( $_POST['ese-replace-btn-shortcodes-scan-nonce'], 'ese-replace-btn-shortcodes-scan' ) ){
				$data['errors'][] = __( 'Request not ok. Please retry using this tool the way it is ment to be used.', 'ese' );
				$data['state'] = '';

				return $data;
			}

			$data['scan_report'] = $this->scanContents( $this->scanContentsQuery() );

			return $data;
		}




		if( isset( $_POST['action'] ) && "ese-replace-btn-shortcodes-process" == $_POST['action'] ){

			$data['state'] = 'process_report';

			if( ! isset( $_POST['ese-replace-btn-shortcodes-process-nonce'] ) || ! wp_verify_nonce( $_POST['ese-replace-btn-shortcodes-process-nonce'], 'ese-replace-btn-shortcodes-process' ) ){
				$data['errors'][] = __( 'Request not ok. Please retry using this tool the way it is ment to be used.', 'ese' );
				$data['state'] = '';

				return $data;
			}

			$data['process_report'] = $this->processContents( $this->scanContentsQuery() );
			
			return $data;
		}

		$data['replaced'] = $this->get_replaced();

		return $data;
	}


	private function scanContents( $query ){

		$contents = array();

		foreach ( $query->posts as $post ) {
			if( ! has_shortcode( $post->post_content, 'btn' ) ){
				continue;
			}

			$new_content = preg_replace_callback("/\[(\w+) (.+?)]/", array( $this, "processShortCode" ), $post->post_content);
			$new_content = str_replace('[/btn]', '</a>', $new_content );

			$contents[] = array(
				'title' => strtoupper($post->post_type) . ':' . esc_html( $post->post_title ),
				'view_url' => get_the_permalink( $post->ID ),
				'edit_url' => add_query_arg(array('post' => $post->ID), '/wp-admin/post.php?post=6602&action=edit'),
				'old_content' => $post->post_content,
				'new_content' => $new_content,
			);
		}

		return $contents;
	}


	private function processContents( $query ){
		$contents = array();
		$replaced = $this->get_replaced();

		foreach ( $query->posts as $post ) {
			if( ! has_shortcode( $post->post_content, 'btn' ) ){
				continue;
			}

			$new_content = preg_replace_callback("/\[(\w+) (.+?)]/", array( $this, "processShortCode" ), $post->post_content);
			$new_content = str_replace('[/btn]', '</a>', $new_content );

			$contents[] = array(
				'title' => strtoupper($post->post_type) . ':' . esc_html( $post->post_title ),
				'view_url' => get_the_permalink( $post->ID ),
				'edit_url' => add_query_arg(array('post' => $post->ID), '/wp-admin/post.php?post=6602&action=edit'),
				'old_content' => $post->post_content,
				'new_content' => $new_content,
			);

			wp_update_post( array(
				'ID' => $post->ID,
				'post_content' => $new_content,
			) );

			if( ! isset( $replaced[ $post->ID ] ) ){
				$replaced[ $post->ID ] = array();
			}

			$replaced[ $post->ID ][] = date('Y-m-d H:i:s');
		}

		$this->set_replaced( $replaced );


		return $contents;
	}





	private function scanContentsQuery(){
		$query_args = array(
			'nopaging' => true,
			'post_type' => array('post','page'),
			'post_status' => 'any',
		);

		$query = new WP_Query( $query_args );

		return $query;
	}

	function processShortCode( $matches ){
		// parse out the arguments
		$dat= explode(" ", $matches[2]);
		$params = array();
		foreach ($dat as $d){
			list($opt, $val) = explode("=", $d);
			$params[$opt] = trim($val, '"');
		}
		switch($matches[1]){
			case "btn":
				$a = '<a';
				if( isset( $params['url'] ) ){
					$a .= ' href="'.esc_url( $params['url'] ).'"';
				}
				if( isset( $params['icon'] ) && 'link' == $params['icon'] ){
					$a .= ' class="button"';
				}
				$a .= '>';
				return $a;        
			break;
		}

	}

	private function get_replaced(){
		$replaced = get_option( self::OPTION_REPLACED );
		if( ! is_array( $replaced )){
			$replaced = array();
		}
		return $replaced;
	}

	private function set_replaced( $replaced ){
		update_option( self::OPTION_REPLACED, $replaced );
	}

}