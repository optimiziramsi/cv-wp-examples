(function( $ ) {
	var shortcode_tag = "ese_panel";
	var settings = ese_wpe[ shortcode_tag ] ? ese_wpe[ shortcode_tag ] : {};
	// $(document),on('click', 'div.ese_wpe.'+shortcode_tag, )

	tinymce.PluginManager.add(shortcode_tag, function( editor, url ) {

		// register panel fromat
		function registerFormat(){
			editor.formatter.register(shortcode_tag, {
				block: 'div',
				classes: 'panel',
				wrapper: true,
			});
		};
		editor.on('init', registerFormat )


		//add button
		editor.addButton(shortcode_tag, {
			image: settings.button_image,
			tooltip: settings.button_tooltip,
			onclick: function(e) {
				editor.formatter.toggle( shortcode_tag );
			},
			onPostRender: function() {
				ctrl = this,    
				editor.on('NodeChange', function(e) {
					ctrl.active( $(e.element).hasClass('panel') || $(e.element).closest('.panel').length > 0 );
				});
			}
		});
	});
})( jQuery );