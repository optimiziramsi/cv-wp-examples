<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

/******************************************************************
*
*       EMAILS MENU AND PAGE
*
******************************************************************/

define('TXT_ESE_UAM_SUBMENU_TITLE', __( 'Manage user access', 'ese' ) );
define('TXT_ESE_UAM_EDIT_EMAILS', __( 'Manage user access', 'ese' ) );
define('TXT_ESE_UAM_EDIT_EMAILS_DESCRIPTION', __( 'Here you can list emails of people, that go into each access group. Use "," for splitting them', 'ese' ) );
define('TXT_ESE_UAM_SAVE', __( 'Save', 'ese' ) );
define('TXT_ESE_UAM_GROUP_NAME', __( 'Manage users user group', 'ese' ) );
define('TXT_ESE_UAM_GROUP_EMAILS', __( 'Emails', 'ese' ) );
define('TXT_ESE_UAM_NONCE_FAILURE', __( 'Nonce failure', 'ese' ) );