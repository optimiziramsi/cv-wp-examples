<?php

class EusprSpanExtra_TableOfContent {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		if( ! is_admin() ){
			return;
		}

		// register meta boxes - with titan framework
		add_action( 'tf_create_options', array( $this, 'add_meta_box' ) );

		// enqueue javascript
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );

	}

	public function add_meta_box(){

		$titan = TitanFramework::getInstance( 'ese-table-of-content' );

		/*******************  metabox - socials  *******************/
		$metaBox_toc = $titan->createMetaBox( array(
			'name' => __( 'Table of content', 'ese' ),
			'post_type' => array( 'page', 'post' ),
			'context' => 'side',
			'priority' => 'core',
		) );

		$metaBox_toc->createOption( array(
			'name' => __( 'Enable', 'ese' ),
			'id' => 'enable',
			'type' => 'checkbox',
			'desc' => __( 'Enable table of content on this page / post', 'ese' ),
			'default' => false,
		) );

		$metaBox_toc->createOption( array(
		    'name' => __( 'Heading depth', 'ese' ),
		    'id' => 'headers',
		    'type' => 'select',
		    'desc' => __( 'Select heading depth to display', 'ese' ),
		    'options' => array(
		        'h1' => 'Heading 1',
		        'h1, h2' => 'Heading 2',
		        'h1, h2, h3' => 'Heading 3',
		        'h1, h2, h3, h4' => 'Heading 4',
		        'h1, h2, h3, h4, h5' => 'Heading 5',
		        'h1, h2, h3, h4, h5, h6' => 'Heading 6',
		    ),
		    'default' => 'h1',
		) );

		$metaBox_toc->createOption( array(
		    'name' => __( 'TOC data', 'ese' ),
		    'id' => 'data',
		    'type' => 'textarea',
		    'default' => '',
		) );

	}

	public function admin_enqueue_scripts( $hook ){
		global $post; 

	    if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
	        if ( 'page' === $post->post_type || 'post' === $post->post_type ) {
	            wp_enqueue_script( 'ese-table-of-content', plugin_dir_url( __FILE__ ) . '/js/table-of-content.js', array( 'jquery' ), ESE_VERSION, true );
	        }
	    }
	}

}