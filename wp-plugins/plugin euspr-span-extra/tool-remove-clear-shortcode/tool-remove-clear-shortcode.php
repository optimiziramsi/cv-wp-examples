<?php

class EusprSpanExtra_ToolRemoveClearShortcode {

	const OPTION_REPLACED = "ese_trcs_replaced";

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		if( ! is_admin() || ! current_user_can( 'manage_options' ) ){
			return;
		}

		// create submenu page
		add_action( 'admin_menu', array( $this, 'addSubmenuToToolsMenu' ) );

	}

	function addSubmenuToToolsMenu(){
		// create submenu under TOOLS menu
		add_submenu_page(
			'tools.php',
			__( 'Remove CLEAR shortcode', 'ese' ),
			__( 'Remove CLEAR shortcode', 'ese' ),
			'manage_options',
			'ese-tool-remove-clear-shortcodes',
			array( $this, 'renderPage' )
		);
	}

	function renderPage(){
		$data = $this->processRequest();

		include dirname( __FILE__ ) . '/views/tool.php';
	}

	private function processRequest(){
		$data = array(
			'state' => '',
			'errors' => array(),
			'messages' => array(),
		);

		$data['state'] = '';


		// DEVELOP
		// $data['state'] = 'scan_report';
		// $data['scan_report'] = $this->scanContents( $this->scanContentsQuery() );
		// return $data;


		if( isset( $_POST['action'] ) && "ese-remove-clear-shortcodes-scan" == $_POST['action'] ){

			$data['state'] = 'scan_report';

			if( ! isset( $_POST['ese-remove-clear-shortcodes-scan-nonce'] ) || ! wp_verify_nonce( $_POST['ese-remove-clear-shortcodes-scan-nonce'], 'ese-remove-clear-shortcodes-scan' ) ){
				$data['errors'][] = __( 'Request not ok. Please retry using this tool the way it is ment to be used.', 'ese' );
				$data['state'] = '';

				return $data;
			}

			$data['scan_report'] = $this->scanContents( $this->scanContentsQuery() );

			return $data;
		}




		if( isset( $_POST['action'] ) && "ese-remove-clear-shortcodes-process" == $_POST['action'] ){

			$data['state'] = 'process_report';

			if( ! isset( $_POST['ese-remove-clear-shortcodes-process-nonce'] ) || ! wp_verify_nonce( $_POST['ese-remove-clear-shortcodes-process-nonce'], 'ese-remove-clear-shortcodes-process' ) ){
				$data['errors'][] = __( 'Request not ok. Please retry using this tool the way it is ment to be used.', 'ese' );
				$data['state'] = '';

				return $data;
			}

			$data['process_report'] = $this->processContents( $this->scanContentsQuery() );
			
			return $data;
		}

		$data['replaced'] = $this->get_replaced();

		return $data;
	}


	private function scanContents( $query ){

		$contents = array();

		foreach ( $query->posts as $post ) {
			if( ! has_shortcode( $post->post_content, 'clear' ) ){
				continue;
			}

			$new_content = str_replace('[clear]', '', $post->post_content );

			$contents[] = array(
				'title' => strtoupper($post->post_type) . ':' . esc_html( $post->post_title ),
				'view_url' => get_the_permalink( $post->ID ),
				'edit_url' => add_query_arg(array('post' => $post->ID), '/wp-admin/post.php?post=6602&action=edit'),
				'old_content' => $post->post_content,
				'new_content' => $new_content,
			);
		}

		return $contents;
	}


	private function processContents( $query ){
		$contents = array();
		$replaced = $this->get_replaced();

		foreach ( $query->posts as $post ) {
			if( ! has_shortcode( $post->post_content, 'clear' ) ){
				continue;
			}

			$new_content = str_replace('[clear]', '', $post->post_content );

			$contents[] = array(
				'title' => strtoupper($post->post_type) . ':' . esc_html( $post->post_title ),
				'view_url' => get_the_permalink( $post->ID ),
				'edit_url' => add_query_arg(array('post' => $post->ID), '/wp-admin/post.php?post=6602&action=edit'),
				'old_content' => $post->post_content,
				'new_content' => $new_content,
			);

			wp_update_post( array(
				'ID' => $post->ID,
				'post_content' => $new_content,
			) );

			if( ! isset( $replaced[ $post->ID ] ) ){
				$replaced[ $post->ID ] = array();
			}

			$replaced[ $post->ID ][] = date('Y-m-d H:i:s');
		}

		$this->set_replaced( $replaced );


		return $contents;
	}





	private function scanContentsQuery(){
		$query_args = array(
			'nopaging' => true,
			'post_type' => array('post','page'),
			'post_status' => 'any',
		);

		$query = new WP_Query( $query_args );

		return $query;
	}

	private function get_replaced(){
		$replaced = get_option( self::OPTION_REPLACED );
		if( ! is_array( $replaced )){
			$replaced = array();
		}
		return $replaced;
	}

	private function set_replaced( $replaced ){
		update_option( self::OPTION_REPLACED, $replaced );
	}

}