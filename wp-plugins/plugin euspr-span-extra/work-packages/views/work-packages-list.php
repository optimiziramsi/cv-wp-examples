<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

?>
<div class="work-packages-wrapper">
	<ul class="collapsible" data-collapsible="accordion">
	<?php foreach ( $work_packages as $work_package_post ) { ?>
		<li>
			<div class="collapsible-header">
				<i class="fa fa-plus"></i><?php echo esc_html( $work_package_post->post_title ); ?>
				<?php if( ! empty( $work_package_post->file_url ) ){ ?>
					<a href="<?php echo esc_url( $work_package_post->file_url ); ?>" class="right button button-tab pdf"><?php echo sprintf( __( 'Download %s', 'ese' ), esc_html( $work_package_post->file_label ) ); ?></a>
				<?php } ?>
			</div>
			<div class="collapsible-body">
				<?php echo apply_filters( 'the_content', $work_package_post->post_content ); ?>
			</div>
		</li>
	<?php } ?>
	</ul>
</div>