<?php

class EusprSpanExtra_WorkPackages {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		// register custom post type
		add_action( 'init', array( $this, 'register_custom_post_type' ) );
		// register meta boxes - with titan framework
		add_action( 'tf_create_options', array( $this, 'add_meta_boxes' ) );
		// manage sup. org. list columns
		add_filter( 'manage_ese_work_package_posts_columns', array( $this, 'manage_ese_work_package_posts_columns') );
		// manage sup. org. list columns content
		add_filter( 'manage_ese_work_package_posts_custom_column', array( $this, 'manage_ese_work_package_custom_column'), 10, 2 );

		// shortcode for listing sup. org.
		add_shortcode( 'work_packages', array( $this, 'shortcode_work_packages_process' ) );

	}


	/*************************************************
			Work Package CUSTOM POST TYPE - START
	*************************************************/
	
	/**
	* Registers a new post type
	* @uses $wp_post_types Inserts new post type object into the list
	*
	* @param string  Post type key, must not exceed 20 characters
	* @param array|string  See optional args description above.
	* @return object|WP_Error the registered post type object, or an error object
	*/
	function register_custom_post_type() {

		$labels = array(
			'name'                => __( 'Work Packages', 'ese' ),
			'singular_name'       => __( 'Work Package', 'ese' ),
			'add_new'             => _x( 'Add New Work Package', 'ese', 'ese' ),
			'add_new_item'        => __( 'Add New Work Package', 'ese' ),
			'edit_item'           => __( 'Edit Work Package', 'ese' ),
			'new_item'            => __( 'New Work Package', 'ese' ),
			'view_item'           => __( 'View Work Package', 'ese' ),
			'search_items'        => __( 'Search Work Packages', 'ese' ),
			'not_found'           => __( 'No Work Packages found', 'ese' ),
			'not_found_in_trash'  => __( 'No Work Packages found in Trash', 'ese' ),
			'parent_item_colon'   => __( 'Parent Work Package:', 'ese' ),
			'menu_name'           => __( 'Work Packages', 'ese' ),
		);

		$args = array(
			'labels'                   => $labels,
			'hierarchical'        => false,
			'description'         => 'Work Packages CPT',
			'taxonomies'          => array(),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => 'edit.php?post_type=ese_partner',
			'show_in_admin_bar'   => false,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-edit',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => false,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'supports'            => array( 'title', 'editor', 'revisions' )
		);

		register_post_type( 'ese_work_package', $args );
	}

	public function manage_ese_work_package_posts_columns( $columns ){
		$use_columns = array();
		foreach ( $columns as $column_key => $column_title ) {
			// skip / remove columns
			if( in_array( $column_key, array('date','uam_access') ) ){
				continue;
			}

			// copy default column
			$use_columns[ $column_key ] = $column_title;

			// rename title label
			if( "title" == $column_key ){
				$use_columns[ $column_key ] = __( 'Work Package Name', 'ese' );
			}

			// add member title column after member name
			if( "title" == $column_key ){
				$use_columns["downloadable_file"] = __( 'Downloadable file', 'ese' );
			}
		}

		return $use_columns;
	}

	public function manage_ese_work_package_custom_column( $column, $post_id ){
		switch ( $column ) {
			case 'downloadable_file':
				$file_label = get_post_meta( $post_id, 'ese-work-packages-cpt_file_label', true );
				$file_url = get_post_meta( $post_id, 'ese-work-packages-cpt_file_url', true );
				if( empty( $file_label ) ){
					$file_label = $file_url;
				}

				if( ! empty( $file_url ) ){
					echo '<a href="'.esc_url( $file_url ).'" target="_blank">' . esc_html( $file_label ) . '</a>';
				}
				break;			
		}
	}

	public function add_meta_boxes(){

		$titan = TitanFramework::getInstance( 'ese-work-packages-cpt' );

		/*******************  metabox - link  *******************/
		$metaBox_attachments = $titan->createMetaBox( array(
			'name' => __( 'Attachment', 'ese' ),
			'post_type' => 'ese_work_package',
		) );

		$metaBox_attachments->createOption( array(
			'name' => __( 'Downloadable file button text', 'ese' ),
			'id' => 'file_label',
			'type' => 'text',
			'desc' => __( 'This content will be visible in button. "Download THIS CONTENT COMES HERE"', 'ese' )
		) );

		$metaBox_attachments->createOption( array(
			'name' => __( 'Downloadable file url', 'ese' ),
			'id' => 'file_url',
			'type' => 'text',
			'desc' => __( 'This file will be available for download', 'ese' )
		) );
	}
	
	/*************************************************
			Work Package CUSTOM POST TYPE - END
	*************************************************/



	/*************************************************
			SHORTCODES - START
	*************************************************/
	
	public function shortcode_work_packages_process() {
		// get all contents
		$work_packages = get_posts(array(
					'nopaging' => true,
					'post_type' => 'ese_work_package',
					// 'post_status' => 'published',
					'orderby' => 'menu_order',
					'order' => 'ASC',
				));
		foreach ( $work_packages as $work_package_key => $work_package_post ) {
			$work_package_post->file_label = get_post_meta( $work_package_post->ID, 'ese-work-packages-cpt_file_label', true );
			$work_package_post->file_url = get_post_meta( $work_package_post->ID, 'ese-work-packages-cpt_file_url', true );
			if( empty( $work_package_post->file_label ) ){
				$work_package_post->file_label = $work_package_post->file_url;
			}
			$work_packages[ $work_package_key ] = $work_package_post;
		}

		ob_start();
		include dirname( __FILE__ ) . '/views/work-packages-list.php';
		return ob_get_clean();
	}
	
	/*************************************************
			SHORTCODES - END
	*************************************************/

}