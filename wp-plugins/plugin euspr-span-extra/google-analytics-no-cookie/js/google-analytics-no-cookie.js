//path to the script that will generate and return the user id
gancGetUidUrl = ganc_vars.gancGetUidUrl;

//just copy and paste this. You only need to edit the url in the last line of this code paragraph
(function(i,s,o,g,r,a,m){i['GoogleNoCookieAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script',ganc_vars.ga_no_cookie_php,'ganc');
//Only edit the url in the above line. Instead of including JS file via PHP script, you could also include ga_no_cookie_min.js directly and set up caching with webserver instructions. Use ?v=1234... to force cache update if you change the JS file or upgrade to a new version of this library.

//Setup tracker id and start sending hits. Enter your tracking id.
ganc('create', ganc_vars.id, '');
//Send page view
ganc('send', 'pageview');

//Optionally, send events. Category and Action are required.
// ganc('send', 'event', 'EVENT_CATEGORY', 'EVENT_ACTION', 'EVENT_LABEL');

//Optional support for opt-out of data collection
//id of dom element that will hold opt-in and opt-out links (optional)
ganc_status_eid = "ganc_status";
//text for the link if ganc is on
ganc_status_on = "Turn off anonymous usage statistics collection.";
//text for the link if ganc is off
ganc_status_off = "Turn on anonymous usage statistics collection";

//That's it! Check GA Real time view to see if it is working. Note: real time mode sometimes shows doubled events and returning visitors appear only after a day or so of usage.