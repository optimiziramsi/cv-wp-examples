<?php

class EusprSpanExtra_GoogleAnalyticsNoCookie {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		add_action( 'init', array( $this, 'add_query_var' ) );
		add_action( 'template_redirect', array( $this, 'fetch_request' ) );

		add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ) );
	}

	function add_query_var(){
		global $wp;
		$wp->add_query_var('ese-ganc');
		$wp->add_query_var('ese-ganc-action');
	}

	function fetch_request(){
		if ( get_query_var('ese-ganc') == 1 && in_array( get_query_var('ese-ganc-action'), array('uid','js') )) {
			switch ( get_query_var('ese-ganc-action') ) {
				case 'js':
					$etag = !empty($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false;
					$last_modified = !empty($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? trim($_SERVER['HTTP_IF_MODIFIED_SINCE']) : 0;
					 
					if(!empty($etag) || !empty($last_modified)) {
						header('Not Modified',true,304);
						die();
					}
					$etag = md5("We use ETag just for caching, not tracking!");
					header('Content-Type: text/javascript');
					header('Expires: ' . gmdate('D, d M Y H:i:s', time()+365*24*3600) . ' GMT');
					header('ETag: "' . $etag . '"');
					require dirname( __FILE__ ) . '/js/ga_no_cookie_min.js';
					exit();

					break;

				case 'uid':
					header('Content-Type: text/javascript');
					$ip = $this->getUserId();
					echo "window['gaNoCookieUid'] = '{$ip}';";
					die();

					break;
			}
		}
	}

	function wp_enqueue_scripts(){
		$config = $this->config();
		wp_enqueue_script( 'ese-google-analytics-no-cookie', plugin_dir_url( __FILE__ ) . '/js/google-analytics-no-cookie.js', array(), ESE_VERSION );
		wp_localize_script( 'ese-google-analytics-no-cookie', 'ganc_vars', array(
			'id' => $config['id'],
			'gancGetUidUrl' => add_query_arg(array('ese-ganc' => 1, 'ese-ganc-action' => 'uid' ), '/'),
			'ga_no_cookie_php' => add_query_arg(array('ese-ganc' => 1, 'ese-ganc-action' => 'js' ), '/'),
		) );
	}

	function getUserIpAddr()
	{
		//code from http://stackoverflow.com/a/3358212
	    if (!empty($_SERVER['HTTP_CLIENT_IP']))
	    {
	        return $_SERVER['HTTP_CLIENT_IP'];
	    }
	    else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
	    {
	        return $_SERVER['HTTP_X_FORWARDED_FOR'];
	    }
	    else if(!empty($_SERVER['REMOTE_ADDR']))
	    {
	        return $_SERVER['REMOTE_ADDR'];
	    }
	    return "UnknownIP";
	}

	function getBrowserAndPlatform() {
		$ua = !empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "";
		
		$browser = "UnknownBrowser";
		if(stripos($ua, "MSIE") !== false) {
			$browser = "MSIE";
		} elseif(stripos($ua, "Safari") !== false) {
			$browser = "Safari";
		} elseif(stripos($ua, "Chrome") !== false) {
			$browser = "Chrome";
		} elseif(stripos($ua, "Firefox") !== false) {
			$browser = "Firefox";
		}
		
		$os = "UnknownOS";
		if(stripos($ua, "Win") !== false) {
			$os = "Windows";
		} elseif(stripos($ua, "Mac") !== false) {
			$os = "Mac";
		} elseif(stripos($ua, "X11") !== false) {
			$os = "Unix";
		} elseif(stripos($ua, "Linux") !== false) {
			$os = "Linux";
		} elseif(stripos($ua, "iPhone") !== false) {
			$os = "iPhone";
		} elseif(stripos($ua, "iPad") !== false) {
			$os = "iPad";
		} elseif(stripos($ua, "iPod") !== false) {
			$os = "iPod";
		}
		return $browser.$os;
	}

	function getScreenSize() {
		return !empty($_GET['ss']) ? $_GET['ss'] : "UnknownSize";
	}

	function getUserId() {
		return md5($this->getUserIpAddr().$this->getBrowserAndPlatform().$this->getScreenSize());
	}

	private function config(){
		$sett = EusprSpanExtra_SettingsPage::getInstance();
		$google_tracking_id = $sett->param('google_tracking_id');
		return array(
			'id' => $google_tracking_id,
		);
	}
}