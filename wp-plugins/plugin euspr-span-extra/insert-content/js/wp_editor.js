(function( $ ) {
	var shortcode_tag = "ese_insert_content";
	var settings = ese_wpe[ shortcode_tag ] ? ese_wpe[ shortcode_tag ] : {};
	// $(document),on('click', 'div.ese_wpe.'+shortcode_tag, )

	tinymce.PluginManager.add(shortcode_tag, function( editor, url ) {

		function replaceShortcodes( content ) {
			return wp.shortcode.replace( shortcode_tag, content, processShortcode );
		}

		function processShortcode( shortcode ){
			var attrs = {
				class: "ese_wpe " + shortcode_tag,
			};
			var shortcode_atts = settings.shortcode_attributes;
			$.each( shortcode_atts, function( attr_key, attr_val ){
				attrs['data-sh-attr-' + attr_key] = shortcode.attrs.named[ attr_key ] ? shortcode.attrs.named[ attr_key ] : "";
			});

			/*
			<div class="ese_wpe ese_insert_content">
				<span class="text">Here will be contents for: Some content title</span>
			</div>
			*/

			var page_post_found = false;
			var insert_content_title = settings.preview_title_none;
			if( ! page_post_found ){
				$.each( shortcode_atts.id.pages.options, function( page_id, page_title ){
					if( page_id == attrs['data-sh-attr-id'] ){
						insert_content_title = settings.page_label + " | " + page_title;
						page_post_found = true;
						return false;
					}
				});
			}
			if( ! page_post_found ){
				$.each( shortcode_atts.id.posts.options, function( post_id, post_title ){
					if( post_id == attrs['data-sh-attr-id'] ){
						insert_content_title = settings.post_label + " | " + post_title;
						page_post_found = true;
						return false;
					}
				});
			}
			if( ! page_post_found && "" != attrs['data-sh-attr-id'] ){
				insert_content_title = settings.preview_title_not_found;
			}

			var $content = $('<div/>');
			$content.append( $('<div/>').addClass('hover').append( $('<span/>').addClass('instructions').text( settings.double_click_to_edit ) ) );
			$content.append( $('<span/>').addClass('text').text( settings.preview_text + " " + insert_content_title ) );
			
			// Set up the codeblock with the parsed shortcode attrs.
			return wp.html.string({
				tag: "div",
				attrs: attrs,
				content: $content.html(),
			});
		}

		function restoreShortcodes( content ) {
			var $holder = $('<div/>');
			$holder.html( content );
			$holder.find("div.ese_wpe."+shortcode_tag).each(function( i, e ){
				var $e = $(e);

				var shortcode_text = "["+shortcode_tag;

				$.each(e.attributes, function() {
					// this.attributes is not a plain object, but an array
					// of attribute nodes, which contain both the name and value
					if(this.specified && 0 === this.name.indexOf('data-sh-attr-')) {
						shortcode_text += ' ' + this.name.substr( ' data-sh-attr-'.length - 1 ) + '="'+this.value+'"';
					}
				});

				shortcode_text += "]";

				var $newContent = $('<p/>');
				$newContent.text( shortcode_text );

				$e.replaceWith( $newContent );
			});

			return $holder.html();
		}

		//add popup
		editor.addCommand(shortcode_tag+'_popup', function(ui, v) {
			var select_options = [];
			$.each(settings.shortcode_attributes.id, function( pages_posts_key, pages_posts_data ){
				var pages_posts_options = [];
				$.each(settings.shortcode_attributes.id[ pages_posts_key ].options, function( page_post_id , page_post_title ){
					pages_posts_options.push({
						text: page_post_title,
						value: page_post_id,
						selected: page_post_id == v.id,
					});
				});

				select_options.push( {
					text: pages_posts_data.group,
					menu: pages_posts_options,
				} );
			});

			var body = [
				{
					type: 'listbox',
				    name: 'id',
				    label: settings.page_label + ' / ' + settings.post_label, 
				    'values': select_options,
				}
			];

			editor.windowManager.open( {
				title: settings.name,
				body: body,
				onsubmit: function( e ) {
					var shortcode_str = '[' + shortcode_tag;
					$.each(settings.shortcode_attributes, function( sc_attr_key, sc_attr_values ){
						var attr_value = typeof e.data[ sc_attr_key ] != 'undefined' && e.data[ sc_attr_key ].length ? e.data[ sc_attr_key ] : "";
						shortcode_str += ' '+sc_attr_key+'="' + attr_value + '"';
					});
					//add panel content
					shortcode_str += ']';
					//insert shortcode to tinymce
					editor.selection.setContent( shortcode_str );
					// editor.insertContent( shortcode_str);
					
					// TODO - zdaj ko dodaš element, na njem ne deluje NOEDITABLE, dokler ne greš na text in nazaj v VIEW
					// morda kak render ali kaj takega
				}
			});
	    });

		//add button
		editor.addButton(shortcode_tag, {
			image: settings.button_image,
			tooltip: settings.button_tooltip,
			onclick: function() {
				editor.execCommand(shortcode_tag+'_popup','',{});
			}
		});

		//replace from shortcode to an image placeholder
		editor.on('BeforeSetcontent', function(event){ 
			event.content = replaceShortcodes( event.content );
		});

		//replace from image placeholder to shortcode
		editor.on('GetContent', function(event){
			event.content = restoreShortcodes(event.content);
		});

		//open popup on placeholder double click
		editor.on('DblClick',function(e) {
			var $e = $(e.target);
			if ( $e.closest('.ese_wpe').length > 0 && $e.closest('.ese_wpe').hasClass(shortcode_tag) ) {
				var $target = $e.closest('.ese_wpe');
				tinymce.activeEditor.selection.select($target.get(0));
				var attrs = {};
				$.each(settings.shortcode_attributes, function( sc_attr_key, sc_attr_values ){
					attrs[ sc_attr_key ] = $target.attr('data-sh-attr-'+sc_attr_key) ? $target.attr('data-sh-attr-'+sc_attr_key) : "";
				});
				editor.execCommand(shortcode_tag+'_popup','',attrs);
			}
		});
	});
})( jQuery );