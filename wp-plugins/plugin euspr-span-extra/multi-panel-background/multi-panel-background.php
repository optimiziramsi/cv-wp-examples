<?php

class EusprSpanExtra_MultiPanelBackground {

	private static $instance = null;

	public $shortcode_tag = 'ese_multi_panel_background';

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		add_shortcode( $this->shortcode_tag, array( $this, 'shortcode_handler' ) );

		if( ! is_admin() ){
			return;
		}

		// enable WP EDITOR functionality
		add_filter('ese_wpe_mce_external_plugins_atts', array( $this, 'ese_wpe_mce_external_plugins_atts' ) );
		add_filter('ese_wpe_mce_external_plugins', array( $this, 'ese_wpe_mce_external_plugins' ) );
		add_filter('ese_wpe_mce_buttons', array( $this, 'ese_wpe_mce_buttons' ) );
		add_action('ese_wpe_admin_enqueue_scripts', array( $this, 'ese_wpe_admin_enqueue_scripts' ) );
		add_filter('ese_wpe_mce_css', array( $this, 'ese_wpe_mce_css' ) );
	}

	function shortcode_handler($atts){
		extract( shortcode_atts(
			array(
				'title' => '',
				'url' => '',
				'link_1_title' => '',
				'link_1_url' => '',
				'link_2_title' => '',
				'link_2_url' => '',
				'link_3_title' => '',
				'link_3_url' => '',
				'link_4_title' => '',
				'link_4_url' => '',
				'link_5_title' => '',
				'link_5_url' => '',
				'link_6_title' => '',
				'link_6_url' => '',
			), $atts )
		);
		
		ob_start();
		include dirname( __FILE__ ) . '/views/shortcode.php';
		$output = ob_get_clean();
		
		return $output;
	}


	/*************************************************
			WP EDITOR - START
	*************************************************/
	
	function ese_wpe_mce_external_plugins_atts( $plugins_atts ){
		$plugins_atts[$this->shortcode_tag ] = array(
			'name' => __( 'Multi Panel', 'ese' ),
			'button_tooltip' => __( 'Insert multi-panel', 'ese' ),
			'button_image' => plugins_url( 'images/wp_editor_btn.png' , __FILE__ ),
			'blank_for_none' => __( 'Leave blank for none', 'ese' ),
			'double_click_to_edit' => __( 'Double click on me to edit content', 'ese' ),
			'shortcode_attributes' => array( 'title', 'url', 'link_1_title', 'link_1_url', 'link_2_title', 'link_2_url', 'link_3_title', 'link_3_url', 'link_4_title', 'link_4_url', 'link_5_title', 'link_5_url', 'link_6_title', 'link_6_url', ),
		);
		return $plugins_atts;
	}
	function ese_wpe_mce_external_plugins( $plugin_array ){
		$plugin_array[$this->shortcode_tag ] = plugins_url( 'js/wp_editor.js' , __FILE__ );
		return $plugin_array;
	}

	function ese_wpe_mce_buttons( $buttons ){
		array_push( $buttons, $this->shortcode_tag );
		return $buttons;
	}

	function ese_wpe_admin_enqueue_scripts(){
		// wp_enqueue_style($this->shortcode_tag, plugins_url( 'css/wp_editor.css' , __FILE__ ) );
	}

	function ese_wpe_mce_css( $mce_css ) {
		if ( ! empty( $mce_css ) )
			$mce_css .= ',';

		$mce_css .= plugins_url( 'css/wp_editor.css', __FILE__ );
		return $mce_css;
	}
	
	/*************************************************
			WP EDITOR - END
	*************************************************/





}