<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

?>
<div class="panel panel-multi panel-bg">
	<h4><a href="<?php echo esc_url( $url ); ?>" class="a-link"><?php echo esc_html( $title ); ?></a></h4>

	<?php if( ! empty( $link_1_title ) && ! empty( $link_1_url ) ){ ?>
	<a href="<?php echo esc_url( $link_1_url ); ?>" class="button link float-left"><?php echo esc_html( $link_1_title ); ?></a>
	<?php } ?>

	<?php if( ! empty( $link_2_title ) && ! empty( $link_2_url ) ){ ?>
	<a href="<?php echo esc_url( $link_2_url ); ?>" class="button link float-left"><?php echo esc_html( $link_2_title ); ?></a>
	<?php } ?>

	<?php if( ! empty( $link_3_title ) && ! empty( $link_3_url ) ){ ?>
	<a href="<?php echo esc_url( $link_3_url ); ?>" class="button link float-left"><?php echo esc_html( $link_3_title ); ?></a>
	<?php } ?>

	<?php if( ! empty( $link_4_title ) && ! empty( $link_4_url ) ){ ?>
	<a href="<?php echo esc_url( $link_4_url ); ?>" class="button link float-left"><?php echo esc_html( $link_4_title ); ?></a>
	<?php } ?>

	<?php if( ! empty( $link_5_title ) && ! empty( $link_5_url ) ){ ?>
	<a href="<?php echo esc_url( $link_5_url ); ?>" class="button link float-left"><?php echo esc_html( $link_5_title ); ?></a>
	<?php } ?>

	<?php if( ! empty( $link_6_title ) && ! empty( $link_6_url ) ){ ?>
	<a href="<?php echo esc_url( $link_6_url ); ?>" class="button link float-left"><?php echo esc_html( $link_6_title ); ?></a>
	<?php } ?>
</div>