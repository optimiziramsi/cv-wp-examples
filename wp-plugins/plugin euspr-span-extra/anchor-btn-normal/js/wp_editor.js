(function( $ ) {
	var shortcode_tag = "ese_anchor_btn_normal";
	var settings = ese_wpe[ shortcode_tag ] ? ese_wpe[ shortcode_tag ] : {};
	// $(document),on('click', 'div.ese_wpe.'+shortcode_tag, )

	tinymce.PluginManager.add(shortcode_tag, function( editor, url ) {

		// register panel fromat
		function registerFormat(){
			$.each(settings.tmce_formats, function( format_id, format_data ){
				editor.formatter.register(format_id, format_data);
			});
		};
		editor.on('init', registerFormat );

		//add button
		editor.addButton(shortcode_tag, {
			image: settings.button_image,
			tooltip: settings.button_tooltip,
			onclick: function(e) {
				editor.formatter.toggle( shortcode_tag );
			},
			onPostRender: function() {
				ctrl = this,    
				editor.on('NodeChange', function(e) {
					ctrl.active(
						$(e.element).hasClass('button') || $(e.element).closest('a.button').length > 0
					);
				});
			}
		});
	});
})( jQuery );