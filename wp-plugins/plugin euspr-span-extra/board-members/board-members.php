<?php

class EusprSpanExtra_BoardMembers {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		// register custom post type
		add_action( 'init', array( $this, 'register_member_custom_post_type' ) );
		// register meta boxes - with titan framework
		add_action( 'tf_create_options', array( $this, 'add_meta_boxes' ) );
		// manage members list columns
		add_filter( 'manage_ese_bmember_posts_columns', array( $this, 'manage_ese_bmember_posts_columns') );
		// manage members list columns content
		add_filter( 'manage_ese_bmember_posts_custom_column', array( $this, 'manage_ese_bmember_custom_column'), 10, 2 );
		// register member image size
		add_action( 'init', array( $this, 'add_member_image_size' ) );

		// register members title category
		add_action( 'init', array( $this, 'register_member_title_custom_post_type' ) );
		// manage members titles list columns
		add_filter( 'manage_ese_bmembers_title_posts_columns', array( $this, 'manage_ese_bmembers_title_posts_columns') );
		// manage members titles list columns content
		add_filter( 'manage_ese_bmembers_title_posts_custom_column', array( $this, 'manage_ese_bmembers_title_custom_column'), 10, 2 );

		// shortcode for listing members
		add_shortcode( 'board_members', array( $this, 'shortcode_board_members_process' ) );

	}



	/*************************************************
			MEMBERS CUSTOM POST TYPE - START
	*************************************************/
	
	/**
	* Registers a new post type
	* @uses $wp_post_types Inserts new post type object into the list
	*
	* @param string  Post type key, must not exceed 20 characters
	* @param array|string  See optional args description above.
	* @return object|WP_Error the registered post type object, or an error object
	*/
	function register_member_custom_post_type() {

		$labels = array(
			'name'                => __( 'Board Members', 'ese' ),
			'singular_name'       => __( 'Board Member', 'ese' ),
			'add_new'             => _x( 'Add New Board Member', 'ese', 'ese' ),
			'add_new_item'        => __( 'Add New Board Member', 'ese' ),
			'edit_item'           => __( 'Edit Board Member', 'ese' ),
			'new_item'            => __( 'New Board Member', 'ese' ),
			'view_item'           => __( 'View Board Member', 'ese' ),
			'search_items'        => __( 'Search Board Members', 'ese' ),
			'not_found'           => __( 'No Board Members found', 'ese' ),
			'not_found_in_trash'  => __( 'No Board Members found in Trash', 'ese' ),
			'parent_item_colon'   => __( 'Parent Board Member:', 'ese' ),
			'menu_name'           => __( 'Board Members', 'ese' ),
		);

		$args = array(
			'labels'                   => $labels,
			'hierarchical'        => false,
			'description'         => 'Board Members CPT',
			'taxonomies'          => array(),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => 'edit.php?post_type=ese_bmember',
			'show_in_admin_bar'   => false,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-businessman',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => false,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'supports'            => array( 'title', 'editor', 'thumbnail','revisions' )
		);

		register_post_type( 'ese_bmember', $args );
	}

	public function manage_ese_bmember_posts_columns( $columns ){
		$use_columns = array();
		foreach ( $columns as $column_key => $column_title ) {
			// skip / remove columns
			if( in_array( $column_key, array('date','uam_access') ) ){
				continue;
			}

			// copy default column
			$use_columns[ $column_key ] = $column_title;

			// rename title label
			if( "title" == $column_key ){
				$use_columns[ $column_key ] = __( 'Member name', 'ese' );
			}

			// add image column after CB column
			if( "cb" == $column_key ){
				$use_columns["member_image"] = __( 'Image', 'ese' );
			}

			// add member title column after member name
			if( "title" == $column_key ){
				$use_columns["member_title"] = __( 'Member Title', 'ese' );
			}
		}

		return $use_columns;
	}

	public function manage_ese_bmember_custom_column( $column, $post_id ){
		switch ( $column ) {
			case 'member_image':
				echo the_post_thumbnail( 'ese-board-members-list' );
				break;

			case 'member_title':
				$title_post_id = get_post_meta( $post_id, 'ese-members-cpt_member_title', true );
				$title_post_title = get_the_title( $title_post_id );
				echo $title_post_title;
				break;			
		}
	}

	public function add_meta_boxes(){

		$titan = TitanFramework::getInstance( 'ese-members-cpt' );

		/*******************  metabox - university  *******************/
		$metaBox_university = $titan->createMetaBox( array(
			'name' => __( 'University', 'ese' ),
			'post_type' => 'ese_bmember',
		) );

		$metaBox_university->createOption( array(
			'name' => __( 'Position', 'ese' ),
			'id' => 'position',
			'type' => 'text',
			'desc' => __( 'Position at university', 'ese' )
		) );

		/*******************  metabox - socials  *******************/
		$metaBox_socials = $titan->createMetaBox( array(
			'name' => __( 'Socials', 'ese' ),
			'post_type' => 'ese_bmember',
		) );

		$metaBox_socials->createOption( array(
			'name' => __( 'Facebook', 'ese' ),
			'id' => 'facebook',
			'type' => 'text',
			'desc' => __( 'Input Facebook url for this board member', 'ese' )
		) );

		$metaBox_socials->createOption( array(
			'name' => __( 'Twitter', 'ese' ),
			'id' => 'twitter',
			'type' => 'text',
			'desc' => __( 'Input Twitter url for this board member', 'ese' )
		) );

		$metaBox_socials->createOption( array(
			'name' => __( 'LinkedIn', 'ese' ),
			'id' => 'linkedin',
			'type' => 'text',
			'desc' => __( 'Input LinkedIn url for this board member', 'ese' )
		) );


		/*******************  metabox - title  *******************/
		$metaBox_member_title = $titan->createMetaBox( array(
			'name' => __( 'Member Title', 'ese' ),
			'post_type' => 'ese_bmember',
			'context' => 'side',
			'priority' => 'default',
		) );

		// prep data for radio meta box
		$members_titles = get_posts( array(
			'posts_per_page' => 999,
			'order' => 'asc',
			'orderby' => 'menu_order',
			'post_type' => 'ese_bmembers_title',
			'post_status' => 'any'
		) );
		$members_titles_options = array();
		foreach ( $members_titles as $members_title ) {
			$members_titles_options[ $members_title->ID ] = $members_title->post_title;
		}
		$metaBox_member_title->createOption( array(
			// 'name' => 'My Radio Option',
			'id' => 'member_title',
			'options' => $members_titles_options,
			'type' => 'radio',
			// 'desc' => 'Select one',
			'default' => '',
		) );
	}

	public function add_member_image_size(){
		add_image_size( 'ese-board-members-list', 100, 100, true );
		add_image_size( 'ese-board-member', 400, 400, true );
	}
	
	/*************************************************
			MEMBERS CUSTOM POST TYPE - END
	*************************************************/


	




	
	/*************************************************
			MEMBERS TITLES CUSTOM POST TYPE - START
	*************************************************/
	
	public function register_member_title_custom_post_type() {

		$labels = array(
			'name'                => __( 'Members Titles', 'ese' ),
			'singular_name'       => __( 'Members Title', 'ese' ),
			'add_new'             => _x( 'Add New Members Title', 'ese', 'ese' ),
			'add_new_item'        => __( 'Add New Members Title', 'ese' ),
			'edit_item'           => __( 'Edit Members Title', 'ese' ),
			'new_item'            => __( 'New Members Title', 'ese' ),
			'view_item'           => __( 'View Members Title', 'ese' ),
			'search_items'        => __( 'Search Members Title', 'ese' ),
			'not_found'           => __( 'No Members Title found', 'ese' ),
			'not_found_in_trash'  => __( 'No Members Title found in Trash', 'ese' ),
			'parent_item_colon'   => __( 'Parent Members Title:', 'ese' ),
			'menu_name'           => __( 'Members Titles', 'ese' ),
		);

		$args = array(
			'labels'                   => $labels,
			'hierarchical'        => false,
			'description'         => 'Members Title CPT',
			'taxonomies'          => array(),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => 'edit.php?post_type=ese_bmember',
			'show_in_admin_bar'   => false,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-groups',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => false,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'supports'            => array( 'title', 'revisions' )
		);

		register_post_type( 'ese_bmembers_title', $args );
	}

	public function manage_ese_bmembers_title_posts_columns( $columns ){
		$use_columns = array();
		if( isset( $columns['cb'] ) ){
			$use_columns['cb'] = $columns['cb'];
		}
		if( isset( $columns['title'] ) ){
			$use_columns['title'] = $columns['title'];
		}
		$use_columns['num_members'] = __( 'Number of members', 'ese' );
		return $use_columns;
	}

	public function manage_ese_bmembers_title_custom_column( $column, $post_id ){
		switch ( $column ) {
			case 'num_members':
				$posts = get_posts(array(
					'nopaging' => true,
					'meta_key' => 'ese-members-cpt_member_title',
					'meta_value' => $post_id,
					'post_type' => 'ese_bmember',
					'post_status' => 'any'
				));
				$num_posts = count( $posts );
				echo sprintf( _n( '%s Board Member', '%s Board Members', $num_posts, 'ese' ), $num_posts );
				break;
		}
	}
	
	/*************************************************
			MEMBERS TITLES CUSTOM POST TYPE - END
	*************************************************/



	/*************************************************
			SHORTCODES - START
	*************************************************/
	
	public function shortcode_board_members_process( $atts ) {
		// get all contents
		$members_posts = get_posts(array(
					'nopaging' => true,
					'post_type' => 'ese_bmember',
					'post_status' => 'published',
					'orderby' => 'menu_order',
					'order' => 'ASC',
				));
		foreach ( $members_posts as $members_post_key => $members_post ) {
			$members_post->member_title_post_id = get_post_meta( $members_post->ID, 'ese-members-cpt_member_title', true );
			$members_post->position = get_post_meta( $members_post->ID, 'ese-members-cpt_position', true );
			$members_post->facebook = get_post_meta( $members_post->ID, 'ese-members-cpt_facebook', true );
			$members_post->linkedin = get_post_meta( $members_post->ID, 'ese-members-cpt_linkedin', true );
			$members_post->twitter = get_post_meta( $members_post->ID, 'ese-members-cpt_twitter', true );
			$members_posts[ $members_post_key ] = $members_post;
		}
		$members_titles_posts = get_posts(array(
					'nopaging' => true,
					'post_type' => 'ese_bmembers_title',
					'post_status' => 'published',
					'orderby' => 'menu_order',
					'order' => 'ASC',
				));
		
		// order them and store into var
		$members = array();
		foreach ( $members_titles_posts as $members_titles_post ) {
			$title_members = array();
			foreach ( $members_posts as $members_post_key => $members_post ) {
				if( $members_titles_post->ID == $members_post->member_title_post_id ){
					$title_members[] = $members_post;
					unset( $members_posts[ $members_post_key ] );
				}
			}
			$members[ $members_titles_post->post_title ] = $title_members;
		}

		// unset not needed vars
		unset( $members_posts );
		unset( $members_titles_posts );
		unset( $title_members );

		ob_start();
		include dirname( __FILE__ ) . '/views/board-members.php';
		return ob_get_clean();
	}
	
	/*************************************************
			SHORTCODES - END
	*************************************************/

}