<?php

class EusprSpanExtra_ContactFormMaterializeFields {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		add_filter( 'grunion_contact_form_field_html', array( $this, 'materialize_input' ), 10, 3 );
	}

	public function materialize_input( $r, $field_label, $post_id ){
		
		$dom = new DOMDocument();
		@$dom->loadHTML( $r );

		$radio_options = array();
		$radio_name = "";
		$radio_label = $field_label;
		

		foreach ($dom->getElementsByTagName('input') as $item) {
			if( 'checkbox' == $item->getAttribute('type') ){
				$name = $item->getAttribute('name');
				$label = $field_label;
				$checked = $item->hasAttribute('checked');
				$required = $item->hasAttribute('required');
				$value = $item->getAttribute('value');

				ob_start();
				?>
				<div>
					<input type="checkbox" id="<?php echo esc_attr( $name ); ?>" <?php echo $checked ? 'checked="checked"' : ''; ?> <?php echo $required ? 'required aria-required="true"' : ""; ?> value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>"/>
					<label for="<?php echo esc_attr( $name ); ?>"><?php echo esc_html( $label ); ?><?php echo $required ? '<span>('.esc_html__( 'required', 'ese' ).')</span>' : ''; ?></label>
				</div>
				<?php
				$r = ob_get_clean();
				$changed = true;
			}
			if( 'radio' == $item->getAttribute('type') ){
				$radio_options[ $item->getAttribute('value') ] = $item->hasAttribute('checked');
				$radio_name = $item->getAttribute('name');
			}
		}

		if( count( $radio_options ) > 0 ){
			ob_start();
			echo '<div>';
			echo '<label class="grunion-field-label">'.esc_html( $radio_label ).'</label>';
			foreach ( $radio_options as $radio_option_value => $checked ) {
				$uid = uniqid();
				?>
				<p>
					<input name="<?php echo esc_html( $radio_name ); ?>" type="radio" id="<?php echo esc_attr( $uid ); ?>" value="<?php echo esc_attr( $radio_option_value ); ?>" <?php echo $checked ? 'checked="checked"' : ''; ?> />
					<label for="<?php echo esc_attr( $uid ); ?>"><?php echo esc_html( $radio_option_value ); ?></label>
				</p>
				<?php
			}
			echo '</div>';
			$r = ob_get_clean();
		}
		
		return $r;
	}
}