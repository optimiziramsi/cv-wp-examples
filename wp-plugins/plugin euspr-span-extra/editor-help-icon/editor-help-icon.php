<?php

class EusprSpanExtra_EditorHelpIcon {

	private static $instance = null;

	public $shortcode_tag = 'ese_euspr_span_help';

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		if( ! is_admin() ){
			return;
		}

		// enable WP EDITOR functionality
		add_filter('ese_wpe_mce_external_plugins_atts', array( $this, 'ese_wpe_mce_external_plugins_atts' ) );
		add_filter('ese_wpe_mce_external_plugins', array( $this, 'ese_wpe_mce_external_plugins' ) );
		add_filter('ese_wpe_mce_buttons', array( $this, 'ese_wpe_mce_buttons' ) );
		// add_action('ese_wpe_admin_enqueue_scripts', array( $this, 'ese_wpe_admin_enqueue_scripts' ) );
		// add_filter('ese_wpe_mce_css', array( $this, 'ese_wpe_mce_css' ) );
	}

	/*************************************************
			WP EDITOR - START
	*************************************************/
	
	function ese_wpe_mce_external_plugins_atts( $plugins_atts ){
		$sett = EusprSpanExtra_SettingsPage::getInstance();

		$plugins_atts[$this->shortcode_tag ] = array(
			'name' => __( 'EUSPR / SPAN help', 'ese' ),
			'button_tooltip' => __( 'EUSPR / SPAN specific functionality instructions', 'ese' ),
			'help_url' => $sett->param('euspr_span_help_page_url'),
		);
		return $plugins_atts;
	}
	function ese_wpe_mce_external_plugins( $plugin_array ){
		$plugin_array[$this->shortcode_tag ] = plugins_url( 'js/wp_editor.js' , __FILE__ );
		return $plugin_array;
	}

	function ese_wpe_mce_buttons( $buttons ){
		array_push( $buttons, $this->shortcode_tag );
		return $buttons;
	}
	
	/*************************************************
			WP EDITOR - END
	*************************************************/



}