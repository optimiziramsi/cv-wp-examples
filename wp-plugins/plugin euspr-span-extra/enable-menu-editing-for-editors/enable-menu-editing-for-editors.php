<?php

class EusprSpanExtra_EnableMenuEditingForEditors {

	private static $instance = null;

	private $render_ids = array();

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		add_action( 'admin_init', array( $this, 'add_the_options_capability') );
		add_action( 'admin_head', array( $this, 'hide_menus_from_editor') );
	}

	function add_the_options_capability(){
		$role_object = get_role( 'editor' );
		$role_object->add_cap( 'edit_theme_options' );
	}

	function hide_menus_from_editor() {

		$user = wp_get_current_user();
		if( $user && $user->roles ){
			if( in_array( 'editor', $user->roles ) ){
				remove_submenu_page( 'themes.php', 'themes.php' ); // hide the theme selection submenu
				remove_submenu_page( 'themes.php', 'widgets.php' ); // hide the widgets submenu
			}
		}
	}

	

}