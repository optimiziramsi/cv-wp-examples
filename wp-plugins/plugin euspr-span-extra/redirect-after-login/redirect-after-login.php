<?php

class EusprSpanExtra_RedirectAfterLogin {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){
		if( ! EusprSpanExtra_Plugin::have_required_plugins( array('wp-session-manager' ) ) ){
			return;
		}

		// on login success
		add_filter( 'login_redirect', array( $this, 'login_redirect' ), 10, 3 );
	}

	function login_redirect( $redirect_to, $request, $user ) {
		
		if( is_wp_error( $user ) ) {
			if( isset( $_POST['ese_login_redirect_url'] ) ){
				$session = WP_Session::get_instance();
				$session['ese_login_redirect_to'] = $_POST['ese_login_redirect_url'];
			}
		} else {
			if ( isset( $user->roles ) && is_array( $user->roles ) ) {
				//check for admins
				if ( in_array( 'subscriber', $user->roles ) ) {
					// redirect them to the default place
					return $this->redirect_to_last_location( $redirect_to );
				}
			}
		}

		return $redirect_to;
	}

	private function redirect_to_last_location( $redirect_to ){
		$session = WP_Session::get_instance();
		if( isset( $session['ese_login_redirect_to'] ) ){
			$redirect_to = $session['ese_login_redirect_to'];
			unset( $session['ese_login_redirect_to'] );
		}

		return $redirect_to;
	}




}