<?php

class EusprSpanExtra_PreventionScienceDatabasesV2 {

	private static $instance = null;

	const CRON_VERSION = "1.0";
	const CRON_VERSION_OPTION = "ese_psdv2_cron_version";
	const CRON_ACTION_HOOK = "ese_psdv2_cron_hook";

	const DB_VERSION = "1.0";
	const DB_VERSION_OPTION = "ese_psdv2_db_version";

	const DB_TABLE_DATA = "ese_dbv2_data";
	const DB_TABLE_LOG = "ese_dbv2_log";

	const SLUG = "ese-psdv2";

	const META_EXPORTS = "_ese_psdv2_exports";
	const META_BACKUPS = "_ese_psdv2_backups";

	const CPT = "ese_p_s_database_v2";

	private $import_export_id = "";
	const ACTION_EXPORT = "export";
	const ACTION_IMPORT = "import";
	const ACTION_UPDATE = "update";
	const ACTION_DELETE = "delete";
	const ACTION_INSERT = "insert";
	const ACTION_BACKUP = "backup";
	const ACTION_RESTORE = "restore";

	const KEEP_REVISIONS_IN_SECONDS = 2592000;	// 30 days = 2592000 seconds

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		add_action( 'init', array( $this, 'db_setup' ) );
		add_action( 'init', array( $this, 'cron_setup' ) );

		// register custom post type
		add_action( 'init', array( $this, 'register_custom_post_type' ) );
		// manage members list columns
		add_filter( 'manage_'.self::CPT.'_posts_columns', array( $this, 'manage_posts_columns') );
		// manage members list columns content
		add_filter( 'manage_'.self::CPT.'_posts_custom_column', array( $this, 'manage_posts_custom_column'), 10, 2 );
		
		// shortcode for listing members
		add_shortcode( 'prevention_science_databases_filter2', array( $this, 'shortcode_process' ) );
		add_shortcode( 'prevention_science_databases_edit', array( $this, 'shortcode_edit_process' ) );

		// export XLS / database
		add_action( 'wp_ajax_' . self::CPT . '-export', array( $this, 'export_xls_handle' ) );

		// cron hook
		add_action( self::CRON_ACTION_HOOK, array( $this, 'cron_process' ) );

		if( ! is_admin() ){
			return;
		}

		add_action( 'tf_create_options', array( $this, 'add_cpt_actions_page' ) );

		add_action( 'load-post.php', array( $this, 'add_meta_boxes_setup') );
		add_action( 'load-post-new.php', array( $this, 'add_meta_boxes_setup') );

		// enqueue admin scripts and styles
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );

		add_action( 'before_delete_post', array( $this, 'database_before_delete_post' ) );
	}



	function db_setup(){

		$installed_db_ver = get_option( self::DB_VERSION_OPTION );

		if ( self::DB_VERSION != $installed_db_ver ) {

			global $wpdb;

			$charset_collate = $wpdb->get_charset_collate();

			$table_data = $wpdb->prefix . self::DB_TABLE_DATA;
			$sql_table_data = "CREATE TABLE $table_data (
				`edd_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`edd_database_id` int(11) NOT NULL,
				`edd_updated` datetime NOT NULL,
				`edd_data` text NOT NULL,
				`edd_user_id` int(10) unsigned NOT NULL,
				`edd_sort_index` int(10) unsigned NOT NULL,
				PRIMARY KEY (`edd_id`),
				KEY `edd_database_id` (`edd_database_id`),
				KEY `edd_updated` (`edd_updated`),
				KEY `edd_sort_index` (`edd_sort_index`),
				KEY `edd_user_id` (`edd_user_id`)
			) $charset_collate";

			$table_log = $wpdb->prefix . self::DB_TABLE_LOG;
			$sql_table_log = "CREATE TABLE $table_log (
				`edl_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`edl_export_id` varchar(32) NOT NULL,
				`edl_action` varchar(32) NOT NULL,
				`edl_edd_id` int(10) unsigned NOT NULL,
				`edd_database_id` int(10) unsigned NOT NULL,
				`edl_user_id` int(10) unsigned NOT NULL,
				`edl_old_edd_data` text NOT NULL,
				`edl_new_edd_data` text NOT NULL,
				`edl_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				PRIMARY KEY (`edl_id`),
				KEY `edl_action` (`edl_action`),
				KEY `edl_edd_id` (`edl_edd_id`),
				KEY `edl_user_id` (`edl_user_id`),
				KEY `edl_updated` (`edl_updated`),
				KEY `edl_export_id` (`edl_export_id`),
				KEY `edd_database_id` (`edd_database_id`)
			) $charset_collate";

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

			dbDelta( $sql_table_data );
			dbDelta( $sql_table_log );

			if( ! $installed_db_ver ){
				add_option( self::DB_VERSION_OPTION, self::DB_VERSION );
			} else {
				update_option( self::DB_VERSION_OPTION, self::DB_VERSION );
			}
		}

	}

	/*************************************************
			Prevention Science Database CUSTOM POST TYPE - START
	*************************************************/
	
	/**
	* Registers a new post type
	* @uses $wp_post_types Inserts new post type object into the list
	*
	* @param string  Post type key, must not exceed 20 characters
	* @param array|string  See optional args description above.
	* @return object|WP_Error the registered post type object, or an error object
	*/
	function register_custom_post_type() {

		$labels = array(
			'name'                => __( 'Prevention Science Databases', 'ese' ),
			'singular_name'       => __( 'Prevention Science Database', 'ese' ),
			'add_new'             => _x( 'Add New Prevention Science Database', 'ese', 'ese' ),
			'add_new_item'        => __( 'Add New Prevention Science Database', 'ese' ),
			'edit_item'           => __( 'Edit Prevention Science Database', 'ese' ),
			'new_item'            => __( 'New Prevention Science Database', 'ese' ),
			'view_item'           => __( 'View Prevention Science Database', 'ese' ),
			'search_items'        => __( 'Search Prevention Science Databases', 'ese' ),
			'not_found'           => __( 'No Prevention Science Databases found', 'ese' ),
			'not_found_in_trash'  => __( 'No Prevention Science Databases found in Trash', 'ese' ),
			'parent_item_colon'   => __( 'Parent Prevention Science Database:', 'ese' ),
			'menu_name'           => __( 'SPAN Databases', 'ese' ),
		);

		$args = array(
			'labels'                   => $labels,
			'hierarchical'        => false,
			'description'         => 'Prevention Science Databases CPT',
			'taxonomies'          => array(),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => 'edit.php?post_type=ese_partner',
			'show_in_admin_bar'   => false,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-book',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => false,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'supports'            => array( 'title' )
		);

		register_post_type( self::CPT, $args );
	}

	public function manage_posts_columns( $columns ){
		$use_columns = array();
		foreach ( $columns as $column_key => $column_title ) {
			// skip / remove columns
			if( in_array( $column_key, array('date','uam_access') ) ){
				continue;
			}

			// copy default column
			$use_columns[ $column_key ] = $column_title;

			// add member title column after member name
			if( "title" == $column_key ){
				$use_columns["actions"] = __( 'Actions', 'ese' );
			}
		}

		return $use_columns;
	}

	public function manage_posts_custom_column( $column, $post_id ){
		switch ( $column ) {

			case 'actions':
				$export_url = add_query_arg( array( 'page' => self::CPT . '-actions', 'tab' => 'export' ), 'tools.php' );
				$import_url = add_query_arg( array( 'page' => self::CPT . '-actions', 'tab' => 'import' ), 'tools.php' );
				$backups_url = add_query_arg( array( 'page' => self::CPT . '-actions', 'tab' => 'backups' ), 'tools.php' );
				?>
				<a href="<?php echo esc_attr( $export_url ); ?>" target="_blank"><?php _e( 'Export XLS', 'ese' ); ?></a> | 
				<a href="<?php echo esc_attr( $import_url ); ?>" target="_blank"><?php _e( 'Import XLS', 'ese' ); ?></a> | 
				<a href="<?php echo esc_attr( $backups_url ); ?>" target="_blank"><?php _e( 'Backups / restore', 'ese' ); ?></a>
				<?php
				break;	
		}
	}

	public function add_meta_boxes_setup(){
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save_meta_boxes' ), 10, 2 );
	}

	public function add_meta_boxes(){

		add_meta_box(
			self::CPT . '-icon',      // Unique ID
			esc_html__( 'Database icon', 'ese' ),    // Title
			array( $this, 'databaseIconMetaboxContent' ),   // Callback function
			self::CPT,         // Admin page (or post type)
			'normal',         // Context
			'default'         // Priority
		);

		add_meta_box(
			self::CPT . '-columns',      // Unique ID
			esc_html__( 'Database columns', 'ese' ),    // Title
			array( $this, 'columnsMetaboxContent' ),   // Callback function
			self::CPT,         // Admin page (or post type)
			'normal',         // Context
			'default'         // Priority
		);

		add_meta_box(
			self::CPT . '-log',      // Unique ID
			esc_html__( 'Log', 'ese' ),    // Title
			array( $this, 'logMetaboxContent' ),   // Callback function
			self::CPT,         // Admin page (or post type)
			'normal',         // Context
			'default'         // Priority
		);
	}

	public function databaseIconMetaboxContent( $post ){
		$this->addDataAndNonceToMetaboxes( $post->ID );
		include dirname( __FILE__ ) . '/views/admin-database-icon.php';
	}

	public function columnsMetaboxContent( $post ){
		$this->addDataAndNonceToMetaboxes( $post->ID );
		include dirname( __FILE__ ) . '/views/admin-columns-metabox.php';
	}

	public function logMetaboxContent( $post ){
		global $wpdb;

		$this->addDataAndNonceToMetaboxes( $post->ID );

		$log_table = $wpdb->prefix . self::DB_TABLE_LOG;
		$log_rows_qry = $wpdb->prepare("SELECT * FROM $log_table WHERE edd_database_id = %d ORDER BY edl_updated DESC LIMIT 9999", $post->ID );
		$log_rows = $wpdb->get_results( $log_rows_qry, ARRAY_A );

		$database_columns = $this->database_columns( $post->ID );
		$columns = array();
		foreach ($database_columns as $database_column) {
			for ($field_index=0; $field_index < $database_column['num_fields']; $field_index++) { 
				$field_key = $database_column['id'] . '_' . $field_index;
				$field_label = $database_column['label'];
				if( $database_column['num_fields'] > 1 ){
					$field_label .= " [". ( $field_index + 1 ) ."]";
				}

				$columns[ $field_key ] = $field_label;
			}
		}



		$logs = array();

		foreach ( $log_rows as $log_row ) {
			$export_id = $log_row['edl_export_id'];

			if( ! isset( $logs[ $export_id ] ) ){
				$user_info = get_userdata( $log_row['edl_user_id'] );

				$logs[ $export_id ] = array(
					'user_email' => $user_info->user_email,
					'date' => date( get_option('date_format'), strtotime( $log_row['edl_updated'] ) ),
					'count_deleted' => 0,
					'count_inserted' => 0,
					'count_updated' => 0,
					'logs' => array(),
				);
			}

			$log_row['edl_old_edd_data'] = @json_decode( $log_row['edl_old_edd_data'], true );
			if( ! is_array( $log_row['edl_old_edd_data'] ) ){
				$log_row['edl_old_edd_data'] = array();
			}

			$log_row['edl_new_edd_data'] = @json_decode( $log_row['edl_new_edd_data'], true );
			if( ! is_array( $log_row['edl_new_edd_data'] ) ){
				$log_row['edl_new_edd_data'] = array();
			}

			$log_row['date'] = date( get_option('date_format'), strtotime( $log_row['edl_updated'] ) );

			$logs[ $export_id ]['logs'][] = $log_row;

			if( self::ACTION_DELETE == $log_row['edl_action'] ){
				$logs[ $export_id ]['count_deleted'] += 1;
			}
			if( self::ACTION_INSERT == $log_row['edl_action'] ){
				$logs[ $export_id ]['count_inserted'] += 1;
			}
			if( self::ACTION_UPDATE == $log_row['edl_action'] ){
				$logs[ $export_id ]['count_updated'] += 1;
			}
		}

		include dirname( __FILE__ ) . '/views/admin-log-metabox.php';
	}

	public function save_meta_boxes( $post_id, $post ){
		/* Verify the nonce before proceeding. */
		if ( !isset( $_POST[ self::CPT . '-edit-nonce' ] ) || !wp_verify_nonce( $_POST[ self::CPT . '-edit-nonce' ], self::CPT . '-edit' ) ){
			return $post_id;
		}

		/* Get the post type object. */
		$post_type = get_post_type_object( $post->post_type );

		/* Check if the current user has permission to edit the post. */
		if ( ! current_user_can( 'edit_post', $post_id ) ){
			return $post_id;
		}

		$this->import_export_id = uniqid('edit-post-');

		// save icon
		update_post_meta( $post_id, '_database_icon', isset( $_POST['_database_icon'] ) ? $_POST['_database_icon'] : '' );

		// save columns
		$columns_data = array();
		$column_fields = array(
			'id',

			'label',
			'num_fields',

			'isViewOnList',
			'isEmail',
			'isWww',
			'isSearch',
			'isFilter',
			'isWide',
			'isShort',
		);
		if( isset( $_POST['_database_columns'] ) && is_array( $_POST['_database_columns'] ) ){
			foreach ($_POST['_database_columns'] as $column_id ) {
				$column_data = array_fill_keys( $column_fields, '' );
				foreach ( $column_data as $column_data_key => $column_data_value ) {
					$post_name = '_database_column_' . $column_id . '_' . $column_data_key;
					$column_data[ $column_data_key ] = isset( $_POST[ $post_name ] ) ? $_POST[ $post_name ] : '';
				}

				$columns_data[] = $column_data;
			}
		}
		update_post_meta( $post_id, '_database_columns', $columns_data );

		// check reverts
		if( isset( $_POST['_database_revert_logs'] ) && is_array( $_POST['_database_revert_logs'] ) ){
			foreach ( $_POST['_database_revert_logs'] as $revert_log_id ) {
				$this->database_revert_log( $post_id, $revert_log_id );
			}
		}
		
	}

	private $nonce_added_to_metaboxes = false;
	private function addDataAndNonceToMetaboxes( $post_id ){

		if( $this->nonce_added_to_metaboxes ){
			return;
		}
		wp_nonce_field( self::CPT . '-edit', self::CPT . '-edit-nonce' );

		$database_columns = $this->database_columns( $post_id );

		$database_data = array(
			'icon' => get_post_meta( $post_id, '_database_icon', true ),
			'columns' => $database_columns,
			'log' => array(),
		);

		?>
		<script type="text/javascript">
			var ese_p_s_database_v2_data = <?php echo json_encode( $database_data ); ?>;
		</script>
		<?php

		$this->nonce_added_to_metaboxes = true;
	}

	public function admin_enqueue_scripts( $hook ){
		global $post; 

		if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
			if ( self::CPT === $post->post_type ) {
				wp_enqueue_script( 'ese-prevention-science-databases-admin', plugin_dir_url( __FILE__ ) . '/js/admin-prevention-science-databases.js', array( 'jquery','jquery-ui-tooltip', 'jquery-ui-sortable' ), ESE_VERSION, true );
				wp_localize_script( 'ese-prevention-science-databases-admin', 'ese_psd_admin', array(
					'show_column_on_list_required' => __( 'You need to set "visible on list" for at least one column', 'ese' ),
					'save_when_hot_initialized' => __( 'Please wait a second, so table functionality can initialize. Then you can save.', 'ese' ),
				) );
				wp_enqueue_style( 'ese-prevention-science-databases-admin', plugin_dir_url( __FILE__ ) . '/css/admin-prevention-science-databases.css', array('ese-font-awesome'), ESE_VERSION );
			}
		}
	}

	public function enqueue_scripts(){
		// we call it from shortcode
		wp_enqueue_script( 'ese-prevention-science-databases', plugin_dir_url( __FILE__ ) . '/js/prevention-science-databases.js', array( 'jquery','ese-select2', 'jquery-ui-tooltip', 'jquery-ui-sortable' ), ESE_VERSION, true );
		wp_localize_script( 'ese-prevention-science-databases', 'ese_psd', array(
			'filter_placeholder' => __( 'Select a filter', 'ese' ),
			'psd_modal_more_columns_title' => __( 'Other info:', 'ese' ),
		) );
		wp_enqueue_style( 'ese-prevention-science-databases', plugin_dir_url( __FILE__ ) . '/css/prevention-science-databases.css', array(), ESE_VERSION );
	}

	public function database_before_delete_post( $post_id ){
		global $wpdb;
		
		if ( self::CPT != get_post_type( $post_id ) ){
			return;
		}

		// delete imports and exports
		$exports = get_post_meta( $post_id, self::META_EXPORTS, false );
		foreach ($exports as $export_data ) {
			$this->delete_export( $post_id, $export_data['export_id'] );
		}
		// delete backups
		$backups = $this->get_database_backups( $post_id );
		foreach ( $backups as $backup_data ) {
			$this->delete_database_backup( $post_id, $backup_data['backup_id'] );
		}

		$table_data = $wpdb->prefix . self::DB_TABLE_DATA;
		$table_log = $wpdb->prefix . self::DB_TABLE_LOG;

		$delete_data_qry = $wpdb->prepare("DELETE FROM $table_data WHERE edd_database_id = %d", $post_id );
		$delete_log_qry = $wpdb->prepare("DELETE FROM $table_log WHERE edd_database_id = %d", $post_id );

		$wpdb->query( $delete_data_qry );
		$wpdb->query( $delete_log_qry );
	}
	
	/*************************************************
			Prevention Science Database CUSTOM POST TYPE - END
	*************************************************/



	/*************************************************
			CPT ACTIONS PAGE - START
	*************************************************/
	
	public function add_cpt_actions_page(){

		$titan = TitanFramework::getInstance( self::CPT . '-actions' );

		$adminPanel = $titan->createAdminPanel( array(
			'id' => self::CPT . '-actions',
		    'name' => __( 'SPAN Databases tools', 'ese' ),
		    'parent' => 'tools.php',
		) );


		/*************************************************
				EXPORT - START
		*************************************************/
		
		$export_tab = $adminPanel->createTab( array(
		    'name' => __( 'Export', 'ese' ),
		) );

		$export_tab->createOption( array(
			'name' => __( 'Help', 'ese' ),
			'id' => 'export_help',
			'type' => 'custom',
			'custom' => $this->cpt_actions_export_help(),
		) );

		$export_tab->createOption( array(
			'name' => __( 'Databases', 'ese' ),
			'id' => 'export_databases',
			'type' => 'custom',
			'custom' => $this->cpt_actions_export_databases_list(),
		) );
		
		/*************************************************
				EXPORT - END
		*************************************************/


		/*************************************************
				IMPORT - START
		*************************************************/
		
		$export_tab = $adminPanel->createTab( array(
		    'name' => __( 'Import', 'ese' ),
		) );

		$export_tab->createOption( array(
			'name' => __( 'Help', 'ese' ),
			'id' => 'import_help',
			'type' => 'custom',
			'custom' => $this->cpt_actions_import_help(),
		) );

		$export_tab->createOption( array(
			'name' => __( 'Import XLS', 'ese' ),
			'id' => 'import_form',
			'type' => 'custom',
			'custom' => $this->cpt_actions_import_form(),
		) );
		
		/*************************************************
				IMPORT - END
		*************************************************/


		/*************************************************
				BACKUPS - START
		*************************************************/
		
		$export_tab = $adminPanel->createTab( array(
		    'name' => __( 'Backups', 'ese' ),
		) );

		$export_tab->createOption( array(
			'name' => __( 'Help', 'ese' ),
			'id' => 'backups_help',
			'type' => 'custom',
			'custom' => $this->cpt_actions_backups_help(),
		) );

		$export_tab->createOption( array(
			'name' => __( 'Database backups', 'ese' ),
			'id' => 'backups_form',
			'type' => 'custom',
			'custom' => $this->cpt_actions_backups_form(),
		) );
		
		/*************************************************
				BACKUPS - END
		*************************************************/
	}

	private function cpt_actions_export_help(){
		if( ! isset( $_GET['page'] ) || 'ese_p_s_database_v2-actions' != $_GET['page'] ){
			return;
		}
		if( isset( $_GET['tab'] ) && 'export' != $_GET['tab'] ){
			return;
		}

		ob_start(); ?>
		<p>
			<!-- TODO - matija - please help woth the text content -->
			<!-- Tukaj lahko izvozite vsebino baze v XLS obliki, jo uredite v EXCELu ali podobnem programu za urejanje tabel, nato pa jo uvozite nazaj v WP. -->
			Here you can export the database into XLS file type. Then you can use MS Excel of similar programs to edit the file and import in back to the website.
		</p>
		<p>
			<strong style="color: red;">Important !</strong> In downloaded XLS, do not edit first lines (EXPORT ID and HEADERS), also do not edit the row titles, otherwise the importing will not work. Thank you.
		</p>
		<p>
			<!-- TODO - matija - please help woth the text content -->
			<!-- <b>Če želite dodati zapis</b>, v prazno vrstico zapišite podatke, pri čemer prvi stolpec, ki je namenjen za ID, pustite prazen. Tako bo sistem vedel, da želite dodati novo vrstico / zapis. -->
			If you want to new data in the cells, leave the first column (reserved for ID) alone.
		</p>
		<p>
			<!-- TODO - matija - please help woth the text content -->
			<!-- <b>Če želite odstraniti zapis</b>, odstranite celotno vrstico ali pobrišite vsebino v celotni vrstici (ID in vso vsebino). Tako bo sistem vedel, da mora to vrstico / zapis odstraniti na spletni strani. -->
			If you want to remove the whole row, delete all data, including data in the first column (reseved for IDs).
		</p>
		<p>
			<!-- TODO - matija - please help woth the text content -->
			<!-- <b>Če želite urediti zapis</b>, poljubno uredite vsebino vrstice, pri čemer morate prvi stolpec (ID) pustiti pri miru / tak kot je. -->
			For just editing the data, leave first column intact (ID column). 
		</p>
		<?php return ob_get_clean();
	}

	private function cpt_actions_export_databases_list(){
		if( ! isset( $_GET['page'] ) || 'ese_p_s_database_v2-actions' != $_GET['page'] ){
			return;
		}
		if( isset( $_GET['tab'] ) && 'export' != $_GET['tab'] ){
			return;
		}

		$export_urls = $this->database_export_urls();

		ob_start(); ?>
		<ul>
			<?php foreach ($export_urls as $export_url => $database_title ) { ?>
			<li>
				<?php echo esc_html( $database_title ); ?>
				<a href="<?php echo esc_attr( $export_url ); ?>" target="_blank"><?php _e( 'Download', 'ese' ); ?></a>
			</li>

			<?php } ?>
		</ul>
		<?php return ob_get_clean();
	}

	private function cpt_actions_import_help(){
		if( ! isset( $_GET['page'] ) || 'ese_p_s_database_v2-actions' != $_GET['page'] ){
			return;
		}
		if( ! isset( $_GET['tab'] ) || 'import' != $_GET['tab'] ){
			return;
		}

		ob_start(); ?>
		<p>
			Here you can import XLS file, that was exported and edited from "Export" section 
		</p>
		<p>
			Additional information how to edit and import XLS file, is found in "Export" tab. 
		</p>
		<?php return ob_get_clean();
	}

	private function cpt_actions_import_form(){
		if( ! isset( $_GET['page'] ) || 'ese_p_s_database_v2-actions' != $_GET['page'] ){
			return;
		}
		if( ! isset( $_GET['tab'] ) || 'import' != $_GET['tab'] ){
			return;
		}

		ob_start();

		$import_data = $this->import_xls_handle();

		?>

		<style>

			.message {
				padding: 15px;
				display: block;
				
			}

			.error2 {
				background: red;
				color: #fff;

			}

			.ok {
				background: green;
				color: #fff;

			}

			.small-admin-panel {
				background: #f3f3f3;
				padding: 10px;
			}

		</style>

		<?php

		if( isset( $import_data['errors'] ) ){
			foreach ( $import_data['errors'] as $error ) {
				?>
				<div class="message error2"><?php echo esc_html( $error ); ?></div>
				<?php
			}
		}

		if( isset( $import_data['messages'] ) ){
			foreach ( $import_data['messages'] as $message ) {
				?>
				<div class="message ok"><?php echo esc_html( $message ); ?></div>
				<?php
			}
		}
		
		if( count( $import_data['errors'] ) > 0 ){

			?>
			<div data-scroll-to-me="" class="message error2">
				<?php _e( 'Looks like there was some problem. Please resolve the issues and start again.', 'ese' ); ?>
				</div>
				<br>
				<a class="button button-primary" href="<?php echo get_the_permalink(); ?>"><?php _e( 'Start again', 'ese' ); ?></a>
			
			<?php

		} else {

			switch ( $import_data['state'] ) {
				case 'preview':
					foreach ($import_data['hidden_inputs'] as $input_name => $input_value ) {
						?>
						<input type="hidden" name="<?php echo esc_attr( $input_name ); ?>" value="<?php echo esc_attr( $input_value ); ?>">
						<?php
					}

					?>

					<h5><?php _e( 'Preview pending changes', 'ese' ); ?></h5>
					<p class="small-admin-panel">
						<!-- TODO Matija - preveri tekste - ko narediš upload datoteke, ti pokaže kaj bo spremenjeno in to moraš potrditi  -->
						<?php echo __( 'No. of removed rows:', 'ese' ) . ' ' . $import_data['report']['remove_rows_ids']; ?><br>
						<?php echo __( 'No. of changed rows:', 'ese' ) . ' ' . $import_data['report']['update_rows']; ?><br>
						<?php echo __( 'No. of added rows:', 'ese' ) . ' ' . $import_data['report']['insert_rows']; ?>
					</p>
					<p>
						
						<input type="submit" value="<?php esc_attr_e( 'Confirm changes', 'ese' ); ?>" style="ouline:none; background: orange; border: none; color: #fff; margin-top: 20px; padding: 15px; cursor:pointer;">
					</p>
					<?php
					break;
				
				case 'process':
					?>
						<h5><?php _e( 'That is it, thank you!', 'ese' ); ?></h5>
						<p class="small-admin-panel">
							<!-- TODO Matija - pregled tekstov - situacija, ko si že zaključil import in je to samo report uvoza -->
							<?php echo __( 'No. of removed rows:', 'ese' ) . ' ' . $import_data['report']['remove_rows_ids']; ?><br>
							<?php echo __( 'No. of changed rows:', 'ese' ) . ' ' . $import_data['report']['update_rows']; ?><br>
							<?php echo __( 'No. of added rows:', 'ese' ) . ' ' . $import_data['report']['insert_rows']; ?>
						</p>
						<p>
							<a class="button button-primary" href="<?php echo get_the_permalink(); ?>"><?php _e( 'Start again', 'ese' ); ?></a>
						</p>
					<?php
					break;

				default:
					?>
					<p>
						<?php _e( 'Upload your XLS file that you exported and edited.', 'ese' ); ?>
					</p>

					<input type="hidden" name="ese-databases-import-xls-action" value="upload_xls">
					<input type="file" name="ese-databases-import-xls-file"><br><br>
					<input type="submit" value="<?php esc_attr_e( 'Upload', 'ese' ); ?>" class="button button-primary">
					<?php wp_nonce_field( 'ese-databases-import-xls-' . get_current_user_id(), 'ese-databases-import-xls-nonce' ); ?>

					<script type="text/javascript">
						jQuery(document).ready(function(){
							var $input = jQuery('input[name="ese-databases-import-xls-file"]');
							$input.closest('form').attr('enctype', 'multipart/form-data');
						});
					</script>

					<?php
					break;
			}
		}
		return ob_get_clean();
	}

	function cpt_actions_backups_help(){
		if( ! isset( $_GET['page'] ) || 'ese_p_s_database_v2-actions' != $_GET['page'] ){
			return;
		}
		if( ! isset( $_GET['tab'] ) || 'backups' != $_GET['tab'] ){
			return;
		}

		ob_start(); ?>
		<p>
			Here are the daily backups of all databases. Backups are retained for 30 days.
		</p>
		<p>
			
			For major changes, you can make manual backup from the list below.
		</p>
		<?php return ob_get_clean();
	}

	private function cpt_actions_backups_form(){
		if( ! isset( $_GET['page'] ) || 'ese_p_s_database_v2-actions' != $_GET['page'] ){
			return;
		}
		if( ! isset( $_GET['tab'] ) || 'backups' != $_GET['tab'] ){
			return;
		}

		$backup_data = $this->database_backups_handle();

		// TODO MAtija - prosim za olepšavo
		ob_start();
		?>
		<div class="databases-backups-holder">
			<input type="hidden" name="ese-databases-backups-action" value="">
			<input type="hidden" name="database" value="">
			<input type="hidden" name="backup" value="">
			<?php wp_nonce_field( 'ese-databases-backups-' . get_current_user_id(), 'ese-databases-backups-nonce' ); ?>

			<style type="text/css">
				ul.databases-backups-list {
					overflow: hidden;
				}
				ul.databases-backups-list li {
					overflow: hidden;
				}
				a.backup-action {
					float: right;
				}

				.databases-backups-holder ul.messages li {
					padding: 10px;
					border: 1px solid green;
					border-radius: 3px;
					background: green;
					color: #fff;
				}

				.databases-backups-holder ul.errors li {
					padding: 10px;
					background: red;
					color: #fff;
					border-radius: 3px;
				}

				.database-backups-list {
					display: none;
				}

				.show-backups .database-backups-list {
					display: block;
				}

			</style>
			<ul class="messages">
				<?php foreach ( $backup_data['messages'] as $txt ) { ?>
				<li><?php echo esc_html( $txt ); ?></li>
				<?php } ?>
			</ul>
			<div class="errors">
				<?php foreach ( $backup_data['errors'] as $txt ) { ?>
				<li><?php echo esc_html( $txt ); ?></li>
				<?php } ?>
			</ul>
			<p style="overflow: hidden;">
				<a href="#" class="button button-primary backup-action" data-database="all"><?php _e( 'Create backups for all databases', 'ese' ); ?></a>
			</p>
			<ul class="databases-backups-list">
				<?php
				$databases = $this->get_databases();
				foreach ($databases as $database) {
					$db_backups = $this->get_database_backups( $database->ID );
					?>
					<li style="overflow: hidden;" class="database-item">
						<b><?php echo esc_html( $database->post_title ); ?></b> (<?php echo count( $db_backups ); ?>)
						<a href="#" class="show-backups-action"><?php _e( 'Show / hide backups', 'ese' ); ?></a>
						<a href="#" class="button button-primary backup-action" data-database="<?php echo esc_attr( $database->ID ); ?>">
							<?php _e( 'Create backup', 'ese' ); ?>
						</a>
						<ul class="database-backups-list">
							<?php
							if( 0 == count( $db_backups ) ){
								?>
								<li><?php _e( 'No backups yet', 'ese' ); ?></li>
								<?php
							}
							foreach ( $db_backups as $db_backup ) {
								?>
								<li>
									<?php echo date( get_option( 'date_format' ) . " " . get_option( 'time_format' ), strtotime( $db_backup['date'] ) ); ?>
									<a href="#" class="restore-action" data-database="<?php echo esc_attr( $database->ID ); ?>" data-backup="<?php echo esc_attr( $db_backup['backup_id'] ); ?>">
										<?php _e( 'Restore', 'ese' ); ?>
									</a>
								</li>
								<?php
							}
							?>
						</ul>
					</li>
					<?php
				}
				?>	
			</ul>
			<script type="text/javascript">
				(function( $ ){
					var $db_bu = $(".databases-backups-holder");

					$db_bu.find('.backup-action').click(function( event ){
						event.preventDefault();
						$db_bu.find("input[name='ese-databases-backups-action']").val('create_backup');
						$db_bu.find("input[name='database']").val( $(this).attr('data-database') );
						$db_bu.closest('form').submit();
					});

					$db_bu.find('.restore-action').click(function( event ){
						event.preventDefault();
						if( ! confirm( <?php echo json_encode( __( 'Are you sure, you want to restore this database?', 'domain' ) ); ?> ) ){
							return;
						}

						$db_bu.find("input[name='ese-databases-backups-action']").val('restore_database');
						$db_bu.find("input[name='database']").val( $(this).attr('data-database') );
						$db_bu.find("input[name='backup']").val( $(this).attr('data-backup') );
						$db_bu.closest('form').submit();
					});

					$db_bu.find('.show-backups-action').click(function( event ){
						event.preventDefault();
						$(this).closest('.database-item').toggleClass('show-backups');
					});
				})(jQuery);
			</script>
		</div>
		<?php
		return ob_get_clean();
	}
	
	/*************************************************
			CPT ACTIONS PAGE - END
	*************************************************/




	/*************************************************
			EXPORT - START
	*************************************************/
	
	public function export_xls_handle(){

		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);

		if( ! isset( $_GET['n'] ) || ! wp_verify_nonce( $_GET['n'], self::CPT . '-export-' . get_current_user_id() ) ){
			_e( 'Nope ... nonce not ok.', 'ese' );
			die();
		}

		$post_id = isset( $_GET['db'] ) ? (int)$_GET['db'] : false;
		if( ! $post_id ){
			_e( 'Nope ... no database param in request.', 'ese' );
			die();
		}

		if( ! current_user_can( 'edit_posts' ) && ! $this->current_user_can_edit_databases() ){
			_e( 'Nope ... you do not have enough permissions to execute that actions.', 'ese' );
			die();
		}

		$post = get_post( $post_id );
		if( ! $post || self::CPT != $post->post_type ){
			_e( 'Nope ... database with that id does not exist.', 'ese' );
			die();
		}


		$columns = $this->database_columns( $post_id );
		$data = $this->database_data( $post_id, $columns );
		$xls_id = uniqid();
		$xls_name = 'database__' . $post_id . '__' . date('Y_m_d') . '__' . $xls_id . '.xls';
		$xls_tmp_path = $this->xlsTmpFolderPath();
		$xls_path = $this->xlsFolderPath() . $xls_name;

		require_once dirname( __FILE__ ) . '/xls-exporter/xls-exporter.php';

		$xls = new EusprSpanExtra_PreventionScienceDatabasesV2_XlsExporter();
		$xls->setId( $xls_id );
		$xls->setDatabaseId( $post_id );
		$xls->setDatabaseName( get_the_title( $post_id ) );
		$xls->setColumns( $columns );
		$xls->setData( $data );
		$xls->process();
		$xls->save( $xls_path );

		$export_data = array(
			'date' => date( 'Y-m-d H:i:s' ),
			'export_id' => $xls_id,
			'file_name' => $xls_name,
			'columns' => $columns,
		);
		add_post_meta( $post_id, self::META_EXPORTS, $export_data );

		$this->import_export_id = $xls_id;
		$this->database_log( $post_id, 0, self::ACTION_EXPORT );
		
		$xls->download( $xls_name );
		// $xls->output();
		die();
	}

	private function delete_export( $post_id, $export_id ){
		$export_data = $this->getExportData( $post_id, $export_id );

		if( ! $export_data ){
			die( "NO EXPORT DATA FOR " . $export_id );
		}

		$reference_file = $this->xlsFolderPath() . $export_data['file_name'];
		$uploaded_file = $this->xlsUploadFolderPath() . $export_id . '.xls';

		// delete uploaded and reference files
		@unlink( $reference_file );
		@unlink( $uploaded_file );
		
		// delete database export data (post meta)
		delete_post_meta( $post_id, self::META_EXPORTS, $export_data );
	}	
	
	/*************************************************
			EXPORT - END
	*************************************************/


	/*************************************************
			IMPORT XLS - START
	*************************************************/

	public function import_xls_handle(){
		
		$import_data = array(
			'state' => 'default',
			'errors' => array(),
			'messages' => array(),
			'hidden_inputs' => array(),
			'report' => array(),
		);

		// upload XLS
		if( isset( $_POST['ese-databases-import-xls-action'] ) && 'upload_xls' == $_POST['ese-databases-import-xls-action'] ){

			if( ! isset( $_POST['ese-databases-import-xls-nonce'] ) || ! wp_verify_nonce( $_POST['ese-databases-import-xls-nonce'], 'ese-databases-import-xls-' . get_current_user_id() ) ){
				// Preveri tekst - uvoz XLSja - se zgodi, ko je nekdo mimo orodja skušal narediti to akcijo - zlonamerni ljudje dobijo to sporočilo
				$import_data['errors'][] = __( 'Nope, nonce not ok! Please repeat the import the way it was ment to be.', 'ese' );
				return $import_data;
			}

			if( ! current_user_can( 'edit_posts' ) && ! $this->current_user_can_edit_databases() ){
				// Preveri tekst - uporabnik nima pravic in sploh ne bi smel pridet do orodja, kjer bi lahko oddal zahtevek za uvoz
				$import_data['errors'][] = __( 'Nope, you do not have enough permissions to execute that action.', 'ese' );
				return $import_data;
			}

			$uploaded_xls = isset( $_FILES[ 'ese-databases-import-xls-file' ] ) ? $_FILES[ 'ese-databases-import-xls-file' ] : false;

			if( ! $uploaded_xls ){
				// Preveri tekst - ko si pozabil uploadati XLS file
				$import_data['errors'][] = __( 'XLS file required', 'ese' );
				return $import_data;
			}

			// check and create folder for uploads
			$uploaded_xls_folder = $this->xlsUploadFolderPath();
			if( ! is_dir( $uploaded_xls_folder ) && ! wp_mkdir_p( $uploaded_xls_folder ) ){
				// Napake se ne bi smela nikoli prikazati, razen če bomo varnostno še bolj zaprli strežnik - gre se za situacijo, ko moramo ustvariti potrebne mape za EXPORT / IMPORT
				$import_data['errors'][] = __( 'Could not create upload folder', 'ese' );
				return $import_data;
			}

			$export_id = false;
			$post_id = false;

			// read xls file and get "header" information
			$xls_data = $this->xlsFileToArray( $uploaded_xls[ 'tmp_name' ] );

			foreach ( $xls_data as $xls_row ) {
				$first_column = $xls_row[0];
				$second_column = $xls_row[1];

				if( "#EXPORT_ID" == $first_column ){
					// export id
					$export_id = $second_column;
				}

				if( "#DATABASE_ID" == $first_column ){
					// export id
					$post_id = $second_column;
				}

				// if( "#ENCODING_CHECK_STRING" == $first_column ){
				// 	// export id
				// 	$encoding_check_string = $second_column;
				// }

				if( "#ID" == $first_column ){
					// next rows represent data
					// we stop reading the file
					break;
				}
			}

			if( ! $export_id ){
				// Tekst - Nismo našli zapisa za EXPORT_ID v XLS datoteke - to bi lahko uporabnik kaj pobrisal v XLS-ju in je zato ta napaka
				$import_data['errors'][] = __( 'Uploaded file has no export id information', 'ese' );
				return $import_data;
			}

			if( ! $post_id ){
				// Tekst - uporabnik se je verjetno igral s "header" informacijami v XLS datoteki
				$import_data['errors'][] = __( 'Uploaded file has no database id information', 'ese' );
				return $import_data;
			}

			if( self::CPT != get_post_type( $post_id ) ){
				// Tekst - ne najdemo baze za uploadan XLS - ali smo bazo pobrisali ali pa naj skuša uporabnik pojebati
				$import_data['errors'][] = __( 'Database for imported file does not exist', 'ese' );
				return $import_data;
			}

			// if( ! $encoding_check_string ){
			// 	// Tekst - uporabnik se je verjetno igral s "header" informacijami v XLS datoteki
			// 	$import_data['errors'][] = __( 'Uploaded file has no encoding check string information', 'ese' );
			// 	return $import_data;
			// }

			$export_data = $this->getExportData( $post_id, $export_id );
			if( ! $export_data ){
				// Tekst - ali nas sluša uporabnik pojebati ali pa je to datoteko (s tem export_id-jem) že uporabil / uploadal in je ne more še enkrat
				$import_data['errors'][] = __( 'No export info exists for uploaded file.', 'ese' );
				return $import_data;
			}

			$reference_file = $this->xlsFolderPath() . $export_data['file_name'];
			if( ! file_exists( $reference_file ) ){
				// Tekst - nekdo od nas (razvojalci) je nekaj naredil z datotekami ali pa je prišlo do napake v kodi - ta napaka se uporabniku ne bi smela zgoditi - nikoli
				$import_data['errors'][] = __( 'Reference file does not exist.', 'ese' );
				return $import_data;
			}

			// move uploaded file to uploads
			$uploaded_xls_tmp_name = $export_id . '.xls';
			if ( !@move_uploaded_file( $uploaded_xls[ 'tmp_name' ], $uploaded_xls_folder . $uploaded_xls_tmp_name ) ){
				// Tekst - napaka se bo zgodila, če bomo poostrili varnost na strežniku ali pa jim bo zmanjkalo prostora kot stranki - napak se ne bi smela zgoditi
				$import_data['errors'][] = __( 'Could not move uploaded file to uploads', 'ese' );
				return $import_data;
			}

			$import_data['hidden_inputs'] = array(
				'eid' => $export_id,
				'db' => $post_id,
			);
			$import_data['state'] = 'preview';
		}

		// process request check
		if( isset( $_POST['process'] ) && '1' == $_POST['process'] ){

			$import_data['state'] = 'process';

			if( ! isset( $_POST['n'] ) || ! wp_verify_nonce( $_POST['n'], 'ese-database-import-process-' . get_current_user_id() ) ){
				// Tekst - nekdo nas jebe v glavo in uporablja orodje, kot ga ne bi smel
				$import_data['errors'][] = __( 'Nope, nonce not ok! Please repeat the import the way it was ment to be.', 'ese' );
				return $import_data;
			}

			if( ! current_user_can( 'edit_posts' ) && ! $this->current_user_can_edit_databases() ){
				// Tekst - nekdo poskuša uporabljati orodje, kot ga ne bi smel in sploh ne bi smel videti orodja - navadnemu / pridnemu uporabniku se to ne bo pokazalo
				$import_data['errors'][] = __( 'Nope, you do not have enough permissions to execute that actions.', 'ese' );
				return $import_data;
			}
			
			$post_id = (int)$_POST['db'];
			$export_id = $_POST['eid'];
		}

		// import / process - pre check
		if( 'preview' == $import_data['state'] || 'process' == $import_data['state'] ){

			$export_data = $this->getExportData( $post_id, $export_id );
			if( ! $export_data ){
				// Tekst - če smo tukaj v procesu, bi tile podatki morali obstajati - če javi tole, je v drugem brskalniku že uploadal datoteko vmes oz v dveh brskalnikh poskuša uploadati isto datoteko
				$import_data['errors'][] = __( 'No export info exists for uploaded file. Please try exporting again.', 'ese' );
				return $import_data;
			}

			$reference_file = $this->xlsFolderPath() . $export_data['file_name'];
			if( ! file_exists( $reference_file ) ){
				// Tekst - nekdo od nas (razvijalci) je nekaj naredil z datotekami ali pa je prišlo do napake v kodi - ta napaka se uporabniku ne bi smela zgoditi - nikoli
				$import_data['errors'][] = __( 'Reference file does not exist.', 'ese' );
				return $import_data;
			}

			$uploaded_file = $this->xlsUploadFolderPath() . $export_id . '.xls';
			if( ! file_exists( $uploaded_file ) ){
				// Tekst - to se navadnemu uporabniku ne sme zgoditi. Ali je vmes nekdo sprožil import za isti file ali pa je prišlo do resnejše programske napake
				$import_data['errors'][] = __( 'Uploaded file does not exist or someone is uploading a XLS file at this moment. Please try again a bit later. Thank you.', 'ese' );
				return $import_data;
			}

			$reference_xls_data = $this->getXlsData( $this->xlsFileToArray( $reference_file ), $export_data['columns'] );
			$uploaded_xls_data = $this->getXlsData( $this->xlsFileToArray( $uploaded_file ), $export_data['columns'] );

			$remove_rows_ids = $this->getRemoveRowsIds( $reference_xls_data, $uploaded_xls_data );
			$update_rows = $this->getUpdateRows( $reference_xls_data, $uploaded_xls_data );
			$insert_rows = $this->getInsertRows( $reference_xls_data, $uploaded_xls_data );

		}

		// import preview finish
		if( 'preview' == $import_data['state'] ){

			$import_data['messages'][] = __( 'File uploaded. Please review pending changes and confirm them to complete the process', 'ese' );

			$import_data['hidden_inputs'] += array(
				'n' => wp_create_nonce( 'ese-database-import-process-' . get_current_user_id() ),
				'process' => 1,
			);

			$import_data['report']['remove_rows_ids'] = count( $remove_rows_ids );
			$import_data['report']['update_rows'] = count( $update_rows );
			$import_data['report']['insert_rows'] = count( $insert_rows );

			return $import_data;
		}

		// process finish
		if( 'process' == $import_data['state'] ){

			$this->import_export_id = $export_id;

			$this->database_log( $post_id, 0, self::ACTION_IMPORT );

			// remove rows
			foreach ( $remove_rows_ids as $remove_rows_id ) {
				$this->database_remove_row( $post_id, $remove_rows_id );
			}
			
			// update rows
			foreach ( $update_rows as $update_row_id => $update_row_data ) {
				$this->database_update_row( $post_id, $update_row_id, $update_row_data );
			}

			// inset rows
			foreach ( $insert_rows as $uploaded_xls_row_index => $insert_row ) {
				$row_id = $this->database_insert_row( $post_id, $insert_row );
				// remember new row id
				$uploaded_xls_data[ $uploaded_xls_row_index ]['id'] = $row_id;
			}

			// reset sort index column
			$this->database_reset_sort_index( $post_id, $uploaded_xls_data );

			$this->delete_export( $post_id, $export_id );

			$import_data['report']['remove_rows_ids'] = count( $remove_rows_ids );
			$import_data['report']['update_rows'] = count( $update_rows );
			$import_data['report']['insert_rows'] = count( $insert_rows );

			$import_data['messages'][] = __( 'Process completed.', 'ese' );

			return $import_data;
		}

		return $import_data;
	}

	private function xlsFileToArray( $xls_file_path ){
		require_once plugin_dir_path( ESE_FILE ) . '/libs/PHPExcel.php';

		try {
			$objPHPExcel = PHPExcel_IOFactory::load( $xls_file_path );
			$sheet = $objPHPExcel->getSheet(0); 
			$highestRow = $sheet->getHighestRow(); 
			$highestColumn = $sheet->getHighestColumn();

			$xls_data = array();
			for ($row = 1; $row <= $highestRow; $row++){ 
			    //  Read a row of data into an array
			    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
			                                    NULL,
			                                    TRUE,
			                                    FALSE);
			    $xls_data[] = $rowData[0];
			}

			return $xls_data;

		} catch(Exception $e) {
			return array();
		}
	}

	private function getExportData( $post_id, $export_id ){
		$exports = get_post_meta( $post_id, self::META_EXPORTS, false );
		foreach ($exports as $export_data ) {
			if( $export_id == $export_data['export_id'] ){
				return $export_data;
			}
		}
		return false;
	}

	private function getXlsData( $xls_data, $columns ){
		// prep row data template
		$row_data_keys = array('id');	// row id is required
		foreach ( $columns as $column ) {
			for ($i=0; $i < $column['num_fields']; $i++) { 
				$row_data_keys[] = $column['id'] . '_' . $i;
			}
		}

		$rows_data = array();

		$parsing_rows = false;
		foreach ( $xls_data as $xls_row ) {
			if( ! $parsing_rows ){
				// reading info data
				$first_column = isset( $xls_row[ 0 ] ) ? $xls_row[ 0 ] : '';

				if( "#ID" == $first_column ){
					// next rows represent data
					// we stop reading the file
					$parsing_rows = true;
					continue;
				}

			} else {
				// now we are parsing rows / data
				$row_data = array_fill_keys( $row_data_keys, '' );
				foreach ($row_data_keys as $row_data_key_index => $row_data_key ) {
					$row_data_column_value = isset( $xls_row[ $row_data_key_index ] ) ? $xls_row[ $row_data_key_index ] : '';
					$row_data[ $row_data_key ] = $row_data_column_value;
				}

				$rows_data[] = $row_data;
			}
		}

		return $rows_data;
	}

	private function getRemoveRowsIds( $reference_xls_data, $uploaded_xls_data ){
		$missing_ids = array();
		foreach ($reference_xls_data as $r_row ) {
			$r_row_exists_in_uploaded_xls = false;

			$r_row_id = $r_row['id'];
			if( empty( $r_row_id ) ){
				continue;
			}

			foreach ( $uploaded_xls_data as $u_row ) {
				$u_row_id = $u_row['id'];
				if( $u_row_id == $r_row_id ){
					$r_row_exists_in_uploaded_xls = true;
					break;
				}
			}

			if( ! $r_row_exists_in_uploaded_xls ){
				$missing_ids[] = $r_row_id;
			}
		}

		return $missing_ids;
	}

	private function getUpdateRows( $reference_xls_data, $uploaded_xls_data ){

		$updated_rows = array();

		foreach ($reference_xls_data as $r_row ) {

			$r_row_id = $r_row['id'];
			if( empty( $r_row_id ) ){
				continue;
			}

			$row_has_changed = false;

			foreach ( $uploaded_xls_data as $u_row ) {
				$u_row_id = $u_row['id'];

				if( $u_row_id != $r_row_id ){
					continue;
				}

				foreach ( $r_row as $column_id => $r_column_value ) {
					$u_column_value = $u_row[ $column_id ];
					if( $r_column_value != $u_column_value ){
						$row_has_changed = true;
						break;
					}
				}

				// we found matching row, stop searching and go to next reference row
				break;
			}

			if( $row_has_changed ){
				$updated_rows[ $r_row_id ] = $u_row;
			}
		}

		return $updated_rows;
	}

	private function getInsertRows( $reference_xls_data, $uploaded_xls_data ){
		$insert_rows = array();
		foreach ($uploaded_xls_data as $u_row_key => $u_row ) {
			// skip rows, that already have row id
			if( ! empty( $u_row['id'] ) ){
				continue;
			}

			// check if row has any data
			$row_data_exists = false;
			foreach ( $u_row as $column_key => $column_value ) {
				if( ! empty( $column_value ) ){
					$row_data_exists = true;
					break;
				}
			}

			// skip rows with no data
			if( ! $row_data_exists ){
				continue;
			}

			// unset ID column
			unset( $u_row['id'] );

			// add row to return variable
			$insert_rows[ $u_row_key ] = $u_row;
		}

		return $insert_rows;
	}
	
	/*************************************************
			IMPORT XLS - END
	*************************************************/

	/*************************************************
			IMPORT / EXPORT UTILS - START
	*************************************************/
	
	private function xlsTmpFolderPath(){
		$upload_dir = wp_upload_dir();
		return $upload_dir['basedir'] . '/euspr_span/xls_export_tmp/';
	}

	private function xlsFolderPath(){
		$upload_dir = wp_upload_dir();
		return $upload_dir['basedir'] . '/euspr_span/xls_export/';
	}

	private function xlsUploadFolderPath(){
		$upload_dir = wp_upload_dir();
		return $upload_dir['basedir'] . '/euspr_span/xls_upload/';
	}

	private function backupsFolderPath(){
		$upload_dir = wp_upload_dir();
		return $upload_dir['basedir'] . '/euspr_span/backups/';
	}

	private function database_export_urls(){
		$export_urls = array();

		// list databases
		$databases = $this->get_databases();
		foreach ($databases as $database) {
			$export_database_url = add_query_arg( array( 'action' => self::CPT . '-export', 'db' => $database->ID, 'n' => wp_create_nonce( self::CPT . '-export-' . get_current_user_id() ) ), admin_url('admin-ajax.php') );
			$database_title = $database->post_title;
			$export_urls[ $export_database_url ] = $database_title;
		}

		return $export_urls;
	}

	private function current_user_can_edit_databases(){
		$sett = EusprSpanExtra_SettingsPage::getInstance();

		$edit_databases_page_id = $sett->param('edit_databases_page');
		$edit_databases_page = get_page( $edit_databases_page_id );

		global $oUserAccessManager;
		$oUamAccessHandler = $oUserAccessManager->getAccessHandler();

		if( count($oUamAccessHandler->getUserGroupsForObject($edit_databases_page->post_type, $edit_databases_page->ID)) > 0 ){
			if( $oUamAccessHandler->checkObjectAccess($edit_databases_page->post_type, $edit_databases_page->ID) ){
				return true;
			}
		}
		
		return false;
	}

	private function array_move(&$a, $oldpos, $newpos) {
	    if ($oldpos==$newpos) {return;}
	    array_splice($a,max($newpos,0),0,array_splice($a,max($oldpos,0),1));
	}
	
	/*************************************************
			IMPORT / EXPORT UTILS - END
	*************************************************/


	/*************************************************
			DATABASE DATA HANDLING - START
	*************************************************/
	
	private function get_databases(){
		$databases = get_posts(array(
					'nopaging' => true,
					'post_type' => self::CPT,
					// 'post_status' => 'published',
					'orderby' => 'menu_order',
					'order' => 'ASC',
				));
		return $databases;
	}

	private function database_columns( $post_id ){
		$columns = get_post_meta( $post_id, '_database_columns', true );
		if( ! is_array( $columns ) ){
			$columns  = array();
		}
		return $columns;
	}

	private function database_data( $post_id, $columns ){
		global $wpdb;

		$database_data = array();

		$data_columns = array();

		if( is_object( $columns ) ){
			$columns = json_decode( json_encode( $columns ), true );
		}

		foreach ( $columns as $column_data ) {
			if( is_object( $column_data ) ){
				$column_data = (array)$column_data;
			}
			for ($i=0; $i < (int)$column_data['num_fields']; $i++) { 
				$data_columns[] = $column_data['id'] . '_' . $i;
			}
		}

		$table_data = $wpdb->prefix . self::DB_TABLE_DATA;
		$qry = $wpdb->prepare( "SELECT edd_id, edd_data FROM $table_data WHERE edd_database_id = %d ORDER BY edd_sort_index ASC", $post_id );
		$rows = $wpdb->get_results( $qry, ARRAY_A );

		foreach ( $rows as $row ) {
			
			$tmp_row_data = array();

			$row_edd_data_arr = @json_decode( $row['edd_data'], true );
			if( ! $row_edd_data_arr || ! is_array( $row_edd_data_arr ) ){
				$row_edd_data_arr = array();
			}

			foreach ( $data_columns as $data_column ) {
				$tmp_row_data[ $data_column ] = isset( $row_edd_data_arr[ $data_column ] ) ? $row_edd_data_arr[ $data_column ] : "";
			}

			$database_data[ (int)$row['edd_id'] ] = $tmp_row_data;

		}

		return $database_data;
	}

	private function database_remove_row( $post_id, $row_id ){
		global $wpdb;

		$old_data = $this->database_row_data( $post_id, $row_id );

		$wpdb->delete( $wpdb->prefix . self::DB_TABLE_DATA, array( 'edd_database_id' => (int)$post_id, 'edd_id' => (int)$row_id ) );

		$this->database_log( $post_id, $row_id, self::ACTION_DELETE, $old_data, array() );
	}

	private function database_update_row( $post_id, $update_row_id, $update_row_data ){
		global $wpdb;

		$old_data = $this->database_row_data( $post_id, $update_row_id );
		
		$wpdb_table = $wpdb->prefix . self::DB_TABLE_DATA;
		$wpdb_data = array(
			'edd_data' => json_encode( $update_row_data ),
			'edd_updated' => date('Y-m-d'),
			'edd_user_id' => get_current_user_id(),
		);
		$wpdb_where = array( 'edd_id' => $update_row_id, 'edd_database_id' => $post_id );
		$wpdb->update( $wpdb_table, $wpdb_data, $wpdb_where );
		
		$this->database_log( $post_id, $update_row_id, self::ACTION_UPDATE, $old_data, $update_row_data );
	}

	private function database_insert_row( $post_id, $row_data ){
		
		global $wpdb;

		$insert_id = null;

		// TODO - escape data
		$wpdb->insert(
			$wpdb->prefix . self::DB_TABLE_DATA,
			array(
				'edd_database_id' => $post_id,
				'edd_updated' => date('Y-m-d H:i:s'),
				'edd_data' => json_encode( $row_data ),
				'edd_user_id' => get_current_user_id(),
			)
		);
		$insert_id = $wpdb->insert_id;

		$this->database_log( $post_id, $insert_id, self::ACTION_INSERT, array(), $row_data );
		

		return $insert_id;
	}

	private function database_reset_sort_index( $post_id, $uploaded_xls_data ){
		// TODO - nekako je potrebno narediti nov vrstni red
		return;

		global $wpdb;

		$table_data = $wpdb->prefix . self::DB_TABLE_DATA;
		$qry = $wpdb->prepare( "SELECT edd_id, edd_sort_index FROM $table_data WHERE edd_database_id = %d ORDER BY edd_sort_index ASC", $post_id );
		$rows = $wpdb->get_results( $qry, ARRAY_A );

		// get current sort
		$sort_by_id = array();
		foreach ( $rows as $row ) {
			$sort_by_id[] = (int)$row['edd_id'];
		}

		// get uploaded sort id
		$uploaded_sort_by_id = array();
		foreach ( $uploaded_xls_data as $uploaded_xls_data_row ) {
			$uploaded_sort_by_id[] = (int)$uploaded_xls_data_row['id'];
		}

		$sort_changes_happened = true;
		while ( $sort_changes_happened ) {
			$sort_changes_happened = false;
			// compare / change new sort
			foreach ( $sort_by_id as $id_key => $id ) {
				$after_id = isset( $sort_by_id[ $id_key - 1 ] ) ? $sort_by_id[ $id_key - 1 ] : 0;

				$uploaded_id_key = array_search( $id, $uploaded_sort_by_id );
				$uploaded_after_id = false !== $uploaded_id_key && isset( $uploaded_sort_by_id[ $uploaded_id_key - 1 ] ) ? $uploaded_sort_by_id[ $uploaded_id_key - 1 ] : 0;

				if( $after_id != $uploaded_after_id && in_array( $uploaded_after_id, $sort_by_id ) ){
					$move_after_key = array_search( $uploaded_after_id, $sort_by_id );

					$this->array_move( $sort_by_id, $id_key, $move_after_key + 1 );
				}
			}
		}

		// update sort in DB
		$sort_index = 1;
		foreach ( $sort_by_id as $edd_id ) {
			$wpdb->update(
				$table_data,
				array( 'edd_sort_index' => $sort_index ),
				array( 'edd_database_id' => (int)$post_id, 'edd_id' => $edd_id )
			);
			$sort_index++;
		}
	}

	private function database_log( $post_id, $row_id, $action, $old_data = array(), $new_data = array() ){
		
		global $wpdb;

		$wpdb->insert(
			$wpdb->prefix . self::DB_TABLE_LOG,
			array(
				'edl_export_id' => $this->import_export_id,
				'edl_action' => $action,
				'edl_edd_id' => $row_id,
				'edd_database_id' => $post_id,
				'edl_user_id' => get_current_user_id(),
				'edl_old_edd_data' => json_encode( $old_data ),
				'edl_new_edd_data' => json_encode( $new_data ),
			)
		);
	}

	private function database_row_data( $post_id, $row_id ){
		global $wpdb;

		$wpdb_table = $wpdb->prefix . self::DB_TABLE_DATA;

		$row_data_qry = $wpdb->prepare("SELECT edd_data FROM $wpdb_table WHERE edd_id = %d AND edd_database_id = %d", $row_id, $post_id);
		$row_data_string = $wpdb->get_var( $row_data_qry );
		$row_data = @json_decode( $row_data_string, true );

		if( ! is_array( $row_data ) ){
			$row_data = array();
		}

		return $row_data;
	}

	private function database_revert_log( $post_id, $revert_log_id ){

		global $wpdb;

		$wpdb_table = $wpdb->prefix . self::DB_TABLE_LOG;
		$log_row_qry = $wpdb->prepare( "SELECT * FROM $wpdb_table WHERE edd_database_id = %d AND edl_id = %d", $post_id, $revert_log_id );
		$log_row = $wpdb->get_row( $log_row_qry, ARRAY_A );

		if( ! $log_row ){
			return;
		}

		$log_row['edl_old_edd_data'] = @json_decode( $log_row['edl_old_edd_data'], true );
		if( ! is_array( $log_row['edl_old_edd_data'] ) ){
			$log_row['edl_old_edd_data'] = array();
		}
		$log_row['edl_new_edd_data'] = @json_decode( $log_row['edl_new_edd_data'], true );
		if( ! is_array( $log_row['edl_new_edd_data'] ) ){
			$log_row['edl_new_edd_data'] = array();
		}

		if( self::ACTION_INSERT == $log_row['edl_action'] ){
			$this->database_remove_row( $post_id, $log_row['edl_edd_id'] );
		}

		if( self::ACTION_DELETE == $log_row['edl_action'] ){
			$this->database_insert_row( $post_id, $log_row['edl_old_edd_data'] );
		}

		if( self::ACTION_UPDATE == $log_row['edl_action'] ){
			$this->database_update_row( $post_id, $log_row['edl_edd_id'], $log_row['edl_old_edd_data'] );
		}

		$wpdb->delete( $wpdb_table, array( 'edl_id' => $revert_log_id ) );
	}

	
	/*************************************************
			DATABASE DATA HANDLING - END
	*************************************************/


	/*************************************************
			CRON - START
	*************************************************/
	
	function cron_setup( $force = false ){
		$installed_cron_ver = get_option( self::CRON_VERSION_OPTION );

		if ( $force || self::CRON_VERSION != $installed_cron_ver ) {

			$timestamp = wp_next_scheduled( self::CRON_ACTION_HOOK );

			//If $timestamp == false schedule daily backups since it hasn't been done previously
			if( $timestamp == false ){
				wp_schedule_event( time(), 'daily', self::CRON_ACTION_HOOK );
			}

			if( ! $installed_cron_ver ){
				add_option( self::CRON_VERSION_OPTION, self::CRON_VERSION );
			} else {
				update_option( self::CRON_VERSION_OPTION, self::CRON_VERSION );
			}
		}
	}

	function cron_deactivate(){
		wp_clear_scheduled_hook( self::CRON_ACTION_HOOK );
	}
	
	function cron_process(){
		$this->delete_old_database_revisions();
		$this->delete_old_database_exports();
		$this->delete_old_database_backups();
		$this->create_database_backups();
	}

	function delete_old_database_revisions(){
		global $wpdb;

		$older_then_time = time() - self::KEEP_REVISIONS_IN_SECONDS;

		$table = $wpdb->prefix . self::DB_TABLE_LOG;
		$delete_qry = $wpdb->prepare( "DELETE FROM $table WHERE edl_updated < %s", date('Y-m-d H:i:s', $older_then_time) );

		$wpdb->query( $delete_qry );
	}

	function delete_old_database_exports(){
		$databases = $this->get_databases();
		foreach ( $databases as $database ) {
			$exports = get_post_meta( $database->ID, self::META_EXPORTS, false );
			foreach ($exports as $export_data ) {
				if( strtotime( $export_data['date'] ) < time() - self::KEEP_REVISIONS_IN_SECONDS ){
					$this->delete_export( $post_id, $export_data['export_id'] );
				}
			}
		}
	}

	function delete_old_database_backups(){
		$databases = $this->get_databases();
		foreach ( $databases as $database ) {
			$backups = get_post_meta( $database->ID, self::META_BACKUPS, false );
			foreach ($backups as $backup_data ) {
				if( strtotime( $backup_data['date'] ) < time() - self::KEEP_REVISIONS_IN_SECONDS ){
					$this->delete_database_backup( $post_id, $backup_data['backup_id'] );
				}
			}
		}
	}

	private function create_database_backups(){
		$databases = $this->get_databases();
		foreach ($databases as $database) {
			$this->create_database_backup( $database->ID );
		}
	}
	
	/*************************************************
			CRON - END
	*************************************************/


	/*************************************************
			DATABASE BACKUP - START
	*************************************************/
	
	function database_backups_handle(){

		$backup_data = array(
			'errors' => array(),
			'messages' => array(),
		);

		if( ! isset( $_POST['ese-databases-backups-action'] ) ){
			return $backup_data;
		}

		if( ! isset( $_POST['ese-databases-backups-nonce'] ) || ! wp_verify_nonce( $_POST['ese-databases-backups-nonce'], 'ese-databases-backups-' . get_current_user_id() ) ){
			$backup_data['errors'][] = __( 'Nope, nope, nope, ... Nonce not ok! Please repeat the backups action the way it was ment to.', 'ese' );
			return $backup_data;
		}

		if( ! current_user_can( 'edit_posts' ) ){
			$backup_data['errors'][] = __( 'Nope, nope, nope, ... you do not have enough permissions to execute that actions.', 'ese' );
			return $backup_data;
		}

		$action = $_POST['ese-databases-backups-action'];

		if( 'create_backup' == $action ){
			$backup_databases = array();
			$database_param = isset( $_POST['database'] ) ? $_POST['database'] : "";
			if( 'all' == $database_param ){
				$databases = $this->get_databases();
				foreach ($databases as $database) {
					$backup_databases[] = $database->ID;
				}
			} else {
				$backup_databases[] = (int)$database_param;
			}

			foreach ( $backup_databases as $backup_database_id ) {
				$this->create_database_backup( $backup_database_id );
			}

			$backup_data['messages'][] = count( $backup_databases ) > 1 ? __( 'Backups created', 'ese' ) : __( 'Backup created', 'ese' );

			return $backup_data;
		}

		if( 'restore_database' == $action ){
			$restore_database = array();
			$restore_database = isset( $_POST['database'] ) ? (int)$_POST['database'] : "";
			$restore_backup = isset( $_POST['backup'] ) ? $_POST['backup'] : "";
			$restore_success = $this->restore_database_backup( $restore_database, $restore_backup );

			if( $restore_success ){
				$backup_data['messages'][] = __( 'Database restored from backup', 'ese' );
			} else {
				
				$backup_data['errors'][] = __( 'Database restored failed. Please try again or contact info@2gika.si.', 'ese' );
			}

			return $backup_data;
		}

		return $backup_data;
	}

	function create_database_backup( $post_id ){
		
		$columns = $this->database_columns( $post_id );

		$json_data = array(
			'columns' => $columns,
			'data' => $this->database_data( $post_id, $columns ),
		);

		$backups_folder = $this->backupsFolderPath();
		if( ! is_dir( $backups_folder ) && ! wp_mkdir_p( $backups_folder ) ){
			wp_die( __( 'Can not create required folder for backups', 'ese' ), __( 'Error creating folder', 'ese' ) );
			return;
		}

		$backup_id = uniqid();

		$json_file_name = 'database_backup__' . $post_id . '__' . date('Y_m_d__H_m_s') . '__' . $backup_id . '.json';

		$json_path = $backups_folder . $json_file_name;

		$fp = fopen( $json_path, 'w' );
		fwrite( $fp, json_encode( $json_data ) );
		fclose( $fp );

		$meta_data = array(
			'date' => date( 'Y-m-d H:i:s' ),
			'backup_id' => $backup_id,
			'file_name' => $json_file_name,
			'columns' => $columns,
		);

		add_post_meta( $post_id, self::META_BACKUPS, $meta_data );

		$this->export_import_id = uniqid('backup-');

		$this->database_log( $post_id, 0, self::ACTION_BACKUP );
	}

	function restore_database_backup( $post_id, $backup_id ){

		$backup_data = false;
		$db_backups = $this->get_database_backups( $post_id );
		foreach ($db_backups as $db_backup) {
			if( $backup_id == $db_backup['backup_id'] ){
				$backup_data = $db_backup;
				break;
			}
		}

		if( ! $backup_data ){
			return false;
		}

		// check file exists
		$backups_folder = $this->backupsFolderPath();
		$json_file_name = $backup_data['file_name'];

		$json_file_path = $backups_folder . $json_file_name;
		if( ! file_exists( $json_file_path ) ){
			return false;
		}

		$json_data = @json_decode( file_get_contents( $json_file_path ), true );

		$this->export_import_id = uniqid('restore-');

		$this->database_log( $post_id, 0, self::ACTION_RESTORE );
		
		$this->create_database_backup( $post_id );

		// delete current rows
		$columns = $this->database_columns( $post_id );
		$current_database_data = $this->database_data( $post_id, $columns );
		foreach ( $current_database_data as $row_id => $row ) {
			$this->database_remove_row( $post_id, $row_id );
		}
		// override columns
		update_post_meta( $post_id, '_database_columns', $json_data['columns'] );
		// insert data / add rows
		foreach ($json_data['data'] as $row_data ) {
			$this->database_insert_row( $post_id, $row_data );
		}
		
		return true;
	}

	function delete_database_backup( $post_id, $backup_id ){
		$backup_data = $this->getBackupData( $post_id, $backup_id );

		if( ! $backup_data ){
			return;
		}

		$backups_folder = $this->backupsFolderPath();
		$json_file_name = $backup_data['file_name'];
		$json_file_path = $backups_folder . $json_file_name;

		// delete files
		@unlink( $json_file_path );
		
		// delete database export data (post meta)
		delete_post_meta( $post_id, self::META_BACKUPS, $backup_data );
	}

	function getBackupData( $post_id, $backup_id ){
		$backups = get_post_meta( $post_id, self::META_BACKUPS, false );
		foreach ($backups as $backup_data ) {
			if( $backup_id == $backup_data['backup_id'] ){
				return $backup_data;
			}
		}
		return false;
	}

	function get_database_backups( $post_id ){
		$backups = get_post_meta( $post_id, self::META_BACKUPS, false );
		return $backups;
	}
	
	/*************************************************
			DATABASE BACKUP - END
	*************************************************/


	/*************************************************
			SHORTCODES - START
	*************************************************/

	public function shortcode_process() {
		// get all contents
		$databases = $this->get_databases();

		foreach ( $databases as $database_key => $database ) {
			$database->icon = ! empty( get_post_meta( $database->ID, '_database_icon', true ) ) ? get_post_meta( $database->ID, '_database_icon', true ) : 'fa-university';
			$database->columns = $this->database_columns( $database->ID );
			$database->data = $this->database_data( $database->ID, $database->columns );
			$databases[ $database_key ] = $database;
		}

		ob_start();
		
		include dirname( __FILE__ ) . '/views/prevention-science-databases-filter.php';

		$this->enqueue_scripts();

		return ob_get_clean();
	}

	public function shortcode_edit_process(){
		ob_start();


		$data = array(
			'export_urls' => $this->database_export_urls(),
			'import' => $this->import_xls_handle(),
		);

		include dirname( __FILE__ ) . '/views/edit-databases.php';

		return ob_get_clean();
	}
	
	/*************************************************
			SHORTCODES - END
	*************************************************/

}