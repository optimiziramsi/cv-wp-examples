(function($){

	// return;

	var $psd, psd_id, $psd_filters, filters, searchString, filterRequired;

	function init(){
		$psd = $(".prevention-science-databses-wrapper");

		if( 0 == $psd.length ){
			return;
		}

		init_database_switching();

		init_more_info();

		$psd.find(".psd-filter").keyup( filterRows );
		$psd.find(".psd-filter").change( filterRows );
	}

	function init_database_switching(){
		$(".psd-list li").click( onDatabaseClick );
		$(".psd-list li:first").click();
	}

	function onDatabaseClick( event ){
		event.preventDefault();

		var li_psd_id = $(this).attr('data-psd-id');

		if( psd_id == li_psd_id ){
			return;
		}

		psd_id = li_psd_id;

		$('span.psd-name').text( $(this).attr('data-psd-name') );

		setDatabase();
	}

	function setDatabase(){

		$(".psd-list li").removeClass('active');
		$(".psd-list li[data-psd-id='"+psd_id+"']").addClass('active');

		$(".ese-psd-table").removeClass('active');
		$(".ese-psd-table[data-psd-id='"+psd_id+"']").addClass('active');

		$(".ese-psd-filters").removeClass('active');
		$(".ese-psd-filters[data-psd-id='"+psd_id+"']").addClass('active');

		initDatabaseFilters();
	}


	function init_more_info(){

		$(document).on("click", ".more-info a.show-more-info", toggleMoreInfo);

	}

	function toggleMoreInfo( event ){
		event.preventDefault();

		$(this).closest('.more-info').toggleClass('active');
	}








	function initDatabaseFilters(){
		$psd_filters = $psd.find(".filters .ese-psd-filters[data-psd-id='"+psd_id+"'] select.custom-filter");
		if( 0 == $psd_filters.length ){
			return;
		}
		$psd_filters
			.select2({
				placeholder: ese_psd.filter_placeholder
			})
			.change( filterRows )
			;
		clearFilters();
	}

	function clearFilters(){
		$psd_filters.select2("val", "");

		$psd.find(".psd-filter").val("");

		filterRows( true );
	}

	var filterRowsTimeout;
	function filterRows( now ){

		filters = {};
		$psd_filters.each(function( i, e ){
			var $select = $(e);

			var vals = $select.val();
			var column_key = $select.attr('data-column-key');

			if( vals && vals.length > 0 ){
				filters[ column_key ] = vals;
			}
		});

		searchString = $psd.find(".psd-filter").val().toLowerCase();

		filterRequired = Object.keys(filters).length > 0 || "" != searchString;

		$psd.find('.filter-loading').show();

		if( true !== now ){
			clearTimeout( filterRowsTimeout );
			filterRowsTimeout = setTimeout( filterRowsProcess, 800 );
			return;
		}

		filterRowsProcess();
	}

	function filterRowsProcess(){

		var $rows = $psd.find(".ese-psd-table[data-psd-id='"+psd_id+"'] div.Table-row");

		if( filterRequired ){

			$rows.removeClass("in-filter");
			// add classes "filtered" to filtered rows
			var $filtered_rows = $psd.find(".ese-psd-table[data-psd-id='"+psd_id+"'] div.Table-row");
			$.each( filteredRowsSelector( filters, searchString ), function( index, filter_content ){
				$filtered_rows = $filtered_rows.filter( filter_content );
			});
			$filtered_rows.addClass('in-filter');

			// add class "filtered" on table, so non filtered rows will hide
			$psd.find(".ese-psd-table[data-psd-id='"+psd_id+"'] div.Table").addClass('filter-enabled');

			$psd.find('.filtered-stats').show();
			$psd.find('.filtered-stats .num-filtered').text( $rows.filter(".in-filter").length );

		} else {
			// this table is not filtered any more
			$psd.find(".ese-psd-table[data-psd-id='"+psd_id+"'] div.Table").removeClass('filter-enabled');
			$rows.removeClass("in-filter");
			$psd.find('.filtered-stats').hide();
		}

		$psd.find('.stats .num-all').text( $rows.length );

		$psd.find('.filter-loading').hide();
	}

	function filteredRowsSelector( filters, searchString ){
		var filter_contents = [];

		$.each(filters, function( header_key, values ){
			var single_filter_contents = [];
			$.each(values, function( value_index, value ){
				single_filter_contents.push( "[data-psd-col"+header_key+"='"+value+"']" );
			});
			filter_contents.push( single_filter_contents.join(', ') );
		});

		if( "" != searchString ){
			filter_contents.push( "[data-psd-search*='"+searchString+"']" );
		}

		return filter_contents;
	}



	init();

})(jQuery);