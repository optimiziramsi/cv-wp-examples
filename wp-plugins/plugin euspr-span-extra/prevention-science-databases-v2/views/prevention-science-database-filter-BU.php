<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

?>
<div class="prevention-science-databses-wrapper">
	<div class="filter-holder">
		<ul class="link-tabs psd-list">
			<?php
			foreach ( $databases as $database_key => $database ) {
				?>
				<li data-psd-id="<?php echo esc_attr( $database_key ); ?>" data-psd-name="<?php echo esc_attr( $database->post_title ); ?>">
					<a href="#" class="button <?php echo esc_attr( substr( $database->icon, 3) ); ?>">
						<i class="fa <?php echo esc_attr( $database->icon ); ?>"></i>
						<?php echo esc_html( $database->post_title ); ?>
					</a>
				</li>
				<?php
				$class_active = '';
			}
			?>
		</ul>
		
		<h5>Seaching in: <span class="psd-name"></span></h5>
		
		<div class="filters">

			<?php
			foreach ( $databases as $database_key => $database ) {
				echo '<div class="ese-psd-filters" data-psd-id="'.esc_attr( $database_key ).'">';
				foreach ( $database->headers as $col => $header ) {
					$header_settings = isset( $database->columnsSettings[ $col ] ) && is_object( $database->columnsSettings[ $col ] ) ? $database->columnsSettings[ $col ] : (object)array();

					if( isset( $header_settings->isFilter ) && $header_settings->isFilter ){
						$options = array_column( $database->data, $col );
						$options = array_unique( $options );
						$options = array_filter( $options );
						asort($options);
						?>
						<label for="filter-1">Filter column <?php echo esc_html( $header ); ?>:</label>
						<select class="browser-default custom-filter" multiple data-column-key="<?php echo esc_attr( $col ); ?>">
							<?php foreach ( $options as $option ) { ?>
							<option value="<?php echo esc_attr( $option ); ?>"><?php echo esc_html( $option ); ?></option>
							<?php } ?>
						</select>
						<?php
					}
				}
				echo '</div>';
			}
			?>
		</div>

		<div class="main-search">
			<div class="search-holder">
				<input type="search" class="psd-filter" placeholder="<?php esc_attr_e( 'Search the database', 'ese' ); ?>">
				<div class="clearfix"></div>
			</div>
		</div>

		<div class="stats">
			<?php _e( 'All rows:', 'ese' ); ?> <span class="num-all"></span>
			<span class="filtered-stats">
				<?php _e( 'Filtered:', 'ese' ); ?> <span class="num-filtered"></span>
			</span>
			<span class="filter-loading">
				<i class="spinner-small icon-play"></i>
				<?php _e( 'Filtering ... please wait', 'ese' ); ?>
				
			</span>
		</div>
	</div>


	<div class="filter-results-holder">

		<?php foreach ( $databases as $database_key => $database ): ?>

		<div class="ese-psd-table" data-psd-id="<?php echo esc_attr( $database_key ); ?>">
			<div class="Table">
				<div class="Table-row Table-header">

					<?php
					$list_column_attr_class = array();
					$attr_data_header = array();

					$more_info_column = array();

					$email_columns = array();
					$website_columns = array();
					$filter_columns = array();
					$search_columns = array();
					?>

					<?php foreach ( $database->headers as $col => $header ):

						$header_settings = isset( $database->columnsSettings[ $col ] ) && is_object( $database->columnsSettings[ $col ] ) ? $database->columnsSettings[ $col ] : (object)array();

						if( isset( $header_settings->isViewOnList ) && $header_settings->isViewOnList ){
							$isWide = isset( $header_settings->isWide ) && $header_settings->isWide;
							$isShort = isset( $header_settings->isShort ) && $header_settings->isShort;

							$classes = array('Table-row-item');
							if( $isWide ){ $classes[] = 'u-Flex-grow3'; }
							elseif( ! $isShort ){ $classes[] = 'u-Flex-grow1'; }

							$list_column_attr_class[ $col ] = esc_attr( implode( ' ', $classes ) );

							?>
							
							<div class="<?php echo $list_column_attr_class[ $col ]; ?>"><?php echo esc_html( $header ); ?></div>

							<?php
							
						} else {
							$more_info_column[ $col ] = esc_html( $header );
						}

						$attr_data_header[ $col ] = esc_attr( $header );
						if( isset( $header_settings->isEmail ) && $header_settings->isEmail ){
							$email_columns[] = $col;
						}
						if( isset( $header_settings->isWebsite ) && $header_settings->isWebsite ){
							$website_columns[] = $col;
						}
						if( isset( $header_settings->isFilter ) && $header_settings->isFilter ){
							$filter_columns[] = $col;
						}
						if( isset( $header_settings->isSearch ) && $header_settings->isSearch ){
							$search_columns[] = $col;
						}

					endforeach; ?>
				</div>

				<?php
				foreach ($database->data as $row_key => $row_data ):
					// echo "<pre>";
					// var_dump( $row_data );
					// echo "</pre>";

					$row_echo_data = array(
						'row_attributes' => array(),
						'list_columns' => array(),
						'other_columns' => array(),
					);

					// data row - columns
					foreach ( $list_column_attr_class as $col => $row_classes ):
						$row_column_content = isset( $row_data[ $col ] ) && ! empty( $row_data[ $col ] ) ? $row_data[ $col ] : '';
						$row_column_content = trim( $row_column_content );
						$html_content = "";
						if( in_array( $col, $email_columns ) && is_email( $row_column_content ) ){
							$html_content = sprintf('<a href="mailto:%s">%s</a>', esc_attr( $row_column_content ), esc_html( $row_column_content ) );
						}
						elseif( in_array( $col, $website_columns ) && ! empty( $row_column_content ) ){
							$html_content = sprintf('<a href="%s" target="_blank">%s</a>', esc_attr( esc_url( $row_column_content ) ), esc_html( $row_column_content ) );
						}
						elseif( ! empty( $row_column_content ) ) {
							$html_content = esc_html( $row_column_content );
						}

						$row_echo_data['list_columns'][ $col ] = $html_content;
					endforeach;
					// other row data - if exists
					foreach ( $more_info_column as $col => $column_header_html_esc ) {
						$other_column_value = isset( $row_data[ $col ] ) && ! empty( $row_data[ $col ] ) ? $row_data[ $col ] : '';
						$other_column_value = trim( $other_column_value );

						if( in_array( $col, $email_columns ) && is_email( $other_column_value ) ){
							$html_content_esc = sprintf('<a href="mailto:%s">%s</a>', esc_attr( $other_column_value ), esc_html( $other_column_value ) );
						}
						elseif( in_array( $col, $website_columns ) && ! empty( $other_column_value ) ){
							$html_content_esc = sprintf('<a href="%s" target="_blank">%s</a>', esc_attr( esc_url( $other_column_value ) ), esc_html( $other_column_value ) );
						}
						else {
							$html_content_esc = esc_html( $other_column_value );
						}
						
						if( ! empty( $other_column_value ) ){
							$row_echo_data['other_columns'][] = '<b>'.$column_header_html_esc.':</b> ' . $html_content_esc;
						}
					}
					// filter and search attributes
					$row_attributes = array();
					foreach ( $filter_columns as $col ):
						$row_attributes['data-psd-col' . $col] = isset( $row_data[ $col ] ) && ! empty( $row_data[ $col ] ) ? esc_attr( trim( $row_data[ $col ] ) ) : '';
					endforeach;
					foreach( $search_columns as $col ):
						if( ! isset( $row_attributes['data-psd-search'] ) ){
							$row_attributes['data-psd-search'] = "";
						}
						$row_attributes['data-psd-search'] .= isset( $row_data[ $col ] ) && ! empty( $row_data[ $col ] ) ? esc_attr( strtolower( trim( $row_data[ $col ] ) ) ) . "\n" : '';
					endforeach;
					foreach ( $row_attributes as $attribute_name => $attribute_value ) {
						$row_echo_data['row_attributes'][] = sprintf( '%s="%s"', $attribute_name, esc_attr( $attribute_value ) ) ;
					}

					

					/*******************  ECHO ROW  *******************/
					?>

					<div class="Table-row" <?php echo implode( " ", $row_echo_data['row_attributes'] ); ?>>
						
						<?php /*******************  ECHO LIST COLUMNS  *******************/ ?>
						<?php foreach ($row_echo_data['list_columns'] as $col => $col_html_esc ): ?>

						<div class="<?php echo $list_column_attr_class[ $col ]; ?>" data-header="<?php echo $attr_data_header[ $col ]; ?>"><?php echo $col_html_esc; ?></div>
						
						<?php endforeach; ?>
						

						<?php /*******************  ECHO MORE INFO / OTHER COLUMNS  *******************/ ?>
						<?php if( count( $row_echo_data['other_columns'] ) > 0 ): ?>

						<div class="more-info">
							<a href="#more" class="show-more-info"><?php _e( 'Show more data', 'ese' ); ?></a>
							<ul class="more-info-content">

								<?php foreach ( $row_echo_data['other_columns'] as $html_esc ): ?>

								<li><?php echo $html_esc; ?></li>

								<?php endforeach; ?>

							</ul>
						</div>
						
						<?php endif; ?>
					</div>

				<?php endforeach; ?>
			</div>
			
			<?php
			/*
			<a href="#" class="button excel">Download CVS <span class="size">(500 kb)</span></a>
			<a href="#" class="button pdf">Download PDF</a>
			*/
			?>
		</div>

		<?php endforeach; ?>

	</div>
</div>