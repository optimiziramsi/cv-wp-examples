<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

?>
<div class="ese-db-v2-log">
	<ul class="logs">
	<?php
	$last_date = "";
	$data_splitter = " | ";

	$revert_row_html = "";
	ob_start(); ?>
	<span class="log-revert-row">
		<a href="#" class="revert-action">
			<?php _e( 'Revert', 'ese' ); ?>
		</a>
		<span class="revert-pending-action">
			<?php _e( 'Revert pending', 'ese' ); ?>
			<a href="#" class="revert-cancel-action">
				<?php _e( 'Cancel revert', 'ese' ); ?>
			</a>
		</span>
	</span>
	<?php
	$revert_row_html = ob_get_clean();

	foreach ( $logs as $export_id => $log ) {
		if( $last_date != $log['date'] ){
			$last_date = $log['date'];
			?>
			<li><h4><?php echo esc_html( $log['date'] ); ?></h4></li>
			<?php
		}

		?>
		<li class="log-item">
			<?php echo esc_html( $log['user_email'] ); ?> | 
			<?php _e( 'Added:', 'ese' ); ?> <?php echo esc_html( $log['count_inserted'] ); ?>
			<?php _e( 'Changed:', 'ese' ); ?> <?php echo esc_html( $log['count_updated'] ); ?>
			<?php _e( 'Removed:', 'ese' ); ?> <?php echo esc_html( $log['count_deleted'] ); ?>
			
			<span class="actions">
				<a href="#" class="show-logs-action">
					<?php _e( 'Show logs', 'ese' ); ?>
				</a>
				<a href="#" class="hide-logs-action">
					<?php _e( 'Hide logs', 'ese' ); ?>
				</a> | 
				<a href="#" class="revert-all">
					<?php _e( 'Revert all / none', 'ese' ); ?>
				</a>
			</span>

			<ul class="log-logs">
				<?php foreach ($log['logs'] as $logs_log ) { ?>
					<li class="logs-log-item" data-log-id="<?php echo esc_attr( $logs_log['edl_id'] ); ?>">
						<?php _e( 'Action:', 'ese' ); ?>
						<?php
							$logs_log_data = "";
							switch ( $logs_log['edl_action'] ) {
								case self::ACTION_BACKUP:
									_e( 'Backup database', 'ese' );
									break;
								case self::ACTION_RESTORE:
									_e( 'Restore database', 'ese' );
									break;
								case self::ACTION_EXPORT:
									_e( 'Downloaded XLS', 'ese' );
									break;
								case self::ACTION_IMPORT:
									_e( 'Uploaded XLS', 'ese' );
									break;
								case self::ACTION_INSERT:
									_e( 'Added row', 'ese' );

									echo $revert_row_html;

									foreach ($columns as $field_key => $field_label) {
										$log_row_column_value = isset( $logs_log['edl_new_edd_data'][ $field_key ] ) ? $logs_log['edl_new_edd_data'][ $field_key ] : "";

										if( empty( $log_row_column_value ) ){
											$log_row_column_value = '/';
										}

										if( ! empty( $logs_log_data ) ){
											$logs_log_data .= $data_splitter;
										}

										$logs_log_data .= "<b>".esc_html( $field_label ).":</b> " . esc_html( $log_row_column_value );
									}
									break;
								case self::ACTION_UPDATE:
									_e( 'Updated row', 'ese' );

									echo $revert_row_html;

									$logs_log_data_old = "";
									foreach ($columns as $field_key => $field_label) {
										$log_row_column_value = isset( $logs_log['edl_old_edd_data'][ $field_key ] ) ? $logs_log['edl_old_edd_data'][ $field_key ] : "";

										if( empty( $log_row_column_value ) ){
											$log_row_column_value = '/';
										}

										if( ! empty( $logs_log_data_old ) ){
											$logs_log_data_old .= $data_splitter;
										}

										$logs_log_data_old .= "<b>".esc_html( $field_label ).":</b> " . esc_html( $log_row_column_value );
									}

									$logs_log_data_new = "";
									foreach ($columns as $field_key => $field_label) {
										$log_row_column_value = isset( $logs_log['edl_new_edd_data'][ $field_key ] ) ? $logs_log['edl_new_edd_data'][ $field_key ] : "";

										if( empty( $log_row_column_value ) ){
											$log_row_column_value = '/';
										}

										if( ! empty( $logs_log_data_new ) ){
											$logs_log_data_new .= $data_splitter;
										}

										$logs_log_data_new .= "<b>".esc_html( $field_label ).":</b> " . esc_html( $log_row_column_value );
									}

									$logs_log_data = $logs_log_data_old . "<br>" . $logs_log_data_new;
									break;
								case self::ACTION_DELETE:
									_e( 'Removed row', 'ese' );

									echo $revert_row_html;

									foreach ($columns as $field_key => $field_label) {
										$log_row_column_value = isset( $logs_log['edl_old_edd_data'][ $field_key ] ) ? $logs_log['edl_old_edd_data'][ $field_key ] : "";

										if( empty( $log_row_column_value ) ){
											$log_row_column_value = '/';
										}

										if( ! empty( $logs_log_data ) ){
											$logs_log_data .= $data_splitter;
										}

										$logs_log_data .= "<b>".esc_html( $field_label ).":</b> " . esc_html( $log_row_column_value );
									}
									break;
							}
						?> | 
						<?php _e( 'Date:', 'ese' ); ?> <?php echo esc_html( $logs_log['date'] ); ?><br>
						
						<small>
							<?php echo $logs_log_data; ?>
						</small>
					</li>
				<?php } ?>
			</ul>
		</li>
		<?php
	}
	?>
	</ul>
</div>