<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

$sett = EusprSpanExtra_SettingsPage::getInstance();
$edit_databases_url = get_permalink( $sett->param('edit_databases_page') );

?>
<div class="prevention-science-databses-wrapper">
	<?php
	$databases_js_data = array();
	$databases_js_data['databases'] = array();

	?>
	<div class="filter-holder">
		<ul class="link-tabs psd-list">
			<?php
			foreach ( $databases as $database_key => $database ) {
				?>
				<li data-psd-id="<?php echo esc_attr( $database_key ); ?>" data-psd-name="<?php echo esc_attr( $database->post_title ); ?>">
					<a href="#" class="button <?php echo esc_attr( substr( $database->icon, 3) ); ?>">
						<i class="fa <?php echo esc_attr( $database->icon ); ?>"></i>
						<?php echo esc_html( $database->post_title ); ?>
					</a>
				</li>
				<?php
				$class_active = '';
			}
			?>

		</ul>
		
		<div class="clearfix"></div>
		
		<div class="edit-databases">
			<a title="<?php esc_attr_e( 'Edit databases', 'ese' ); ?>" href="<?php echo esc_attr( $edit_databases_url ); ?>" class="button-orange button-circle">
				<i class="fa fa-edit"></i>
			</a>
		</div>
		
		<h5>Seaching in: <span class="psd-name"></span></h5>
		
		<div class="filters">

			<?php
			foreach ( $databases as $database_key => $database ) {
				echo '<div class="ese-psd-filters" data-psd-id="'.esc_attr( $database_key ).'">';
				foreach ( $database->columns as $column ) {
					$column_settings = (object)$column;

					if( isset( $column_settings->isFilter ) && 'yes' == $column_settings->isFilter ){
						$column_filter_options = array();
						foreach ( $database->data as $data_row ) {
							for ($field_index=0; $field_index < $column_settings->num_fields; $field_index++) { 
								$row_column_field_key = $column_settings->id . '_' . $field_index;
								$column_filter_options[] = isset( $row_column_field_key ) ? $data_row[ $row_column_field_key ] : "";
							}
						}
						$column_filter_options = array_unique( $column_filter_options );
						$column_filter_options = array_filter( $column_filter_options );
						asort( $column_filter_options );
						?>
						<label for="filter-1">Filter column <?php echo esc_html( $column_settings->label ); ?>:</label>
						<select class="browser-default custom-filter" multiple data-column-key="<?php echo esc_attr( 'col_'.$column_settings->id.'_values' ); ?>">
							<?php foreach ( $column_filter_options as $column_filter_option ) { ?>
							<option value="<?php echo esc_attr( $column_filter_option ); ?>"><?php echo esc_html( $column_filter_option ); ?></option>
							<?php } ?>
						</select>
						<?php
					}
				}
				echo '</div>';
			}
			?>
		</div>

		<div class="main-search">
			<div class="search-holder">
				<input type="search" class="psd-filter" placeholder="<?php esc_attr_e( 'Search the database', 'ese' ); ?>">
				<div class="clearfix"></div>
			</div>
		</div>

		<div class="stats">
			<?php _e( 'All rows:', 'ese' ); ?> <span class="num-all"></span>
			<span class="filtered-stats">
				<?php _e( 'Filtered:', 'ese' ); ?> <span class="num-filtered"></span>
			</span>
			<span class="filter-loading">
				<i class="spinner-small icon-play"></i>
				<?php _e( 'Filtering ... please wait', 'ese' ); ?>
				
			</span>
		</div>
	</div>

	<div class="filter-results-holder">
		<?php foreach ( $databases as $database_key => $database ):

			$database_js_data = array();
			$database_js_data['columns_default'] = array();
			$database_js_data['search_columns'] = array();
			$database_js_data['rows'] = array();

			?>

		<div class="ese-psd-table" data-psd-id="<?php echo esc_attr( $database_key ); ?>">
			<div class="Table">
				<div class="Table-row Table-header">

					<?php
					$list_column_attr_class = array();
					$attr_data_header = array();

					$more_info_column = array();

					$email_columns = array();
					$website_columns = array();
					$search_columns = array();
					?>

					<?php foreach ( $database->columns as $column ):

						$column_settings = (object)$column;

						if( isset( $column_settings->isViewOnList ) && 'yes' == $column_settings->isViewOnList ){
							$isWide = isset( $column_settings->isWide ) && 'yes' == $column_settings->isWide;
							$isShort = isset( $column_settings->isShort ) && 'yes' == $column_settings->isShort;

							$classes = array('Table-row-item');
							if( $isWide ){ $classes[] = 'u-Flex-grow3'; }
							elseif( ! $isShort ){ $classes[] = 'u-Flex-grow1'; }
							$classes[] = 'truncate';

							$list_column_attr_class[ $column_settings->id ] = esc_attr( implode( ' ', $classes ) );
							$database_js_data['columns_default'][ 'col_'.$column_settings->id.'_html' ] = $column_settings->label;

							?>
							
							<div class="<?php echo $list_column_attr_class[ $column_settings->id ]; ?>"><?php echo esc_html( $column_settings->label ); ?></div>

							<?php
							
						} else {
							$more_info_column[ $column_settings->id ] = esc_html( $column_settings->label );
						}

						$attr_data_header[ $column_settings->id ] = esc_attr( $column_settings->label );
						if( isset( $column_settings->isEmail ) && 'yes' == $column_settings->isEmail ){
							$email_columns[] = $column_settings->id;
						}
						if( isset( $column_settings->isWebsite ) && 'yes' == $column_settings->isWebsite ){
							$website_columns[] = $column_settings->id;
						}
						if( isset( $column_settings->isSearch ) && 'yes' == $column_settings->isSearch ){
							$database_js_data['search_columns'][] = 'col_'.$column_settings->id.'_values';
						}

					endforeach; ?>
					<div class="Table-row-item u-Flex-grow1"><?php echo esc_html_e( 'Read more', 'ese' ); ?></div>
				</div>

				<?php
				foreach ( $database->data as $data_row ):

					$row_js_data = array();

					// data row - columns
					foreach ( $database->columns as $column ):

						$column_settings = (object)$column;

						if( isset( $column_settings->isViewOnList ) && 'yes' == $column_settings->isViewOnList ){
							$row_column_contents = array();

							for ($field_index=0; $field_index < $column_settings->num_fields; $field_index++) { 
								$row_column_field_key = $column_settings->id . '_' . $field_index;
								$row_column_content = isset( $data_row[ $row_column_field_key ] ) ? $data_row[ $row_column_field_key ] : "";
								$row_column_content = trim( $row_column_content );
								$row_column_contents[] = $row_column_content;
							}

							$row_column_contents = array_unique( $row_column_contents );
							$row_column_contents = array_filter( $row_column_contents );

							$row_js_data['col_'.$column_settings->id.'_values'] = $row_column_contents;

							foreach ( $row_column_contents as $row_column_content_key => $row_column_content ) {

								$html_content = "";
								if( 'yes' == $column_settings->isEmail && is_email( $row_column_content ) ){
									$html_content = sprintf('<a href="mailto:%s">%s</a>', esc_attr( $row_column_content ), esc_html( $row_column_content ) );
								}
								elseif( 'yes' == $column_settings->isWww && ! empty( $row_column_content ) ){
									$html_content = sprintf('<a href="%s" target="_blank">%s</a>', esc_attr( esc_url( $row_column_content ) ), esc_html( $row_column_content ) );
								}
								elseif( ! empty( $row_column_content ) ) {
									$html_content = esc_html( $row_column_content );
								}

								$row_column_contents[ $row_column_content_key ] = $html_content;
							}

							$row_column_contents = array_unique( $row_column_contents );
							$row_column_contents = array_filter( $row_column_contents );

							$row_column_html = implode("<br>", $row_column_contents);

							$row_js_data['col_'.$column_settings->id.'_html'] = $row_column_html;

						}
					endforeach;
					// other row data - if exists
					$row_js_data['other_columns_html'] = '';
					foreach ( $database->columns as $column ):

						$column_settings = (object)$column;

						if( isset( $column_settings->isViewOnList ) && 'no' == $column_settings->isViewOnList ){

							$row_column_contents = array();

							for ($field_index=0; $field_index < $column_settings->num_fields; $field_index++) { 
								$row_column_field_key = $column_settings->id . '_' . $field_index;
								$row_column_content = isset( $data_row[ $row_column_field_key ] ) ? $data_row[ $row_column_field_key ] : "";
								$row_column_content = trim( $row_column_content );
								$row_column_contents[] = $row_column_content;
							}

							$row_column_contents = array_unique( $row_column_contents );
							$row_column_contents = array_filter( $row_column_contents );

							$row_js_data['col_'.$column_settings->id.'_values'] = $row_column_contents;

							foreach ( $row_column_contents as $row_column_content_key => $row_column_content ) {

								$html_content = "";
								if( 'yes' == $column_settings->isEmail && is_email( $row_column_content ) ){
									$html_content = sprintf('<b>%s:</b> <a href="mailto:%s">%s</a>', esc_html( $column_settings->label ), esc_attr( $row_column_content ), esc_html( $row_column_content ) );
								}
								elseif( 'yes' == $column_settings->isWww && ! empty( $row_column_content ) ){
									$html_content = sprintf('<b>%s:</b> <a href="%s" target="_blank">%s</a>', esc_html( $column_settings->label ), esc_attr( esc_url( $row_column_content ) ), esc_html( $row_column_content ) );
								}
								elseif( ! empty( $row_column_content ) ) {
									$html_content = sprintf('<b>%s:</b> %s', esc_html( $column_settings->label ), esc_html( $row_column_content ) );
								}

								$row_column_contents[ $row_column_content_key ] = $html_content;
							}

							$row_column_contents = array_unique( $row_column_contents );
							$row_column_contents = array_filter( $row_column_contents );

							$row_column_html = implode("<br>", $row_column_contents);

							if( ! empty( $row_column_html ) ){
								if( ! empty( $row_js_data['other_columns_html'] ) ){
									$row_js_data['other_columns_html'] .= "<br>";
								}
								$row_js_data['other_columns_html'] .= $row_column_html;
							}

						}

					endforeach;

					$database_js_data['rows'][] = $row_js_data;

				endforeach; ?>
			</div>
			
			<?php
			
			// <a href="#" class="button excel">Download CVS <span class="size">(500 kb)</span></a>
			// <a href="#" class="button pdf">Download PDF</a>
			?>


			<?php
				// ********************** ROW TEMPLATE HTML ***********************
			?>
			<div class="psd-row-template">

				<div class="Table-row">
					<?php
					// *******************  ECHO LIST COLUMNS  *******************
					?>
					<?php foreach ( $list_column_attr_class as $col_id => $row_classes ): ?>

					<div class="<?php echo $row_classes; ?>" data-header="<?php echo $attr_data_header[ $col_id ]; ?>" data-psd-col="<?php echo esc_attr( $col_id ); ?>">Here will be col <?php echo esc_html( $col_id ); ?> data</div>
					
					<?php endforeach; ?>
					<div class="Table-row-item u-Flex-grow1"><a href="#readmore" class="psd-readmore"><?php echo esc_html_e( 'Read more', 'ese' ); ?></a></div>
				</div>
			</div>
		</div>
		
		<?php $databases_js_data['databases'][ $database_key ] = $database_js_data; ?>

		<?php endforeach; ?>
	</div>
	
	<!-- Modal Structure -->
	<div id="psd-modal" class="modal">
		<div id="psd-modal-content" class="modal-content">
			<h5>Country</h5>
			<p>Slovenia</p>
			<h5>Topic</h5>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident commodi dolore voluptatibus suscipit ipsam ducimus voluptate aut reprehenderit delectus repellendus.</p>
			<h5>Mail</h5>
			<p><a href="mailto:info@2gika.si">info@2gika.si</a></p>
			<h5>University</h5>
			<p>
				Consectetur adipisicing elit. Eos, neque!
			</p>


		</div>
		<div class="modal-footer">
			<a href="#!" class="modal-action modal-close right"><?php _e( 'Close', 'ese' ); ?></a>
		</div>
	</div>

	<?php /********************** js data ***********************/ ?>
	<script type="text/javascript">
		var psd_data = <?php echo json_encode( $databases_js_data ); ?>;
	</script>
</div>