<?php

class EusprSpanExtra_SettingsPage {

	private static $instance = null;
	private $params;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){
		add_action( 'tf_create_options', array( $this, 'plugin_options' ) );
	}

	function plugin_options(){
		$titan = TitanFramework::getInstance( 'ese' );

		$adminPanel = $titan->createAdminPanel( array(
			'id' => 'ese-options',
		    'name' => __( 'EUSPR / SPAN', 'ese' ),
		    'parent' => 'options-general.php',
		) );

		/*************************************************
				FOOTER - START
		*************************************************/
		
		$footer_tab = $adminPanel->createTab( array(
		    'name' => __( 'Footer', 'ese' ),
		) );

		$footer_tab->createOption( array(
			'name' => __( 'Cookies page', 'ese' ),
			'id' => 'footer_cookies_page',
			'type' => 'select-pages',
			'desc' => __( 'Page where cookie link shows', 'ese' ),
		) );

		$footer_tab->createOption( array(
			'name' => __( 'Supporting organisations page', 'ese' ),
			'id' => 'footer_supporting_organisations_page',
			'type' => 'select-pages',
			'desc' => __( 'Page supporting organisations', 'ese' ),
		) );
		
		/*************************************************
				FOOTER - END
		*************************************************/



		/*************************************************
				GOOGLE ANALYTICS - START
		*************************************************/
		
		$google_analytics_tab = $adminPanel->createTab( array(
		    'name' => __( 'Google Analytics', 'ese' ),
		) );

		$google_analytics_tab->createOption( array(
			'name' => __( 'Tracking ID', 'ese' ),
			'id' => 'google_tracking_id',
			'type' => 'text',
			'desc' => __( 'Google Analytics tracking id', 'ese' ),
		) );
		
		/*************************************************
				GOOGLE ANALYTICS - END
		*************************************************/




		/*************************************************
				GOOGLE RECAPTCHA - START
		*************************************************/
		
		$google_recaptcha_tab = $adminPanel->createTab( array(
		    'name' => __( 'Google Recaptcha', 'ese' ),
		    		'enabled' => true,
		'language' => 'en',
		'secret' => '6LcS7xgTAAAAAAuRaqqle8O50zH8Wd3CB6K2Vd_a',
		'site_key' => '6LcS7xgTAAAAAPzcfYcQCdO8nI0RTtGlrKna8fa_',
		) );

		// Matija vključil rekapčo




		$google_recaptcha_tab->createOption( array(
			'name' => __( 'Enabled', 'ese' ),
			'id' => 'google_recaptcha_enabled',
			'type' => 'checkbox',
			'desc' => __( 'Should system use google recaptcha.', 'ese' ),
		) );

		$google_recaptcha_tab->createOption( array(
			'name' => __( 'Language', 'ese' ),
			'id' => 'google_recaptcha_language',
			'type' => 'text',
			'desc' => __( 'Google recaptcha language.', 'ese' ),
		) );

		$google_recaptcha_tab->createOption( array(
			'name' => __( 'Secret', 'ese' ),
			'id' => 'google_recaptcha_secret',
			'type' => 'text',
			'desc' => __( 'Google recaptcha secret.', 'ese' ),
		) );

		$google_recaptcha_tab->createOption( array(
			'name' => __( 'Site key', 'ese' ),
			'id' => 'google_recaptcha_site_key',
			'type' => 'text',
			'desc' => __( 'Google recaptcha site key.', 'ese' ),
		) );
		
		/*************************************************
				GOOGLE RECAPTCHA - END
		*************************************************/



		/*************************************************
				EUSPR SPAN HELP PAGE - START
		*************************************************/
		
		$euspr_span_help_tab = $adminPanel->createTab( array(
		    'name' => __( 'Help page', 'ese' ),
		) );

		$euspr_span_help_tab->createOption( array(
			'name' => __( 'Help page url', 'ese' ),
			'id' => 'euspr_span_help_page_url',
			'type' => 'text',
			'desc' => __( 'This page url will be used as help page when on editor. This should be private page.', 'ese' ),
		) );
		
		/*************************************************
				EUSPR SPAN HELP PAGE - END
		*************************************************/



		/*************************************************
				EDIT DATABASES on FRONT - START
		*************************************************/
		
		$edit_databases_tab = $adminPanel->createTab( array(
		    'name' => __( 'Edit Databases', 'ese' ),
		) );

		$edit_databases_tab->createOption( array(
			'name' => __( 'Edit databases page', 'ese' ),
			'id' => 'edit_databases_page',
			'type' => 'select-pages',
			'desc' => __( 'Page where users can edit databases', 'ese' ),
		) );
		
		/*************************************************
				EDIT DATABASES on FRONT - END
		*************************************************/


		$adminPanel->createOption( array(
		    'type' => 'save',
		) );

	}

	function param( $param_name ){
		$titan = TitanFramework::getInstance( 'ese' );
		return $titan->getOption( $param_name );
	}

}