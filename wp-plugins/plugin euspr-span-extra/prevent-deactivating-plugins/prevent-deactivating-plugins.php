<?php

class EusprSpanExtra_PreventDeactivatingPlugins {

	private static $instance = null;

	private $prevent_deactivation = array(
		'mimetypes-link-icons/mime_type_link_images.php',
		'jetpack/jetpack.php',
		'rotatingtweets/rotatingtweets.php',
		'simple-custom-post-order/simple-custom-post-order.php',
		'wp-hide-dashboard/wp-hide-dashboard.php',
		'wp-mail-smtp/wp_mail_smtp.php',
		'wp-session-manager/wp-session-manager.php',
		'user-access-manager/user-access-manager.php',
		'titan-framework/titan-framework.php',
		'euspr-span-extra/euspr-span-extra.php',
	);

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		if( ! is_admin() ){
			return;
		}

		add_filter( 'plugin_action_links', array( $this, 'disable_plugin_deactivation' ), 99, 4 );
	}

	function disable_plugin_deactivation( $actions, $plugin_file, $plugin_data, $context ) {
		// Remove edit link for all
		if ( array_key_exists( 'edit', $actions ) ){
			unset( $actions['edit'] );
		}
		// Remove deactivate link for crucial plugins
		if ( array_key_exists( 'deactivate', $actions ) && in_array( $plugin_file, $this->prevent_deactivation ) ) {
			unset( $actions['deactivate'] );
		}
		return $actions;
	}


}