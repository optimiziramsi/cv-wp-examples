<?php

class EusprSpanExtra_Partners {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		// register custom post type
		add_action( 'init', array( $this, 'register_custom_post_type' ) );
		// manage sup. org. list columns
		add_filter( 'manage_ese_partner_posts_columns', array( $this, 'manage_ese_partner_posts_columns') );

		// shortcode for listing sup. org.
		add_shortcode( 'partners_list', array( $this, 'shortcode_partners_process' ) );

	}


	/*************************************************
			Partner CUSTOM POST TYPE - START
	*************************************************/
	
	/**
	* Registers a new post type
	* @uses $wp_post_types Inserts new post type object into the list
	*
	* @param string  Post type key, must not exceed 20 characters
	* @param array|string  See optional args description above.
	* @return object|WP_Error the registered post type object, or an error object
	*/
	function register_custom_post_type() {

		$labels = array(
			'name'                => __( 'Partners', 'ese' ),
			'singular_name'       => __( 'Partner', 'ese' ),
			'add_new'             => _x( 'Add New Partner', 'ese', 'ese' ),
			'add_new_item'        => __( 'Add New Partner', 'ese' ),
			'edit_item'           => __( 'Edit Partner', 'ese' ),
			'new_item'            => __( 'New Partner', 'ese' ),
			'view_item'           => __( 'View Partner', 'ese' ),
			'search_items'        => __( 'Search Partners', 'ese' ),
			'not_found'           => __( 'No Partners found', 'ese' ),
			'not_found_in_trash'  => __( 'No Partners found in Trash', 'ese' ),
			'parent_item_colon'   => __( 'Parent Partner:', 'ese' ),
			'menu_name'           => __( 'Partners', 'ese' ),
		);

		$args = array(
			'labels'                   => $labels,
			'hierarchical'        => false,
			'description'         => 'Partners CPT',
			'taxonomies'          => array(),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => 'edit.php?post_type=ese_partner',
			'show_in_admin_bar'   => false,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-groups',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => false,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'supports'            => array( 'title', 'editor', 'revisions' )
		);

		register_post_type( 'ese_partner', $args );
	}

	public function manage_ese_partner_posts_columns( $columns ){
		$use_columns = array();
		foreach ( $columns as $column_key => $column_title ) {
			// skip / remove columns
			if( in_array( $column_key, array('date','uam_access') ) ){
				continue;
			}

			// copy default column
			$use_columns[ $column_key ] = $column_title;

			// rename title label
			if( "title" == $column_key ){
				$use_columns[ $column_key ] = __( 'Partner Name', 'ese' );
			}
		}

		return $use_columns;
	}
	
	/*************************************************
			Partner CUSTOM POST TYPE - END
	*************************************************/



	/*************************************************
			SHORTCODES - START
	*************************************************/
	
	public function shortcode_partners_process() {
		// get all contents
		$partners = get_posts(array(
					'nopaging' => true,
					'post_type' => 'ese_partner',
					// 'post_status' => 'published',
					'orderby' => 'menu_order',
					'order' => 'ASC',
				));

		ob_start();
		include dirname( __FILE__ ) . '/views/partners-list.php';
		return ob_get_clean();
	}
	
	/*************************************************
			SHORTCODES - END
	*************************************************/

}