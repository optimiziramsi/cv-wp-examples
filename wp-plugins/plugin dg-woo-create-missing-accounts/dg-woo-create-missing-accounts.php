<?php
/**
* Plugin Name: WooCommerce Create Missing Accounts
* Description: This plugin is a tool to create missing accounts in WooCommerce orders
* Plugin URI: http://2gika.si
* Author: 2gika
* Author URI: http://2gika.si
* Version: 1.0
* License: GPL2
* Text Domain: dwcma
* Domain Path: languages
*/

/*
Copyright (C) 2016  2gika  info@2gika.si

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
/**
 * Snippet Wordpress Plugin Boilerplate based on:
 *
 * - https://github.com/purplefish32/sublime-text-2-wordpress/blob/master/Snippets/Plugin_Head.sublime-snippet
 * - http://wordpress.stackexchange.com/questions/25910/uninstall-activate-deactivate-a-plugin-typical-features-how-to/25979#25979
 *
 * By default the option to uninstall the plugin is disabled,
 * to use uncomment or remove if not used.
 *
 * This Template does not have the necessary code for use in multisite. 
 *
 * Also delete this comment block is unnecessary once you have read.
 *
 * Version 1.0
 */


if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'plugins_loaded', array( 'DgWooCreateMissingAccounts', 'getInstance' ) );

class DgWooCreateMissingAccounts {

	private static $instance = null;
	private static $slug = 'dg-woo-create-missing-accounts';
	private static $domain = 'dwcma';

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {

		// TODO - check for user permissions
		if( ! is_admin() ){
			return;
		}

		// create submenu page
		add_action( 'admin_menu', array( $this, 'addSubmenuToToolsMenu' ) );
	}

	public function addSubmenuToToolsMenu(){

		// create submenu under TOOLS menu
		add_submenu_page(
			'tools.php',
			__( 'Create missing accounts', self::$domain ),
			__( 'Create missing accounts', self::$domain ),
			'manage_options',
			self::$slug,
			array( $this, 'renderPage' )
		);
	}

	public function renderPage(){
		
		// TODO - check for user permissions
		$data = $this->processRequest();

		include dirname( __FILE__ ) . '/views/tool.php';
	}

	private function processRequest(){
		$data = array(
			'state' => '',
			'errors' => array(),
			'messages' => array(),
		);

		$data['state'] = '';

		if(
			! $this->is_woocommerce_active() ||
			! function_exists( 'wc_create_new_customer' ) ||
			! class_exists( 'WC_Order' ) ||
			'yes' !== get_option( 'woocommerce_registration_generate_username' ) ||
			'yes' !== get_option( 'woocommerce_registration_generate_password' )
		){
			$data['state'] = 'requirements';
			return $data;
		}





		if( isset( $_POST['action'] ) && "wcma-scan-orders" == $_POST['action'] ){

			$data['state'] = 'scan_report';

			if( ! isset( $_POST['wcma-scan-orders-nonce'] ) || ! wp_verify_nonce( $_POST['wcma-scan-orders-nonce'], 'wcma-scan-orders' ) ){
				$data['errors'][] = __( 'Request not ok. Please retry using this tool the way it is ment to be used.', self::$domain );
				$data['state'] = '';

				return $data;
			}

			$data['scan_report'] = $this->scanOrders( $this->scanOrdersQuery() );

			return $data;
		}




		if( isset( $_POST['action'] ) && "wcma-process-orders" == $_POST['action'] ){

			$data['state'] = 'process_report';

			if( ! isset( $_POST['wcma-process-orders-nonce'] ) || ! wp_verify_nonce( $_POST['wcma-process-orders-nonce'], 'wcma-process-orders' ) ){
				$data['errors'][] = __( 'Request not ok. Please retry using this tool the way it is ment to be used.', self::$domain );
				$data['state'] = '';

				return $data;
			}

			$data['process_report'] = $this->processOrders( $this->scanOrdersQuery() );
			
			return $data;
		}



		return $data;
	}

	private function scanOrders( $query ){

		$emails = array();
		$num_users = 0;
		$num_incorrect_emails = 0;

		foreach ( $query->posts as $post_id ) {
			$email = get_post_meta( $post_id, '_billing_email', true );
			if( ! in_array( $email, $emails ) ){
				$emails[] = $email;

				if( ! email_exists( $email ) ){
					$num_users++;
				}

				if( ! is_email( $email ) ){
					$num_incorrect_emails++;
				}
			}
		}

		return array(
			'num_orders' => $query->found_posts,
			'num_emails' => count( $emails ),
			'num_users' => $num_users,
			'num_incorrect_emails' => $num_incorrect_emails,
		);
	}

	private function processOrders( $query ){
		$num_orders = 0;
		$num_users = 0;
		$errors = array();

		foreach ( $query->posts as $order_id ) {
			$order = wc_get_order( $order_id );

			if( ! $order ){
				$errors[] = sprintf( __( 'Skipped order with id %s because it does not exist any more', self::$domain ), $order_id );
				continue;
			}

			$email = get_post_meta( $order_id, '_billing_email', true );

			if( ! is_email( $email ) ){
				$errors[] = sprintf( __( 'Skipped order with id %s because email "%s" is incorect', self::$domain ), $order_id, $email );
				continue;
			}

			// get user
			$user = get_user_by( 'email', $email );

			// create user
			if( ! $user ){
				// create user
				$user_id = wc_create_new_customer( $email );
				if( is_wp_error( $user_id ) ){
					$errors[] = sprintf( __( 'Could not create user with email "%s". Reason: %s Skipping this order.', self::$domain ), $email, $user_id->get_error_message() );
					continue;
				}
				$user = get_user_by( 'id', $user_id );

				$num_users++;
			}

			// assign user to order
			update_post_meta( $order_id, '_customer_user', $user->ID );

			// add note to order
			$order->add_order_note( sprintf( 'User with ID %s is now assigned to this order.', $user->ID ) );

			$num_orders++;
		}


		return array(
			'num_orders' => $num_orders,
			'num_users' => $num_users,
			'errors' => $errors,
		);
	}

	private function scanOrdersQuery(){
		$query_args = array(
			'fields' => 'ids',
			'nopaging' => true,
			'post_type' => 'shop_order',
			'post_status' => 'any',
			'meta_query' => array(
				array(
					'key' => '_customer_user',
					'value'   => '0',
					'compare' => '=',
				),
			),
		);

		$query = new WP_Query( $query_args );

		return $query;
	}

	private function is_woocommerce_active(){
		return in_array( 'woocommerce/woocommerce.php', $this->get_active_plugins() ) || array_key_exists( 'woocommerce/woocommerce.php', $this->get_active_plugins() );
	}

	private $active_plugins;
	private function get_active_plugins(){
		if( ! isset( $this->active_plugins ) ){
			$this->active_plugins = (array) get_option( 'active_plugins', array() );

			if ( is_multisite() )
				$this->active_plugins = array_merge( $this->active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
		}

		return $this->active_plugins;
	}
}
