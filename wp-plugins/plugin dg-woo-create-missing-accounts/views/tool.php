<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}
?>
<div class="wrap">
	<h1><?php _e( 'Create missing accounts', self::$domain ); ?></h1>
	<p>
		<?php _e( 'This tool scans for WooCommerce orders that do not have wp user assigned to it. Then creates wp users and assign them to orders.', self::$domain ); ?>
	</p>

	<?php
	// TODO - echo errors and messages
	?>

	<?php switch ( $data['state'] ) {
		// when we complete scanning
		case 'requirements':
			?>
			<h3><?php _e( 'Some required functionlity for this tool is missing.', self::$domain ); ?></h3>

			<table class="form-table">
				<tbody>
					<tr>
						<th scope="row"><?php _e( 'WooCommerce plugin', self::$domain ); ?></th>
						<td>
							<p class="description"><span style="color: #1AA20F;">Ok </span><?php _e( 'WooCommerce plugin', self::$domain ); ?></p>
							<p class="description"><span style="color: #B82700;">Not found </span><?php _e( 'WooCommerce function "wc_create_new_customer"', self::$domain ); ?></p>
							<p class="description"><?php _e( 'WooCommerce class "WC_Order"', self::$domain ); ?></p>
							<p class="description"><?php _e( 'Enable autocreation of username from email - WooCommerce / Settings / User Accounts / look at the bottom', self::$domain ); ?></p>
							<p class="description"><?php _e( 'Enable autocreation of password for user - WooCommerce / Settings / User Accounts / look at the bottom', self::$domain ); ?></p>
						</td>
					</tr>
				</tbody>
			</table>

			<p><?php _e( 'Please resolve those issues or contact developer at info@2gika.si', self::$domain ); ?></p>
			<?php
			break;

		// when we complete scanning
		case 'scan_report':
			?>
			<h3><?php _e( 'Scan report', self::$domain ); ?></h3>

			<table class="form-table">
				<tbody>
					<tr>
						<th scope="row"><?php _e( 'Orders with no user assigned', self::$domain ); ?></th>
						<td><p class="description"><?php echo $data['scan_report']['num_orders']; ?></p></td>
					</tr>
					<tr>
						<th scope="row"><?php _e( 'Number of distinct emails', self::$domain ); ?></th>
						<td><p class="description"><?php echo $data['scan_report']['num_emails']; ?></p></td>
					</tr>
					<tr>
						<th scope="row"><?php _e( 'Number of incorrect emails', self::$domain ); ?></th>
						<td><p class="description"><?php echo $data['scan_report']['num_incorrect_emails']; ?></p></td>
					</tr>
					<tr>
						<th scope="row"><?php _e( 'Number of missing wp users', self::$domain ); ?></th>
						<td><p class="description"><?php echo $data['scan_report']['num_users']; ?></p></td>
					</tr>
				</tbody>
			</table>

			<h3><?php _e( 'Start with the process', self::$domain ); ?></h3>
			<p><?php _e( 'Keep in mind that this can take some time', self::$domain ); ?></p>
			<form method="post">
				<input type="hidden" name="action" value="wcma-process-orders" />
				<?php wp_nonce_field( 'wcma-process-orders', 'wcma-process-orders-nonce' ); ?>

				<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php esc_attr_e( 'Start processing', self::$domain ); ?>" /></p>
			</form>
			<?php
			break;

		// when we complete processing
		case 'process_report':
			?>
			<h3><?php _e( 'Process report', self::$domain ); ?></h3>

			<?php if( count( $data['process_report']['errors'] ) > 0 ){ ?>
				<h4><?php _e( 'There were some errors:', self::$domain ); ?></h4>
				<ul>
					<?php foreach ( $data['process_report']['errors'] as $error ) { ?>
					<li><?php echo esc_html( $error ); ?></li>
					<?php } ?>
				</ul>
			<?php } ?>
			

			<table class="form-table">
				<tbody>
					<tr>
						<th scope="row"><?php _e( 'Number of orders got user assigned', self::$domain ); ?></th>
						<td><p class="description"><?php echo $data['process_report']['num_orders']; ?></p></td>
					</tr>
					<tr>
						<th scope="row"><?php _e( 'Number of users created', self::$domain ); ?></th>
						<td><p class="description"><?php echo $data['process_report']['num_users']; ?></p></td>
					</tr>
				</tbody>
			</table>

			<h3><?php _e( 'Start again?', self::$domain ); ?></h3>
			<p><?php _e( 'Keep in mind that this can take some time', self::$domain ); ?></p>
			<form method="post">
				<input type="hidden" name="action" value="wcma-scan-orders" />
				<?php wp_nonce_field( 'wcma-scan-orders', 'wcma-scan-orders-nonce' ); ?>

				<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php esc_attr_e( 'Rescan', self::$domain ); ?>" /></p>
			</form>
			<?php
			break;
		
		// default start screen
		default:
			?>
			<h3><?php _e( 'Start with scanning the orders', self::$domain ); ?></h3>
			<p><?php _e( 'Keep in mind that this can take some time', self::$domain ); ?></p>
			<form method="post">
				<input type="hidden" name="action" value="wcma-scan-orders" />
				<?php wp_nonce_field( 'wcma-scan-orders', 'wcma-scan-orders-nonce' ); ?>

				<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php esc_attr_e( 'Scan orders', self::$domain ); ?>" /></p>
			</form>
			<?php
			break;
	} ?>

	<div style="padding:20px; border: 1px solid #ddd; background: #f3f3f3; border-radius: 3px; box-shadow: 0px 1px 1px rgba(0,0,0,0.07); background: linear-gradient(#fff, #f3f3f3);">
		<h3 style="margin-top:0px;">2GIKA | Smart Web Solutions</h3>
		<h4>
			If this plugin helped you, you can <strong style="color: #0084E0;">donate us a cup of coffee</strong>  (or more) and keep us going ☺. Thank you.
		</h4>

		<p>
			Request a feature or report a bug at  <a title="Mail Us" href="mailto:support@2gika.si" target="_blank">support@2gika.si</a>
		</p>

		<p>
			<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
			<input type="hidden" name="cmd" value="_s-xclick">
			<input type="hidden" name="hosted_button_id" value="HRDT7DPDNQ2A8">
			<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
			<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
			</form>
		</p>
		<p>For more WordPress plugins and other tools visit us at <a title="Visit Us" target="_blank" href="http://www.2gika.si">http://www.2gika.si</a></p>
	</div>

</div>