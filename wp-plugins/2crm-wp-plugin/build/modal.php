<?php


$commands_arr = array(
	'' => "select your command",
	'create_project' => "create: project",
	'create_task' => "create: task",
	'create_contact' => "create: contact",
	'create_organisation' => "create: organisation",
	'create_invoice' => "create: invoice",
	'create_ticket' => "create: ticket",
	'create_document' => "create: document",
	'go_to_projects_list' => "go to: projects list",
	'go_to_project_inalbea' => "go to: project: Inalbea",
	'go_to_project_kontus' => "go to: project: Kontus",
	'go_to_project_zeleni_svet' => "go to: project: Zeleni svet",
	'go_to_project_e_meni' => "go to: project: E-meni",
	'go_to_project_mem2gal' => "go to: project: mem2gal",
	'go_to_tasks_list' => "go to: tasks list",
	'go_to_task_pripravi_račun' => "go to: task: pripravi račun",
	'go_to_task_prenesi_stran' => "go to: task: prenesi stran",
	'go_to_contacts_list' => "go to: contacts list",
	'go_to_contact_matija_štrlekar' => "go to: contact: Matija Štrlekar",
	'go_to_contact_marina_novak' => "go to: contact: Marina Novak",
	'go_to_contact_matjaž_furlan' => "go to: contact: Matjaž Furlan",
	'go_to_organisations_list' => "go to: organisations list",
	'go_to_organisation_utrip' => "go to: organisation: Utrip",
	'go_to_organisation_biro_4m_d_o_o_' => "go to: organisation: Biro 4m d.o.o.",
	'go_to_organisation_inalbea_d_o_o_' => "go to: organisation: Inalbea d.o.o.",
	'go_to_organisation_igor_donkov,_kreativne_rešitve_s_p_' => "go to: organisation: Igor Donkov, kreativne rešitve S.P.",
	'go_to_invoices_list' => "go to: invoices list",
	'go_to_invoice_inalbea_' => "go to: invoice: Inalbea ",
	'go_to_invoice_moj_mojster' => "go to: invoice: Moj mojster",
	'go_to_tickets_list' => "go to: tickets list",
	'go_to_ticket_vpis_v_webmail_ni_mogoč' => "go to: ticket: vpis v webmail ni mogoč",
	'go_to_ticket_pošiljanje_pošte_ne_deluje' => "go to: ticket: pošiljanje pošte ne deluje",
	'go_to_ticket_mem2gal' => "go to: ticket: mem2gal",

);


?>
<a class="button-flat" data-toggle="modal" data-animation="" href='#modal-id' style="display:none;">Trigger modal</a>
<div class="modal fade dcrm-modal-terminal" id="modal-id">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">2CRM terminal commands</h4>
			</div>
			<div class="modal-body">
				<form action="" method="GET">
					<input type="hidden" name="page" value="2crm_terminal_command" />
					<p>
						<select name="command" id="dcrm-terminal-command" data-placeholder="Select Your Command">
							<?php foreach($commands_arr AS $value => $title){ ?>
							<option value="<?=$value?>"><?=$title?></option>
							<?php } ?>
						</select>
					</p>
					<p>
						<input type="text" name="dcrm_command">
						<ul class="dcrm_commands">
							<?php foreach($commands_arr AS $value => $title){ ?>
							<li data-command="<?=$value?>"><?=$title?></li>
							<?php } ?>
						</ul>
					</p>
				</form>
			</div>
			<div class="modal-footer">
				<a class="button-flat-grey" data-dismiss="modal">Close</a>
				<a class="button-flat">Save changes</a>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->