<div class="dcrm-page">
	<div class="header">
		<h2>Projects</h2>
		<div class="view">
			<select name="" id="" class="view">
				<option selected="selected" value="">All</option>
				<option value="">Active</option>
			</select>
			<div class="view-settings">
				<div class="glyphicon glyphicon-cog"><br></div>
			</div>
		</div>
		<div class="actions">
			<select name="" id="">
				<option selected="selected" value="">Bulk actions</option>
				<option value="">edit</option>
				<option value="">delete</option>
				<option value="">mark completed</option>
				<option value="">mass edit</option>
			</select>
			<a href="#new" class="header-btn">Add new</a>
			<a href="#new" class="header-btn">Add new2</a>
		</div>
	</div>
	<div class="list">
		<div class="item head">
			<div class="cb"><br></div>
			<div class="title">Naziv</div>
			<div class="assigned-to">Dodeljeno</div>
		</div>
		<div class="item">
			<div class="cb"><input type="checkbox"></div>
			<div class="title">Projekt št. 1</div>
			<div class="assigned-to">Matija</div>
		</div>
		<div class="item">
			<div class="cb"><input type="checkbox"></div>
			<div class="title">Projekt št. 1</div>
			<div class="assigned-to">Matija</div>
		</div>
		<div class="item">
			<div class="cb"><input type="checkbox"></div>
			<div class="title">Projekt št. 1</div>
			<div class="assigned-to">Matija</div>
		</div>
		<div class="item foot">
			<div class="cb"><br></div>
			<div class="title">Naziv</div>
			<div class="assigned-to">Dodeljeno</div>
		</div>
	</div>
	<?php include('modal.php'); ?>
</div>