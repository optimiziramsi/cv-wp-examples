<?php
/**
* Plugin Name: WooCommerce Export mailing list from orders
* Description: Exports mailing list in CSV format for importing into mailing providers like MailChimp, MailerLite etc.
* Plugin URI: http://2gika.si
* Author: 2gika
* Author URI: http://2gika.si
* Version: 1.0
* License: GPL2
* Text Domain: dwemfo
* Domain Path: languages
*/

/*
Copyright (C) 2016  2gika  info@2gika.si

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
/**
 * Snippet Wordpress Plugin Boilerplate based on:
 *
 * - https://github.com/purplefish32/sublime-text-2-wordpress/blob/master/Snippets/Plugin_Head.sublime-snippet
 * - http://wordpress.stackexchange.com/questions/25910/uninstall-activate-deactivate-a-plugin-typical-features-how-to/25979#25979
 *
 * By default the option to uninstall the plugin is disabled,
 * to use uncomment or remove if not used.
 *
 * This Template does not have the necessary code for use in multisite. 
 *
 * Also delete this comment block is unnecessary once you have read.
 *
 * Version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'plugins_loaded', array( 'DgWooExportMailsFromOrders', 'getInstance' ) );

class DgWooExportMailsFromOrders {

	private static $instance = null;
	private static $slug = 'dg-woo-export-mails-from-orders';
	private static $prefix = 'dwemfo';
	private static $domain = 'dwemfo';

	private $csvSeparator = ";";

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {

		// TODO - check for user permissions
		if( ! is_admin() || ! current_user_can( 'manage_options' ) ){
			return;
		}

		// create submenu page
		add_action( 'admin_menu', array( $this, 'addSubmenuToToolsMenu' ) );

		// register ajax
		add_action( 'wp_ajax_' . self::$prefix . '-export-emails', array( $this, 'handle_ajax_request' ) );
	}

	/******************************************************************
	*
	*       TOOLS PAGE
	*
	******************************************************************/

	public function addSubmenuToToolsMenu(){

		// create submenu under TOOLS menu
		add_submenu_page(
			'tools.php',
			__( 'Export emails from orders', self::$domain ),
			__( 'Export emails from orders', self::$domain ),
			'manage_options',
			self::$slug,
			array( $this, 'renderPage' )
		);
	}

	public function renderPage(){
		
		if( ! is_admin() || ! current_user_can( 'manage_options' ) ){
			return;
		}

		$data = $this->processRequest();

		include dirname( __FILE__ ) . '/views/tool.php';
	}

	private function processRequest(){
		$data = array(
			'state' => '',
			'errors' => array(),
			'messages' => array(),
		);

		$data['state'] = '';

		if(
			! $this->is_woocommerce_active()
		){
			$data['state'] = 'requirements';
			return $data;
		}


		// if( isset( $_POST['action'] ) && self::$prefix . '-scan-orders' == $_POST['action'] ){
		if( isset( $_POST['action'] ) && self::$prefix . '-scan-orders' == $_POST['action'] ){

			$data['state'] = 'scan_report';

			if( ! isset( $_POST[ self::$prefix . '-scan-orders-nonce'] ) || ! wp_verify_nonce( $_POST[ self::$prefix . '-scan-orders-nonce'],  self::$prefix . '-scan-orders' ) ){
				$data['errors'][] = __( 'Request not ok. Please retry using this tool the way it is ment to be used.', self::$domain );
				$data['state'] = '';

				return $data;
			}

			$data['scan_report'] = $this->scanOrders( $this->scanOrdersQuery() );

			return $data;
		}


		if( isset( $_POST['action'] ) && self::$prefix . "-process-orders" == $_POST['action'] ){

			$data['state'] = 'process_report';

			if( ! isset( $_POST[ self::$prefix . '-process-orders-nonce'] ) || ! wp_verify_nonce( $_POST[ self::$prefix . '-process-orders-nonce'], self::$prefix . '-process-orders' ) ){
				$data['errors'][] = __( 'Request not ok. Please retry using this tool the way it is ment to be used.', self::$domain );
				$data['state'] = '';

				return $data;
			}

			$data['process_report'] = $this->processOrders( $this->scanOrdersQuery() );
			
			return $data;
		}

		return $data;
	}

	private function scanOrders( $query ){

		$emails = array();
		$num_incorrect_emails = 0;

		foreach ( $query->posts as $post_id ) {
			$email = get_post_meta( $post_id, '_billing_email', true );

			if( ! is_email( $email ) ){
				$num_incorrect_emails++;
				continue;
			}

			if( ! in_array( $email, $emails ) ){
				$emails[] = $email;
			}
		}

		return array(
			'num_orders' => $query->found_posts,
			'num_emails' => count( $emails ),
			'num_incorrect_emails' => $num_incorrect_emails,
		);
	}

	private function processOrders( $query ){
		$num_orders = 0;
		$num_users = 0;
		$errors = array();

		foreach ( $query->posts as $order_id ) {
			$order = wc_get_order( $order_id );

			if( ! $order ){
				$errors[] = sprintf( __( 'Skipped order with id %s because it does not exist any more', self::$domain ), $order_id );
				continue;
			}

			$email = get_post_meta( $order_id, '_billing_email', true );

			if( ! is_email( $email ) ){
				$errors[] = sprintf( __( 'Skipped order with id %s because email "%s" is incorect', self::$domain ), $order_id, $email );
				continue;
			}

			// get user
			$user = get_user_by( 'email', $email );

			// create user
			if( ! $user ){
				// create user
				$user_id = wc_create_new_customer( $email );
				if( is_wp_error( $user_id ) ){
					$errors[] = sprintf( __( 'Could not create user with email "%s". Reason: %s Skipping this order.', self::$domain ), $email, $user_id->get_error_message() );
					continue;
				}
				$user = get_user_by( 'id', $user_id );

				$num_users++;
			}

			// assign user to order
			update_post_meta( $order_id, '_customer_user', $user->ID );

			// add note to order
			$order->add_order_note( sprintf( 'User with ID %s is now assigned to this order.', $user->ID ) );

			$num_orders++;
		}

		return array(
			'num_orders' => $num_orders,
			'num_users' => $num_users,
			'errors' => $errors,
		);
	}

	private function scanOrdersQuery(){
		$query_args = array(
			'fields' => 'ids',
			'nopaging' => true,
			'post_type' => 'shop_order',
			'post_status' => 'any',
			// 'meta_query' => array(
			// 	array(
			// 		'key' => '_customer_user',
			// 		'value'   => '0',
			// 		'compare' => '=',
			// 	),
			// ),
		);

		$query = new WP_Query( $query_args );

		return $query;
	}


	/******************************************************************
	*
	*       EXPORT CSV
	*
	******************************************************************/
	
	public function handle_ajax_request(){

		// TODO - nonce check
		if( ! isset( $_POST[ self::$prefix . '-export-emails-nonce' ] ) || ! wp_verify_nonce( $_POST[ self::$prefix . '-export-emails-nonce' ],  self::$prefix . '-export-emails' ) ){
			wp_die( __( 'Request not ok. Please retry using this tool the way it is ment to be used.', self::$domain ) );
		}

		if( ! current_user_can( 'manage_options' ) ){
			wp_die( __( 'You do not have enough permissions for this action.', self::$domain ) );
		}

		// create tmp file
		$tmp_csv_fp = fopen( 'php://output', 'w' );

		// vars
		$csv_download_name = 'woocommerce_emails_from_orders__' . date('Y_m_d') . '.csv';

		// prep headers
		header("Content-type: application/force-download");
		header('Content-Encoding: UTF-8');
		header('Content-type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachment; filename="'.$csv_download_name.'"');
		// echo "\xEF\xBB\xBF"; // UTF-8 BOM
		fwrite( $tmp_csv_fp, "\xEF\xBB\xBF" );

		// prep CSV content
		$this->processCsv( $tmp_csv_fp, $this->scanOrdersQuery() );

		// close the output buffer
		fclose( $tmp_csv_fp );

		exit();
	}

	private function processCsv( $fp, $query ){
		
		$emails = array();

		foreach ( $query->posts as $post_id ) {

			$email = get_post_meta( $post_id, '_billing_email', true );

			if( ! is_email( $email ) ){
				$num_incorrect_emails++;
				continue;
			}

			if( ! in_array( $email, $emails ) ){
				$emails[] = $email;

				$first_name = get_post_meta( $post_id, '_billing_first_name', true );
				$last_name = get_post_meta( $post_id, '_billing_last_name', true );

				$row_data = array(
					$email,
					$first_name,
					$last_name,
				);

				$this->csvRow( $fp, $row_data, count( $row_data ) );
			}
		}
	}

	private function csvRow( $fp, $values, $num_columns ){
		$val_arr = array();
		for ($i=0; $i < $num_columns; $i++) { 
			$values_val = isset( $values[ $i ] ) ? $values[ $i ] : "";
			$val_arr[] = $this->utils_csv_content( $values_val );
		}

		fputcsv( $fp, $val_arr, $this->csvSeparator );
	}


	private function utils_csv_content( $content ){
		$content = htmlspecialchars_decode( $content, ENT_NOQUOTES);
		$content = $this->decode_entities_full( $content, ENT_COMPAT, "utf-8" );
		// $content = mb_convert_encoding( $content, 'UTF-16LE', 'UTF-8');

		return $content;
	}

	/**
	 * Helper function for drupal_html_to_text().
	 *
	 * Calls helper function for HTML 4 entity decoding.
	 * Per: http://www.lazycat.org/software/html_entity_decode_full.phps
	 */
	private function decode_entities_full($string, $quotes = ENT_COMPAT, $charset = 'ISO-8859-1') {
	  return html_entity_decode( preg_replace_callback('/&([a-zA-Z][a-zA-Z0-9]+);/', array( $this, 'convert_entity' ), $string), $quotes, $charset ); 
	}

	/**
	 * Helper function for decode_entities_full().
	 *
	 * This contains the full HTML 4 Recommendation listing of entities, so the default to discard  
	 * entities not in the table is generally good. Pass false to the second argument to return 
	 * the faulty entity unmodified, if you're ill or something.
	 * Per: http://www.lazycat.org/software/html_entity_decode_full.phps
	 */
	function convert_entity($matches, $destroy = true) {
	  static $table = array('quot' => '&#34;','amp' => '&#38;','lt' => '&#60;','gt' => '&#62;','OElig' => '&#338;','oelig' => '&#339;','Scaron' => '&#352;','scaron' => '&#353;','Yuml' => '&#376;','circ' => '&#710;','tilde' => '&#732;','ensp' => '&#8194;','emsp' => '&#8195;','thinsp' => '&#8201;','zwnj' => '&#8204;','zwj' => '&#8205;','lrm' => '&#8206;','rlm' => '&#8207;','ndash' => '&#8211;','mdash' => '&#8212;','lsquo' => '&#8216;','rsquo' => '&#8217;','sbquo' => '&#8218;','ldquo' => '&#8220;','rdquo' => '&#8221;','bdquo' => '&#8222;','dagger' => '&#8224;','Dagger' => '&#8225;','permil' => '&#8240;','lsaquo' => '&#8249;','rsaquo' => '&#8250;','euro' => '&#8364;','fnof' => '&#402;','Alpha' => '&#913;','Beta' => '&#914;','Gamma' => '&#915;','Delta' => '&#916;','Epsilon' => '&#917;','Zeta' => '&#918;','Eta' => '&#919;','Theta' => '&#920;','Iota' => '&#921;','Kappa' => '&#922;','Lambda' => '&#923;','Mu' => '&#924;','Nu' => '&#925;','Xi' => '&#926;','Omicron' => '&#927;','Pi' => '&#928;','Rho' => '&#929;','Sigma' => '&#931;','Tau' => '&#932;','Upsilon' => '&#933;','Phi' => '&#934;','Chi' => '&#935;','Psi' => '&#936;','Omega' => '&#937;','alpha' => '&#945;','beta' => '&#946;','gamma' => '&#947;','delta' => '&#948;','epsilon' => '&#949;','zeta' => '&#950;','eta' => '&#951;','theta' => '&#952;','iota' => '&#953;','kappa' => '&#954;','lambda' => '&#955;','mu' => '&#956;','nu' => '&#957;','xi' => '&#958;','omicron' => '&#959;','pi' => '&#960;','rho' => '&#961;','sigmaf' => '&#962;','sigma' => '&#963;','tau' => '&#964;','upsilon' => '&#965;','phi' => '&#966;','chi' => '&#967;','psi' => '&#968;','omega' => '&#969;','thetasym' => '&#977;','upsih' => '&#978;','piv' => '&#982;','bull' => '&#8226;','hellip' => '&#8230;','prime' => '&#8242;','Prime' => '&#8243;','oline' => '&#8254;','frasl' => '&#8260;','weierp' => '&#8472;','image' => '&#8465;','real' => '&#8476;','trade' => '&#8482;','alefsym' => '&#8501;','larr' => '&#8592;','uarr' => '&#8593;','rarr' => '&#8594;','darr' => '&#8595;','harr' => '&#8596;','crarr' => '&#8629;','lArr' => '&#8656;','uArr' => '&#8657;','rArr' => '&#8658;','dArr' => '&#8659;','hArr' => '&#8660;','forall' => '&#8704;','part' => '&#8706;','exist' => '&#8707;','empty' => '&#8709;','nabla' => '&#8711;','isin' => '&#8712;','notin' => '&#8713;','ni' => '&#8715;','prod' => '&#8719;','sum' => '&#8721;','minus' => '&#8722;','lowast' => '&#8727;','radic' => '&#8730;','prop' => '&#8733;','infin' => '&#8734;','ang' => '&#8736;','and' => '&#8743;','or' => '&#8744;','cap' => '&#8745;','cup' => '&#8746;','int' => '&#8747;','there4' => '&#8756;','sim' => '&#8764;','cong' => '&#8773;','asymp' => '&#8776;','ne' => '&#8800;','equiv' => '&#8801;','le' => '&#8804;','ge' => '&#8805;','sub' => '&#8834;','sup' => '&#8835;','nsub' => '&#8836;','sube' => '&#8838;','supe' => '&#8839;','oplus' => '&#8853;','otimes' => '&#8855;','perp' => '&#8869;','sdot' => '&#8901;','lceil' => '&#8968;','rceil' => '&#8969;','lfloor' => '&#8970;','rfloor' => '&#8971;','lang' => '&#9001;','rang' => '&#9002;','loz' => '&#9674;','spades' => '&#9824;','clubs' => '&#9827;','hearts' => '&#9829;','diams' => '&#9830;','nbsp' => '&#160;','iexcl' => '&#161;','cent' => '&#162;','pound' => '&#163;','curren' => '&#164;','yen' => '&#165;','brvbar' => '&#166;','sect' => '&#167;','uml' => '&#168;','copy' => '&#169;','ordf' => '&#170;','laquo' => '&#171;','not' => '&#172;','shy' => '&#173;','reg' => '&#174;','macr' => '&#175;','deg' => '&#176;','plusmn' => '&#177;','sup2' => '&#178;','sup3' => '&#179;','acute' => '&#180;','micro' => '&#181;','para' => '&#182;','middot' => '&#183;','cedil' => '&#184;','sup1' => '&#185;','ordm' => '&#186;','raquo' => '&#187;','frac14' => '&#188;','frac12' => '&#189;','frac34' => '&#190;','iquest' => '&#191;','Agrave' => '&#192;','Aacute' => '&#193;','Acirc' => '&#194;','Atilde' => '&#195;','Auml' => '&#196;','Aring' => '&#197;','AElig' => '&#198;','Ccedil' => '&#199;','Egrave' => '&#200;','Eacute' => '&#201;','Ecirc' => '&#202;','Euml' => '&#203;','Igrave' => '&#204;','Iacute' => '&#205;','Icirc' => '&#206;','Iuml' => '&#207;','ETH' => '&#208;','Ntilde' => '&#209;','Ograve' => '&#210;','Oacute' => '&#211;','Ocirc' => '&#212;','Otilde' => '&#213;','Ouml' => '&#214;','times' => '&#215;','Oslash' => '&#216;','Ugrave' => '&#217;','Uacute' => '&#218;','Ucirc' => '&#219;','Uuml' => '&#220;','Yacute' => '&#221;','THORN' => '&#222;','szlig' => '&#223;','agrave' => '&#224;','aacute' => '&#225;','acirc' => '&#226;','atilde' => '&#227;','auml' => '&#228;','aring' => '&#229;','aelig' => '&#230;','ccedil' => '&#231;','egrave' => '&#232;','eacute' => '&#233;','ecirc' => '&#234;','euml' => '&#235;','igrave' => '&#236;','iacute' => '&#237;','icirc' => '&#238;','iuml' => '&#239;','eth' => '&#240;','ntilde' => '&#241;','ograve' => '&#242;','oacute' => '&#243;','ocirc' => '&#244;','otilde' => '&#245;','ouml' => '&#246;','divide' => '&#247;','oslash' => '&#248;','ugrave' => '&#249;','uacute' => '&#250;','ucirc' => '&#251;','uuml' => '&#252;','yacute' => '&#253;','thorn' => '&#254;','yuml' => '&#255;'
						   );
	  if (isset($table[$matches[1]])) return $table[$matches[1]];
	  // else 
	  return $destroy ? '' : $matches[0];
	}



	/******************************************************************
	*
	*       UTILS
	*
	******************************************************************/
	
	

	private function is_woocommerce_active(){
		return in_array( 'woocommerce/woocommerce.php', $this->get_active_plugins() ) || array_key_exists( 'woocommerce/woocommerce.php', $this->get_active_plugins() );
	}

	private $active_plugins;
	private function get_active_plugins(){
		if( ! isset( $this->active_plugins ) ){
			$this->active_plugins = (array) get_option( 'active_plugins', array() );

			if ( is_multisite() )
				$this->active_plugins = array_merge( $this->active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
		}

		return $this->active_plugins;
	}
}