<?php
/**
* Plugin Name: Šolska galerija
* Description: PLugin, ki wp stran spremeni v aplikacijo šolska galerija
* Plugin URI: http://2gika.si
* Author: 2gika
* Author URI: http://2gika.si
* Version: 1.0
* License: GPL2
* Text Domain: dgsg
* Domain Path: languages
*/

if ( ! defined( 'ABSPATH' ) ) exit;

define( 'SG_FILE', __FILE__ );
define( 'SG_VERSION', '0.1' );

require_once dirname( SG_FILE ) . '/titan-framework-checker/titan-framework-checker.php';

require_once dirname( SG_FILE ) . '/admin/gallery-metabox.php';

require_once dirname( SG_FILE ) . '/includes/sg-permissions.class.php';
require_once dirname( SG_FILE ) . '/includes/sg-params.class.php';
require_once dirname( SG_FILE ) . '/includes/theme_functions.php';

add_action( 'plugins_loaded', array( 'DgSolskaGalerija', 'getInstance' ) );
register_activation_hook( __FILE__, array( 'DgSolskaGalerija', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'DgSolskaGalerija', 'deactivate' ) );
// register_uninstall_hook( __FILE__, array( 'DgSolskaGalerija', 'uninstall' ) );

class DgSolskaGalerija {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {

		// admin section
		DgSolskaGalerija_Admin_GalleryMetabox::getInstance();
		
		add_action( 'init', array( $this, 'add_image_sizes' ) );
		
	}

	public static function activate() {}

	public static function deactivate() {}



	function add_image_sizes(){

		$sizes = array(
			SgParams::IMG_SIZE_THUMB => array(
					'width' => 200,
					'height' => 200,
					'crop' => true,
				),
			SgParams::IMG_SIZE_LARGE => array(
					'width' => 1200,
					'height' => 900,
					'crop' => false,
				),
		);

		$sizes = apply_filters( 'sg_image_sizes', $sizes );
		foreach ( $sizes as $size_key => $size_params ) {
			add_image_size( $size_key, $size_params['width'], $size_params['height'], $size_params['crop'] );
		}
		
	}
	

}