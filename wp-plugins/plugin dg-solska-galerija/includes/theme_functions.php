<?php

/**
Funkcije, ki se jih kliče iz teme za šolsko galerijo
*/

/**
 * Vrne url, ki kaže na root galerije
 * @return string
 */
function sg_get_home_url(){
	return home_url( '/' );
}

/**
 * Izpiše naslov šolske galerije / šole
 * @return string
 */
function sg_title(){
	// TODO
	echo "OŠ Louisa Adamiča";
}

function sg_logo_image_src( $size = 'thumb' ){
	// TODO
	return 'http://dummyimage.com/200x200/4d494d/686a82.gif&text=LA';
}

/**
 * Pove, če smo v root-u galerije / na primarni galeriji / strani
 * @return boolean
 */
function sg_is_home(){
	return is_front_page();
}



/**
 * Vrne seznam / drevo do trenutne galerije
 * @return [type] [description]
 */
function sg_gallery_breadcrumbs(){
	global $post;

	$breadcrumbs = array();

	if ( is_front_page() ) {
		return $breadcrumbs;
	}

	if( ! is_page() ){
		return $breadcrumbs;	
	}

	if( $post->post_parent ){
		
		$anc = get_post_ancestors( $post->ID );
		$anc = array_reverse( $anc );

		foreach ( $anc as $ancestor ) {
			if( get_option( 'page_on_front' ) == $ancestor ){
				continue;
			}
			$breadcrumbs[] = array(
				'title' => get_the_title($ancestor),
				'url' => get_permalink($ancestor),
			);
		}

	}

	$breadcrumbs[] = array(
		'title' => get_the_title(),
		'url' => get_permalink( $post->ID ),
	);

	return $breadcrumbs;
}








/**
 * Ali ima šolska galerija / šole tudi dislocirane enote / podružnice
 * @return boolean
 */
function sg_dislocated_locations_enabled(){
	// TODO
	return false;
}

/**
 * Ali ima šola vnešeno kakšno dislocirano enoto / podružnico
 * @return boolean
 */
function sg_has_dislocated_locations(){
	// TODO
	return false;
}

/**
 * Vrne vse dislocirane enote šole / podružnice šole
 * @return array
 */
function sg_get_dislocated_locations(){
	// TODO
	$locations = array();
	for ($i=0; $i < 6; $i++) { 
		$locations[] = array(
			'title' => 'Podružnica ' . $i,
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem distinctio magnam adipisci eum error dolores quia sapiente, non corporis fuga officia dolore ipsum voluptatem asperiores vero commodi in quasi!',
			'image_src' => 'http://dummyimage.com/300x300/4d494d/686a82.gif&text=Podruznica+' . $i,
			'url' => '#',
		);
	}

	return $locations;
}






/**
 * Nam pove, če ima trenutna galerija kaj podgalerij
 * @return boolean
 */
function sg_gallery_has_subgalleries(){
	return ( count( sg_gallery_subgalleries() ) > 0 );
}

/**
 * Vrne seznam podgalerij trenutne galerije
 * @return array
 */
function sg_gallery_subgalleries(){
	global $post;

	$subgalleries = array();

	// nastavimo post staus, ki ga iščemo
	$post_status = 'publish';
	if( sg_user_can( SgPermissions::MANAGE_GALLERIES ) ){
		$post_status = array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit' /*, 'trash' */);
	}

	$subpages_args = array(
		'sort_order' => 'asc',
		'sort_column' => 'menu_order',
		'hierarchical' => 1,
		// 'exclude' => '',
		// 'include' => '',
		// 'meta_key' => '',
		// 'meta_value' => '',
		// 'authors' => '',
		// 'child_of' => $post_id,
		'parent' => $post->ID,
		// 'exclude_tree' => '',
		'number' => '',
		'offset' => 0,
		'post_type' => 'page',
		'post_status' => $post_status
	);
	$subpages = get_pages( $subpages_args );
	foreach ($subpages as $subpage) {
		$subgalleries[] = array(
			'title' => $subpage->post_title,
			'image_src' => 'http://dummyimage.com/300x300/4d494d/686a82.gif&text=Galerija+' . $i,	// TODO
			'url' => get_permalink( $subpage->ID ),
		);
	}

	return $subgalleries;
}




/**
 * Vrne podatek, če trenutna galerija vsebuje slike
 * @return boolean
 */
function sg_gallery_has_images(){
	return ( count( sg_gallery_images() ) > 0);
}

/**
 * Vrne seznam slik v trenutni galeriji
 * @return array
 */
function sg_gallery_images(){
	global $post;

	$images = array();


	$post_images_meta = get_post_meta( $post->ID, SgParams::META_IMAGES, true );
	if( ! is_array( $post_images_meta ) ){
		$post_images_meta = array();
	}

	// zagotovimo, da imamo v array-u res samo številke
	$post_images_meta = array_map('intval', $post_images_meta );
	// obdržimo samo unikate
	$post_images_meta = array_unique( $post_images_meta );
	// odstranimo prazne vrednosti
	$post_images_meta = array_values( $post_images_meta );

	// če ni slik v meta, takoj vrnemo prazen array / ni slik
	if( 0 == count( $post_images_meta ) ){
		return $images;
	}

	// TODO - nekako moramo dobiti samo slike, glede na to, ali so potrjene za objavo ali ne
	// oz ali ima uporabnik pravico pridet do njih ali ne / glede na pravice
	// podobno, kot je to za galerije

	// na bolj varen način dobimo slike iz galerije
	$_attachments_vars = array(
		'include' => $post_images_meta,
		'post_status' => 'inherit',
		'post_type' => 'attachment',
		'post_mime_type' => 'image',
		'order' => $args['order'],
		'orderby' => $args['orderby']
	);
	$_attachments = get_posts( $_attachments_vars );

	foreach ( $_attachments as $key => $val ) {

		$aid = $val->ID;

		$thumb = wp_get_attachment_image_src( $aid , SgParams::IMG_SIZE_THUMB );
		$full = wp_get_attachment_image_src( $aid , SgParams::IMG_SIZE_LARGE );

		$_post = get_post( $aid );


		$image = array(

			'title' => $_post->post_title,
			'alttext' => get_post_meta( $aid, '_wp_attachment_image_alt', true ),
			'caption' => $_post->post_excerpt,
			'description' => $_post->post_content,

			'images' => array(),

		);

		// size small
		$image['images']['small'] = array(
			'src' => $thumb[0],
			'width' => $thumb[1],
			'height' => $thumb[2],
		);

		// size large
		$image['images']['large'] = array(
			'src' => $full[0],
			'width' => $full[1],
			'height' => $full[2],
		);

		// add image to array
		$images[] = $image;
	}

	return $images;
}







/**
 * Podobno kot funkcija current_user_can, pri čemer smo osredotočeni na šolsko galerijo in te previce
 * @param  string $permission tip pravice
 * @return boolean
 */
function sg_user_can( $permission ){
	// TODO
	return true;
}