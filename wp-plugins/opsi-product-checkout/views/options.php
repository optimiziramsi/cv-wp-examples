<?php
if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

$v = OpSi_ProductCheckout_View::getInstance();
$o = OpSi_ProductCheckout_Options::getInstance();

?>
<div class="wrap">
<h1><?php _e('Product Checkout', 'opsi_pc'); ?></h1>

<form method="post" action="<?php echo esc_attr( admin_url('admin-post.php') ); ?>">
	
	<?php wp_nonce_field(self::NONCE_ACTION); ?>
	<input type="hidden" name="action" value="<?php echo esc_attr(self::SAVE_ACTION); ?>">
	
	<?php
	/******************************************************************
	*
	*       PAYMILL
	*
	******************************************************************/
	?>

	<h2><?php _e('Paymill options', 'opsi_pc'); ?></h2>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Public key', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('paymill_public_key') ); ?>" value="<?php echo esc_attr( $o->get('paymill_public_key') ); ?>" class="regular-text code" /></td>
		</tr>
		 
		<tr valign="top">
			<th scope="row"><?php _e('Private key', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('paymill_private_key') ); ?>" value="<?php echo esc_attr( $o->get('paymill_private_key') ); ?>" class="regular-text code" /></td>
		</tr>
	</table>

	<h4><?php _e('Paymill sandbox / development options', 'opsi_pc'); ?></h4>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Enable paymill sandbox', 'opsi_pc'); ?></th>
			<td>
				<fieldset>
					<label for="<?php echo esc_attr( $v->input_name('paymill_sandbox_enabled') ); ?>">
						<input name="<?php echo esc_attr( $v->input_name('paymill_sandbox_enabled') ); ?>" type="checkbox" id="<?php echo esc_attr( $v->input_name('paymill_sandbox_enabled') ); ?>" value="yes" <?php checked( 'yes', $o->get('paymill_sandbox_enabled') ); ?>>
					</label>
				</fieldset>
			</td>
		</tr>
		 
		<tr valign="top">
			<th scope="row"><?php _e('Public key', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('paymill_sandbox_public_key') ); ?>" value="<?php echo esc_attr( $o->get('paymill_sandbox_public_key') ); ?>" class="regular-text code" /></td>
		</tr>
		 
		<tr valign="top">
			<th scope="row"><?php _e('Private key', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('paymill_sandbox_private_key') ); ?>" value="<?php echo esc_attr( $o->get('paymill_sandbox_private_key') ); ?>" class="regular-text code" /></td>
		</tr>
	</table>
	<hr>


	<?php
	/******************************************************************
	*
	*       PAYPAL
	*
	******************************************************************/
	?>

	<h2><?php _e('Paypal API options'); ?></h2>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('API username', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('paypal_api_username') ); ?>" value="<?php echo esc_attr( $o->get('paypal_api_username') ); ?>" class="regular-text code" /></td>
		</tr>
		 
		<tr valign="top">
			<th scope="row"><?php _e('API password', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('paypal_api_password') ); ?>" value="<?php echo esc_attr( $o->get('paypal_api_password') ); ?>" class="regular-text code" /></td>
		</tr>
		 
		<tr valign="top">
			<th scope="row"><?php _e('API signature', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('paypal_api_signature') ); ?>" value="<?php echo esc_attr( $o->get('paypal_api_signature') ); ?>" class="regular-text code" /></td>
		</tr>
	</table>

	<h4><?php _e('Paypal API sandbox / development options', 'opsi_pc'); ?></h4>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Enable paypal sandbox', 'opsi_pc'); ?></th>
			<td>
				<fieldset>
					<label for="<?php echo esc_attr( $v->input_name('paypal_api_sandbox_enabled') ); ?>">
						<input name="<?php echo esc_attr( $v->input_name('paypal_api_sandbox_enabled') ); ?>" type="checkbox" id="<?php echo esc_attr( $v->input_name('paypal_api_sandbox_enabled') ); ?>" value="yes" <?php checked( 'yes', $o->get('paypal_api_sandbox_enabled') ); ?>>
					</label>
				</fieldset>
			</td>
		</tr>
		 
		<tr valign="top">
			<th scope="row"><?php _e('API username', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('paypal_api_sandbox_username') ); ?>" value="<?php echo esc_attr( $o->get('paypal_api_sandbox_username') ); ?>" class="regular-text code" /></td>
		</tr>
		 
		<tr valign="top">
			<th scope="row"><?php _e('API password', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('paypal_api_sandbox_password') ); ?>" value="<?php echo esc_attr( $o->get('paypal_api_sandbox_password') ); ?>" class="regular-text code" /></td>
		</tr>
		 
		<tr valign="top">
			<th scope="row"><?php _e('API signature', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('paypal_api_sandbox_signature') ); ?>" value="<?php echo esc_attr( $o->get('paypal_api_sandbox_signature') ); ?>" class="regular-text code" /></td>
		</tr>
	</table>

	<h4><?php _e('Other paypal info', 'opsi_pc'); ?></h4>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Brand name', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('paypal_brand_name') ); ?>" value="<?php echo esc_attr( $o->get('paypal_brand_name') ); ?>" class="regular-text code" /></td>
		</tr>
		<tr valign="top">
			<th scope="row"><?php _e('Locale code', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('paypal_locale_code') ); ?>" value="<?php echo esc_attr( $o->get('paypal_locale_code') ); ?>" class="regular-text code" /></td>
		</tr>
		<tr valign="top">
			<th scope="row"><?php _e('Country code', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('paypal_country_code') ); ?>" value="<?php echo esc_attr( $o->get('paypal_country_code') ); ?>" class="regular-text code" /></td>
		</tr>
	</table>
	<hr>



	<?php
	/******************************************************************
	*
	*       SMS API
	*
	******************************************************************/
	?>

	<h2><?php _e('SMSAPI options'); ?></h2>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Enable', 'opsi_pc'); ?></th>
			<td>
				<fieldset>
					<label for="<?php echo esc_attr( $v->input_name('smsapi_enabled') ); ?>">
						<input name="<?php echo esc_attr( $v->input_name('smsapi_enabled') ); ?>" type="checkbox" id="<?php echo esc_attr( $v->input_name('smsapi_enabled') ); ?>" value="yes" <?php checked( 'yes', $o->get('smsapi_enabled') ); ?>>
					</label>
				</fieldset>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row"><?php _e('Username', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('smsapi_un') ); ?>" value="<?php echo esc_attr( $o->get('smsapi_un') ); ?>" class="regular-text code" /></td>
		</tr>
		 
		<tr valign="top">
			<th scope="row"><?php _e('Password', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('smsapi_ps') ); ?>" value="<?php echo esc_attr( $o->get('smsapi_ps') ); ?>" class="regular-text code" /></td>
		</tr>
		
		<tr valign="top">
			<th scope="row"><?php _e('From', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('smsapi_from') ); ?>" value="<?php echo esc_attr( $o->get('smsapi_from') ); ?>" class="regular-text code" /></td>
		</tr>

		<tr valign="top">
			<th scope="row"><?php _e('Country code', 'opsi_pc'); ?><br><small><?php _e('Like "386" without quotes for Slovenia.', 'opsi_pc'); ?></small></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('smsapi_cc') ); ?>" value="<?php echo esc_attr( $o->get('smsapi_cc') ); ?>" class="regular-text code" /></td>
		</tr>
		 
		<tr valign="top">
			<th scope="row"><?php _e('Sender id', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('smsapi_sid') ); ?>" value="<?php echo esc_attr( $o->get('smsapi_sid') ); ?>" class="regular-text code" /></td>
		</tr>
		 
		<tr valign="top">
			<th scope="row"><?php _e('Sender name', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('smsapi_sname') ); ?>" value="<?php echo esc_attr( $o->get('smsapi_sname') ); ?>" class="regular-text code" /></td>
		</tr>
	</table>
	<hr>



	<?php
	/******************************************************************
	*
	*       MAIL
	*
	******************************************************************/
	?>

	<h2><?php _e('Mail options'); ?></h2>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Enable', 'opsi_pc'); ?></th>
			<td>
				<fieldset>
					<label for="<?php echo esc_attr( $v->input_name('mail_enabled') ); ?>">
						<input name="<?php echo esc_attr( $v->input_name('mail_enabled') ); ?>" type="checkbox" id="<?php echo esc_attr( $v->input_name('mail_enabled') ); ?>" value="yes" <?php checked( 'yes', $o->get('mail_enabled') ); ?>>
					</label>
				</fieldset>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row"><?php _e('Sender name', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('mail_sender_name') ); ?>" value="<?php echo esc_attr( $o->get('mail_sender_name') ); ?>" class="regular-text code" /></td>
		</tr>
		 
		<tr valign="top">
			<th scope="row"><?php _e('Sender email', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('mail_sender_email') ); ?>" value="<?php echo esc_attr( $o->get('mail_sender_email') ); ?>" class="regular-text code" /></td>
		</tr>

		<tr valign="top">
			<th scope="row"><?php _e('Enable sandbox / development', 'opsi_pc'); ?></th>
			<td>
				<fieldset>
					<label for="<?php echo esc_attr( $v->input_name('mail_dev_enabled') ); ?>">
						<input name="<?php echo esc_attr( $v->input_name('mail_dev_enabled') ); ?>" type="checkbox" id="<?php echo esc_attr( $v->input_name('mail_dev_enabled') ); ?>" value="yes" <?php checked( 'yes', $o->get('mail_dev_enabled') ); ?>>
					</label>
				</fieldset>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row"><?php _e('Development email address', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('mail_dev_sendto') ); ?>" value="<?php echo esc_attr( $o->get('mail_dev_sendto') ); ?>" class="regular-text code" /></td>
		</tr>
		
	</table>
	<hr>

	<?php
	/******************************************************************
	*
	*       ORDER
	*
	******************************************************************/
	?>

	<h2><?php _e('WooCommerce Order'); ?></h2>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Country', 'opsi_pc'); ?></th>
			<td><input type="text" name="<?php echo esc_attr( $v->input_name('order_country') ); ?>" value="<?php echo esc_attr( $o->get('order_country') ); ?>" class="regular-text code" /></td>
		</tr>
	</table>
	<hr>

	<?php
	/******************************************************************
	*
	*       TERM PAGE
	*
	******************************************************************/
	?>

	<h2><?php _e('Terms and conditions page options', 'opsi_pc'); ?></h2>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Default page', 'opsi_pc'); ?></th>
			<td>
				<select name="<?php echo esc_attr( $v->input_name('terms_page') ); ?>">
					<option></option>
					<?php
					$terms_page = $o->get('terms_page');
					$pages = get_posts( array(
							'orderby'          => 'post_title',
							'order'            => 'ASC',
							'posts_per_page'   => -1,
							'post_status'      => 'publish',
							'post_type'        => 'page',
						) );
					foreach ($pages as $page) {
						?>
						<option value="<?php echo esc_attr($page->ID); ?>" <?php selected($terms_page, $page->ID); ?>><?php echo esc_html( $page->post_title ); ?></option>
						<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><?php _e('Open as', 'opsi_pc'); ?></th>
			<td>
				<fieldset>
					<label>
						<input type="radio" name="<?php echo esc_attr( $v->input_name('terms_open_as') ); ?>" value="popup_window" <?php checked( 'popup_window', $o->get('terms_open_as') ); ?>> <?php esc_html_e('Popup window', 'opsi_pc'); ?>
						&nbsp;
						&nbsp;
						<?php esc_html_e('window width'); ?> <input type="number" name="<?php echo esc_attr( $v->input_name('terms_window_width') ); ?>" value="<?php echo esc_attr( $o->get('terms_window_width') ); ?>" min="200" step="1">
						&nbsp;
						&nbsp;
						<?php esc_html_e('window height'); ?> <input type="number" name="<?php echo esc_attr( $v->input_name('terms_window_height') ); ?>" value="<?php echo esc_attr( $o->get('terms_window_height') ); ?>" min="200" step="1">
					</label>
					<br>
					<label>
						<input type="radio" name="<?php echo esc_attr( $v->input_name('terms_open_as') ); ?>" value="new_window" <?php checked( 'new_window', $o->get('terms_open_as') ); ?>> <?php esc_html_e('New tab / window', 'opsi_pc'); ?>
					</label>
					<br>
					<label>
						<input type="radio" name="<?php echo esc_attr( $v->input_name('terms_open_as') ); ?>" value="same_window" <?php checked( 'same_window', $o->get('terms_open_as') ); ?>> <?php esc_html_e('Same tab / window', 'opsi_pc'); ?>
					</label>
				</fieldset>
			</td>
		</tr>
	</table>
	<hr>

	<?php
	/******************************************************************
	*
	*       POSTS
	*
	******************************************************************/
	?>

	<h2><?php _e('Posts options', 'opsi_pc'); ?></h2>
	<p><?php _e('For form post number and post autocomplete', 'opsi_pc'); ?></p>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Default page', 'opsi_pc'); ?></th>
			<td>
				<p><?php _e('Paste each post / city in separate rows. Each row for post / city must be in format "POST_NUMBER CITY NAME THAT CAN HAVE MULTIPLE WORDS".', 'opsi_pc'); ?></p>
				<p>
					<?php
					$post_numbers = $o->get('post_numbers');
					$post_numbers_str = "";
					foreach ($post_numbers as $post_number_data) {
						$row = trim( $post_number_data['post_number'] . ' ' . $post_number_data['city'] );
						if( ! empty( $post_numbers_str ) ){
							$post_numbers_str .= "\n";
						}
						$post_numbers_str .= $row;
					}
					?>
					<textarea name="<?php echo esc_attr( $v->input_name('post_numbers') ); ?>" rows="10" cols="50" id="<?php echo esc_attr( $v->input_name('post_numbers') ); ?>" class="large-text code"><?php echo $post_numbers_str; ?></textarea></p>
			</td>
		</tr>
	</table>
	<hr>
	
	<?php submit_button(); ?>

</form>
</div>