<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

$v = OpSi_ProductCheckout_View::getInstance();
$o = OpSi_ProductCheckout_Options::getInstance();

wp_nonce_field('opsi-product-checkout-metabox-nonce', 'opsi-pc-nonce');
?>
<p>
	<input type="checkbox" name="<?php echo esc_attr($v->input_name('enabled')); ?>" <?php checked($meta['enabled'], 'yes'); ?> value="yes" /> <?php esc_html_e('Enabled', 'opsi-pc'); ?>
</p>
<h4><?php esc_html_e('Product', 'opsi_pc'); ?></h4>
<p>
	<select name="<?php echo esc_attr($v->input_name('product_id')); ?>">
		<option value=""></option>
		<?php foreach ($woo_products as $woo_product) { ?>
			<option value="<?php echo esc_attr($woo_product->ID); ?>" <?php selected($woo_product->ID, $meta['product_id']); ?> >
				<?php echo esc_html($woo_product->post_title . ' [' . $v->product_formated_price($woo_product->ID) . ']'); ?>
			</option>
		<?php } ?>
	</select>
</p>
<p>
	<?php esc_html_e('Quantity'); ?>: <input type="number" step="1" min="1" name="<?php echo esc_attr($v->input_name('quantity')); ?>" value="<?php echo esc_attr($meta['quantity']); ?>">
</p>
<p>
	<?php esc_html_e('Shipment cost / price'); ?>: <input type="number" step="0.01" min="0" name="<?php echo esc_attr($v->input_name('shipment_price')); ?>" value="<?php echo esc_attr($meta['shipment_price']); ?>"> <?php echo esc_html(get_woocommerce_currency_symbol()); ?>
</p>
<p>
	<input type="checkbox" name="<?php echo esc_attr($v->input_name('mail_enabled')); ?>" <?php checked($meta['mail_enabled'], 'yes'); ?> value="yes" /> <?php esc_html_e('Send mail on product purchase', 'opsi-pc'); ?>
	<?php if( ! $v->boolean( $o->get('mail_enabled') ) ){ ?>&nbsp;<small><?php _e('(Mails are disabled in "Product Checkout" settings page. To send mails, enable it in settings.)', 'opsi_pc'); ?></small><?php } ?>
</p>
<p>
	<?php esc_html_e('Mail subject'); ?>: <input type="text" name="<?php echo esc_attr($v->input_name('mail_subject')); ?>" value="<?php echo esc_attr($meta['mail_subject']); ?>">
</p>
<p>
	<input type="checkbox" name="<?php echo esc_attr($v->input_name('sms_enabled')); ?>" <?php checked($meta['sms_enabled'], 'yes'); ?> value="yes" /> <?php esc_html_e('Send sms on product purchase.', 'opsi-pc'); ?>
	<?php if( ! $v->boolean( $o->get('smsapi_enabled') ) ){ ?>&nbsp;<small><?php _e('(Sms messages are disabled in "Product Checkout" settings page. To send sms messages, enable it in settings.)', 'opsi_pc'); ?></small><?php } ?>
</p>
<h4><?php esc_html_e('Upsell products'); ?> <small><?php esc_html_e( '(Optional) / (you can select multiple products)', 'opsi_pc' ); ?></small></h4>
<p>
	<select name="<?php echo esc_attr($v->input_name('upsell_products')); ?>[]" multiple class="wc-enhanced-select">
		<?php foreach ($woo_products as $woo_product) { ?>
			<option value="<?php echo esc_attr($woo_product->ID); ?>" <?php echo in_array($woo_product->ID, $meta['upsell_products']) ? "selected" : ""; ?>><?php echo esc_html($woo_product->post_title . ' [' . $v->product_formated_price($woo_product->ID) . '] / [ ID : '.$woo_product->ID.']'); ?></option>
		<?php } ?>
	</select>
</p>
<p>
	<?php esc_html_e('Upsell product page', 'opsi_pc'); ?> <select name="<?php echo esc_attr($v->input_name('upsell_page')); ?>">
		<?php foreach ($pages as $page) { ?>
			<option value="<?php echo esc_attr($page->ID); ?>" <?php selected($page->ID, $meta['upsell_page']); ?> <?php echo $page->ID == $post->ID ? 'disabled' : ''; ?>><?php echo esc_html($page->post_title); ?></option>
		<?php } ?>
	</select> <small><?php esc_html_e('Only used if at least one upsell product is selected', 'opsi_pc'); ?></small>
</p>
<p>
	<input type="checkbox" name="<?php echo esc_attr($v->input_name('upsell_confirm_enabled')); ?>" <?php checked($meta['upsell_confirm_enabled'], 'yes'); ?> value="yes" /> <?php esc_html_e('User must confirm upsell product.', 'opsi-pc'); ?>
</p>
<p>
	<?php esc_html_e('User confirm content'); ?>: <input type="text" name="<?php echo esc_attr($v->input_name('upsell_confirm_content')); ?>" value="<?php echo esc_attr($meta['upsell_confirm_content']); ?>">
	<small><?php _e('(used only when "user must confirm upsell product" is enabled)', 'opsi_pc'); ?></small>
</p>
<p>
	<input type="checkbox" name="<?php echo esc_attr($v->input_name('mail_upsell_enabled')); ?>" <?php checked($meta['mail_upsell_enabled'], 'yes'); ?> value="yes" /> <?php esc_html_e('Send mail on upsell product purchase', 'opsi-pc'); ?>
	<?php if( ! $v->boolean( $o->get('mail_enabled') ) ){ ?>&nbsp;<small><?php _e('(Mails are disabled in "Product Checkout" settings page. To send mails, enable it in settings.)', 'opsi_pc'); ?></small><?php } ?>
</p>
<p>
	<?php esc_html_e('Mail subject'); ?>: <input type="text" name="<?php echo esc_attr($v->input_name('mail_upsell_subject')); ?>" value="<?php echo esc_attr($meta['mail_upsell_subject']); ?>">
</p>
<p>
	<input type="checkbox" name="<?php echo esc_attr($v->input_name('sms_uspell_enabled')); ?>" <?php checked($meta['sms_uspell_enabled'], 'yes'); ?> value="yes" /> <?php esc_html_e('Send sms on upsell product purchase.', 'opsi-pc'); ?>
	<?php if( ! $v->boolean( $o->get('smsapi_enabled') ) ){ ?>&nbsp;<small><?php _e('(Sms messages are disabled in "Product Checkout" settings page. To send sms messages, enable it in settings.)', 'opsi_pc'); ?></small><?php } ?>
</p>
<h4><?php esc_html_e('Thank you page', 'opsi_pc'); ?></h4>
<p>
	<select name="<?php echo esc_attr($v->input_name('thankyou_page')); ?>">
		<?php foreach ($pages as $page) { ?>
			<option value="<?php echo esc_attr($page->ID); ?>" <?php selected($page->ID, $meta['thankyou_page']); ?> <?php echo $page->ID == $post->ID ? 'disabled' : ''; ?>><?php echo esc_html($page->post_title); ?></option>
		<?php } ?>
	</select>
</p>
<h4><?php esc_html_e('Payment options', 'opsi_pc'); ?></h4>
<p>
	<input type="checkbox" name="<?php echo esc_attr($v->input_name('enable_payment_cod')); ?>" <?php checked($meta['enable_payment_cod'], 'yes'); ?> value="yes" /> <?php esc_html_e('Cash on delivery', 'opsi-pc'); ?><br>
	<input type="checkbox" name="<?php echo esc_attr($v->input_name('enable_payment_paypal')); ?>" <?php checked($meta['enable_payment_paypal'], 'yes'); ?> value="yes" /> <?php esc_html_e('Paypal', 'opsi-pc'); ?>
	<?php if(!$v->payment_type_available('paymill')){ ?><small>(<?php esc_html_e('This payment type is currently not active. Check Product Checkout settings.'); ?>)</small><?php } ?>
	<br>
	<input type="checkbox" name="<?php echo esc_attr($v->input_name('enable_payment_paymill')); ?>" <?php checked($meta['enable_payment_paymill'], 'yes'); ?> value="yes" /> <?php esc_html_e('Paymill', 'opsi-pc'); ?>
	<?php if(!$v->payment_type_available('paymill')){ ?><small>(<?php esc_html_e('This payment type is currently not active. Check Product Checkout settings.'); ?>)</small><?php } ?>
</p>
<h4><?php esc_html_e('Template', 'opsi_pc'); ?></h4>
<p>
	<?php esc_html_e('Template code / file name'); ?>: <input type="text" name="<?php echo esc_attr($v->input_name('template')); ?>" value="<?php echo esc_attr($meta['template']); ?>"> (<?php esc_html_e('optional - if left empty "default" template will be used.', 'opsi_pc'); ?>)
</p>
<h4><?php esc_html_e('Terms', 'opsi_pc'); ?></h4>
<p>
	<select name="<?php echo esc_attr($v->input_name('terms_page_id')); ?>">
		<option value=""><?php esc_html_e('Default page set in settings for "Product Checkout".'); ?></option>
		<?php foreach ($pages as $page) { ?>
			<option value="<?php echo esc_attr($page->ID); ?>" <?php selected($page->ID, $meta['terms_page_id']); ?> <?php echo $page->ID == $post->ID ? 'disabled' : ''; ?>><?php echo esc_html($page->post_title); ?></option>
		<?php } ?>
	</select>
</p>