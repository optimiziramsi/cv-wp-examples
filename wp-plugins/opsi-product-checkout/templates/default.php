<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

$v = OpSi_ProductCheckout_View::getInstance();
$h = OpSi_ProductCheckout_Handler::getInstance();

?>

<form action="" method="post" novalidate>
	
	<?php $h->dump_data(); ?>

	<?php wp_nonce_field($v->input_name('nonce')) ?>
	<input type="hidden" name="<?php echo esc_attr( $v->input_name('submit') ); ?>" value="yes">


	<section>
		
		<!-- fields -->
		<?php foreach( $v->form_fields() as $field_id => $field ){ ?>

			<label for="<?php esc_attr($field_id); ?>" class="pp-input-label">
				<?php echo esc_html( isset($field['label']) ? $field['label'] : __('Field label not set', 'opsi_pc') ); ?>
				<?php if(isset($field['required']) && $field['required']) { ?>
					<span class="red">*</span>
				<?php } ?>
				<?php if( $v->error_for($field_id) ){ ?>
					<span class="pp-input-error"><i class="material-icons">error</i> <?php echo esc_html($v->error_for($field_id)); ?></span>
				<?php } ?>
				</label>
			<input
				type="text"
				placeholder="<?php echo esc_attr( isset($field['placeholder']) ? $field['placeholder'] : '' ); ?>"
				title="<?php echo esc_attr(isset($field['label']) ? $field['label'] : __('Field label not set', 'opsi_pc')); ?>"
				name="<?php echo esc_attr( $v->input_name($field_id) ); ?>"
				value="<?php echo esc_attr($v->old($field_id)); ?>"
				maxlength="255"
				></input>

		<?php } ?>

		<!-- payment options -->
		<label for="payment" class="pp-input-label">
			Izberite način plačila <?php if( $v->error_for('payment_option') ){ ?><span class="pp-input-error"><i class="material-icons">error</i> <?php echo esc_html($v->error_for('payment_option')); ?></span><?php } ?>
			<?php if( $v->error_for('paymill') ){ ?><span class="pp-input-error"><i class="material-icons">error</i> <?php echo esc_html($v->error_for('paymill')); ?></span><?php } ?>
		</label>
		<?php foreach( $v->payment_options() as $payment_option_id => $payment_option ){

			if( ! $v->payment_option_enabled( $meta, $payment_option_id ) ){
				continue;
			}

			$checked = ($v->old('payment_option') == $payment_option_id);
			if('cod' == $payment_option_id && empty($v->old('payment_option'))){
				$checked = true;
			}

			?>
			
			<p class="pp-radio-holder">
				<input type="radio" name="<?php echo esc_attr( $v->input_name('payment_option') ); ?>" value="<?php echo esc_attr($payment_option_id); ?>" placeholder="" <?php echo ($checked ? 'checked="checked"' : ''); ?>><span class="pp-radio-label">
					<?php echo esc_html($payment_option['label']); ?>
				</span>

				<?php
				if( isset( $payment_option['images'] ) && is_array( $payment_option['images'] ) ){
					foreach ($payment_option['images'] as $idx => $img_url) {
						?>
						<img class="pp-select-cards pp-select-first-card" width="45" src="<?php echo esc_attr($img_url); ?>" alt="">
						<?php
					}
				}
				?>

				<?php
				if( isset( $payment_option['content_callback'] ) ){
					echo '<div class="payment_option_content ' . $payment_option_id . '">';
					call_user_func($payment_option['content_callback']);
					echo '</div>';
				}
				?>
			</p>

		<?php } ?>

	</section>
	
	<?php if( $v->error_for('terms_agree') ){ ?><span class="pp-input-error"><i class="material-icons">error</i> <?php echo esc_html($v->error_for('terms_agree')); ?></span><?php } ?>
	<input type="checkbox" name="<?php echo esc_attr( $v->input_name('terms_agree') ); ?>" value="yes" <?php checked('yes', $v->old('terms_agree') ); ?>> <?php echo sprintf( _e('Agree to %s', 'opsi_pc'), $v->get_terms_link( $meta, __('terms of use', 'opsi_pc') ) ); ?>


	<input type="submit" value="ORDER PRODUCT">

</form>