<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

$v = OpSi_ProductCheckout_View::getInstance();
$h = OpSi_ProductCheckout_Handler::getInstance();

?>

<?php if( $v->error_for('upsell') ){ ?>
<p>
	ERROR: <?php echo $v->error_for('upsell'); ?>
</p>
<?php } ?>

<form action="" method="post">

	<?php wp_nonce_field($v->input_name('upsell_nonce')) ?>
	<input type="hidden" name="<?php echo esc_attr( $v->input_name('upsell_submit') ); ?>" value="yes">
	<input type="hidden" name="<?php echo esc_attr( $v->input_name('upsell_product_id') ); ?>" value="-1">
	<input type="submit" value="<?php esc_attr_e('No thanks', 'opsi_pc'); ?>">

</form>

<form action="" method="post">

	<?php wp_nonce_field($v->input_name('upsell_nonce')) ?>
	<input type="hidden" name="<?php echo esc_attr( $v->input_name('upsell_submit') ); ?>" value="yes">
	<input type="hidden" name="<?php echo esc_attr( $v->input_name('upsell_product_id') ); ?>" value="17916">
	<input type="submit" value="<?php esc_attr_e('Add this product', 'opsi_pc'); ?>">
	
</form>

<form action="" method="post">

	<?php wp_nonce_field($v->input_name('upsell_nonce')) ?>
	<input type="hidden" name="<?php echo esc_attr( $v->input_name('upsell_submit') ); ?>" value="yes">
	<input type="hidden" name="<?php echo esc_attr( $v->input_name('upsell_product_id') ); ?>" value="16450">
	<input type="submit" value="<?php esc_attr_e('Add this product', 'opsi_pc'); ?>">

</form>