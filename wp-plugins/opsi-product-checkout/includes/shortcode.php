<?php

class OpSi_ProductCheckout_Shortcode {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {
		
		add_shortcode( 'product_checkout', array( $this, 'product_checkout' ) );
		add_shortcode( 'product_checkout_upsell', array( $this, 'product_checkout_upsell' ) );

		add_action( 'wp_enqueue_scripts', array( $this, 'check_enqueue_scripts' ) );

	}

	public function product_checkout( $atts ) {
		// $atts = shortcode_atts( array(), $atts, 'product_checkout' );
		
		$v = OpSi_ProductCheckout_View::getInstance();
		$h = OpSi_ProductCheckout_Handler::getInstance();

		// check for previous data and reset it if needed
		if( $h->get_order_id() ){
			$h->reset_data();
		}

		$meta = OpSi_ProductCheckout_Metabox::get_meta( get_the_ID() );



		ob_start();
		$template_path = $v->template_path($meta['template']);
		include $template_path ;
		$content = ob_get_clean();

		$h = OpSi_ProductCheckout_Handler::getInstance();
		$h->reset_data();

		// $this->enqueue_scripts( get_the_ID() );

		return $content;
	}

	public function product_checkout_upsell( $atts ){
		// $atts = shortcode_atts( array(), $atts, 'product_checkout' );
		
		// helppers
		$v = OpSi_ProductCheckout_View::getInstance();
		$h = OpSi_ProductCheckout_Handler::getInstance();

		// $h->dump_data();

		// check for previous order, if not found, return empty string
		if( is_null( $h->get_order_id() ) || is_null( $h->get_page_id() ) )
			return '';

		// get page meta
		$meta = OpSi_ProductCheckout_Metabox::get_meta( $h->get_page_id() );

		// check we are on the page that is defined for upsell
		if( get_the_ID() != $meta['upsell_page'] )
			return '';

		// check upsell products exists
		if( 0 == count( $meta['upsell_products'] ) )
			return '';

		$template_path = $v->upsell_template_path( $meta['template'] );

		ob_start();
		include $template_path ;
		$content = ob_get_clean();

		// $this->enqueue_scripts( $h->get_page_id() );

		return $content;
	}

	public function check_enqueue_scripts(){
		// helppers
		$v = OpSi_ProductCheckout_View::getInstance();
		$h = OpSi_ProductCheckout_Handler::getInstance();

		// check if page id is set in handler / it is upsell page
		if( ! is_null( $h->get_page_id() ) ){

			$meta = OpSi_ProductCheckout_Metabox::get_meta( $h->get_page_id() );

			if( get_the_ID() == $meta['upsell_page'] ) {
				$this->enqueue_scripts( $h->get_page_id() );
				return;
			}
		}

		// check if current page is product checkout page
		$meta = OpSi_ProductCheckout_Metabox::get_meta( get_the_ID() );
		if( $v->boolean( $meta['enabled'] ) ){
			$this->enqueue_scripts( get_the_ID() );
			return;
		}
	}

	private function enqueue_scripts( $page_id ){
		// helppers
		$v = OpSi_ProductCheckout_View::getInstance();
		$o = OpSi_ProductCheckout_Options::getInstance();

		// page meta
		$meta = OpSi_ProductCheckout_Metabox::get_meta( $page_id );

		wp_enqueue_script('opsi_pc', $v->url('assets/js/script.js'), array('jquery'), OPSI_PRODUCTCHECKOUT_VERSION, false);

		wp_localize_script('opsi_pc', 'opsi_pc', array(
			'payment_option_input_name' => $v->input_name('payment_option'),

			'paymill_currency' => get_woocommerce_currency(),
			'paymill_amount_int' => $v->get_order_total_price( $meta ) * 100,

			'paymill_token_input_name' => $v->input_name('paymill_token'),

			'paymill_card_number_input_id' => $v->input_name('paymill_card_number'),
			'paymill_card_cvc_input_id' => $v->input_name('paymill_card_cvc'),
			'paymill_card_expiry_month_input_id' => $v->input_name('paymill_card_expiry_month'),
			'paymill_card_expiry_year_input_id' => $v->input_name('paymill_card_expiry_year'),
			'paymill_card_holder_name_input_id' => $v->input_name('paymill_card_holder_name'),

			'paymill_error_getting_token' => __('Checking you card returned error.\nError message is:\n', 'opsi_pc'),

			'post_numbers' => $o->get('post_numbers'),
			'post_numbers_input_name' => $v->input_name('postcode'),
			'city_input_name' => $v->input_name('city'),

			'terms_open_as_window' => ( 'popup_window' == $o->get('terms_open_as') ? true : false ),
			'terms_anchor_class' => $v->input_name('terms_anchor_class'),
			'terms_window_width' => $o->get('terms_window_width'),
			'terms_window_height' => $o->get('terms_window_height'),

			'upsell_confirm_enabled' => $v->boolean( $meta['upsell_confirm_enabled'] ) && ! empty( $meta['upsell_confirm_content'] ),
			'upsell_confirm_content' => $meta['upsell_confirm_content'],
			'upsell_confirm_input' => $v->input_name('upsell_product_id'),

			) );
	}

}