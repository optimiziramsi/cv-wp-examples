<?php

class OpSi_ProductCheckout_View {

	private $input_prefix = 'opsi_product_checkout_';

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {
		
	}

	/*************************************************
			LISTS - START
	*************************************************/
	
	/**
	 * returns list of form fields
	 */
	public function form_fields(){
		// key => field id
		// values (key => (type) : description)
		// 		label: (string) : display name of the field ()
		// 		required: (bool) : if field is required
		// 		placeholder: (string) : (optional) content to describe field
		$fields = [
			'name' => [ 'label' => __('Name and lastname', 'opsi_pc'), 'required' => true, 'placeholder' => __('Name and lastname', 'opsi_pc') ],
			'email' => [ 'label' => __('E-mail', 'opsi_pc'), 'required' => true, 'placeholder' => __('E-mail where you will recieve order information', 'opsi_pc') ], 
			'street' => [ 'label' => __('Street', 'opsi_pc'), 'required' => true, 'placeholder' => __('Street', 'opsi_pc') ],
			'postcode' => [ 'label' => __('Post', 'opsi_pc'), 'required' => true, 'placeholder' => __('Post', 'opsi_pc') ], 
			'city' => [ 'label' => __('City', 'opsi_pc'), 'required' => true, 'placeholder' => __('City', 'opsi_pc') ], 
			'phone' => [ 'label' => __('Phone', 'opsi_pc'), 'required' => true, 'placeholder' => __('Phone', 'opsi_pc') ], 
		];

		return apply_filters('opsi_pc_form_fields', $fields);
	}

	/**
	 * returns list of payment options
	 */
	public function payment_options(){
		// key => payment id
		// value => key:data values
		// 		label : (string)
		// 		images: (array of url) : (optional)
		// 		content_callback: (string / array)

		$options = [
			'cod' => [ 'label' => __('Cach on delivery', 'opsi_pc') ],

			'paypal' => [ 'label' => __('Paypal', 'opsi_pc'), 'images' => [
					$this->url('assets/images/paypal.png')
				]
			],

			'paymill' => [ 'label' => __('Credit card', 'opsi_pc'), 'images' => [
					$this->url('assets/images/visa.png', OPSI_PRODUCTCHECKOUT_FILE),
					$this->url('assets/images/mastercard.png', OPSI_PRODUCTCHECKOUT_FILE)
				],
				'content_callback' => array($this, 'paymill_html')
			],
		];

		return apply_filters('opsi_pc_payment_options', $options);
	}
	
	/*************************************************
			LISTS - END
	*************************************************/

	/*************************************************
			HELPERS - START
	*************************************************/
	
	public function input_name($for){
		return $this->input_prefix . str_replace(' ', '_', $for);
	}

	public function url($path){
		return plugins_url($path, OPSI_PRODUCTCHECKOUT_FILE);
	}

	public function formated_price($price){
		$decimal_separator = wc_get_price_decimal_separator();
		$thousand_separator = wc_get_price_thousand_separator();
		$decimals = wc_get_price_decimals();
		$price_format = get_woocommerce_price_format();
		$currency = '';
		$negative        = $price < 0;

		$price = floatval( $negative ? $price * -1 : $price );
		$price = number_format( $price, $decimals, $decimal_separator, $thousand_separator );

		$formatted_price = ( $negative ? '-' : '' ) . sprintf( $price_format, get_woocommerce_currency_symbol( $currency ), $price );
		return $formatted_price;
	}

	public function product_formated_price( $product_id ){
		$_product = wc_get_product( $product_id );
		if( ! $_product ){
			return '';
		}
		$price = $_product->get_price();

		return $this->formated_price($price);
	}

	public function payment_type_available( $payment_type_code ){
		// TODO
		return true;
	}

	public function payment_option_enabled( $meta, $payment_option ){
		return isset( $meta['enable_payment_' . $payment_option] ) && $this->boolean( $meta['enable_payment_' . $payment_option] ) ? true : false;
	}

	public function boolean( $str ){
		return 'yes' == $str;
	}

	public function get_order_product_quantity( $meta, $is_upsell = false ){
		return $is_upsell ? 1 : (int)$meta['quantity'];
	}

	public function get_order_product_price( $meta, $is_upsell = false ){
		$_product = wc_get_product( $is_upsell ? $meta['upsell_product_id'] : $meta['product_id'] );
		$product_price = $_product->get_price();
		return (float)$product_price;
	}

	public function get_order_product_name( $meta, $is_upsell = false ){
		$_product = wc_get_product( $is_upsell ? $meta['upsell_product_id'] : $meta['product_id'] );
		return $_product->get_title();
	}

	public function get_order_shipping_price( $meta, $is_upsell = false ){
		return $is_upsell ? 0 : (float)$meta['shipment_price'];
	}

	public function get_order_total_price( $meta, $is_upsell = false ){
		// product price
		$product_price = $this->get_order_product_price( $meta, $is_upsell );
		// quantity
		$quantity = $this->get_order_product_quantity( $meta, $is_upsell );
		// shipping price
		$shipping_price = $this->get_order_shipping_price( $meta, $is_upsell );

		return ($product_price * $quantity + $shipping_price);
	}

	public function get_terms_link( $meta, $content, $title = "" ){
		// helpers
		$o = OpSi_ProductCheckout_Options::getInstance();

		$terms_page_id = isset( $meta['terms_page_id'] ) && (int)$meta['terms_page_id'] > 0 ? (int)$meta['terms_page_id'] : $o->get('terms_page');
		$href = get_permalink( $terms_page_id );

		$target = 'new_window' == $o->get('terms_open_as') ? "_blank" : "";

		$class = $this->input_name('terms_anchor_class');
		$title = empty( $title ) ? $content : $title;

		return sprintf( '<a href="%s" target="%s" class="%s" title="%s">%s</a>', esc_attr( $href ), esc_attr( $target ), esc_attr( $class ), esc_attr( $title ), esc_html( $content ) );
	}
	
	/*************************************************
			HELPERS - END
	*************************************************/

	/*************************************************
			OLD / SUBMITED FORM VALUES - START
	*************************************************/
	
	public function old($for){
		$h = OpSi_ProductCheckout_Handler::getInstance();
		$val = $h->field( $for );
		return ! is_null( $val ) ? $val : '';
	}
	
	/*************************************************
			OLD / SUBMITED FORM VALUES - END
	*************************************************/

	/*************************************************
			ERRORS - START
	*************************************************/
	
	/**
	 * get error by id / key if it exists
	 */
	public function error_for($for){
		$h = OpSi_ProductCheckout_Handler::getInstance();
		$val = $h->error( $for );
		return ! is_null( $val ) ? $val : '';
	}
	
	/*************************************************
			ERRORS - END
	*************************************************/

	/*************************************************
			TEMPLATES - START
	*************************************************/

	public function template_path($template){
		return $this->get_template_path( $template, '' );
	}

	public function upsell_template_path($template){
		return $this->get_template_path( $template, 'upsell' );
	}

	public function mail_template_path( $template, $upsell ){
		return $this->get_template_path( $template, $upsell ? 'mail-order-upsell' : 'mail-order' );
	}

	public function sms_template_path( $template, $upsell ){
		return $this->get_template_path( $template, $upsell ? 'sms-order-upsell' : 'sms-order' );
	}

	private function get_template_path( $template, $code ){
		if( empty( $template ) )
			$template = 'default';

		$template_postfix = ( ! empty( $code ) ? '-'.$code : '' ) . '.php';

		$default_prefix = 'default';
		$template_prefix = $template;

		// check if template exists in theme
		$path = get_template_directory() . '/opsi-product-checkout/' . $template_prefix . $template_postfix;
		if( file_exists( $path ) )
			return $path;

		// check default template in theme
		$path = get_template_directory() . '/opsi-product-checkout/' . $default_prefix . $template_postfix;
		if( file_exists( $path ) )
			return $path;

		// if no template found, use plugin template as example
		$path = OPSI_PRODUCTCHECKOUT_PATH . '/templates/' . $default_prefix . $template_postfix;
		return $path;
	}
	
	/*************************************************
			TEMPLATES - END
	*************************************************/

	/*************************************************
			OTHER - START
	*************************************************/
	
	public function paymill_html(){
		// helppers
		$o = OpSi_ProductCheckout_Options::getInstance();

		$paymill_config = $o->get_paymill_config();

		// page meta
		$meta = OpSi_ProductCheckout_Metabox::get_meta( get_the_ID() );

		?>
		<span class="paymillMsg"></span>
		
		<img class="pp-select-cards pp-select-first-card" width="45" src="<?php echo esc_attr( $this->url('assets/images/visa.png') ); ?>" alt="">
		<img class="pp-select-cards" width="45" src="<?php echo esc_attr( $this->url('assets/images/mastercard.png') ); ?>" alt="">

		<input type="hidden" name="<?php echo esc_attr( $this->input_name('paymill_token') ); ?>" autocomplete="off" value="">
	
		<script type="text/javascript">
			PAYMILL_PUBLIC_KEY = <?php echo json_encode( $paymill_config['public_key'] ); ?>;
		</script>
		<script src="https://bridge.paymill.com/"></script>

		<span class="paymill-inputs" style="width: 100%;">

			Plačilo in validacija poteka preko varne povezave in ponudnika Paymill.
			<br>
			<br>
			
			<div class="bg-wrapper-paymill">

			<div class="paymill-field-left">
				<label>Številka kartice<span class="red">*</span></label>
				<input type="text" required id="<?php echo esc_attr( $this->input_name('paymill_card_number') ); ?>" autocomplete="off" size="20">
			</div>

			<div class="paymill-field-right">
				<label>Varnostna koda (CVV/CVC/CID)<span class="red">*</span></label>
				<input type="text" required id="<?php echo esc_attr( $this->input_name('paymill_card_cvc') ); ?>" autocomplete="off" size="4">
			</div>

			<div class="paymill-field-left">
				<label>Ime in priimek lastnika kartice<span class="red">*</span></label>
				<input type="text" required id="<?php echo esc_attr( $this->input_name('paymill_card_holder_name') ); ?>" autocomplete="off" size="4">
			</div>


			<div class="paymill-field-right">
				<label>Datum veljavnosti<span class="red">*</span></label>

				<div class="paymill-field-left">
				<select required id="<?php echo esc_attr( $this->input_name('paymill_card_expiry_month') ); ?>" autocomplete="off">
					<option value=""></option>
					<?php for ($i=1; $i <= 12; $i++) { ?>
					<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
					<?php } ?>
				</select>
				</div>

				<div class="paymill-field-right">
				<select required id="<?php echo esc_attr( $this->input_name('paymill_card_expiry_year') ); ?>" autocomplete="off">
					<option value=""></option>
					<?php $current_year = (int)date('Y'); ?>
					<?php for ($i=$current_year; $i <= $current_year+12; $i++) { ?>
					<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
					<?php } ?>
				</select>
				</div>

			</div>
			<span class="clearfix"></span>


		</div>

		</span>

		<?php
	}
	
	/*************************************************
			OTHER - END
	*************************************************/

}