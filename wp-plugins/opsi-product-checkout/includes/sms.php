<?php

class OpSi_ProductCheckout_Sms {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {
		add_action( 'opsi_product_checkout_order_processed', array( $this, 'send_order_sms' ) );
		add_action( 'opsi_product_checkout_upsell_product_added', array( $this, 'send_order_updated_sms' ) );
	}

	public function send_order_sms( $order ){
		// helppers
		$v = OpSi_ProductCheckout_View::getInstance();
		$h = OpSi_ProductCheckout_Handler::getInstance();
		$o = OpSi_ProductCheckout_Options::getInstance();

		// meta
		$meta = OpSi_ProductCheckout_Metabox::get_meta( get_the_ID() );

		// check enabled
		if( ! $v->boolean( $o->get( 'smsapi_enabled' ) ) || ! $v->boolean( $meta['sms_enabled'] ) )
			return;
		
		// sms template
		$template_path = $v->sms_template_path( $meta['template'], false );

		// sms content
		ob_start();
		include $template_path;
		$sms_content = ob_get_clean();

		// send sms
		$this->send_sms( $sms_content );
	}

	public function send_order_updated_sms( $order ){
		// helppers
		$v = OpSi_ProductCheckout_View::getInstance();
		$h = OpSi_ProductCheckout_Handler::getInstance();
		$o = OpSi_ProductCheckout_Options::getInstance();

		// meta
		$meta = OpSi_ProductCheckout_Metabox::get_meta( $h->get_page_id() );

		// check enabled
		if( ! $v->boolean( $o->get( 'smsapi_enabled' ) ) || ! $v->boolean( $meta['sms_uspell_enabled'] ) )
			return;
		
		// sms template
		$template_path = $v->sms_template_path( $meta['template'], true );

		// sms content
		ob_start();
		include $template_path;
		$sms_content = ob_get_clean();

		// send sms
		$this->send_sms( $sms_content );
	}

	private function send_sms( $message ){
		// helpers
		$v = OpSi_ProductCheckout_View::getInstance();
		$o = OpSi_ProductCheckout_Options::getInstance();

		// trim content
		$message = trim( $message );

		// set reciever
		$to = $v->old('phone');

		// set smsapi url
		$smsapi_url = 'http://www.smsapi.si/poslji-sms';

		// get smsapi data
		$data = array(
			'to' => $to,
			'm' => $message,
			'un' => $o->get('smsapi_un'),
			'ps' => $o->get('smsapi_ps'),
			'from' => $o->get('smsapi_from'),
			'cc' => $o->get('smsapi_cc'),
			'sid' => $o->get('smsapi_sid'),
			'sname' => $o->get('smsapi_sname'),
		);

		// send request
		$response = wp_remote_post( $smsapi_url, array( 'body' => $data ) );

		// TODO handle response
		
		// TODO return success / error
	}

}