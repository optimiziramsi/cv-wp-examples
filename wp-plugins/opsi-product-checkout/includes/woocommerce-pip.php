<?php

class OpSi_ProductCheckout_WoocommercePip {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {
		// add_action( 'opsi_product_checkout_order_created', array( $this, 'generate_order_pip' ) );
	}

	public function generate_order_pip( $order ){

		if( class_exists('WC_PIP') && method_exists( $woo_affiliate, 'instance' ) ){

			$pip_instance = WC_PIP::instance();

			if( method_exists( $pip_instance, 'get_handler_instance' ) ){

				$pip_handler = $pip_instance->get_handler_instance();

				if( method_exists( $pip_handler, 'generate_invoice_number' ) ){
					
					$pip_handler->generate_invoice_number( $order->get_id() );

				}

			}
		}
		
	}

}