<?php

class OpSi_ProductCheckout_Options {

	const OPTIONS = "opsi-product-checkout";
	const URL_SLUG = "opsi-product-checkout";
	const SAVE_ACTION = "opsi_pc_save_options";
	const NONCE_ACTION = "opsi_pc_save_options";

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {
		if( !is_admin() || ! current_user_can('manage_options')){
			return;
		}
		
		add_action( 'admin_menu', array( $this, 'add_plugin_options_page' ) );
		add_action( 'admin_post_' . self::SAVE_ACTION, array( $this, 'save_options' ) );
		
	}

	/*************************************************
			OPTIONS MENU / PAGE - START
	*************************************************/
	
	public function add_plugin_options_page(){
		add_options_page(
            __('Product Checkout', 'opsi_pc'), 
            __('Product Checkout', 'opsi_pc'), 
            'manage_options',
            self::URL_SLUG,
            array( $this, 'options_page_html' )
        );
	}

	public function options_page_html(){
		include OPSI_PRODUCTCHECKOUT_PATH . '/views/options.php';
	}

	public function save_options(){
		// nonce and user check
		if( ! isset( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], self::NONCE_ACTION ) || ! current_user_can('manage_options') ){
			return;
		}

		$v = OpSi_ProductCheckout_View::getInstance();

		$new_options = $this->all();
		foreach ($new_options as $param => $val) {
			
			$post_key = $v->input_name( $param );
			$post_val = isset( $_POST[$post_key] ) ? $_POST[$post_key] : '';

			// post numbers must be parsed into array
			if('post_numbers' == $param){
				$post_numbers = explode("\n", $post_val);
				$post_numbers = array_filter( $post_numbers );

				$post_val = array();
				foreach ($post_numbers as $post_row) {
					$post_row_exp = explode(' ', $post_row, 2);
					if( 2 == count( $post_row_exp ) && ! empty( $post_row_exp[0] ) && ! empty( $post_row_exp[1] ) ){
						$post_val[] = array( 'post_number' => $post_row_exp[0], 'city' => $post_row_exp[1] );
					}
				}
			}
			// others we parse as strings
			else {
				$post_val = sanitize_text_field( $post_val );
			}

			$this->set( $param, $post_val, false );
		}

		$this->save();

		wp_redirect( add_query_arg('page', self::URL_SLUG, admin_url('options-general.php')) );
		exit();
	}
	
	/*************************************************
			OPTIONS MENU / PAGE - END
	*************************************************/

	private $options_cache = null;
	public function all(){
		if( ! is_null( $this->options_cache ) ){
			return $this->options_cache;
		}
		
		$options_val = get_option( self::OPTIONS, false );

		$options = shortcode_atts( array(
			// paymill
			'paymill_public_key' => '',
			'paymill_private_key' => '',

			'paymill_sandbox_enabled' => '',
			'paymill_sandbox_public_key' => '',
			'paymill_sandbox_private_key' => '',

			// paypal
			'paypal_api_username' => '',
			'paypal_api_password' => '',
			'paypal_api_signature' => '',

			'paypal_api_sandbox_enabled' => '',
			'paypal_api_sandbox_username' => '',
			'paypal_api_sandbox_password' => '',
			'paypal_api_sandbox_signature' => '',

			'paypal_brand_name' => '',
			'paypal_locale_code' => '',
			'paypal_country_code' => '',

			// sms api
			'smsapi_enabled' => '',
			'smsapi_un' => '',
			'smsapi_ps' => '',
			'smsapi_from' => '',
			'smsapi_cc' => '',
			'smsapi_sid' => '',
			'smsapi_sname' => '',

			// mail
			'mail_enabled' => '',
			'mail_sender_email' => '',
			'mail_sender_name' => '',
			'mail_dev_enabled' => '',
			'mail_dev_sendto' => '',

			// wc order
			'order_country' => '',

			// terms and conditions default page
			'terms_page' => '',
			'terms_open_as' => 'popup_window',	// [ popup_window, new_window, same_window ]
			'terms_window_width' => 424,
			'terms_window_height' => 600,

			// posts
			'post_numbers' => '',


		), $options_val );

		if( ! is_array( $options['post_numbers'] ) ){
			$options['post_numbers'] = array();
		}

		$this->options_cache = $options;

		return $this->options_cache;
	}

	public function get($param){
		$all = $this->all();
		if( isset( $all[$param] ) ){
			return $all[$param];
		}
		return null;
	}

	public function get_paymill_config(){
		// helppers
		$v = OpSi_ProductCheckout_View::getInstance();

		// check if sandobox / development
		$use_sandbox = $v->boolean( $this->get('paymill_sandbox_enabled') );
		
		$config = array(
			'public_key' => $use_sandbox ? $this->get('paymill_sandbox_public_key') : $this->get('paymill_private_key'),
			'private_key' => $use_sandbox ? $this->get('paymill_sandbox_private_key') : $this->get('paymill_public_key'),
		);

		return $config;
	}

	public function get_paypal_config(){
		// helppers
		$v = OpSi_ProductCheckout_View::getInstance();

		// check if sandobox / development
		$use_sandbox = $v->boolean( $this->get('paypal_api_sandbox_enabled') );

		$config = array(
			'api_url' => $use_sandbox ? 'https://api-3t.sandbox.paypal.com/nvp' : 'https://api-3t.paypal.com/nvp',
			'url' => $use_sandbox ? 'https://www.sandbox.paypal.com/webscr' : 'https://www.paypal.com/webscr',
			'version' => '64',

			'username' => $use_sandbox ? $this->get('paypal_sandbox_username') : $this->get('paypal_api_username'),
			'password' => $use_sandbox ? $this->get('paypal_sandbox__password') : $this->get('paypal_api_password'),
			'signature' => $use_sandbox ? $this->get('paypal_sandbox_psignature') : $this->get('paypal_api_signature'),
		);

		return $config;
	}

	public function set( $param, $value, $save_options = true ){
		
		// fill options cache		
		$this->all();

		// if param exists in options cache, set its value
		if( isset( $this->options_cache[$param] ) ){
			$this->options_cache[$param] = $value;
		}

		// save options
		if( $save_options ){
			$this->save();
		}
	}

	private function save(){
		$options = $this->all();

		update_option( self::OPTIONS, $options );
		$this->options_cache = null;
	}

}