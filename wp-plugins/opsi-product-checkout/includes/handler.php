<?php

class OpSi_ProductCheckout_Handler {

	private $fields = array();
	private $errors = array();
	private $meta = null;

	private $paypal = null;

	private $paymill_token = null;
	private $paymill_transation_id = null;

	private $order_id = null;
	private $selected_upsell_product_id = null;
	private $page_id = null;

	private $_is_upsell_request = false;

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {
		
		add_action('init', array( $this, 'session' ));

		add_action('template_redirect', array( $this, 'check_paypal_responde' ));
		add_action('template_redirect', array( $this, 'check_form_submit' ));
		add_action('template_redirect', array( $this, 'check_upsell_submit' ));

	}

	public function session(){
		// start if needed
		if( ! session_id() )
			session_start();

		// set data from session if data exists
		$this->set_data_from_session();
	}

	public function reset_data(){
		$this->fields = array();
		$this->errors = array();
		$this->paymill_transation_id = null;
		$this->paypal = null;
		$this->order_id = null;
		$this->page_id = null;
		$this->selected_upsell_product_id = null;

		$this->save_session();
	}

	public function check_paypal_responde(){
		$v = OpSi_ProductCheckout_View::getInstance();
		$get_param = $v->input_name('paypal');

		if( isset( $_GET[ $get_param ] ) ){
			$this->paypal_responde_process();
		}
	}

	public function check_form_submit(){
		$v = OpSi_ProductCheckout_View::getInstance();
		$submit_post_key = $v->input_name('submit');

		if( isset( $_POST[$submit_post_key] ) && 'yes' == $_POST[$submit_post_key] ){
			$this->form_submit_process();
		}
	}

	public function check_upsell_submit(){
		$v = OpSi_ProductCheckout_View::getInstance();
		$submit_post_key = $v->input_name('upsell_submit');

		if( isset( $_POST[$submit_post_key] ) && 'yes' == $_POST[$submit_post_key] ){
			$this->upsell_submit_process();
		}
	}

	public function has_errors( $key = null ){
		return is_null( $key ) ? count( $this->errors ) > 0 : ( is_array($this->errors) && isset( $this->errors[$key] ) );
	}

	public function dump_data(){
		var_dump( $this->fields );
		var_dump( $this->errors );
		var_dump( $this->paypal );
		var_dump( $this->order_id );
		var_dump( $this->selected_upsell_product_id );
		var_dump( $this->paymill_transation_id );
		var_dump( $this->page_id );
	}

	public function field( $key ){
		return isset( $this->fields ) && is_array( $this->fields ) && isset( $this->fields[$key] ) ? $this->fields[$key] : null;
	}


	public function error( $key ){
		return isset( $this->errors ) && is_array( $this->errors ) && isset( $this->errors[$key] ) ? $this->errors[$key] : null;
	}

	public function get_order_id(){
		return $this->order_id;
	}

	public function get_page_id(){
		return $this->page_id;
	}


	/******************************************************************
	*
	*       PRIVATE FUNCTIONS
	*
	******************************************************************/
	
	private function form_submit_process(){
		$v = OpSi_ProductCheckout_View::getInstance();

		// sanity check
		if( ! isset( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], $v->input_name('nonce') ) ){
			$this->reload_page();
		}

		// get meta
		$this->set_current_page_meta();

		// check if product_checkout is enabled
		if( ! $v->boolean( $this->meta['enabled'] ) ){
			$this->reload_page();
		}
		
		// reset session data
		$this->reset_data();

		// check product exists and is simple products
		$this->check_product_exists_and_is_simple_product();

		// get submited form fields data
		$form_fields = [];
		foreach( $v->form_fields() as $field_id => $field_data ){
			$value = $this->submited_val( $field_id );

			$form_fields[ $field_id ] = $value;

			// check for empty
			if( $field_data['required'] && ! $value ){
				$error_msg = sprintf( __('Field "%s" is required.', 'opsi_pc'), $field_data['label'] );
				$this->set_error( $field_id, $error_msg );
			}
		}
		// get other required info
		$form_fields['payment_option'] = $this->submited_val('payment_option');
		$form_fields['terms_agree'] = $this->submited_val('terms_agree');
		// save data for later use
		$this->fields = $form_fields;

		// check email field format
		if( isset( $this->fields['email'] ) && ! $this->has_errors('email') && ! filter_var( $this->fields['email'], FILTER_VALIDATE_EMAIL ) ){
			$this->set_error( 'email', __('Wrong email format', 'opsi_pc') );
		}

		// check terms of use
		if( ! $v->boolean( $this->fields['terms_agree'] ) ){
			$this->set_error( 'terms_agree', __('You have to accept the terms to make the purchase.', 'opsi_pc') );
		}

		// TODO check post number and city (from list of post numbers and cities in settings)
		
		// check phone number
		if( isset( $this->fields['phone'] ) && ! $this->has_errors('email') && ! preg_match("/^[0-9\ ]+$/", $this->fields['phone']) ){
			$this->set_error( 'phone', __('Wrong phone number format.', 'opsi_pc') );
		}

		// check selected payment option
		switch ( $this->fields['payment_option'] ) {
			case 'cod':
				if( ! $v->boolean($this->meta['enable_payment_cod']) ){
					$this->set_error( 'payment_option', __('Cash on delivery payment is not available.', 'opsi_pc') );
				}
				break;

			case 'paypal':
				if( ! $v->boolean($this->meta['enable_payment_paypal']) || ! $v->payment_type_available('paypal') ){
					$this->set_error( 'payment_option', __('PaPal payment is not available.', 'opsi_pc') );
				}
				break;

			case 'paymill':
				if( ! $v->boolean($this->meta['enable_payment_paymill']) || ! $v->payment_type_available('paymill') ){
					$this->set_error( 'payment_option', __('Credit card payment is not available.', 'opsi_pc') );
				} elseif( ! $this->submited_val('paymill_token') ){
					$this->set_error( 'payment_option', __('Missing info for credit card payment.', 'opsi_pc') );
				}
				break;
			
			default:
				$this->set_error('payment_option', __('You must choose payment option.', 'opsi_pc') );
				break;
		}

		// check for errors
		$this->reload_on_errors();

		// process selected payment option
		switch (  $this->fields['payment_option'] ) {
			case 'paypal':
				$this->create_paypal_redirect_link_and_redirect();
				break;

			case 'paymill':
				$this->process_paymill_payment_request();
				break;
		}

		// check for errors
		$this->reload_on_errors();

		// looks like verything is all right, create order in woocommerce
		$this->create_and_complete_order();
	}

	private function paypal_responde_process(){
		$v = OpSi_ProductCheckout_View::getInstance();

		// get meta
		if( $this->page_id ){	// uspell page
			$this->set_upsell_page_meta();
		} else {
			$this->set_current_page_meta();
		}

		$error_key = $this->_is_upsell_request ? 'uspell' : 'payment_option';

		// check if product_checkout is enabled
		if( ! $v->boolean( $this->meta['enabled'] ) )
			return;

		// check fields data
		if( ! ( is_array( $this->fields ) && count( $this->fields ) > 0 ) )
			$this->set_error($error_key, __('Missing form data.', 'opsi_pc') );
		$this->reload_on_errors();

		// require lib
		require_once( OPSI_PRODUCTCHECKOUT_PATH . '/libs/paypal.php' );
		$paypal = PaypalHelper::getInstance();
		
		$paypal_status = isset( $_GET[ $v->input_name('paypal') ] ) ? $_GET[ $v->input_name('paypal') ] : '';

		// if canceled
		if( 'success' != $paypal_status )
			$this->set_error($error_key, __('You canceled payment. Please repeat the process or select "pay on delivery" payment option.', 'opsi_pc'));
		$this->reload_on_errors();

		// get token order info
		$token = isset( $_GET['token'] ) ? $_GET['token'] : '';
		$payerId = isset( $_GET['PayerID'] ) ? $_GET['PayerID'] : '';
		$response = $paypal->GetExpressCheckoutDetails( array( 'TOKEN' => $token ) );

		// check for errors
		if ( is_array($response) && $response['ACK'] == 'Failure' && isset( $response['L_LONGMESSAGE0'] ) )
			$this->set_error($error_key, sprintf( __('There was an error processing your payment.\nPlease contact administrator of this page or try again.\nError messages: %s', 'opsi_pc'), esc_html( $response['L_LONGMESSAGE0'] ) ) );
		$this->reload_on_errors();

		// check for other errors / respond is not success
		if( ! ( is_array($response) && isset( $response['ACK'] ) && $response['ACK'] == 'Success' ) )
			$this->set_error($error_key, __('There was unknown error processing your payment.\nPlease contact administrator of this page or try again.', 'opsi_pc') );
		$this->reload_on_errors();

		// if respond is not for this page reset data and reload it
		if( is_null( $this->paypal ) || ! is_array( $this->paypal ) )
			$this->set_error($error_key, __('There is no information about paypal request.', 'opsi_pc') );
		$this->reload_on_errors();
		

		// check same paypal request
		if( $token != $this->paypal['token'] || get_the_ID() != $this->paypal['page_id'] )
			$this->set_error($error_key, __('Can\'t find order information.', 'opsi_pc') );
		$this->reload_on_errors();
			
		// everything should be ok now but check it any way
		if( ! ( is_array( $response ) && isset( $response['ACK'] ) && $response['ACK'] == 'Success' ) )
			$this->set_error($error_key, __('Unknown paypal error. Please try again or contact administrator of this page.', 'opsi_pc') );
		$this->reload_on_errors();

		// do paypal payment
		$do_checkout_response = $paypal->DoExpressCheckoutPayment( $response );

		// check for failure reponse
		if( is_array( $do_checkout_response ) && isset( $do_checkout_response['ACK'] ) && $do_checkout_response['ACK'] == 'Failure' && isset( $do_checkout_response['L_LONGMESSAGE0'] ) )
			$this->set_error($error_key, sprintf( __('Error processing Paypal payment.\nMessage: %s', 'opsi_pc'), esc_html( $do_checkout_response['L_LONGMESSAGE0'] ) ) );
		$this->reload_on_errors();

		// check for other error / if not success repond
		if( ! ( is_array( $do_checkout_response ) && isset( $do_checkout_response['ACK'] ) && $do_checkout_response['ACK'] == 'Success' ) )
			$this->set_error($error_key, __('Unknown paypal error. Please try again or contact administrator of this page.', 'opsi_pc') );
		$this->reload_on_errors();

		// reset paypal data
		$this->paypal = null;

		// create / process order
		if( $this->_is_upsell_request ){
			$this->add_upsell_product_to_order_and_redirect();
		} else {
			$this->create_and_complete_order();
		}
	}

	private function upsell_submit_process(){
		$v = OpSi_ProductCheckout_View::getInstance();

		// sanity check
		if( ! isset( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], $v->input_name('upsell_nonce') ) ){
			$this->reload_page();
		}

		// get meta
		$this->set_upsell_page_meta();

		// check we are on the page that is defined for upsell
		if( get_the_ID() != $this->meta['upsell_page'] )
			return;

		// check if product_checkout is enabled
		if( ! $v->boolean( $this->meta['enabled'] ) )
			return;

		// get upsell product id
		$this->selected_upsell_product_id = ( '' != $this->submited_val( 'upsell_product_id' ) ) ? (int)$this->submited_val( 'upsell_product_id' ) : null;

		// check upsell product is set
		if( is_null( $this->selected_upsell_product_id ) )
			$this->set_error('upsell', __('Missing field for upsell product.', 'opsi_pc') );
		$this->reload_on_errors();
		
		// check if it is reject request
		if( -1 == $this->selected_upsell_product_id ){
			$this->redirect_to_thankyou_page();
		}

		// check upsell product is selected option
		if( ! in_array( $this->selected_upsell_product_id, $this->meta['upsell_products'] ) ){
			$this->selected_upsell_product_id = null;
			$this->set_error('upsell', __('This upsell product is not available.', 'opsi_pc') );
		}
		$this->reload_on_errors();

		// check order_id exists
		if( ! wc_get_order( $this->order_id ) )
			$this->set_error('upsell', __('Previous order not found', 'opsi_pc') );
		$this->reload_on_errors();

		// check payment type and process if needed
		switch ( $this->fields['payment_option'] ) {
			case 'paypal':
				$this->create_paypal_redirect_link_and_redirect( true );
				break;

			case 'paymill':
				$this->process_paymill_upsell();
				break;
		}

		// check for errors
		$this->reload_on_errors();

		// finish
		$this->add_upsell_product_to_order_and_redirect();		
	}

	private function set_current_page_meta(){
		$this->meta = OpSi_ProductCheckout_Metabox::get_meta( get_the_ID() );
	}

	private function set_upsell_page_meta(){
		$this->_is_upsell_request = true;
		$this->meta = OpSi_ProductCheckout_Metabox::get_meta( $this->page_id );
	}

	private function create_paypal_redirect_link_and_redirect( $is_upsell = false ){
		// helppers
		$o = OpSi_ProductCheckout_Options::getInstance();
		$v = OpSi_ProductCheckout_View::getInstance();

		// current page url
		$current_page_url = home_url( add_query_arg( NULL, NULL ) );
		$logo_url = $v->url('assets/images/popolnapostava.png');

		require_once( OPSI_PRODUCTCHECKOUT_PATH . '/libs/paypal.php' );
		$paypal = PaypalHelper::getInstance();

		// basic params
		$requestParams = array(
			'RETURNURL' => add_query_arg( array( $v->input_name('paypal') => 'success', ), $current_page_url ),
			'CANCELURL' => add_query_arg( array( $v->input_name('paypal') => 'cancel', ), $current_page_url ),
		);

		// locale, company info, logo, ...
		$orderParams = array(
			'LOGOIMG' => $logo_url, //You can paste here your website logo image which will be displayed to the customer on the PayPal chechout page
			"MAXAMT" => ceil( $v->get_order_total_price( $this->meta, $is_upsell ) ), //Set max transaction amount
			"NOSHIPPING" => "1", //I do not want shipping
			"ALLOWNOTE" => "0", //I do not want to allow notes
			"BRANDNAME" => $o->get('paypal_brand_name'),
			"GIFTRECEIPTENABLE" => "0",
			"GIFTMESSAGEENABLE" => "0",
			"SOLUTIONTYPE" => "Sole",	// enable credit card payments
			// "LANDINGPAGE" => "Billing",	// force credit card tab on paypal website
			"LOCALECODE" => $o->get('paypal_locale_code'),
			"COUNTRYCODE" => $o->get('paypal_country_code'),
		);

		// product
		$item = array(
			'PAYMENTREQUEST_0_AMT' => $v->get_order_product_price( $this->meta, $is_upsell )  * $v->get_order_product_quantity( $this->meta, $is_upsell ),
			'PAYMENTREQUEST_0_CURRENCYCODE' => get_woocommerce_currency(),
			'PAYMENTREQUEST_0_ITEMAMT' => $v->get_order_product_price( $this->meta, $is_upsell ) * $v->get_order_product_quantity( $this->meta, $is_upsell ),

			'L_PAYMENTREQUEST_0_NAME0' => $v->get_order_product_name( $this->meta, $is_upsell ),
			// 'L_PAYMENTREQUEST_0_DESC0' => 'Item description',
			'L_PAYMENTREQUEST_0_AMT0' => $v->get_order_product_price( $this->meta, $is_upsell ),
			'L_PAYMENTREQUEST_0_QTY0' => $v->get_order_product_quantity( $this->meta, $is_upsell ),
		);

		// join args
		$paypal_request_args = array_merge( $requestParams + $orderParams + $item );

		// check for delivery charges and add them to paypal args
		if( $v->get_order_shipping_price( $this->meta, $is_upsell ) > 0 ){
			$paypal_request_args['PAYMENTREQUEST_0_AMT'] += $v->get_order_shipping_price( $this->meta, $is_upsell );
			$paypal_request_args['PAYMENTREQUEST_0_ITEMAMT'] += $v->get_order_shipping_price( $this->meta, $is_upsell );

			$paypal_request_args += array(
				'L_PAYMENTREQUEST_0_NAME1' => __('Shipping', 'opsi_pc'),
				// 'L_PAYMENTREQUEST_0_DESC1' => 'Item description',
				'L_PAYMENTREQUEST_0_AMT1' => $v->get_order_shipping_price( $this->meta, $is_upsell ),
				'L_PAYMENTREQUEST_0_QTY1' => 1,
			);
		}

		//Send request and wait for response
		//Now we will call SetExpressCheckout API operation. 
		$response = $paypal->SetExpressCheckout( $paypal_request_args );

		//Now you will be redirected to the PayPal to enter your customer data
		//After that, you will be returned to the RETURN URL
		if (is_array($response) && $response['ACK'] == 'Success') { //Request successful
			//Now we have to redirect user to the PayPal
			$token = $response['TOKEN'];

			// data for later check
			$this->paypal = array(
				'token' => $token,
				'price' => $paypal_request_args['PAYMENTREQUEST_0_AMT'],
				'page_id' => get_the_ID(),
				);

			// save data for later use
			$this->save_session();

			// redirect
			$paypal->redirectToPayPal( $token );
		} else if (is_array($response) && $response['ACK'] == 'Failure' && isset( $response['L_LONGMESSAGE0'] ) ) {
			// paypal error
			// echo "<pre>";
			// var_dump($response);
			// echo "</pre>";
			$this->set_error( 'payment_option', sprintf( __('Error processing payment. Message: %s', 'opsi_pc'), esc_html( $response['L_LONGMESSAGE0'] ) ) );
		} else {
			// unknown error
			$this->set_error( 'payment_option', __('Error processing payment. Please contact administrator of the page or try again.', 'opsi_pc') );
		}
	}

	private function process_paymill_payment_request(){
		// helpers
		$v = OpSi_ProductCheckout_View::getInstance();
		$o = OpSi_ProductCheckout_Options::getInstance();

		// require lib
		require_once(OPSI_PRODUCTCHECKOUT_PATH . '/libs/paymill-php/autoload.php');

		// get paymill token
		$token = $this->submited_val( 'paymill_token' );

		// get paymill config
		$paymill_config = $o->get_paymill_config();

		// prep request
		$paymill_request = new Paymill\Request( $paymill_config['private_key'] );

		// prep transation request
		$transaction = new \Paymill\Models\Request\Transaction();
		$transaction
				->setAmount( $v->get_order_total_price( $this->meta ) * 100 ) // e.g. "4200" for 42.00 EUR
                ->setCurrency( get_woocommerce_currency() )
				->setToken( $token )
				;

		// request
		try {
			$response  = $paymill_request->create( $transaction );
			$payment_obj = $response->getPayment();
			$paymill_paymentId = $payment_obj->getId();

		} catch(\Paymill\Services\PaymillException $e){
			//Do something with the error informations below
			$e->getResponseCode();
			$e->getStatusCode();
			$e->getErrorMessage();

			$this->set_error( 'payment_option', sprintf( __('Error processing payment. Message: %s', 'opsi_pc'), $e->getErrorMessage() ) );
			return;
		}

		// all ok
		$this->paymill_transation_id = $paymill_paymentId;
	}

	private function process_paymill_upsell(){
		// helpers
		$v = OpSi_ProductCheckout_View::getInstance();
		$o = OpSi_ProductCheckout_Options::getInstance();

		// check payment id exists
		if( ! $this->paymill_transation_id )
			$this->set_error( 'upsell', __('No info about previous payment. Please contact administrator of this page.', 'opsi_pc') );
		$this->reload_on_errors();

		// require lib
		require_once(OPSI_PRODUCTCHECKOUT_PATH . '/libs/paymill-php/autoload.php');

		// get paymill config
		$paymill_config = $o->get_paymill_config();

		// prep request
		$paymill_request = new Paymill\Request( $paymill_config['private_key'] );

		// prep transation request
		$transaction = new \Paymill\Models\Request\Transaction();
		$transaction
				->setAmount( $v->get_order_total_price( $this->meta, true ) * 100 ) // e.g. "4200" for 42.00 EUR
                ->setCurrency( get_woocommerce_currency() )
				->setPayment( $this->paymill_transation_id )
				;

		// request
		try {
			$response  = $paymill_request->create( $transaction );
			$payment_obj = $response->getPayment();
			$paymill_paymentId = $payment_obj->getId();

		} catch(\Paymill\Services\PaymillException $e){
			//Do something with the error informations below
			$e->getResponseCode();
			$e->getStatusCode();
			$e->getErrorMessage();

			$this->set_error( 'upsell', sprintf( __('Error processing payment. Message: %s', 'opsi_pc'), $e->getErrorMessage() ) );
			return;
		}
	}

	private function create_and_complete_order(){
		// create order
		$order = $this->create_order();

		// check for errors
		$this->reload_on_errors();

		// allow other plugins to interract
		do_action( 'opsi_product_checkout_order_created', $order );

		// if cach on delivery, set status processing
		if( 'cod' ==  $this->fields['payment_option'] ){
			$note = '';	// TODO set note content
			$order->update_status( 'processing', $note );
		}

		// if paymill is payment options, set order paid
		if( 'paymill' ==  $this->fields['payment_option'] ){
			$order->payment_complete( $this->paymill_transation_id );
			// TODO add note
		}

		// if paypal is payment option, set transation id
		if( 'paypal' ==  $this->fields['payment_option'] ){
			$order->payment_complete();
			// TODO add note
		}

		// allow other plugins to interract
		do_action( 'opsi_product_checkout_order_processed', $order );
		
		// check for errors
		$this->reload_on_errors();

		// save order_id
		$this->order_id = $order->get_id();

		// save page id (for upsell)
		$this->page_id = get_the_ID();

		// save data
		$this->save_session();

		// reload to specified "on checkout complete" page
		$this->reload_to_next_page();
	}

	private function add_upsell_product_to_order_and_redirect(){
		// get order
		$order = wc_get_order( $this->order_id );

		if( ! $order )
			$this->set_error('upsell', __('Order not found.', 'opsi_pc') );
		$this->reload_on_errors();

		// get product
		$_product = wc_get_product( $this->selected_upsell_product_id );
		if( ! $_product )
			$this->set_error('upsell', __('Product not found.', 'opsi_pc') );
		$this->reload_on_errors();

		// check product exists
		if( is_null( $_product ) ){
			$this->set_error('upsell', __('Product not found.', 'opsi_pc') );
			return false;
		}
		// and is simple product
		if( 'simple' !== $_product->get_type() ){
			$this->set_error('upsell', __('Only simple products supported.', 'opsi_pc') );
			return false;
		}

		$order->add_product( $_product, 1 );

		// calculate totals
		$order->calculate_totals();

		// allow other plugins to interact
		do_action( 'opsi_product_checkout_upsell_product_added', $order );

		// redirect to thankyou page
		$this->redirect_to_thankyou_page();
	}

	private function create_order(){
		// TODO check required data / fields
		
		// get product
		$product = wc_get_product( $this->meta['product_id'] );
		// check product exists
		if( is_null( $product ) ){
			$this->set_error('process_error', __('Product not found.', 'opsi_pc') );
			return false;
		}
		// and is simple product
		if( 'simple' !== $product->get_type() ){
			$this->set_error('process_error', __('Only simple products supported.', 'opsi_pc') );
			return false;
		}

		// get quantity
		$quantity = (int)$this->meta['quantity'];
		if( $quantity <= 0 ){
			$this->set_error('process_error', __('Quantity must be a positive number.', 'opsi_pc') );
			return false;
		}

		// get user
		$user_id = $this->create_user_if_needed();
		if( ! $user_id )
			return false;
		
		// create order object
		$order = wc_create_order();

		// check order created
		if( is_wp_error( $order ) ){
			$this->set_error('process_error', __('Can not create order.', 'opsi_pc') );
			return false;
		}

		// set user
		$order->set_customer_id( $user_id );

		// get address country
		$o = OpSi_ProductCheckout_Options::getInstance();
		$address_country = $o->get('order_country');

		// set delivery and address data
		$address = array(
			'first_name' => $this->fields['name'],
			'email'      => $this->fields['email'],
			'phone'      => $this->fields['phone'],
			'address_1'  => $this->fields['street'],
			'city'       => $this->fields['street'],
			'postcode'   => $this->fields['postcode'],
			'country'    => $address_country,
			);
		$order->set_address( $address, 'billing' );
		$order->set_address( $address, 'shipping' );
		
		// add product
		$order->add_product( $product, $quantity);

		// add shipping if needed
		if( $shipping_item = $this->create_and_get_shipping_item_if_needed( $order ) )
			$order->add_item( $shipping_item );

		// set payment type
		$order->set_payment_method( $this->fields['payment_option'] );

		// calculate totals
		$order->calculate_totals();

		// return order object
		return $order;
	}

	private function create_user_if_needed(){
		// we assume no user is set
		$user_id = false;

		// check for logged in user
		if( !$user_id && is_user_logged_in() ){
			$user_id = get_current_user_id();
		}

		// get submited email
		$email = $this->fields['email'];

		// check user exists
		if( ! $user_id && false !== email_exists( $email ) ){
			$user_id = email_exists( $email );
		}

		// create user if not set yet
		if( ! $user_id ){
			$random_password = wp_generate_password();
			$user_id = wp_create_user( $email, $random_password, $email );
			// sanity check
			if( is_wp_error( $user_id ) ){
				// TODO handle error
				
				// reset $user_id param
				$user_id = false;
			}
		}

		// check if we still do not have user_id and set error
		if( ! $user_id ){
			$this->set_error('process_error', __('Can not create user.', 'opsi_pc') );
			return false;
		}

		return $user_id;
	}

	private function create_and_get_shipping_item_if_needed( $order ){

		$shipping_price = (float)$this->meta['shipment_price'];

		if( $shipping_price <= 0 ){
			return NULL;
		}
		$item = new WC_Order_Item_Fee();
		$item->set_props( array(
			'name'      => __('Shipping', 'opsi_pc'), // $fee->name,
			'tax_class' => 0, // $fee->taxable ? $fee->tax_class : 0,
			'total'     => $shipping_price, // $fee->amount,
			'total_tax' => 0, // $fee->tax,
			// 'taxes'     => array(
			// 	'total' => $fee->tax_data,
			// ),
			'order_id'  => $order->get_id(),
		) );
		$item->save();

		return $item;
	}

	private function reload_on_errors(){
		// if errors, reload page
		if( $this->has_errors() ){
			// var_dump( $this->fields );
			// var_dump( $this->errors );

			// die("TESTING SUBMITED DATA");

			$this->save_session();
			$this->reload_page();
		}
	}

	private function reload_to_next_page(){
		// check if we need to redirect to thank you page or upsell page
		if( is_array( $this->meta['upsell_products'] ) && count( $this->meta['upsell_products'] ) > 0 ){
			// we have upsell product and page
			$this->redirect_to_upsell_page();
		} else {
			// no upsell product, redirect to thank you page
			$this->redirect_to_thankyou_page();
		}
	}

	private function redirect_to_upsell_page(){
		$this->reload_page( (int)$this->meta['upsell_page'] );
	}

	private function redirect_to_thankyou_page(){
		$this->reset_data();
		$this->reload_page( (int)$this->meta['thankyou_page'] );
	}

	private function reload_page( $page_id = null ){
		if( is_null( $page_id ) && $this->_is_upsell_request )
			$page_id = (int)$this->meta['upsell_page'];
		wp_redirect( ! is_null( $page_id ) ? get_permalink( $page_id ) : get_permalink() );
		exit();
	}

	private function save_session(){
		$v = OpSi_ProductCheckout_View::getInstance();

		$_SESSION[ $v->input_name('session') ] = array(
			'fields' => $this->fields,
			'errors' => $this->errors,
			'paymill_transation_id' => $this->paymill_transation_id,
			'paypal' => $this->paypal,
			'order_id' => $this->order_id,
			'selected_upsell_product_id' => $this->selected_upsell_product_id,
			'page_id' => $this->page_id,
		);
	}

	private function set_data_from_session(){
		$v = OpSi_ProductCheckout_View::getInstance();
		if( isset( $_SESSION, $_SESSION[ $v->input_name('session') ] ) ){
			$this->fields = isset( $_SESSION[ $v->input_name('session') ]['fields'] ) ? $_SESSION[ $v->input_name('session') ]['fields'] : array();
			$this->errors = isset( $_SESSION[ $v->input_name('session') ]['errors'] ) ? $_SESSION[ $v->input_name('session') ]['errors'] : array();
			$this->paymill_transation_id = isset( $_SESSION[ $v->input_name('session') ]['paymill_transation_id'] ) ? $_SESSION[ $v->input_name('session') ]['paymill_transation_id'] : null;
			$this->paypal = isset( $_SESSION[ $v->input_name('session') ]['paypal'] ) ? $_SESSION[ $v->input_name('session') ]['paypal'] : null;
			$this->order_id = isset( $_SESSION[ $v->input_name('session') ]['order_id'] ) ? $_SESSION[ $v->input_name('session') ]['order_id'] : null;
			$this->selected_upsell_product_id = isset( $_SESSION[ $v->input_name('session') ]['selected_upsell_product_id'] ) ? $_SESSION[ $v->input_name('session') ]['selected_upsell_product_id'] : null;
			$this->page_id = isset( $_SESSION[ $v->input_name('session') ]['page_id'] ) ? $_SESSION[ $v->input_name('session') ]['page_id'] : null;
		}
	}

	private function set_error( $key, $message ){
		if( ! is_array( $this->errors ) ){
			$this->errors = array();
		}

		$this->errors[ $key ] = $message;
	}

	private function submited_val( $field_id ){
		$v = OpSi_ProductCheckout_View::getInstance();

		$post_key = $v->input_name( $field_id );
		$post_val = isset( $_POST[$post_key] ) ? $_POST[$post_key] : '';

		return trim( sanitize_text_field( $post_val ) );
	}

	private function check_product_exists_and_is_simple_product(){
		$product = wc_get_product( $this->meta['product_id'] );
		// check product exists
		if( is_null( $product ) ){
			$this->set_error('process_error', __('Product not found.', 'opsi_pc') );
			return false;
		}
		// and is simple product
		if( 'simple' !== $product->get_type() ){
			$this->set_error('process_error', __('Only simple products supported.', 'opsi_pc') );
			return false;
		}
	}






}