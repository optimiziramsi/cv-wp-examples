<?php

class OpSi_ProductCheckout_Metabox {

	const META = "opsi-product-checkout";

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {
		if( !is_admin() || ! current_user_can('edit_pages')){
			return;
		}

		add_action('add_meta_boxes', array( $this, 'add_meta_boxes') );
		add_action('save_post', array( $this, 'save_meta'));
	}

	public function add_meta_boxes(){
		$screens = ['page'];
		foreach ($screens as $screen) {
			add_meta_box(
				'opsi-product-checkout-metabox',           // Unique ID
				__('Product Checkout Page', 'opsi-pc'),  // Box title
				array($this, 'metabox_html_callback'),  // Content callback, must be of type callable
				$screen                   // Post type
			);
		}
	}

	public function metabox_html_callback($post){
		$meta = self::get_meta($post->ID);

		$woo_products = get_posts(array(
				'posts_per_page'   => -1,
				'post_type'        => 'product',	// only simple products available
				'post_status'      => 'publish',
			));

		$pages = get_posts(array(
				'posts_per_page'   => -1,
				'post_type'        => 'page',	// only simple products available
				'post_status'      => 'publish',
			));

		// var_dump($meta);

		include OPSI_PRODUCTCHECKOUT_PATH . '/views/metabox.php';
	}

	public function save_meta($post_id){
		if( ! isset( $_POST['opsi-pc-nonce'] ) || ! wp_verify_nonce($_POST['opsi-pc-nonce'], 'opsi-product-checkout-metabox-nonce')){
			return;
		}

		$v = OpSi_ProductCheckout_View::getInstance();

		$data = self::$meta_defaults;
		foreach ($data as $field => $default_value) {
			$post_key = $v->input_name($field);
			$post_val = isset($_POST[$post_key]) ? $_POST[$post_key] : '';

			$is_boolean_field = ('yes' == $default_value || 'no' == $default_value);
			if( $is_boolean_field ){
				$post_val = ('yes' == $post_val) ? 'yes' : 'no';
			}
			$is_array_field = is_array( $default_value );
			if( $is_array_field ){
				$post_val = is_array( $post_val ) ? $post_val : array();
			}

			if( ! $is_array_field ){
				$post_val = sanitize_text_field( $post_val );
			}

			$data[$field] = $post_val;
		}

		// var_dump($data);
		// die();

		update_post_meta($post_id, self::META, $data);
	}

	private static $meta_defaults = array(
			'enabled' => 'no',
			'product_id' => '',
			'quantity' => '1',
			'shipment_price' => '0',
			'mail_enabled' => 'yes',
			'mail_subject' => '',
			'sms_enabled' => 'yes',
			'upsell_products' => [],
			'upsell_page' => '',
			'upsell_confirm_enabled' => 'yes',
			'upsell_confirm_content' => '',
			'mail_upsell_enabled' => 'yes',
			'mail_upsell_subject' => '',
			'sms_uspell_enabled' => 'yes',
			'thankyou_page' => '',
			'template' => '',
			'enable_payment_cod' => 'yes',
			'enable_payment_paypal' => 'yes',
			'enable_payment_paymill' => 'yes',
			'terms_page_id' => '',
			);
	public static function get_meta($post_id){
		$meta_data = get_post_meta( $post_id, self::META, true );

		$attributes = shortcode_atts( self::$meta_defaults, $meta_data );

		if( ! is_array( $attributes['upsell_products'] ) ){
			$attributes['upsell_products'] = array();
		}

		return $attributes;
	}

	public static function current_page_meta(){
		return self::get_meta( get_the_ID() );
	}


}