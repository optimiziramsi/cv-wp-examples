<?php

class OpSi_ProductCheckout_Affiliate {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {
		add_action( 'opsi_product_checkout_order_created', array( $this, 'trigger_affiliate' ) );
	}

	public function trigger_affiliate( $order ){
		// trigger affiliate action
		if( class_exists('Affiliate_WP_WooCommerce') ){
			$woo_affiliate = new Affiliate_WP_WooCommerce();
			if( method_exists( $woo_affiliate, 'add_pending_referral' ) ){
				$woo_affiliate->add_pending_referral( $order->get_id() );
			}
		}
	}

}