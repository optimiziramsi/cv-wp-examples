<?php

class OpSi_ProductCheckout_Mail {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {
		add_action( 'opsi_product_checkout_order_processed', array( $this, 'send_order_mail' ) );
		add_action( 'opsi_product_checkout_upsell_product_added', array( $this, 'send_order_updated_mail' ) );
	}

	public function send_order_mail( $order ){
		// helppers
		$v = OpSi_ProductCheckout_View::getInstance();
		$h = OpSi_ProductCheckout_Handler::getInstance();
		$o = OpSi_ProductCheckout_Options::getInstance();

		// meta
		$meta = OpSi_ProductCheckout_Metabox::get_meta( get_the_ID() );

		// check enabled
		if( ! $v->boolean( $o->get( 'mail_enabled' ) ) || ! $v->boolean( $meta['mail_enabled'] ) )
			return;
		
		// mail template
		$template_path = $v->mail_template_path( $meta['template'], false );

		// mail content
		ob_start();
		include $template_path;
		$mail_content = ob_get_clean();

		// mail subject
		$mail_subject = $meta['mail_subject'];

		// send mail
		$this->send_mail( $mail_subject, $mail_content );
	}

	public function send_order_updated_mail( $order ){
		// helppers
		$v = OpSi_ProductCheckout_View::getInstance();
		$h = OpSi_ProductCheckout_Handler::getInstance();
		$o = OpSi_ProductCheckout_Options::getInstance();

		// meta
		$meta = OpSi_ProductCheckout_Metabox::get_meta( $h->get_page_id() );

		// check enabled
		if( ! $v->boolean( $o->get( 'mail_enabled' ) ) || ! $v->boolean( $meta['mail_upsell_enabled'] ) )
			return;
		
		// mail template
		$template_path = $v->mail_template_path( $meta['template'], true );

		// mail content
		ob_start();
		include $template_path ;
		$mail_content = ob_get_clean();

		// mail subject
		$mail_subject = $meta['mail_upsell_subject'];

		// send mail
		$this->send_mail( $mail_subject, $mail_content );
	}

	public function set_sender_name( $original_sender_name ){
		// helppers
		$o = OpSi_ProductCheckout_Options::getInstance();

		// if sender name is set, use it
		if( '' != $o->get('mail_sender_name') )
			return $o->get('mail_sender_name');

		return $original_sender_name;
	}

	public function set_sender_email( $original_sender_email ){
		// helppers
		$o = OpSi_ProductCheckout_Options::getInstance();

		// if sender name is set, use it
		if( '' != $o->get('mail_sender_email') )
			return $o->get('mail_sender_email');

		return $original_sender_email;
	}

	private function send_mail( $subject, $content ){
		// helppers
		$v = OpSi_ProductCheckout_View::getInstance();
		$o = OpSi_ProductCheckout_Options::getInstance();

		// mail headers / html support
		$headers = array('Content-type: text/html; charset=utf-8');

		// trim subject
		$subject = trim( $subject );
		// trim content
		$content = trim( $content );

		// set reciever
		$send_to = $v->old('email');

		// set dev reciever
		if( $v->boolean( $o->get( 'mail_dev_enabled' ) ) )
			$send_to = $o->get( 'mail_dev_sendto' );

		// set sender name hook
		add_filter( 'wp_mail_from_name', array( $this, 'set_sender_name' ), 999 );
		// set sender email hook
		add_filter( 'wp_mail_from', array( $this, 'set_sender_email' ), 999 );

		// send mail
		$mail_sent = wp_mail( $send_to, $subject, $content, $headers);

		// TODO handle mail error

		// remove set sender name hoook
		remove_filter( 'wp_mail_from_name', array( $this, 'set_sender_name' ) );
		// remove set sender email hook
		remove_filter( 'wp_mail_from', array( $this, 'set_sender_email' ) );

	}

}